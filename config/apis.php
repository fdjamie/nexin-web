<?php

return [
    'test_api' => 'v1/',
    'login' => 'login/',
    'register' => 'register/',
    'user' => 'v1/user/',    
    'create-trust-user' => 'v1/create-trust-user/',
    'trust-user' => 'v1/trust-user/',
    'speciality' => 'v1/speciality/',
    'trust' => 'v1/trust/',
    'division' => 'v1/division/',
    'directorate' => 'v1/directorate/',
    'directoratespeciality' => 'v1/directoratespeciality/',
    'site' => 'v1/site/',
    'location' => 'v1/location/',
    'staff' => 'v1/staff/',
    'grade' => 'v1/grade/',
    'role' => 'v1/role/',
    'shift' => 'v1/shift/',
    'get-tree' => 'v1/get-tree/',
    'rotation' => 'v1/template/rotation/',
    'post' => 'v1/post/',
    'posts' => 'v1/posts/',
    'post-details' => 'v1/post-details/',
    'rotation-detail' => 'v1/rotation/',
    'service' => 'v1/service/',
    'service/template' => 'v1/service/template/',
    'shift-type' => 'v1/shift-type/',
    'rota-template' => 'v1/rota-template/',
    'directorate-staff' => 'v1/directorate-staff/',
    'category' => 'v1/category/',
    'sub-category' => 'v1/sub-category/',
    'shift-detail' => 'v1/shift-detail/',
    'rota-group' => 'v1/rota-group/',
    'additional-grade' => 'v1/additional-grade/',
    'group' => 'v1/group/',
    'subgroup' => 'v1/subgroup/',
    'directorate-service' => 'v1/directorate-service/',
    'directorate-service/template' => 'v1/directorate-service/template/',
    'qualification' => 'v1/qualification/',
    'additional-parameter' => 'v1/additional-parameter/',
    'category-priority' => 'v1/category-priority/',
    'color' => 'v1/color/',
    'priority-set' => 'v1/priority-set/',
    'speciality-priority-set' => 'v1/speciality-priority-set/',
    'assign' => 'v1/assign/',
    'transition-time' => 'v1/transition-time/',
    'deleted' => 'v1/deleted/',
    'transition-time' => 'v1/transition-time/',
    'command' => 'v1/command/',
    'optimal-assignment' => 'v1/optimal-assignment/',
    'get-user-trusts-tree'=>'v1/get-user-trusts-tree/',

    'store-user-role'=>'v1/user-roles',
    'get-trust-roles'=>'v1/user-roles',
    'get-module-permissions'=>'v1/get-module-permissions',
    'store-role-permission'=>'v1/store-role-permission',
    'get-user-roles-permissions'=>'v1/get-user-role-permission',

    'getAllTrusts'=>'/v1/get-all-trusts',
    'signupRequest'=>'/v1/signup-request',
    'get-all-requests'=>'/v1/get-all-requests/',

    'getUserAccountStatus'=>'/v1/get-user-account-status/',
    'updateRequestStatus'=>'/v1/update-request-status',

    'emailTemplates'=>'/v1/email-templates/',
    'trustTemplateHtml'=>'v1/trust-template-html/',
    'emailTypeTemplate'=>'v1/email-type-templates/',
    'updateTemplate'=>'v1/change-template',

    'sendEmail'=>'v1/send-email',
    'getUserEmails'=>'v1/get-user-emails',

    'createNewTemplate'=>'v1/create-new-template',










];
