<?php

return [

    'directorates'=>'directorates',
    'general-services'=>'general-services',
    'additional-parameters'=>'additional-parameters',
    'transition-times'=>'transition-times',
    'specialities'=>'specialities',
    'parameters'=>'parameters',
    'rota-groups'=>'rota-groups',
    'staff'=>'staff',
    'grades'=>'grades',
    'roles'=>'roles',
    'groups'=>'groups',
    'category'=>'category',
    'sites'=>'sites',
    'user'=>'user',

];
