$(document).ready(function () {
    $('#sp_spin').hide();
    $('#directorate_speciality_id').change(function () {
        $('#sp_spin').show();
        $('#position').hide();
        jQuery.ajax({
            url: APP_URL+ '/getAdditionalGradePositions/' + $(this).val() + '/' + $('#directorate_id').val(),
            type: 'get',
            success: function (data) {
                var position = '';
                $.each(data, function (index, value) {
                    position += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#position').html(position).show();
                $('#sp_spin').hide();
            }
        });
    });
});

