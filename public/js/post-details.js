$(document).ready(function () {
    var count = $('.copy-row').length + 1;
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#fa_spin,#sp_spin,#rt_spin,#rg_spin,#spin,#trash-1').hide();

    if ($(':checkbox').is(':checked')) {
        $('#endDate').attr("disabled", "disabled");
    }
    $(':checkbox').change(function () {
        if ($(this).is(':checked')) {
            $('#endDate').attr("disabled", "disabled");
            $('#endDate').val("");
        } else {
            $("#endDate").removeAttr("disabled");
        }
    });

    $('#new-parameter').click(function (event) {
        event.preventDefault();
        var name = count-1;
        var new_parameter = $('.copy-row').first().clone().appendTo('.parameters-form');
        $(new_parameter).find('.parameter').attr('id', 'parameter-' + count);
        $(new_parameter).find('.value').attr('id', 'value-' + count);
        var id = $(new_parameter).find('.value').attr('id');
        $(new_parameter).find('.parameter').val('select');
        $(new_parameter).find('.select2-container').remove();
        $('#'+id).attr('name','value['+ name +'][]');
        var value_data = getValueData($('#directorate_id').val(), $('#directorate_speciality_id').val(), null);
          $('#'+id).html(value_data);
        $('#' + id).select2({
            multiple: true,
            width: '100%',
            id : 'new'
        });
        $(new_parameter).find('.parameter').bind('change', function (new_parameter) {
            var values = getValueData($('#directorate_id').val(), $('#directorate_speciality_id').val(), $(this).val());
            var value_id = $(this).parents('.copy-row').find('.value').attr('id');
            $('#' + value_id).html(values);
        });
        $(new_parameter).find('.fa-trash').attr('id','trash-'+count);
        $(new_parameter).find('.fa-trash').show();
        $(new_parameter).find('.fa-trash').bind('click', function (new_parameter) {
            $(this).parents('.copy-row').remove();
        });
        count++;
    });

    var assignedWeek = function () {
        $('#assigned_week').hide();
        $('#spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getrotatemplateweeks/' + $('#rota_template_id').val(),
            type: 'get',
            success: function (data) {
                $('#assigned_week').attr('max', data);
                $('#assigned_week').val(1);
                $('#assigned_week').show();
                $('#spin').hide();
            }
        });

    };

    var getValueData = function (d_id, s_id, p_id) {
        var values = '';
        jQuery.ajax({
            url: APP_URL+ '/getAdditionalValues/' + d_id + '/' + s_id + '/' + p_id,
            type: 'get',
            async: false,
            success: function (data) {
                console.log(data);
                $.each(data[1], function (index, value) {
                    values += "<option value='" + index + "'>" + value + "</option>";
                });
            }
        });
        return values;
    };

    var getParameterData = function (d_id, s_id, p_id) {
        var parameters = '';
        jQuery.ajax({
            url: APP_URL+ '/getAdditionalValues/' + d_id + '/' + s_id + '/' + p_id,
            type: 'get',
            async: false,
            success: function (data) {
                $.each(data[0], function (index, value) {
                    parameters += "<option value='" + index + "'>" + value + "</option>";
                });
            }
        });
        return parameters;
    };

    var updateRotaTemplate = function (id) {
        $('#rota_template_id').hide();
        $('#rt_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getRotaGroupRotaTemplates/' + id,
            type: 'get',
            success: function (data) {
                var rota = '';
                $.each(data, function (index) {
                    rota += "<option value='" + data[index].id + "'>" + data[index].name + "</option>";
                });
                $('#rota_template_id').html(rota).show();
                $('#rt_spin').hide();
            }
        });
    };

    $("#rota_template_id").change(function () {
        assignedWeek();
    });
    $("#directorate_id").change(function () {
        $('#directorate_form').submit();
    });
    $('#rota_group_id').change(function () {
        updateRotaTemplate($(this).val());
    });
    $('#parameter-1').change(function () {
        var values = getValueData($('#directorate_id').val(), $('#directorate_speciality_id').val(), $(this).val());
        $('#value-1').html(values);
    });
    
    $('.delete').click(function(){
       $(this).parents('.copy-row').remove();
    });
});