$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    orientation: "bottom auto"
});
$('#start_time,#end_time,#edit_start_time,#edit_end_time').clockpicker({'autoclose': true});
var getLocations = function () {
    $('#spinn').show();
    $('#locations').hide();
    jQuery.ajax({
        url: APP_URL+ '/getSiteLocations/' + $('#site_id').val(),
        type: 'get',
        success: function (data) {
            var locations = '';
            $.each(data, function (index) {
                locations += "<div class='col-md-4'><input type='checkbox' name='location_id' value='" + data[index].id + "'> <label>" + data[index].name + "</label></div>";
            });
            $('#spinn').hide();
            $('#locations').html('<div class="row">' + locations + '</div>').show();
        }
    });
};
var getLocationsEdit = function (siteId, events) {
    $('#spin').show();
    $('#locations_edit').hide();
    jQuery.ajax({
        url: APP_URL+ '/getSiteLocations/' + siteId,
        type: 'get',
        success: function (data) {
            var locations = '';
            $.each(data, function (index) {
                for (i = 0; i < events.location_id.length; i++) {
                    if (events.location_id[i] == data[index].id) {
                        locations += "<div class='col-md-4'><input type='checkbox' name='location_id' value='" + data[index].id + "' checked='checked'> <label>" + data[index].name + "</label></div> ";
                        return;
                    }
                }
                locations += "<div class='col-md-4'><input type='checkbox' name='location_id' value='" + data[index].id + "'> <label>" + data[index].name + "</label></div>";
            });
            $('#spin').hide();
            $('#locations_edit').html('<div class="row">' + locations + '</div>').show();
        }
    });
};
$('#location_id').hide();
getLocations();
$('#site_id').change(function () {
    getLocations();
});
$('#cycle_length').bind('change click', function (e) {
    if ($(this).val() == 1)
        $('#calendar-service').fullCalendar('changeView', 'agendaWeek');
    else
        $('#calendar-service').fullCalendar('changeView', $(this).val() + 'Week');
    $('#starting_week').attr('max', $(this).val());
    if ($('#starting_week').val() > $(this).val()) {
        $('#starting_week').val($(this).val());
    }
});
weekView = $('#week_view').val();
$('#week_view').bind('change click', function (e) {
    if (weekView < $('#week_view').val()) {
        $('#calendar-service').fullCalendar('next');
        weekView = $('#week_view').val();
    } else if (weekView > $('#week_view').val()) {
        $('#calendar-service').fullCalendar('prev');
        weekView = $('#week_view').val();
    }

});
var addEventFormSubmit = function (id) {
    var locations = [];
    var names = [];

    var start = moment($('#event_start_date').val() + ' ' + $('#start_time').val());
    var end = moment($('#event_start_date').val() + ' ' + $('#end_time').val());
    if ($('#end_time').val() < $('#start_time').val()) {
        end.add(1, 'day');
    }
    $('#locations input:checked').each(function () {
        locations.push(this.value);
        names.push($(this).next('label').text());
    });
    var source = [{
            title: $('#title').val() + ' (' + names + ')',
            start: start,
            end: end,
            id: 'event-' + id,
            icon: 'times-circle-o',
            location_id: locations,
            site_id: $('#site_id').val(),
        }, ];
    $('#calendar-service').fullCalendar('addEventSource', source);
    $('#add').modal('hide');
};
var updateEventFormSubmit = function (id) {
    var start = moment($('#event_start_date_edit').val() + ' ' + $('#edit_start_time').val());
    var end = moment($('#event_end_date_edit').val() + ' ' + $('#edit_end_time').val());
    var locations = [];
    var names = [];
    $('#locations_edit input:checked').each(function () {
        locations.push(this.value);
        names.push($(this).next('label').text());
    });
    eventSource.title = $('#edit_title').val() + '(' + names + ')';
    eventSource.start = start;
    eventSource.end = end;
    eventSource.location_id = locations;
    eventSource.site_id = $('#edit_site_id').val();
    $('#calendar-service').fullCalendar('updateEvent', eventSource);
    $('#edit').modal('hide');
};
$(document).on('click', '.fa-times-circle-o', function () {
    $('#calendar-service').fullCalendar('removeEvents', $(this).attr('id'));
});
var calenderLoad = function (events) {
    $('#calendar-service').fullCalendar({
        header: {
            left: '',
//            center: 'title',
            center: '',
            right: ''
        },
        views: {
            '2Week': {
                type: 'agenda',
                duration: {weeks: 2},
                buttonText: '2 weeks'
            },
            '3Week': {
                type: 'agenda',
                duration: {weeks: 3},
                buttonText: '3 weeks'
            },
            '4Week': {
                type: 'agenda',
                duration: {weeks: 4},
                buttonText: '4 weeks'
            },
        },
        firstDay: 1,
        defaultView: 'agendaWeek',
        allDaySlot: false,
        selectable: true,
        editable: true,
        disableDragging: false,
        slotDuration: '01:00',
        columnFormat: 'ddd',
        contentHeight: 'auto',
        timeFormat: 'HH:mm',
        axisFormat: 'HH:mm',
        events: events,
        eventRender: function (event, element) {
            if (event.icon) {
                element.find(".fc-time").prepend("<div style='margin:10px 0px;'><i class='event-delete fa fa-" + event.icon + "' id=" + event.id + "></i></div> ");
            }
        },
        eventClick: function (event, element) {
            $('#edit').modal({show: 'true'});
            $('#edit_site_id').val(event.site_id);
            $('#edit_start_time').val(event.start.format('HH:mm'));
            $('#edit_end_time').val(event.end.format('HH:mm'));
            $('#event_start_date_edit').val(event.start.format('YYYY-MM-DD'));
            $('#event_end_date_edit').val(event.end.format('YYYY-MM-DD'));
            eventSource = event;
            getLocationsEdit($('#edit_site_id').val(), eventSource);
        },
        eventResizeStop: function (event) {
            $('#calendar-service').fullCalendar('updateEvent', event);
        },
        dayClick: function (date) {
            $('#add').modal({show: 'true'});
            $('#day').val(date.day());
            $('#start_time').val(date.format('HH:mm'));
            $('#end_time').val(date.format('HH:mm'));
            $('#event_start_date').val(date.format('YYYY-MM-DD'));
        },
    });
};
$('#addEvent').submit(function (e) {
    id++;
    e.preventDefault();
    addEventFormSubmit(id);
});
$('#updateEvent').submit(function (e) {
    e.preventDefault();
    updateEventFormSubmit();
});

$('#edit_site_id').change(function () {
    getLocationsEdit($(this).val(), eventSource);
});
