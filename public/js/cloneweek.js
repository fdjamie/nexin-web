
$(document).ready(function () {
    weekno = parseInt($('select').length/7);
});

$('.addweek').on('click', function () {addNewWeek();});
$('.fa-trash').on('click', function () {removeWeekRow($(this).parents('.template-contents'));});

var addNewWeek = function () {
    var i = 0;
    week_html = $(".template-contents").first().clone();
    next_week = week_html;
    $(next_week).find("select").each(function () {
        $(this).val('').prop('name', 'contents[shift][' + weekno + '][' + i + ']');
        i++;
    });
    $(next_week).find('.fa-trash').show();
    $(next_week).appendTo("#empty-container").find('.fa-trash').bind('click', function () {
        removeWeekRow($(this).parents('.template-contents'));
    });
    weekno++;
};
var removeWeekRow = function ($element) {    $element.remove();};
