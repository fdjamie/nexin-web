$(document).ready(function () {
    var tableData = function () {
        var allData = null
        jQuery.ajax({
            async: false,
            url: APP_URL+ '/servicepostdata',
            type: 'get',
            success: function (data) {
                allData = data;
            }
        });
        return allData;
    };
    var data = tableData();
    console.log(data);
});