$(document).ready(function () {
    $('#rotaSpin,.shiftSpin').hide();
    // =========================== DATE PICKER CODE =========================
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    // =========================== END DATE CODE ===============================
    if ($(':checkbox').is(':checked')) {
        $('#endDate').attr("disabled", "disabled");
    }
    $(':checkbox').change(function () {
        if ($(this).is(':checked')) {
            $('#endDate').attr("disabled", "disabled");
            $('#endDate').val("");
        } else {
            $("#endDate").removeAttr("disabled");
        }
    });
    // =========================== COPY WEEK/ REMOVE WEEK CODE =================
    weekno = parseInt($('select').length / 7);
    $('.addweek').on('click', function () {
        addNewWeek();
    });
    $('.fa-trash').on('click', function () {
        removeWeekRow($(this).parents('.template-contents'));
    });
    var addNewWeek = function () {
        var i = 0;
        week_html = $(".template-contents").first().clone();
        next_week = week_html;
        $(next_week).find("select").each(function () {
            $(this).val('').prop('name', 'content[shift_type][' + weekno + '][' + i + ']');
            i++;
        });
        $(next_week).find('.fa-trash').show();
        $(next_week).appendTo("#empty-container").find('.fa-trash').bind('click', function () {
            removeWeekRow($(this).parents('.template-contents'));
        });
        weekno++;
    };
    var removeWeekRow = function ($element) {
        $element.remove();
    };
    // =========================== REPOPULATE ROTAGROUPS WEEK CODE =============
    var updateRotaGroups = function (id) {
        $('#rotaSpin').show();
        $('#rota_group_id').hide();
        jQuery.ajax({
            url: APP_URL+ '/getDirectorateRotaGroups/' + id,
            type: 'get',
            success: function (data) {
                var rota_groups = '';
                $.each(data, function (index, value) {
                    rota_groups += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#rotaSpin').hide();
                $('#rota_group_id').html(rota_groups).show();
                updateShiftTypes(id,$('#rota_group_id').val());
            }
        });
    };
    $('#directorate_id').change(function () {
        updateRotaGroups($(this).val());
    });
    // =========================== REPOPULATE SHIFT TYPES CODE =================
    var updateShiftTypes = function (did,gid) {
        $('.shiftSpin').show();
        jQuery.ajax({
            url: APP_URL+ '/getRotaGroupShiftTypes/' +did+'/'+gid,
            type: 'get',
            success: function (data) {
                var rota_groups = "<option value=''> Off </option>";
                $.each(data, function (index, value) {
                    rota_groups += "<option value='" + index + "'>" + value + "</option>";
                });
                $('.shiftSpin').hide();
                $('.shift_type').html(rota_groups);
            }
        });
    };
    $('#rota_group_id').change(function () {
        updateShiftTypes($('#directorate_id').val(),$(this).val());
    });

});