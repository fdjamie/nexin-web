$(document).ready(function(){
    $('#position_spin').hide();
   var updateRoles = function(id){
       $('#position_spin').show();
       $('#position').hide();
       jQuery.ajax({
            url: APP_URL+ '/getRolesPositions/' + id,
            type: 'get',
            success: function (data) {
                var roles = '';
                $.each(data, function (index, value) {
                    roles += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#position').html(roles).show();
                $('#position_spin').hide();
            }
        });
   };
   $('#directorate_id').change(function(){
       updateRoles($(this).val());
   });
});