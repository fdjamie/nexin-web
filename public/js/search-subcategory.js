function formatRepo(repo) {
    if (repo.loading)
        return repo.text;
    var markup = "<div class='select2-result-repository__title'>" + repo.name + "</div>";
    markup += "<div class='select2-result-repository__forks'><i class='fa fa-sitemap'></i> " + repo.level + " </div>";

    return markup;
}

function formatRepoSelection(repo) {
    return repo.name || repo.text;
}

$(".serach-subcategory").select2({
    ajax: {
//      url: "https://api.github.com/search/repositories",
        url: APP_URL+"/searchSubcategory",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                category_id: $('#category_id').val(),
                page: params.page
            };
        },
        processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
                results: data.items,
                pagination: {
                    more: (params.page * 30) < data.total_count
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) {
        return markup;
    }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});


