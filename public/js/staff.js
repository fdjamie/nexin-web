$(document).ready(function () {
    $('#spin,#gd_spin,#rl_spin').hide();
//    $('.js-example-basic-multiple').select2({
//        multiple: true,
//    });
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        orientation: "bottom auto"
    });
    $('#directorate').change(function () {
        updateSpeciality($(this).val());
        updateGrade($(this).val());
        updateRole($(this).val());
    });
    var updateSpeciality = function (id) {
        $('#directorate_speciality_id').hide();
        $('#spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {
                    speciality += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#directorate_speciality_id').html(speciality).show();
                $('#spin').hide();
            }
        });
    };

    var updateGrade = function (id) {
        $('#grade_id').hide();
        $('#gd_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getgrade/' + id,
            type: 'get',
            success: function (data) {
                var grade = '';
                $.each(data, function (index, value) {
                    grade += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#grade_id').html(grade).show();
                $('#gd_spin').hide();
            }
        });
    };

    var updateRole = function (id) {
        $('#role_id').hide();
        $('#rl_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getRoles/' + id,
            type: 'get',
            success: function (data) {
                var role = '';
                $.each(data, function (index, value) {
                    role += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#role_id').html(role).show();
                $('#rl_spin').hide();
            }
        });
    };
});