$(document).ready(function () {
    $('#gp_spin,#sg_spin').hide();
    var updateGroups = function (id) {
        $('#group_id').hide();
        $('#gp_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getgroups/' + id,
            type: 'get',
            success: function (data) {
                var group = '';
                $.each(data, function (index, value) {
                    group += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#group_id').html(group).show();
                $('#gp_spin').hide();
                updateSubgroups($('#group_id').val(), id);
            }
        });
    };
    var updateSubgroups = function (g_id, d_id) {
        $('#subgroup_id').hide();
        $('#sg_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getsubgroups/' + g_id + '/' + d_id,
            type: 'get',
            success: function (data) {
                var group = '';
                $.each(data, function (index, value) {
                    group += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#subgroup_id').html(group).show();
                $('#sg_spin').hide();
            }
        });
    };
    $('#directorate_id').change(function () {
        updateGroups($(this).val());
    });
    $('#group_id').change(function () {
        updateSubgroups($(this).val(), $('#directorate_id').val());
    });
});