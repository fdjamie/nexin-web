var colorCode = function () {
    var colors_data;
    $.ajax({
        url: APP_URL + '/getColorsCode',
        type: 'get',
        async: false,
        success: function (data) {
            colors_data = data;
        }
    });
    return colors_data;
};

var colors = colorCode();

$('#error-alert').hide();

$('.remove-row').click(function () {
    removeRow($(this).parents('.copy-row'));
});

$('.royg').change(function () {
    $('#' + $(this).attr('id')).next('.color-box').css('background', colors[$(this).val()]);
});

$('.add').click(function (event) {
    event.preventDefault();
    var tablebody_id = $(this).closest('tbody').attr('id');
    var clone_row = $('#' + tablebody_id).find('.copy-row').last().clone();
    var new_row = clone_row.appendTo('#' + tablebody_id);
    $(new_row).find('.add-row').html('<a class="fa fa-trash remove-row" id="row-' + count + '"></a>');
    var count = $(new_row).closest('tbody').children().length - 1;
    setIdValue(new_row, count);
    bindTrashRemove(new_row);
    bindDropdownChange(new_row);
});

var setIdValue = function (new_row, count) {
    var number = count;
    var priority_id = $(new_row).find('.priority').attr('id').split('-');
    priority_id = priority_id[0] + '-' + count;
    $(new_row).find('.priority').attr('id', priority_id);
    var level = $(new_row).find('.level').attr('id').split('-');
    var level_id = level[0] + '-' + count;
    var pre_level_id = '#' + level[0] + '-' + (number - 1);
    var level_value = parseInt($(pre_level_id).val()) + 1;
    $(new_row).find('.level').attr('id', level_id);
    $(new_row).find('.level').val((level_value));
    if ($(new_row).find('.row-id')) {
        $(new_row).find('.row-id').val(0);
    }
    var from_id = $(new_row).find('.from').attr('id');
    var from = from_id.split('-');
    var from_last = from_id.split('_');
    var pre_from_id = '#' + from[0] + '-' + (number - 1) + '_' + from_last[1];
    $(new_row).find('.from').attr('id', from[0] + '-' + count + '_' + from_last[1]);
    $(new_row).find('.from').val($(pre_from_id).val());

    var to_id = $(new_row).find('.to').attr('id');
    var to = to_id.split('-');
    var to_last = to_id.split('_');
    var pre_to_id = '#' + to[0] + '-' + (number - 1) + '_' + to_last[1];
    $(new_row).find('.to').attr('id', to[0] + '-' + count + '_' + to_last[1]);
    $(new_row).find('.to').val($(pre_to_id).val());


};

var bindTrashRemove = function (new_row) {
    $(new_row).find('.fa-trash').bind('click', function () {
        removeRow($(this).parents('.copy-row'));
    });
};

var bindDropdownChange = function (new_row) {
    $(new_row).find('.royg').bind('change', function () {
        $('#' + $(this).attr('id')).next('.color-box').css('background', colors[$(this).val()]);
    });
};

var removeRow = function ($row) {
    $row.remove();
};

$('.delete-row').click(function () {
    var row = $(this).parents('.copy-row');
    $.ajax({
        url: APP_URL + '/speciality-priority-set/delete/' + $(this).attr('id'),
        type: 'get',
        success: function (data) {
            removeRow(row);
        }
    });
});


var disableButton = function (id, value) {
    var button = $('#' + id).parents('tbody').find('.add-row').find('.add');
    var table_row = $('#' + id).parents('tr');
    if (value == false) {
        button.attr('disabled', 'disabled');
        table_row.addClass('bg-danger');
    } else {
        if (button.attr('disabled'))
            button.removeAttr('disabled');
        if (table_row.hasClass('bg-danger'))
            table_row.removeClass('bg-danger');
    }
};