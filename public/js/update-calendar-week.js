$(document).ready(function () {
    updateWeekNo();
    $('.fc-prev-button, .fc-next-button, .fc-agendaWeek-button, .fc-2Weeks-button, .fc-3Weeks-button, .fc-4Weeks-button, .fc-5Weeks-button').bind('click', updateWeekNo);
});

var updateWeekNo = function(){
    var cycle_length = $('#cycle_length').val();
    var starting_date = $('#start_date').val();
    var starting_week = $('#starting_week').val();
    var id = $('#calendar-service').children('.fc').attr('id')
    var view = $('#'+id).fullCalendar('getView');
    var sweek = moment.utc(starting_date+'T00:00:00').startOf('week').add(1, 'day');
    var week = view.start.diff(sweek,'week');
    week+=(starting_week-1);
    week%=cycle_length;
    if(week < 0)week+=parseInt(cycle_length);
//    console.log(view.start.week(),sweek.week(),week)
    for(i=1;i<=5;i++){
        week%=cycle_length;
        $('.week-'+i).text('Week '+(++week));
    }
};
