$(document).ready(function () {
    $('#serviceSpin,#specialitySpin,#rotaSpin,#categorySpin,#splitSpin').hide();
    $('.time').clockpicker({'autoclose': true});
    //====================================== CATEGORY/SUB CHECK BOX JS =============================================
    var categoryCheck = function (catgegory, category_check, sub_category, sub_category_check) {
        var category_id = $(catgegory).val();
        var directorate_id = $('#directorate_id').val();
        if ($(category_check).is(':checked')) {
            $(catgegory).removeAttr("disabled");
            $(sub_category_check).removeAttr("disabled");
        } else {
            $(catgegory).attr("disabled", "disabled");
            $(sub_category).attr("disabled", "disabled");
            $(sub_category_check).attr("disabled", "disabled");
            $(sub_category_check).prop('checked', false);
            category_id = null;
        }
        if ($(sub_category).is(':disabled'))
            var subcategory_id = null;
        else
            var subcategory_id = $(sub_category).val();
        var service_id = '#service_id';
        var directorate_speciality_id = $('#directorate_speciality_id').val();
        if (category_check == '#category_check_1') {
            service_id = '#service_id_1';
            directorate_speciality_id = $('#directorate_speciality_id_1').val();
        }
        if (category_check == '#category_check_2') {
            service_id = '#service_id_2';
            directorate_speciality_id = $('#directorate_speciality_id_2').val();
        }
        $(service_id).hide();
        updateService(directorate_id, directorate_speciality_id, category_id, subcategory_id, service_id);
    };
    var subCategoryCheck = function (sub_category, sub_category_check) {
        console.log(sub_category_check);
        var subcategory_id = $(sub_category).val();
        var directorate_id = $('#directorate_id').val();
        if ($(sub_category_check).is(':checked')) {
            $(sub_category).removeAttr("disabled");
        } else {
            $(sub_category).attr("disabled", "disabled");
            subcategory_id = null;
        }
        var service_id = '#service_id';
        var directorate_speciality_id = $('#directorate_speciality_id').val();
        var category_id = $('#category_id').val();
        if (sub_category_check == '#sub_category_check_1') {
            service_id = '#service_id_1';
            directorate_speciality_id = $('#directorate_speciality_id_1').val();
            category_id = $('#category_id_1').val();
        }
        if (sub_category_check == '#sub_category_check_2') {
            service_id = '#service_id_2';
            directorate_speciality_id = $('#directorate_speciality_id_2').val();
            category_id = $('#category_id_2').val();
        }
        $(service_id).hide();
        updateService(directorate_id, directorate_speciality_id, category_id, subcategory_id, service_id);
    };
    //====================================== SERVICE ENABLE DISABLE JS =============================================
    var serviceCheck = function (service, service_checck) {
        if ($(service_checck).is(':checked')) {
            $(service).removeAttr("disabled");
        } else {
            $(service).attr("disabled", "disabled");
        }
    };
    //====================================== NUMBER OF SPLIT INFO JS ===============================================
    var splitTimes = function (id) {
        switch (id) {
            case '0':
                $('#split_1_time').attr('disabled', 'disabled');
                $('.split_2').attr("disabled", "disabled");
                $('#split_2_time').attr('disabled', 'disabled');
                $('.split_3').attr("disabled", "disabled");
                break;
            case '1':
                $('#split_1_time').removeAttr('disabled');
                $('.split_2').removeAttr("disabled");
                if (!$('#split_2_time').is('[disabled=disabled]')) {
                    $('#split_2_time').attr('disabled', 'disabled');
                    $('.split_3').attr("disabled", 'disabled');
                }
                break;
            case '2':
                $('#split_1_time').removeAttr('disabled');
                $('.split_2').removeAttr("disabled");
                $('#split_2_time').removeAttr('disabled');
                $('.split_3').removeAttr("disabled");
                break;
        }
    };
    //====================================== ENABLE/DISABLE SPLIT FIELDS JS ========================================
    $('#split_1_time').change(function () {
        $('#end_time').val($(this).val());
        $('#start_time_1').val($(this).val());
    });
    $('#split_2_time').change(function () {
        $('#end_time_1').val($(this).val());
        $('#start_time_2').val($(this).val());
    });
    //====================================== AJAX CALLS JS=========================================================
    var updateService = function (did, sid, cid, sbid, service_id) {
        console.log('d = ' + did, 's = ' + sid, 'c = ' + cid, 'sb = ' + sbid);
        jQuery.ajax({
            url: APP_URL+ '/getShiftTypeService/' + did + '/' + sid + '/' + cid + '/' + sbid,
            type: 'get',
            success: function (data) {
                console.log(data);
                var service = '';
                $.each(data, function (index, value) {
                    service += "<option value='" + index + "'>" + value + "</option>";
                });
                $(service_id).html(service).show();
            }
        });
    };
    var updateSpeciality = function (id) {
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var service = '<option value="0">Parent Speciality</option>';
                service += '<option value="-1">Directorate Services</option>';

                $.each(data, function (index, value) {
                    service += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#specialitySpin').hide();
                $('#directorate_speciality_id').html(service).show();
            }
        });
    };
    var updateRotaGroups = function (id) {
        jQuery.ajax({
            url: APP_URL+ '/getDirectorateRotaGroups/' + id,
            type: 'get',
            success: function (data) {
                var rota_groups = '';
                $.each(data, function (index, value) {
                    rota_groups += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#rotaSpin').hide();
                $('#rota_group_id').html(rota_groups).show();
            }
        });
    };
    var upadteSubCategory = function (did, cid, sub_category_id, sid) {
        $(sub_category_id).hide();
        jQuery.ajax({
            url: APP_URL+ '/getSpecialitySubcategory/' + did + '/' + cid + '/' + sid,
            type: 'get',
            success: function (data) {
                var sub_category = '';
                $.each(data, function (index, value) {
                    sub_category += "<option value='" + index + "'>" + value + "</option>";
                });
                $(sub_category_id).html(sub_category).show();
//                updateService($('#directorate_speciality_id').val(),id);
            }
        });
    };
//====================================== FUNCTION CALLS JS =====================================================
    $(".directorate_speciality_id").change(function () {
        if ($('#category_id').is(':disabled'))
            var category_id = null;
        else
            var category_id = $('#category_id').val();
        if ($('#sub_category_id').is(':disabled'))
            var subcategory_id = null;
        else
            var subcategory_id = $('#sub_category_id').val();

        if ($(this).attr('id') == 'directorate_speciality_id') {
            $('#service_id').hide();
            upadteSubCategory($('#directorate_id').val(), $('#category_id').val(), '#sub_category_id', $(this).val());
            updateService($('#directorate_id').val(), $(this).val(), category_id, subcategory_id, '#service_id');
        }
        if ($(this).attr('id') == 'directorate_speciality_id_1') {
            if ($('#category_id_1').is(':disabled'))
                var category_id = null;
            else
                var category_id = $('#category_id_1').val();

            if ($('#sub_category_id_1').is(':disabled'))
                var subcategory_id = null;
            else
                var subcategory_id = $('#sub_category_id_1').val();

            $('#service_id_1').hide();
            upadteSubCategory($('#directorate_id').val(), $('#category_id_1').val(), '#sub_category_id_1', $(this).val());
            updateService($('#directorate_id').val(), $(this).val(), category_id, subcategory_id, '#service_id_1');
        }
        if ($(this).attr('id') == 'directorate_speciality_id_2') {
            if ($('#category_id_2').is(':disabled'))
                var category_id = null;
            else
                var category_id = $('#category_id_2').val();

            if ($('#sub_category_id_2').is(':disabled'))
                var subcategory_id = null;
            else
                var subcategory_id = $('#sub_category_id_2').val();
            $('#service_id_2').hide();
            upadteSubCategory($('#directorate_id').val(), $('#category_id_2').val(), '#sub_category_id_2', $(this).val());
            updateService($('#directorate_id').val(), $(this).val(), category_id, subcategory_id, '#service_id_2');
        }

    });
//    $("#directorate_id").change(function () {
//        $('#specialitySpin').show();
//        $('#directorate_speciality_id').hide();
//        updateSpeciality($(this).val());
//        $('#rotaSpin').show();
//        $('#rota_group_id').hide();
//        updateRotaGroups($(this).val());
//    });
    $('.category_id').change(function () {
        if ($(this).attr('id') == 'category_id') {
            $('#service_id,#sub_category_id').hide();
            upadteSubCategory($('#directorate_id').val(), $(this).val(), '#sub_category_id', $('#directorate_speciality_id').val());
            updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $(this).val(), $('#sub_category_id').val(), '#service_id');
        }
        if ($(this).attr('id') == 'category_id_1') {
            $('#service_id_1,#sub_category_id_1').hide();
            upadteSubCategory($('#directorate_id').val(), $(this).val(), '#sub_category_id_1', $('#directorate_speciality_id_1').val());
            updateService($('#directorate_id').val(), $('#directorate_speciality_id_1').val(), $(this).val(), $('#sub_category_id_1').val(), '#service_id_1');
        }
        if ($(this).attr('id') == 'category_id_2') {
            $('#service_id_2,#sub_category_id_2').hide();
            upadteSubCategory($('#directorate_id').val(), $(this).val(), '#sub_category_id_2', $('#directorate_speciality_id_2').val());
            updateService($('#directorate_id').val(), $('#directorate_speciality_id_2').val(), $(this).val(), $('#sub_category_id_2').val(), '#service_id_2');
        }
    });
    $('.sub_category_id').change(function () {
        if ($(this).attr('id') == 'sub_category_id') {
            $('#service_id').hide();
            updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $(this).val(), '#service_id');
        }
        if ($(this).attr('id') == 'sub_category_id_1') {
            $('#service_id_1').hide();
            updateService($('#directorate_id').val(), $('#directorate_speciality_id_1').val(), $('#category_id_1').val(), $(this).val(), '#service_id_1');
        }
        if ($(this).attr('id') == 'sub_category_id_2') {
            $('#service_id_2').hide();
            updateService($('#directorate_id').val(), $('#directorate_speciality_id_2').val(), $('#category_id_2').val(), $(this).val(), '#service_id_2');
        }
    });
    splitTimes($('#splits').val());
    $('#splits').change(function () {
        splitTimes($(this).val());
    });
    $('.category_check').change(function () {
        if ($(this).attr('id') == 'category_check')
            categoryCheck('#category_id', '#' + $(this).attr('id'), '#sub_category_id', '#sub_category_check', '#service_id');
        if ($(this).attr('id') == 'category_check_1')
            categoryCheck('#category_id_1', '#' + $(this).attr('id'), '#sub_category_id_1', '#sub_category_check_1', '#service_id_1');
        if ($(this).attr('id') == 'category_check_2')
            categoryCheck('#category_id_2', '#' + $(this).attr('id'), '#sub_category_id_2', '#sub_category_check_2', '#service_id_1');
    });
    $('.sub_category_check').change(function () {
        if ($(this).attr('id') == 'sub_category_check')
            subCategoryCheck('#sub_category_id', '#' + $(this).attr('id'));
        if ($(this).attr('id') == 'sub_category_check_1')
            subCategoryCheck('#sub_category_id_1', '#' + $(this).attr('id'));
        if ($(this).attr('id') == 'sub_category_check_2')
            subCategoryCheck('#sub_category_id_2', '#' + $(this).attr('id'));
    });
    $('.service_check').change(function () {
        if ($(this).attr('id') == 'service_check')
            serviceCheck('#service_id', '#' + $(this).attr('id'));
        if ($(this).attr('id') == 'service_check_1')
            serviceCheck('#service_id_1', '#' + $(this).attr('id'));
        if ($(this).attr('id') == 'service_check_2')
            serviceCheck('#service_id_2', '#' + $(this).attr('id'));
    });
    if (!$('#category_check_1').is(':checked'))
        $('#category_id_1').attr('disabled', 'disabled');
    if (!$('#category_check_2').is(':checked'))
        $('#category_id_2').attr('disabled', 'disabled');

    if (!$('#sub_category_check_1').is(':checked'))
        $('#sub_category_id_1').attr('disabled', 'disabled');
    if (!$('#sub_category_check_2').is(':checked'))
        $('#sub_category_id_2').attr('disabled', 'disabled');

    if (!$('#service_check_1').is(':checked'))
        $('#service_id_1').attr('disabled', 'disabled');
    if (!$('#service_check_2').is(':checked'))
        $('#service_id_2').attr('disabled', 'disabled');
});