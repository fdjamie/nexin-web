
$(document).ready(function () {
    $('#sp_spin,#pr_spin,#ct_spin, #sb_spin').hide();
    $('#directorate_id').change(function () {
        updateCategory($(this).val());
        updateSpeciality($(this).val());
        updateTree($(this).val());
    });

    $('#type').change(function () {
        checkType($(this).val());
    });

    $('#directorate_speciality_id').change(function () {
        updatePriority($(this).val());
    });

    $('#subcategory_id,#category_id').change(function () {
        updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#subcategory_id').val(), $('#type').val());
    });

    $('.check').change(function () {
        disableEnableField($(this).attr('id'));
        if ($(this).attr('id') == 'category_check') {
            if ($(this).is(':checked')) {
                $('#subcategory_check').removeAttr('disabled');
                $('#subcategory_id').removeAttr('disabled');
            } else {
                $('#subcategory_check').attr('disabled', 'disabled');
                $('#subcategory_id').attr('disabled', 'disabled');
            }
        }
    });

    var disableEnableField = function (id) {
        id = id.split('_');
        id = '#' + id[0] + '_id';
        if ($(id).attr('disabled')) {
            $(id).removeAttr('disabled');
        } else {
            $(id).attr('disabled', 'disabled');
            $(id).val(null);
        }
        updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#subcategory_id').val(), $('#type').val());
    };

    var updateCategory = function (id) {
        $('#category_id').hide();
        $('#ct_spin').show();
        jQuery.ajax({
            url: APP_URL + '/getcategory/' + id,
            type: 'get',
            success: function (data) {
                var group = '';
                $.each(data, function (index, value) {
                    group += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#category_id').html(group).show();
                $('#ct_spin').hide();
                updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#subcategory_id').val(), $('#type').val());
            }
        });

    };

    var updateSpeciality = function (id) {
        $('#directorate_speciality_id').hide();
        $('#sp_spin').show();
        jQuery.ajax({
            url: APP_URL + '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {

                    speciality += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#directorate_speciality_id').html(speciality).show();
                $('#sp_spin').hide();
                updatePriority($('#directorate_speciality_id').val());
            }
        });

    };

    var updatePriority = function (id) {
        $('#priority').hide();
        $('#pr_spin').show();
        jQuery.ajax({
            url: APP_URL + '/getCategoryPriority/' + id,
            type: 'get',
            success: function (data) {
                var priority = '';
                $.each(data, function (index, value) {

                    priority += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#priority').html(priority).show();
                $('#pr_spin').hide();
            }
        });

    };

    var checkType = function (value) {
        switch (value) {
            case 'category':
                $('#sub-div,#service-div').hide();
                $('#subcategory_id,#service_id').attr('disabled', 'disabled');
                break;

            case 'subcategory':
                if ($('#service-div').show()) {
                    $('#service-div').hide();
                    $('#service_id').attr('disabled', 'disabled');
                }
                $('#sub-div').show();
                $('#subcategory_id').removeAttr('disabled');
                break;

            case 'service':
                updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#subcategory_id').val(), $('#type').val());
                if ($('#sub-div').hide()) {
                    $('#sub-div').show();
                    $('#subcategory_id').removeAttr('disabled');
                }
                $('#service-div').show();
                $('#service_id').removeAttr('disabled');
                break;
            case 'general_service':
                updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#subcategory_id').val(), $('#type').val());
                if ($('#sub-div').hide()) {
                    $('#sub-div').show();
                    $('#subcategory_id').removeAttr('disabled');
                }
                $('#service-div').show();
                $('#service_id').removeAttr('disabled');
                break;
        }
    };

    var updateTree = function (id) {
        jQuery.ajax({
            url: APP_URL + '/getCategoryTree/' + id,
            type: 'get',
            success: function (data) {
                $('#tree').html(data);
                treeJs();
            }
        });
    };

    var updateService = function (d_id, s_id, c_id, sb_id, type) {
        $('#service_id').hide();
        $('#sb_spin').show();
        if (type == 'service') {
            jQuery.ajax({
                url: APP_URL + '/getServiceDropdown/' + d_id + '/' + s_id + '/' + c_id + '/' + sb_id,
                type: 'get',
                success: function (data) {
                    var service = '';
                    $.each(data, function (index, value) {
                        service += "<option value='" + index + "'>" + value + "</option>";
                    });
                    $('#sb_spin').hide();
                    $('#service_id').html(service).show();
                }
            });
        } else {
            jQuery.ajax({
                url: APP_URL + '/getDirectorateServiceDropdown/' + d_id + '/' + c_id + '/' + sb_id,
                type: 'get',
                success: function (data) {
                    var service = '';
                    $.each(data, function (index, value) {
                        service += "<option value='" + index + "'>" + value + "</option>";
                    });
                    $('#sb_spin').hide();
                    $('#service_id').html(service).show();
                }
            });
        }

    };

    //######################### MultiNested Tree JS ##################
    var treeJs = function () {
        $.getScript(APP_URL + '/js/MultiNestedList.js')
                .done(function (script) {
                    console.log('Resource loaded successfully.');
                })
                .fail(function () {
                    console.log('Could not load the resource.');
                });
    };


    // ################################################################
    treeJs();
});