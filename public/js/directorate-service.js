$(document).ready(function () {
    
    $('#sp_spin,#sb_spin,#ct_spin,#spt_spin').hide();
    $("#directorate").change(function () {
        updateSpeciality($(this).val());
        updateCategory($(this).val());
    });
    
    $("#category_id").change(function () {
        updateSubCategory($('#directorate').val(), $(this).val());
    });
    
    var updateSpeciality = function (id) {
        $('#all-check-boxes').hide();
        $('#spt_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {
                    speciality += '<div class="form-group col-md-3 main-check-box">';
                    speciality += '<input id="' + index + '" class="check-boxes" checked="checked" name="selected_speciality[]" type="checkbox" value="' + index + '">';
                    speciality += '<label for="' + index + '" class="awesome">' + value + '</label>';
                    speciality += '</div>';
                });
                $('#all-check-boxes').html(speciality).show();
                $('#spt_spin').hide();
                checkbox();
            }
        });
    };
    
    var categoryCheck = function () {
        if ($('#category_check').is(':checked')) {
            $('#category_id').removeAttr("disabled");
//            $('#sub_category_id').removeAttr("disabled");
//            $('#sub_category_check').prop('checked', true);
        } else {
            $('#category_id').attr("disabled", "disabled");
//            $('#sub_category_id').attr("disabled", "disabled");
            $('#sub_category_check').prop('checked', false);
            subCategoryCheck();
        }
    };

    var subCategoryCheck = function () {
        if ($('#sub_category_check').is(':checked')) {
            $('#sub_category_id').removeAttr("disabled");
        } else {
            $('#sub_category_id').attr("disabled", "disabled");
        }
    };

    var updateCategory = function (id) {
        $('#category_id').hide();
        $('#ct_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getcategory/' + id,
            type: 'get',
            success: function (data) {
                var category = '';
                $.each(data, function (index, value) {

                    category += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#category_id').html(category).show();
                $('#ct_spin').hide();
                updateSubCategory(id, $('#category_id').val());
            }
        });
    };

    var updateSubCategory = function (did, cid) {
        $('#sub_category_id').hide();
        $('#sb_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getsubcategories/' + did + '/' + cid,
            type: 'get',
            success: function (data) {
                var sub_category = '';
                $.each(data, function (index, value) {

                    sub_category += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#sub_category_id').html(sub_category).show();
                $('#sb_spin').hide();
            }
        });
    };

    var checkbox = function () {
        var all = 0;
        var slected = 0;
        $('.check-boxes').each(function () {
            all++;
            if ($(this).is(':checked'))
                slected++;
        });
        if (slected == all)
            $('#check_all').prop('checked', true);
    };
    checkbox();

    $('#check_all').change(function () {
        if ($(this).is(':checked')) {
            $('.check-boxes').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $('.check-boxes').each(function () {
                $(this).prop('checked', false);
            });
        }
    });
    
    $('.check-boxes').change(function () {
        if (!$(this).is(':checked')) {
            $('#check_all').prop('checked', false);
        } else {
            checkbox();
        }
    });

    $('#category_check').change(function () {
        categoryCheck();
    });
    
    $('#sub_category_check').change(function () {
        subCategoryCheck();
    });

    categoryCheck();
    subCategoryCheck();
});

