$(document).ready(function () {
    $('#ct_spin,#spt_spin').hide();
    var updateCategory = function (id) {
        $('#category_id').hide();
        $('#ct_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getcategory/' + id,
            type: 'get',
            success: function (data) {
                var group = '';
                $.each(data, function (index, value) {
                    group += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#category_id').html(group).show();
                $('#ct_spin').hide();
            }
        });
    };
    
    var updateSpeciality = function (id) {
        $('#all-check-boxes').hide();
        $('#spt_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {
                    speciality += '<div class="form-group col-md-3 main-check-box">';
                    speciality += '<input id="' + index + '" class="check-boxes" checked="checked" name="selected_speciality[]" type="checkbox" value="' + index + '">';
                    speciality += '<label for="' + index + '" class="awesome">' + value + '</label>';
                    speciality += '</div>';
                });
                $('#all-check-boxes').html(speciality).show();
                $('#spt_spin').hide();
                checkbox();
            }
        });
    };
    
    $('#directorate_id').change(function () {
        updateCategory($(this).val());
        updateSpeciality($(this).val());
    });
    
});