$(document).ready(function () {
    $('#serviceSpin,#specialitySpin,#rotaSpin,#categorySpin,#subSpin').hide();
    $('.time').clockpicker({'autoclose': true});
    //=================================== CATEGORY CHECK BOX JS ==========================================
    var categoryCheck = function (service) {
        service = service || 0;
        $('#serviceSpin').show();
        $('#service_id').hide();
        var category_id = $('#category_id').val();
        if ($('#category_check').is(':checked')) {
            $('#category_id').removeAttr("disabled");
            $('#sub_category_check').removeAttr("disabled");
//            $('#sub_category_id').removeAttr("disabled");
//            $('#sub_category_check').prop('checked', true);
        } else {
            $('#category_id').attr("disabled", "disabled");
            $('#sub_category_id').attr("disabled", "disabled");
            $('#sub_category_check').attr("disabled", "disabled");
            $('#sub_category_check').prop('checked', false);
            category_id = null;
        }
        if ($('#sub_category_id').is(':disabled'))
            var subcategory_id = null;
        else
            var subcategory_id = $('#sub_category_id').val();
        updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), category_id, subcategory_id);
    };

    var subCategoryCheck = function () {
        $('#serviceSpin').show();
        $('#service_id').hide();
        var subcategory_id = null;
        if ($('#sub_category_check').is(':checked')) {
            $('#sub_category_id').removeAttr("disabled");
            subcategory_id = $('#sub_category_id').val();
        } else {
            $('#sub_category_id').attr("disabled", "disabled");
        }
        updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), subcategory_id);
    };

    var serviceCheck = function () {
        var subcategory_id = null;
        if ($('#service_check').is(':checked')) {
            $('#service_id').removeAttr("disabled");
        } else {
            $('#service_id').attr("disabled", "disabled");
        }
    };
    //=================================== AJAX CALLS JS ==================================================
    var updateService = function (d_id, s_id, c_id, sb_id) {
        $('#serviceSpin').show();
        $('#service_id').hide();
        jQuery.ajax({
            url: APP_URL+ '/getShiftTypeService/' + d_id + '/' + s_id + '/' + c_id + '/' + sb_id,
            type: 'get',
            success: function (data) {
                console.log(data);
                var service = '';
                $.each(data, function (index, value) {
                    service += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#serviceSpin').hide();
                $('#service_id').html(service).show();
            }
        });
    };

    var updateSpeciality = function (id) {
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var service = '<option value="0">Parent Speciality</option>';
                service += '<option value="-1">Directorate Services</option>';
                $.each(data, function (index, value) {
                    service += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#specialitySpin').hide();
                $('#directorate_speciality_id').html(service).show();
                if ($('#category_id').is(':disabled'))
                    var category_id = null;
                else
                    var category_id = $('#category_id').val();
                if ($('#sub_category_id').is(':disabled'))
                    var subcategory_id = null;
                else
                    var subcategory_id = $('#sub_category_id').val();
                updateService(id, $('#directorate_speciality_id').val(), category_id, subcategory_id);
                updateSubCategory(id, $('#category_id').val(), $('#directorate_speciality_id').val());
            }
        });
    };

    var updateRotaGroups = function (id) {
        jQuery.ajax({
            url: APP_URL+ '/getDirectorateRotaGroups/' + id,
            type: 'get',
            success: function (data) {
                var rota_groups = '';
                $.each(data, function (index, value) {
                    rota_groups += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#rotaSpin').hide();
                $('#rota_group_id').html(rota_groups).show();
            }
        });
    };

    var updateCategory = function (id) {
        $('#category_id').hide();
        $('#ct_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getcategory/' + id,
            type: 'get',
            success: function (data) {
                var category = '';
                $.each(data, function (index, value) {

                    category += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#category_id').html(category).show();
                $('#ct_spin').hide();
                updateSubCategory(id, $('#category_id').val(), $('#directorate_speciality_id').val());
                updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#sub_category_id').val());
            }
        });
    };

    var updateSubCategory = function (did, cid, sid) {
        $('#sub_category_id').hide();
        $('#subSpin').show();
        jQuery.ajax({
            url: APP_URL+ '/getSpecialitySubcategory/' + did + '/' + cid + '/' + sid,
            type: 'get',
            success: function (data) {
                var sub_category = '';
                $.each(data, function (index, value) {

                    sub_category += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#sub_category_id').html(sub_category).show();
                $('#subSpin').hide();
                updateService(did, $('#directorate_speciality_id').val(), cid, $('#sub_category_id').val());
            }
        });
    };
    //=================================== CHANGE EVENTS CALLS JS ==========================================
//    categoryCheck();
//    subCategoryCheck();
//    serviceCheck();
    $('#category_check').change(function () {
        categoryCheck();
    });
    $('#sub_category_check').change(function () {
        subCategoryCheck();
    });
    $('#service_check').change(function () {
        serviceCheck();
    });
    $('#category_id').change(function () {
        $('#subSpin,#serviceSpin').show();
        $('#service_id').hide();
//        $('#sub_category_id').hide();
//        updateSubCategory($('#directorate_id').val(), $(this).val());
    });

    $('#sub_category_id').change(function () {
        updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), $('#category_id').val(), $('#sub_category_id').val());
    });
    $("#directorate_id").change(function () {
        $('#specialitySpin').show();
        $('#directorate_speciality_id').hide();
        updateSpeciality($(this).val());
        $('#rotaSpin').show();
        $('#rota_group_id').hide();
        updateRotaGroups($(this).val());
        updateCategory($(this).val());
    });
    $("#directorate_speciality_id").change(function () {
        $('#serviceSpin').show();
        $('#service_id').hide();
        if ($('#category_id').is(':disabled'))
            var category_id = null;
        else
            var category_id = $('#category_id').val();
        if ($('#sub_category_id').is(':disabled'))
            var subcategory_id = null;
        else
            var subcategory_id = $('#sub_category_id').val();
        updateService($('#directorate_id').val(), $('#directorate_speciality_id').val(), category_id, subcategory_id);
        updateSubCategory($('#directorate_id').val(),$('#category_id').val(), $(this).val());
    });
});