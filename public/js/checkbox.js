$(document).ready(function () {
    var checkbox = function () {
        var all = 0;
        var slected = 0;
        $('.check-boxes').each(function () {
            all++;
            if ($(this).is(':checked'))
                slected++;
        });
        if (slected == all)
            $('#check_all').prop('checked', true);
    };
    checkbox();

    $('#check_all').change(function () {
        if ($(this).is(':checked')) {
            $('.check-boxes').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $('.check-boxes').each(function () {
                $(this).prop('checked', false);
            });
        }
    });

    $('.check-boxes').change(function () {
        if (!$(this).is(':checked')) {
            $('#check_all').prop('checked', false);
        } else {
            checkbox();
        }
    });
});