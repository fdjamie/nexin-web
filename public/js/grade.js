$(document).ready(function(){
    $('#role_spin,#position_spin').hide();
   var updateRoles = function(id){
       $('#role_spin').show();
       $('#role_id').hide();
       jQuery.ajax({
            url: APP_URL+ '/getRoles/' + id,
            type: 'get',
            success: function (data) {
                console.log(data);
                var roles = '';
                $.each(data, function (index, value) {
                    roles += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#role_id').html(roles).show();
                $('#role_spin').hide();
            }
        });
   };
   var updateGradePositions = function(id){
       $('#position_spin').show();
       $('#position').hide();
       jQuery.ajax({
            url: APP_URL+ '/getGradesPosition/' + id,
            type: 'get',
            success: function (data) {
                var position = '';
                $.each(data, function (index, value) {
                    position += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#position').html(position).show();
                $('#position_spin').hide();
            }
        });
   };
   $('#directorate_id').change(function(){
       updateRoles($(this).val());
       updateGradePositions($(this).val());
   });
});