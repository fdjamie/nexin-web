$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
$('#sp_spin,#rt_spin,#p_spin,#fa_spin,#spin').hide();
var assignedWeek = function (id) {
    $('#template_position').hide();
    $('#spin').show();
    jQuery.ajax({
        url: APP_URL+ '/getrotatemplateweeks/' + id,
        type: 'get',
        success: function (data) {
            $('#template_position').attr('max', data);
            $('#template_position').val(1);
            $('#template_position').show();
            $('#spin').hide();
        }
    });
};
var updateSpeciality = function () {
    $('#directorate_speciality_id').hide();
    $('#sp_spin').show();
    jQuery.ajax({
        url: APP_URL+ '/getdirectoratespeciality/' + $('#directorate_id').val(),
        type: 'get',
        success: function (data) {
            var speciality = '';
            $.each(data, function (index, value) {

                speciality += "<option value='" + index + "'>" + value + "</option>";
            });
            $('#directorate_speciality_id').html(speciality).show();
            $('#sp_spin').hide();
            getPosts();
        }
    });
};
var getPosts = function () {
    $('#post_id').hide();
    $('#p_spin').show();
    if ($('#directorate_speciality_id').val()) {
        jQuery.ajax({
            url: APP_URL+ '/getRotationsPosts/' + $('#directorate_speciality_id').val(),
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {

                    speciality += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#post_id').html(speciality).show();
                $('#p_spin').hide();
                getRotas();
                assignedWeek('post');
            }
        });
    } else {
        var speciality = "<option value=''></option>";
        $('#post_id').html(speciality).show();
        $('#p_spin').hide();
        getRotas();
        assignedWeek('post');
    }
};

var updateRotaTemplate = function (id) {
    $('#template_id').hide();
    $('#rt_spin').show();
    jQuery.ajax({
        url: APP_URL+ '/getRotaGroupRotaTemplates/' + id,
        type: 'get',
        success: function (data) {
            var rota = '';
            $.each(data, function (index) {
                rota += "<option value='" + data[index].id + "'>" + data[index].name + "</option>";
            });
            $('#template_id').html(rota).show();
            $('#rt_spin').hide();
            assignedWeek($('#template_id').val());
        }
    });
};
$("#directorate_id").change(function () {
    $('#directorate_form').submit();
});
$('#directorate_speciality_id').change(function () {
    getPosts();
});
$('#rota_group_id').change(function () {
    updateRotaTemplate($(this).val());
});
$('#template_id').change(function () {
    assignedWeek($(this).val());
});