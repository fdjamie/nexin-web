$(document).ready(function () {
    var count = 1;
    $('#sp_spin,#sb_spin,#ct_spin').hide();

    $("#directorate").change(function () {
        updateSpeciality($(this).val());
        updateCategory($(this).val());
    });

//    $("#category_id").change(function () {
//        updateSubCategory($('#directorate').val(), $(this).val(), $('#directorate_speciality_id').val());
//    });

//    $("#sub_category_id").change(function () {
//        var type = 'new';
//        if ($("#child-dropdown").has('div').length)
//            type = 'replace';
//        updateChildSubCategory($(this).val(), $('#directorate_speciality_id').val(), type);
//    });

    $('.child-check').change(function () {
        if ($(this).is(':checked'))
            toggleSelect('#child-1', 1);
        else
            toggleSelect('#child-1', 0);
    });

    $("#directorate_speciality_id").change(function () {
        updateSubCategory($('#directorate').val(), $('#category_id').val(), $(this).val());
    });

    var updateSpeciality = function (id) {
        $('#directorate_speciality_id').hide();
        $('#sp_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {

                    speciality += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#directorate_speciality_id').html(speciality).show();
                $('#sp_spin').hide();
                updateSubCategory(id, $('#category_id').val(), $('#directorate_speciality_id').val());
            }
        });
    };

    var updateCategory = function (id) {
        $('#category_id').hide();
        $('#ct_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getcategory/' + id,
            type: 'get',
            success: function (data) {
                var category = '';
                $.each(data, function (index, value) {

                    category += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#category_id').html(category).show();
                $('#ct_spin').hide();
                updateSubCategory(id, $('#category_id').val(), $('#directorate_speciality_id').val());
            }
        });
    };

    var updateSubCategory = function (did, cid, sid) {
        $('#sub_category_id').hide();
        $('#sb_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getSpecialitySubcategory/' + did + '/' + cid + '/' + sid,
            type: 'get',
            success: function (data) {
                var sub_category = '';
                $.each(data, function (index, value) {
                    sub_category += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#sub_category_id').html(sub_category).show();
                $('#sb_spin').hide();
            }
        });
    };

//    var updateChildSubCategory = function (id, sid, type) {
//        jQuery.ajax({
//            url: APP_URL+ '/getChildSubcategory/' + id + '/' + sid,
//            type: 'get',
//            success: function (data) {
//                var sub_category = '';
//                if (type == 'new')
//                    sub_category = '<div class="form-group"><select class="form-control sub-child" id="child-' + count + '" name="sub_category_id">';
//                $.each(data, function (index, value) {
//                    sub_category += "<option value='" + index + "'>" + value + "</option>";
//                });
//                if (type == 'new') {
//                    sub_category += '</select></div>';
//                    $(sub_category).appendTo('#child-dropdown');
//                }else{
//                    $('#child-'+count).html(sub_category);
//                }
////                count++;
//            }
//        });
//    };

    var toggleSelect = function (select_id, value) {
        if (value == 1) {
            if ($(select_id).attr('disabled'))
                $(select_id).removeAttr('disabled');
        } else {
            $(select_id).attr('disabled', 'disabled');
        }
    };
});

