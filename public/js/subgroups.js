$(document).ready(function () {
    $('#gp_spin,#sg_spin').hide();
    var updateGroups = function (id) {
        $('#group_id').hide();
        $('#gp_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getgroups/' + id,
            type: 'get',
            success: function (data) {
                var group = '';
                $.each(data, function (index, value) {
                    group += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#group_id').html(group).show();
                $('#gp_spin').hide();
            }
        });
    };
    $('#directorate_id').change(function () {
        updateGroups($(this).val());
    });
});