$('.fa-spin').hide();
$("#speciality_id,#directorate_id").change(function () {
    $("#directorate_form").submit();
});
$("#category_id").change(function () {
    $("#category_form").submit();
});
serviceCheck = [];
$('.level-table').DataTable();
var spin = '<tr id="loader"><td colspan="7"><i class="fa fa-spinner fa-spin fa-2x" aria-hidden="true"></i></td></tr>';
var getOptions = function (resources, events, label) {
    var options = {
        aspectRatio: 2,
        scrollTime: '05:00',
        header: {
            left: '',
            center: '',
            right: ''
        },
        defaultView: 'timelineDay',
        resourceLabelText: label,
        resources: resources,
        events: events,
        slotLabelFormat: 'H:mm',
    };
    return options;
};
jQuery.ajax({
    url: APP_URL + '/serviceTimeline',
    type: 'get',
    data: {directorate_id: $('#directorate_id').val(), speciality_id: $('#speciality_id').val(), date: $('#date-header').val()},
    success: function (data) {
        var options = getOptions(data.resources, data.events, 'Services');
        $('#services').fullCalendar(options);
    }
});
jQuery.ajax({
    url: APP_URL + '/staffTimeline',
    type: 'get',
    data: {directorate_id: $('#directorate_id').val(), speciality_id: $('#speciality_id').val(), date: $('#date-header').val()},
    success: function (data) {
        var options = getOptions(data.resources, data.events, 'Staff');
        $('#staff').fullCalendar(options);
    }
});
var staffData = function (staff) {
    var table = '';
    table += '<thead class="text-center"><tr><th>Staff</th><th>Role</th><th>Grade</th><th>Info</th></tr><tbody></thead>';
    $.each(staff, function (index, value) {
        count.push(0);
        table += '<tr><td width="25%"> ' + value.name + '</td><td width="25%">' + value.role.name + '</td><td width="25%">' + value.grade.name + '</td><td width="25%"><i class="fa fa-info-circle" aria-hidden="true"></i></td><tr></tbody>';
    });
    return table;
};
var requirementData = function (requirement) {
    var table = '<tr><th> level </th> <th> color </th> <th> Service </th> <th> Statement </th> <th> number </th>';
    $.each(requirement, function (level, levelValue) {
        var checks = [];
        var levels = [];
        $.each(levelValue, function (serviceId, serviceRequirements) {
            var success = '';
            var fail = '';
            checks[0] = {value: false, color: 'bg-danger'};
            $.each(serviceRequirements, function (index, value) {
                table += '<tr><td>' + level + '</td><td> <div class="color-box" style = "background:' + value.color + '"></div></td><td> ' + value.name + '<i class="fa icon" aria-hidden="true"> </i></td>\n\
            <td>' + value.parameter + " " + value.operator + " " + value.value_name + " ," + value.value2_name + '</td>\n\
                 <td>' + value.number + '</td>';
                var test = 0;
                for (i = 0; i < value.checks.length; i++) {
                    count[i] = (value.checks[i].value != -1 && value.checks[i].value != 0 ? count[i] + 1 : count[i] + 0);
                    var bg_color = "";
                    var check_icon = "";
                    var staff = '';
                    var service = '';
                    var start = '';
                    var start_time = '';
                    var end = '';
                    var day = '';
                    var end_day = '';
                    var duration = '';
                    var text = '';
                    var assign = '';
                    if (value.checks[i].value === 1) {
                        bg_color = "bg-success";
                        staff = 'data-staff = "' + value.checks[i].staff + '"';
                        service = 'data-service = "' + value.checks[i].service + '"';
                        if (value.starting > value.checks[i].start_time)
                            start_time = value.starting;
                        else
                            start_time = value.checks[i].start_time;
                        start = 'data-start = "' + start_time + '"';
                        if (value.ending < value.checks[i].end_time)
                            end = 'data-ending = "' + value.ending + '"';
                        else
                            end = 'data-ending = "' + value.checks[i].end_time + '"';
                        day = 'data-day = "' + value.day + '"';
                        end_day = 'data-endday = "' + value.end_day + '"';
                        duration = 'data-duration = "' + value.duration + '"';
                        text = '<i class="fa fa-check" aria-hidden="true" ' + staff + ' ' + service + ' ' + start + ' ' + end + ' ' + day + ' ' + end_day + ' ' + duration + ' ></i>';
                        checks[value.service_id]++;
                        if (test == 0)
                            test++;
                    }
                    if (value.checks[i].value === 0) {
                        bg_color = "bg-danger";
                        text = '<i class="fa fa-times" aria-hidden="true" ></i>';
                    }
                    if (value.checks[i].value === 's' && value.checks[i].service == serviceId) {
                        bg_color = "bg-primary";
                        if (value.starting > value.checks[i].start_time)
                            start_time = value.starting;
                        else
                            start_time = value.checks[i].start_time;
                        start = 'data-start = "' + start_time + '"';
                        assign = 'data-assign = "' + value.checks[i].assign_id + '"';
                        end = 'data-ending = "' + time + '"';
                        if (parseInt(start_time) == parseInt(time) && value.checks[i].end_time != time) {
                            text = '<b class="assign-status" ' + start + ' ' + assign + ' ' + end + ' > S </b>';
                        } else if ((parseInt(start_time) < parseInt(time) && value.checks[i].end_time > time)) {
                            text = '<b class="assign-status" ' + start + ' ' + assign + ' ' + end + ' > P </b>';
                        }else if(value.checks[i].change == 1 && (value.checks[i].end_time <= time || value.checks[i].end_time == value.checks[i].start_time)){
                            text = '<b ' + start + ' ' + assign + ' ' + end + ' > C </b>';
                        }
                        checks[value.service_id]++;
                        if (test == 0)
                            test++;
                    }
                    table += '<td style="width: 37px; " class="' + bg_color + '"> ' + text + '</td>';
                }
                if (test == 0) {
                    fail = 'fail';
                    var requirementText = 'N';
                    var requirementBg = 'bg-danger';
                } else {
                    var requirementText = 'Y';
                    var requirementBg = 'bg-success';
                    success = 'success';
                }
                table += '<td style="width: 37px; " class="' + requirementBg + '"> <b>' + requirementText + '</b></td><td class="' + level + '-' + serviceId + '"></td></tr>';

            });
            if (fail != '') {
                checks[serviceId] = {value: false, color: 'bg-danger', borderColor: '#cc0001'};
            } else {
                checks[serviceId] = {value: true, color: 'bg-success', borderColor: '#090'};
            }
        });
        serviceCheck.push({level: level, checks: checks});
        table += '<tr><td colspan=5></td></tr>';
    });
    table += '<tr><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>';
    for (j = 0; j < count.length; j++) {
        table += '<td style="width: 37px;" class="bg-warning"><b>' + count[j] + '</b></td>';
    }
    table += '</tr>';
    var data = {table: table, check: serviceCheck};
    return data;
};
$('.event').click(function () {
    count = [];
    $('.fa-spin').show();
    time = $(this).text();
    $('#event-time').text(time);
    $('#staff-table,#requirement-table,#no-data').html('');
    var date = moment($('#date-header').val()).format('YYYY-MM-DD') + ' ' + $(this).attr('data');
    var getData = {directorate_id: $('#directorate_id').val(), speciality_id: $('#speciality_id').val(), date: date};
    $.get(APP_URL + '/getRequiremetByDateTime', getData, function (data) {
        var returnData = $.parseJSON(data);
        console.log(returnData);
        var staffTable = '';
        var requirementTable = '';
        staffTable = staffData(returnData.staff);
        if (returnData.requirement.length != 0) {
            requirementTable = requirementData(returnData.requirement);
        } else {
            var noData = 'No Data Found';
        }
        $('.fa-spin').hide();
        if (returnData.requirement.length != 0) {
            $('#requirement-table').html(requirementTable.table);
            var width = 0;
            for (k = 1; k <= 4; k++) {
                width = width + $('#requirement-table tr th:nth-child(' + k + ')').width();
            }

            $('#staff-table').css('margin-left', width);
            $('#staff-table').html(staffTable);
        } else {
            $('#no-data').html(noData);
        }
        var check = requirementTable.check;
        $('.fa-check').click(function () {
            $('#service_id').val($(this).attr('data-service'));
            $('#staff_id').val($(this).attr('data-staff'));
            $('#duration').val($(this).attr('data-duration'));
            $('#start').val($(this).attr('data-start'));
            $('#end').val($(this).attr('data-ending'));
            $('#day').val($(this).attr('data-day'));
            $('#endday').val($(this).attr('data-endday'));
            $('#assign_date').val(moment($('#date-header').val()).format('YYYY-MM-DD'));
            $('#info-modal').modal('show');

        });
        $('.assign-status').click(function () {
            $('#ending').val($(this).attr('data-ending'));
            $('#assign_id').val($(this).attr('data-assign'));
            $('#update-modal').modal('show');
        });
//        console.log(check);
        $.each(check, function (index, value) {
            $.each(value.checks, function (serviceId, checkValue) {
                if (serviceId != 0 && typeof checkValue !== 'undefined') {
                    var id = '.' + value.level + '-' + serviceId;
                    if ($(id)) {
                        $(id).addClass(checkValue.color);
                        $(id).css('border-color', checkValue.borderColor);
                    }
                }
            });
        });
    });
});