$(document).ready(function () {
    $('.red-trash,.yellow-trash,.orange-trash,.green-trash,.blue-trash').hide();

    var red_count = $('.red-form').length + 1, orange_count = $('.orange-form').length + 1, yellow_count = $('.yellow-form').length + 1, green_count = $('.green-form').length + 1, blue_count = $('.blue-form').length + 1;
//########################## SWITCH FUNCTIONS FOR CLONE AND VALUES #####################

    var cloneForm = function (id) {
        switch (id) {
            case 'red-add':
                generatingRow('.red-form', '.red-trash', 'red', red_count, '.red-box');
                red_count++;
                break;
            case 'orange-add':
                generatingRow('.orange-form', '.orange-trash', 'orange', orange_count, '.orange-box');
                orange_count++;
                break;
            case 'yellow-add':
                generatingRow('.yellow-form', '.yellow-trash', 'yellow', yellow_count, '.yellow-box');
                yellow_count++;
                break;
            case 'green-add':
                generatingRow('.green-form', '.green-trash', 'green', green_count, '.green-box');
                green_count++;
                break;
            case 'blue-add':
                generatingRow('.blue-form', '.blue-trash', 'blue', blue_count, '.blue-box');
                blue_count++;
                break;
        }

    };

    var updateValue = function (id, value, number, new_row) {
        number = number || null;
        switch (id) {
            case 'red_parameter':
                if (value == 'name')
                    replaceValueWithText('red_value', number, 'red[value][]');
                else {
                    if (new_row.find('.value').attr('type') == 'text')
                        relaceValueWithSelect(new_row, 'red');
                    else {
                        var values = getValueData(value);
                        var value_id = getId('#red_value', number);
                        if ($('#red_value_2-' + number).show()) {
                            $('#red_value_2-' + number).html(values);
                        }
                        $(value_id).html(values);
                    }
                }
                break;
            case 'orange_parameter':
                if (value == 'name')
                    replaceValueWithText('orange_value', number, 'orange[value][]');
                else {
                    if (new_row.find('.value').attr('type') == 'text')
                        relaceValueWithSelect(new_row, 'orange');
                    else {
                        var values = getValueData(value);
                        var value_id = getId('#orange_value', number);
                        $(value_id).html(values);
                    }

                }
                break;
            case 'yellow_parameter':
                if (value == 'name')
                    replaceValueWithText('#yellow_value', number, 'yellow[value][]');
                else {
                    if (new_row.find('.value').attr('type') == 'text')
                        relaceValueWithSelect(new_row, 'yellow');
                    else {
                        var values = getValueData(value);
                        var value_id = getId('#yellow_value', number);
                        $(value_id).html(values);
                    }
                }
                break;
            case 'green_parameter':
                if (value == 'name')
                    replaceValueWithText('green_value', number, 'green[value][]');
                else {
                    if (new_row.find('.value').attr('type') == 'text')
                        relaceValueWithSelect(new_row, 'green');
                    else {
                        var values = getValueData(value);
                        var value_id = getId('#green_value', number);
                        $(value_id).html(values);
                    }
                }
                break;
            case 'blue_parameter':
                if (value == 'name')
                    replaceValueWithText('blue_value', number, 'blue[value][]');
                else {
                    if (new_row.find('.value').attr('type') == 'text')
                        relaceValueWithSelect(new_row, 'blue');
                    else {
                        var values = getValueData(value);
                        var value_id = getId('#blue_value', number);
                        $(value_id).html(values);
                    }
                }
                break;
        }
    };
//#################################### AJAX CALLS ######################################

    var getValueData = function (value) {
        var values = '';
        jQuery.ajax({
            url: APP_URL + '/getValues/' + value + '/' + $('#directorate_id').val(),
            type: 'get',
            async: false,
            success: function (data) {
                console.log(data);
                $.each(data, function (index, value) {
                    values += "<option value='" + index + "'>" + value + "</option>";
                });
            }
        });
        return values;
    };

    var getOperatorData = function (value) {
        var values = '';
        jQuery.ajax({
            url: APP_URL + '/getOperators/' + value,
            type: 'get',
            async: false,
            success: function (data) {
                $.each(data, function (index, value) {
                    values += "<option value='" + index + "'>" + value + "</option>";
                });
            }
        });
        return values;
    };

    var deleteRequirement = function (id, row) {
        var values = '';
        jQuery.ajax({
            url: APP_URL + '/service/requirement/delete/' + id,
            type: 'get',
            success: function (data) {
                removeForm(row);
            }
        });
    };

//############################ BINDING AND CLONING METHODS ###################################    

    var removeForm = function ($element) {
        $element.remove();
    };

    var bindParameterChange = function (new_row) {
        $(new_row).find('.parameter').bind('change', function () {
            var id = $(this).attr('id');
            var string = id.split('-');
            id = string[0];
            var number = string[1];
            updateValue(id, $(this).val(), number, new_row);
            updateOperatorValues(new_row, $(this).val());
        });
    };

    var bindOperatorChange = function (new_row) {
        $(new_row).find('.operator').bind('change', function () {
            updateValueFields(new_row, $(this).val());
        });
    };

    var updateValueFields = function (new_row, value) {
        if (value == 'between') {
            $(new_row).find('.value-2').show();
            $(new_row).find('.value_2').removeAttr('disabled');
            $(new_row).find(".value-div").removeClass('col-md-4');
            $(new_row).find(".value-div").addClass('col-md-2');
            var values = getValueData($(new_row).find('.parameter').val());
            $(new_row).find('.value_2').html(values);
        } else {
            if ($(new_row).find(".value-div").hasClass('col-md-2')) {
                $(new_row).find(".value-div").removeClass('col-md-2');
                $(new_row).find(".value-div").addClass('col-md-4');
                $(new_row).find('.value-2').hide();
                $(new_row).find('.value-2').attr('disabled', 'disabled');
            }
        }
    };
    var updateOperatorValues = function (new_row, value) {
        var parameter = new_row.find('.parameter :selected').attr('value');
        var directorate_id = $('#directorate_id').val();
        var send = {directorate_id: directorate_id, name: parameter};
        var values = '';
        jQuery.ajaxSetup({async: false});
        var responseData;
        var url = APP_URL + '/service/get-operatorby-parameter';
        $.get(url, send, function (data) {
            responseData = data[0]['operator'];
        });

        var check = new_row.find('.operator').attr('id');
        $.each(responseData, function (index, value) {
                values += "<option value='" + index + "'>" + value + "</option>";
        });
         $("#" + check).empty();
            $("#" + check).empty().append(values);
    };

    var bindTrashRemove = function (new_row, class_name, trash) {
        $(new_row).find(trash).show();
        $(new_row).find('.fa-trash').bind('click', function () {
            removeForm($(this).parents(class_name));
        });
    };

    var bindingId = function (new_row, color, count) {
        $(new_row).find('.parameter').attr('id', color + '_parameter-' + count);
        $(new_row).find('.value').attr('id', color + '_value-' + count);
        $(new_row).find('.operator').attr('id', color + '_operator-' + count);
        $(new_row).find('.status').attr('id', color + '_status-' + count);
        $(new_row).find('.number').attr('id', color + '_number-' + count);
        if ($(new_row).find('.id'))
            $(new_row).find('.id').remove();
    };

    var generatingRow = function (form, trash, color, count, box) {
        var new_row = $(form).first().clone().appendTo(box);
        bindTrashRemove(new_row, form, trash);
        bindingId(new_row, color, count);
        bindParameterChange(new_row);
        if (new_row.find('.value').attr('type') == 'text')
            relaceValueWithSelect(new_row, color);
        bindOperatorChange(new_row);
        new_row.find('.parameter').val('select');

    };

    var replaceValueWithText = function (id, number, name) {
        if (number == null)
            $('#' + id).replaceWith('<input class="form-control value" name="' + name + '" type="text" id="' + id + '">');
        else
            $('#' + id + '-' + number).replaceWith('<input class="form-control value" name="' + name + '" type="text" id="' + id + '-' + number + '">');
    };

    var relaceValueWithSelect = function (new_row, color) {
        var value = new_row.find('.parameter').val();
        if (value == 'name')
            value = 'role';
        var id = new_row.find('.value').attr('id');
        var values = getValueData(value);
        $('#' + id).replaceWith('<select class="form-control value" id="' + id + '" name="' + color + '[value][]" >' + values + '</select>');
    };

    var getId = function (id, number) {
        if (number == null)
            var result = id;
        else
            var result = id + '-' + number;
        return result;
    };

//############################### ONCHANGE FUNCTION CALLS #####################################

    $('.add').click(function (event) {
        event.preventDefault();
        var id = $(this).attr('id');
        cloneForm(id);
    });

    $('.parameter').change(function () {
        var id = $(this).attr('id');
        var string = id.split('-');
        id = string[0];
        var number = string[1];
        var color = id.split('_');
        color = color[0];
        var new_row = $(this).parents('.' + color + '-form');
        updateValue(id, $(this).val(), number, new_row);
        updateOperatorValues(new_row, $(this).val());
    });

    $('.operator').change(function () {
//        alert('chagned');
        var id = $(this).attr('id').split('_');
        console.log(id[0]);
        var new_row = $(this).parents('.' + id[0] + '-form');
        updateValueFields(new_row, $(this).val());
    });

    $('.delete').click(function () {
        if ($(this).parents().hasClass('red-form'))
            deleteRequirement($(this).attr('id'), $(this).parents('.red-form'));
        if ($(this).parents().hasClass('orange-form'))
            deleteRequirement($(this).attr('id'), $(this).parents('.orange-form'));
        if ($(this).parents().hasClass('yellow-form'))
            deleteRequirement($(this).attr('id'), $(this).parents('.yellow-form'));
        if ($(this).parents().hasClass('green-form'))
            deleteRequirement($(this).attr('id'), $(this).parents('.green-form'));
        if ($(this).parents().hasClass('blue-form'))
            deleteRequirement($(this).attr('id'), $(this).parents('.blue-form'));
    });
});