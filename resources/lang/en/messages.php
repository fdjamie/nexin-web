<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'error' => 'Unknown Error Occured! Try again Or Contact With Admin',


    'enabled'=>'Successfully Enabled..',

    'disabled'=>'Successfully Disabled..',

    'passwordNotMatch'=>'Your  password does not matches with your account password. Please try again.',

    'codeLimitExceeded'=>'Sorry Code has been sent many times.Limit Exceeded.Please Contact Admin',

    'codeSent'=>'New Code has been Sent Kindly Check. ',

    'codeExpired'=>'Code has been expired try new code. ',


    'codeIncorrect'=>'Sorry you have entered Incorrect Code.Please Enter correct code',

    'phoneMissing'=>'You haven\'t added phone number yet. Please add valid phone number to activate sms code verification!',

    'keyGenerated'=>'Secret Key is generated, Please verify Code to Enable 2FA',

    'phoneNotFound'=>'Sorry Phone Number is missing in your account .please contact with admin'









];
