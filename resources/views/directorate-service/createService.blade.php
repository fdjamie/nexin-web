@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-life-bouy"></i>Add Services
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-service'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-service') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['DirectorateServiceController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,null, [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('check_all', 'Specialties', ['class' => 'awesome']); !!} <span id="spt_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            <div class="clearfix"></div>
                            {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!} 
                            {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                        </div>
                        <div id="all-check-boxes">
                            @foreach($specialities as $speciality)
                            <div class="form-group col-md-3 main-check-box">
                                {!! Form::checkbox('specialities[]', $speciality->id ,true,['id'=>$speciality->id,'class'=>'check-boxes']); !!}
                                {!! Form::label($speciality->id, $speciality->name, ['class' => 'awesome']); !!}
                            </div>
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} {!! Form::checkbox('category_check', 1 ,true,['id'=>'category_check']); !!} <span id="ct_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('category_id', $categories,null,['class' => 'form-control','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!} {!! Form::checkbox('sub_category_check', 1 ,true,['id'=>'sub_category_check']); !!} <span id="sb_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('sub_category_id', $sub_categories,null,['class' => 'form-control','id'=>'sub_category_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('contactable', 'Contactable ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('contactable', 1,1); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('min_standards', 'Min Standards ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('min_standards', 1,1); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-life-bouy"></i>Upload Services
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['DirectorateServiceController@import']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-life-bouy"></i>Upload Master Data
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['DirectorateServiceController@importReferenceData']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/directorate-service.js')}}" type="text/javascript"></script>
@endsection