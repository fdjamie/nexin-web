@extends('admin.admin')

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-life-bouy"></i>{{$service->name}}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-service'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-service') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['DirectorateServiceController@update',$service->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $service->name,['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$service->directorate_id, [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('check_all', 'Specialties', ['class' => 'awesome']); !!} <span id="spt_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            <div class="clearfix"></div>
                            {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!} 
                            {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                        </div>
                        <div id="all-check-boxes">
                            @foreach($specialities as $speciality)
                            <div class="form-group col-md-3 main-check-box">
                                {!! Form::checkbox('specialities[]', $speciality->id ,((in_array($speciality->id,$service_specialties)?true:false)),['id'=>$speciality->id,'class'=>'check-boxes']); !!}
                                {!! Form::label($speciality->id, $speciality->name, ['class' => 'awesome']); !!}
                            </div>
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} {!! Form::checkbox('category_check', 1 ,$service->category_id>0?true:null,['id'=>'category_check']); !!} <span id="ct_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('category_id', $categories,$service->category_id,['class' => 'form-control','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!} {!! Form::checkbox('sub_category_check', 1 ,$service->sub_category_id>0?true:null,['id'=>'sub_category_check']); !!} <span id="sb_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('sub_category_id', $sub_categories,$service->sub_category_id,['class' => 'form-control','id'=>'sub_category_id']); !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('contactable', 'Contactable ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('contactable', 1,$service->contactable); !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('min_standards', 'Min Standards ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('min_standards', 1,$service->min_standards); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/directorate-service.js')}}" type="text/javascript"></script>
@endsection