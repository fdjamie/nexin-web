@section('modal')
<div class="modal fade" id="add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Event</h4>
                {!! Form::open(['method' => 'post', 'id'=>'addEvent']) !!}
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    {!! Form::hidden('service_id', $service->id,['class' => 'form-control','id'=>'service_id']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                    {!! Form::text('start_time', '',['class' => 'form-control','id'=>'start_time']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                    {!! Form::text('end_time', '',['class' => 'form-control','id'=>'end_time']); !!}
                    {!! Form::hidden('event_start_date', '',['class' => 'form-control','id'=>'event_start_date']); !!}
                    {!! Form::hidden('event_end_date', '',['class' => 'form-control','id'=>'event_end_date']); !!}
                    {!! Form::hidden('title', $service->name,['id'=>'title']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Event</h4>
                {!! Form::open(['method' => 'post', 'id'=>'updateEvent']) !!}
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    {!! Form::hidden('service_id', $service->id,['class' => 'form-control','id'=>'edit_service_id']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                    {!! Form::text('edit_start_time', '',['class' => 'form-control','id'=>'edit_start_time']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                    {!! Form::text('edit_end_time', '',['class' => 'form-control','id'=>'edit_end_time']); !!}
                    {!! Form::hidden('event_start_date_edit', '',['class' => 'form-control','id'=>'event_start_date_edit']); !!}
                    {!! Form::hidden('event_end_date_edit', '',['class' => 'form-control','id'=>'event_end_date_edit']); !!}
                    {!! Form::hidden('title', $service->name,['id'=>'edit_title']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection