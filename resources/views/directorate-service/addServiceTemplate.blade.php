@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" integrity="sha256-lBtf6tZ+SwE/sNMR7JFtCyD44snM3H2FrkB/W400cJA=" crossorigin="anonymous" />
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.min.js" integrity="sha256-LPgEyZbedErJpp8m+3uasZXzUlSl9yEY4MMCEN9ialU=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Services Calender View</h1>
            </div>

            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!--        <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="/service">Service</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-3">
                            <i class="fa fa-life-bouy"></i>{{$service->name}}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::text('directorate',$service->directorate->name, ['class' => 'form-control','id'=>'directorate','disabled'=>'true']); !!}
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            {!! Form::label('cycle_length', 'Cycle Length', ['class' => 'awesome']); !!}
                            {!! Form::number('cycle_length',1, ['class' => 'form-control','id'=>'cycle_length','min'=>1,'max'=>4]); !!}
                        </div>
                        <!--<div class="col-md-2">-->
                        {{--!! Form::label('week_view', 'Week View', ['class' => 'awesome']); !!--}}
                        {{--!! Form::number('week_view',1, ['class' => 'form-control','id'=>'week_view','min'=>1,'max'=>1]); !!--}}
                        <!--</div>-->
                        <div class="col-md-3">
                            {!! Form::label('', '', ['class' => 'awesome']); !!}
                            {!! Form::button('Add Events !', ['class'=> 'btn green form-control','id'=>'addAllEvents']); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('starting_week', 'Starting Week', ['class' => 'awesome']); !!}
                            {!! Form::number('starting_week',1, ['class' => 'form-control','id'=>'starting_week','min'=>1,'max'=>1]); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('service_start', 'Start Date', ['class' => 'awesome']); !!}
                            {!! Form::text('service_start',date('Y-m-d'), ['class' => 'form-control datepicker','id'=>'service_start']); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('service_end', 'End Date', ['class' => 'awesome']); !!}
                            {!! Form::text('service_end',date('Y-m-d'), ['class' => 'form-control datepicker','id'=>'service_end']); !!}
                            {!! Form::hidden('template_date',date('Y-m-d'), ['id'=>'template_date']); !!}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class='alert alert-success alert-dismissible' role='alert' id="no-time-selected" style="display: none;">
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            No Timings selected for {{$service->name}} , Please select timings again. 
                        </div>
                        <div id='calendar-service'>
                            {{--!! $calendar->calendar() !!--}}
                            {{--!! $calendar->script() !!--}}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@include('service.addEditModals')
@section('extra-js')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="{{asset('js/services-template.js')}}" type="text/javascript"></script>
<script>
var serviceId = $("#service_id").val();
calenderLoad([]);
id = 1;
$('#cycle_length').trigger('click');
$('#addAllEvents').click(function () {
    $(this).attr('disabled', true);
    var content = {};
    if ($('#service_start').val() != '')
        content['start_date'] = $('#service_start').val();
    else
        content['start_date'] = '0000-00-00';
    if ($('#service_end').val() != '') {
        content['end_date'] = $('#service_end').val();
    } else {
        content['end_date'] = '0000-00-00';
    }
    content['cycle_length'] = $('#cycle_length').val();
    content['starting_week'] = $('#starting_week').val();
    content['events'] = new Array();
    var eventId = 0;
    var eventsData = $('#calendar-service').fullCalendar('clientEvents');
    var intital_week = $('#calendar-service').fullCalendar('getView').start.week();
    $.each(eventsData, function (index) {
        var locations = eventsData[index].location_id;
        var duration = (eventsData[index].end._d.getTime() - eventsData[index].start._d.getTime()) / (1000 * 60);       //  Calculate Time Difference in microseconds/(1000*60) to get seconds
        var week_no = eventsData[index].start.week() - intital_week + 1;
        var day = eventsData[index].start._d.getDay();
        content['events'].push({
            id: eventId++,
            week_no: week_no,
            day: day,
            start: eventsData[index].start.format('HH:mm'),
            duration: duration,
            site_id: eventsData[index].site_id,
            location_id: locations,
        });
    });
    jQuery.ajax({
        type: "POST",
        url: APP_URL+'/directorate-service/template',
        data: {service_id: serviceId, template: content},
        success: function (data) {
            $('#addAllEvents').removeAttr('disabled');
            window.location.href = '/directorate-services';
        }
    });
});
</script>
@endsection