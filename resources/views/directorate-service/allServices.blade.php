@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy"></i> Service 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-service'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-service') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-service'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-service') }}
                    </div>
                    @endif
                    @if (Illuminate\Support\Facades\Session::has('info-service'))
                    <div class='alert alert-info alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                        @foreach(session('info-service') as $info)
                        <li class='list-group-item-info'>{{ $info }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        @if(__authorize(config('module.general-services'),'add'))
                                        <a href="{{url('/directorate-service/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>

                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                        <a href="{{url('/directorate-service/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Services <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($directorates)==0)
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th colspan="5"> No Directorate added in the system.</th>
                                </tr>
                            </thead>
                        </table>
                        @endif
                        <ul class="nav nav-tabs">
                            @foreach($directorates as $directorate_node)
                            @if($directorate_node->id == $directorates[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$directorate_node->id}}" data-toggle="tab">{{ $directorate_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($directorates as $directorate_node)
                            @if($directorate_node->id == $directorates[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$directorate_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$directorate_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                               <th  class="text-center">Name</th>
                                                <th class="text-center">Contactable</th>
                                                <th class="text-center">Min Standards</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($directorate_node->services as $service)
                                            <tr>
                                                <td  class="text-center" width="25%">{{$service->name}}</td>
                                                <td class="text-center">
                                                    @if($service->contactable == true)
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    @else
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($service->min_standards == true)
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    @else
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{url('/directorate-service/'.$service->id.'/requirements')}}" ><i class="fa fa-list" aria-hidden="true"></i></a> 
                                                    <a href="{{url('/directorate-service/template/'.$service->id)}}" ><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                                    @if(__authorize(config('module.general-services'),'edit'))
                                                    <a href="{{url('/directorate-service/edit/'.$service->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                        @endif
                                                        @if(__authorize(config('module.general-services'),'delete'))
                                                    <a data-toggle="modal" href="#small" id="{{$service->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<!--<script src="{{url('/js/checkbox_update.js')}}"></script>-->
<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/directorate-service/delete")}}' +'/'+ $(this).attr('id'));
        });
    });

</script>
@endsection