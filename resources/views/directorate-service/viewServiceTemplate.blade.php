@extends('admin.admin')

@section('extra-css')
<!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>-->
<link rel="stylesheet" href="{{url('/css/full_calendar.css')}}"/>
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Services Calender View</h1>
            </div>

            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!--        <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="/service">Services</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-3">
                            <i class="fa fa-life-bouy"></i>{{$service->name}}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::text('directorate',$service->directorate->name, ['class' => 'form-control','id'=>'directorate','disabled'=>'true']); !!}
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            {!! Form::label('cycle_length', 'Cycle Length', ['class' => 'awesome']); !!}
                            {!! Form::number('cycle_length', $cycle_length, ['class' => 'form-control','id'=>'cycle_length','min'=>1,'max'=>4,'disabled'=>'true']); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('', '', ['class' => 'awesome']); !!}
                            {!! Form::hidden('title',$service->name, ['id'=>'title']); !!}
                            <a href="edit/{{$service->id}}">{!! Form::button('Add / Update Events !', ['class'=> 'btn green form-control','id'=>'updateAllEvents']); !!}</a>
                            {{-- <a href="directorate-service/template/edit/{{$service->id}}">{!! Form::button('Add / Update Events !', ['class'=> 'btn green form-control','id'=>'updateAllEvents']); !!}</a> --}}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!}
                            {!! Form::text('start_date',$start_date, ['class' => 'form-control','id'=>'start_date','disabled'=>'true']); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!}
                            {!! Form::text('end_date',$end_date, ['class' => 'form-control','id'=>'end_date','disabled'=>'true']); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('starting_week', 'Starting Week', ['class' => 'awesome']); !!}
                            {!! Form::number('starting_week', $starting_week, ['class' => 'form-control','id'=>'starting_week','min'=>1,'max'=>4,'disabled'=>'true']); !!}
                        </div>

                    </div>
                    @if (Illuminate\Support\Facades\Session::has('service-template-create'))
                    <div class='alert alert-info alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('service-template-create') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div id='calendar-service'>
                            {!! $calendar->calendar() !!}
                            {!! $calendar->script() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('extra-js')

<!--<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>-->
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>-->

<script type="text/javascript" src="{{url('/js/update-calendar-week.js')}}"></script>
<script src="{{url('/js/full_calendar.js')}}"></script>
<script>
$(document).ready(function () {
    var updateCalender = function (date, view) {
        jQuery.ajax({
            url: APP_URL+'/set-calender-date',
            type: 'post',
            data: {date: date, view: view},
            success: function (data) {
                location.reload();
            }
        });
    };
    var id = $('#calendar-service').find('.fc').attr('id');
    $('.fc-next-button').bind('click', function () {
        var end = $('#' + id).fullCalendar('getView').end._d;
        end = moment(end).format('YYYY-MM-DD');
        var view = $('#' + id).fullCalendar('getView').name;
        updateCalender(end, view);
    });

    $('.fc-prev-button').bind('click', function () {
        var start = $('#' + id).fullCalendar('getView').start._d;
        var view = $('#' + id).fullCalendar('getView').name;
        if (view == 'agendaWeek')
            start = moment(start).subtract(1, 'weeks').format('YYYY-MM-DD');
        else if (view == '2Weeks')
            start = moment(start).subtract(2, 'weeks').format('YYYY-MM-DD');
        else if (view == '3Weeks')
            start = moment(start).subtract(3, 'weeks').format('YYYY-MM-DD');
        else if (view == '4Weeks')
            start = moment(start).subtract(4, 'weeks').format('YYYY-MM-DD');
        else if (view == '5Weeks')
            start = moment(start).subtract(5, 'weeks').format('YYYY-MM-DD');
        updateCalender(start, view);
    });
});
</script>

@endsection