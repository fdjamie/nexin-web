@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Directorate Service</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-life-bouy"></i>Directorate Services
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <div class="clearfix overflow-scroll">
                            <table class="table table-responsive  table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th> Name </th>
                                        <th> Directorate </th>
                                        <th> Category </th>
                                        <th> Result </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($view_data as $service)
                                    @if(isset($service['directorate_error']) || 
                                        isset($service['service_error']) || 
                                        isset($service['category_error']) || 
                                        isset($service['subcategory_error']) || 
                                        isset($service['standards_error']) || 
                                        isset($service['contactable_error']) || 
                                        isset($service['cycle_length_error']) || 
                                        isset($service['starting_week_error']))
                                    <tr class="bg-danger">
                                        @else
                                    <tr class="odd gradeX">
                                        @endif    
                                        <td width='10%'>{{ $service['name'] }}</td>
                                        <td width='10%'>{{ $service['directorate'] }}</td>
                                        <td width='10%'>{{ $service['category'] }}</td>
                                        <td class="table-danger" width='70%'>
                                            <ul>
                                                @if(isset($service['directorate_error']))<li class="list-group-item-danger"> {{$service['directorate_error']}}</li>@endif 
                                                @if(isset($service['speciality_error']))
                                                @foreach($service['speciality_error'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}</li>
                                                @endforeach
                                                @endif
                                                @if(isset($service['service_error']))<li class="list-group-item-danger"> {{$service['service_error']}}</li>@endif 
                                                @if(isset($service['category_error']))<li class="list-group-item-danger"> {{$service['category_error']}}</li>@endif 
                                                @if(isset($service['subcategory_error']))<li class="list-group-item-danger"> {{$service['subcategory_error']}}</li>@endif 
                                                @if(isset($service['standards_error']))<li class="list-group-item-danger"> {{$service['standards_error']}}</li>@endif 
                                                @if(isset($service['contactable_error']))<li class="list-group-item-danger"> {{$service['contactable_error']}}</li>@endif 
                                                @if(isset($service['operations_error']))<li class="list-group-item-danger"> {{$service['operations_error']}}</li>@endif 
                                                @if(isset($service['cycle_length_error']))<li class="list-group-item-danger"> {{$service['cycle_length_error']}}</li>@endif 
                                                @if(isset($service['starting_week_error']))<li class="list-group-item-danger"> {{$service['starting_week_error']}}</li>@endif 
                                                @if(isset($service['events_error']['week_no']))
                                                @foreach($service['events_error']['week_no'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}</li>
                                                @endforeach
                                                @endif
                                                @if(isset($service['events_error']['day']))
                                                @foreach($service['events_error']['day'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}</li>
                                                @endforeach
                                                @endif
                                                @if(isset($service['events_error']['duration']))
                                                @foreach($service['events_error']['duration'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}</li>
                                                @endforeach
                                                @endif 
                                                @if(isset($service['events_error']['site']))
                                                @foreach($service['events_error']['site'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}</li>
                                                @endforeach
                                                @endif 
                                                @if(isset($service['events_error']['location']))
                                                @foreach($service['events_error']['location'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}</li>
                                                @endforeach
                                                @endif 
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-toolbar">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/directorate-service/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        @if(isset($flag))
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['DirectorateServiceController@uploadReferenceData'] ]) !!}
                                        @else
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['DirectorateServiceController@upload'] ]) !!}
                                        @endif
                                        {!! Form::hidden('file_name',$file_name); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green','disabled'=>(isset($master_error) && !$master_error ?true:false)]); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection