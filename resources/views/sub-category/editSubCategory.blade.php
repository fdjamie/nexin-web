@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sitemap" aria-hidden="true"></i>{{$sub_category->name}} 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-category'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-category') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['SubCategoryController@update', $sub_category->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $sub_category->name, ['class' => 'form-control']); !!}
                        </div>
                        @if($sub_category->parent_id == 0)
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates,$sub_category->category->directorate->id,['class' => 'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} <span id="ct_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('category_id', $categories,$sub_category->category->id,['class' => 'form-control']); !!}
                        </div>
                        @else
                        {!! Form::hidden('category_id',$sub_category->category->id); !!}
                        {!! Form::hidden('parent_id',$sub_category->parent_id); !!}
                        @endif
                        <div class="form-group">
                            {!! Form::label('check_all', 'Specialties', ['class' => 'awesome']); !!} <span id="spt_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            <div class="clearfix"></div>
                            {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!} 
                            {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                        </div>
                        <div id="all-check-boxes">
                            @foreach($specialities as $speciality)
                            <div class="form-group col-md-3 main-check-box">
                                {!! Form::checkbox('specialities[]', $speciality->id ,((in_array($speciality->id,$subcategory_specialties)?true:false)),['id'=>$speciality->id,'class'=>'check-boxes']); !!}
                                {!! Form::label($speciality->id, $speciality->name, ['class' => 'awesome']); !!}
                            </div>
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/subcategory.js')}}" type="text/javascript"></script>
<script src="{{url('/js/checkbox.js')}}" type="text/javascript"></script>
@endsection
