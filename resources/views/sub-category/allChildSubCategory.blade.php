@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sitemap" aria-hidden="true"></i>{{implode('/ ',$parents)}} 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-category'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-category') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-category'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-category') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/sub-category')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" ><i class="fa fa-arrow-left"></i> Sub Category </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id='sample_3'>
                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th> Name </th>
                                    <th> Sub Categories </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subcategories as $sub_category)
                                <tr class="odd gradeX">
                                    <td width='25%'>{{$sub_category->id}}</td>
                                    <td width='25%'>{{$sub_category->name}}</td>
                                    <td width='25%'>{{(count($sub_category->child_recursive)?'Y':'N')}}</td>
                                    
                                    <td class="text-center">
                                        <a href="{{url('/sub-category/'.$sub_category->id.'/sub-category')}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a data-toggle="modal" href="#add-child" data="{{$sub_category->id}}" class="add-sub" ><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <a href="{{url('/sub-category/edit/'.$sub_category->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a data-toggle="modal" href="#small" id="{{$sub_category->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body"> You want to delete this record? </div>
            <div class="modal-footer">
                <a href="" id='delete-button'><button type="submit" class="btn btn-danger" >Delete</button></a>
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-sm" id="add-child" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            {!! Form::open(['method' => 'post', 'action' => ['SubCategoryController@store']]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add new child subcategory.</h4>
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                    {!! Form::text('name', old('name'),['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                </div>
                {!! Form::hidden('parent_id', 0,['id' => 'parent_id']); !!}
            </div>
            <div class="modal-footer">
                {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/sub-category/delete")}}' +'/'+ $(this).attr('id'));
    });
    $('.add-sub').click(function () {
        $('#parent_id').val($(this).attr('data'));
    });
    $('#directorate_id').change(function () {
        $('form').submit();
    });
</script>
@endsection