@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@php 
$level = 1;
function childCount($nodes, $count){
    if(isset($nodes['child_recursive'])){
        $count ++;
        childCount($nodes['child_recursive'],$count);
    }
    return $count;
}
@endphp
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-sitemap" aria-hidden="true"></i>Sub Categories 
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['SubCategoryController@index']]) !!}
                        <div class="form-group col-md-4 col-md-offset-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control col-md-3','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-category'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-category') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-category'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-category') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/sub-category/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($categories as $category_node)
                            @if($category_node->id == $categories[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$category_node->id}}" data-toggle="tab">{{ $category_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @if(!count($categories))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                <tr>
                                    <th width='100%' class="text-center"><b> No category for this directorate.</b></td>
                                </tr>    
                            </table>
                            @endif
                            @foreach($categories as $category_node)
                            @if($category_node->id == $categories[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$category_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$category_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Id</th>
                                                <th> Name </th>
                                                <th> Sub Categories </th>
                                                <th> Number </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($category_node->sub_category))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($category_node->sub_category as $sub_category)
                                            <tr class="odd gradeX">
                                                <td width='25%'>{{$sub_category->id}}</td>
                                                <td>{{$sub_category->name}}</td>
                                                <td>{{(count($sub_category->child_recursive)?'Y':'N')}}</td>
                                                <td>{{count($sub_category->child_recursive) }}</td>
                                                <td class="text-center">
                                                    <a href="{{url('/sub-category/'.$sub_category->id.'/sub-category')}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    <a data-toggle="modal" href="#add-child" data="{{$sub_category->id}}" class="add-sub" ><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                    <a href="{{url('/sub-category/edit/'.$sub_category->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a data-toggle="modal" href="#small" id="{{$sub_category->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
<div class="modal fade bs-modal-sm" id="add-child" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            {!! Form::open(['method' => 'post', 'action' => ['SubCategoryController@store']]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add new child subcategory.</h4>
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                    {!! Form::text('name', old('name'),['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                </div>
                    {!! Form::hidden('parent_id', 0,['id' => 'parent_id']); !!}
            </div>
            <div class="modal-footer">
                {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/sub-category/delete")}}' +'/'+ $(this).attr('id'));
    });
    $('.add-sub').click(function () {
        $('#parent_id').val($(this).attr('data'));
    });
    $('#directorate_id').change(function () {
        $('form').submit();
    });
</script>
@endsection