<div class="col-md-12">
    <h2 class="green col-md-3"><i class="fa fa-star" aria-hidden="true"></i> Ideal</h2>
    <div class="form-group col-md-2">
        <br>
        <div class="btn-group">
            <button id="green-add" class="btn sbold green-background white-text add" > <i class="fa fa-plus"></i></button>
        </div>
        <a  href="{!!  route('condition',['condition'=>'green', 'service_id'=>$id,'directorate_id'=>$directorate_id]) !!}" id="" class="btn sbold green-background white-text "  ><i class="fa fa-link" aria-hidden="true"></i></a>
    </div>
</div>
@if(!count($green_requirements))
<div class="green-form clearfix">
    <div class="form-group col-md-1">
        {!! Form::text('green[condition][]' , '',[ 'class'=>'form-control coniditon','id'=>'green_condition-1' ,'readonly'=>'']) !!}
    </div>
    <div class="form-group col-md-1">
        {!! Form::select('green[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'green_parameter-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('green[operator][]', $operators ,null, [ 'class'=>'form-control operator','id'=>'green_operator-1']); !!}
    </div>
    <div class="form-group col-md-4 clearfix">
        {!! Form::select('green[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'green_value-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix value-2">
        {!! Form::select('green[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'green_value_2-1','disabled'=>'disabled']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('green[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'green_status-1']); !!}
    </div>
    <div class="form-group col-md-1 clearfix">
        {!! Form::number('green[number][]', 1, [ 'class'=>'form-control number','id'=>'green_number-1','min'=> 1]); !!}
    </div>
    <div class="form-group col-md-2 clearfix green-trash">
        <a class="fa fa-trash" ></a>
    </div>
</div>    
@else

@foreach($green_requirements as $requirement)
    <div class="green-form clearfix">
        {!! Form::hidden('green[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'green_id-'.$green_count]); !!}
        <div class="form-group col-md-1">
            {!! Form::text('green[condition][]' ,($requirement->condition == '' || is_null($requirement->condition)?'':$requirement->id),[ 'class'=>'form-control condition','id'=>'green_condition-1' ,'readonly'=>'']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::select('green[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'green_parameter-'.$green_count]); !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('green[operator][]', $requirement->operators ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'green_operator-'.$green_count]); !!}
        </div>
        <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
            @if($requirement->parameter == 'name')
            {!! Form::text('green[value][]',$requirement->value, [ 'class'=>'form-control value','id'=>'green_value-'.$green_count]); !!}
            @else
            {!! Form::select('green[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'green_value-'.$green_count]); !!}
            @endif
        </div>
        @if($requirement->operator == 'between')
        <div class="form-group col-md-2 clearfix value-2">
            {!! Form::text('green[value_2][]',$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'green_value_2-'.$green_count]); !!}
        </div>
        @endif
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('green[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'green_status-'.$green_count]); !!}
        </div>
        <div class="form-group col-md-1 clearfix">
            {!! Form::number('green[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'green_number-'.$green_count,'min'=> 1]); !!}
        </div>
        @if(!is_null($requirement->condition) || !empty($requirement->condition))
        <div class="form-group  col-md-1 clearfix ">
           <a class="fa fa-external-link show-condition"  title="Show Condition" style="color:green" id="" data-id="{!! $requirement->condition  !!}"> </a>
        </div>
        @else
        <div class="form-group col-md-1 clearfix {{($green_count==1?'green-trash':'')}}">
            <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
        </div>
        @endif
    </div>
@php $green_count++; @endphp 
@endforeach
@endif