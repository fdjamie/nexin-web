<div class="col-md-12">
    <h2 class="blue col-md-3"><i class="fa fa-exclamation" aria-hidden="true"> </i> OverStaffed</h2>
    <div class="form-group col-md-2">
        <br>
        <div class="btn-group">
            <button id="blue-add" class="btn sbold blue-background white-text add" > <i class="fa fa-plus"></i></button>
        </div>
        <a  href="{!!  route('condition',['condition'=>'blue', 'service_id'=>$id,'directorate_id'=>$directorate_id]) !!}" id="" class="btn sbold blue-background white-text "  ><i class="fa fa-link" aria-hidden="true"></i></a>
    </div>
</div>
@if(!count($blue_requirements))
<div class="blue-form clearfix">
    <div class="form-group col-md-1 ">
        {!! Form::text('blue[condition][]' , '',[ 'class'=>'form-control coniditon','id'=>'blue_condition-1' , 'readonly'=>'']) !!}
    </div>
    <div class="form-group col-md-1 ">
        {!! Form::select('blue[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'blue_parameter-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('blue[operator][]', $operators ,null, [ 'class'=>'form-control operator','id'=>'blue_operator-1']); !!}
    </div>
    <div class="form-group col-md-4 clearfix">
        {!! Form::select('blue[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'blue_value-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix value-2">
        {!! Form::select('blue[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'blue_value_2-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('blue[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'blue_status-1']); !!}
    </div>
    <div class="form-group col-md-1 clearfix">
        {!! Form::number('blue[number][]', 1, [ 'class'=>'form-control number','id'=>'blue_number-1','min'=> 1]); !!}
    </div>
    <div class="form-group col-md-2 clearfix blue-trash">
        <a class="fa fa-trash" ></a>
    </div>
</div>
@else
@foreach($blue_requirements as $requirement)
    <div class="blue-form clearfix">
        {!! Form::hidden('blue[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'blue_id-'.$blue_count]); !!}
        <div class="form-group col-md-1">
            {!! Form::text('blue[condition][]' ,($requirement->condition == '' || is_null($requirement->condition)?'':$requirement->id),[ 'class'=>'form-control condition','id'=>'blue_condition-1' ,'readonly'=>'']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::select('blue[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'blue_parameter-'.$blue_count]); !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('blue[operator][]', $requirement->operators ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'blue_operator-'.$blue_count]); !!}
        </div>
        <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
            @if($requirement->parameter == 'name')
            {!! Form::text('blue[value][]',$requirement->value, [ 'class'=>'form-control value','id'=>'blue_value-'.$blue_count]); !!}
            @else
            {!! Form::select('blue[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'blue_value-'.$blue_count]); !!}
            @endif
        </div>
        @if($requirement->operator == 'between')
        <div class="form-group col-md-2 clearfix value-2">
            {!! Form::text('blue[value_2][]',$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'blue_value_2-'.$blue_count]); !!}
        </div>
        @endif
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('blue[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'blue_status-'.$blue_count]); !!}
        </div>
        <div class="form-group col-md-1 clearfix">
            {!! Form::number('blue[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'blue_number-'.$blue_count,'min'=> 1]); !!}
        </div>
        @if(!is_null($requirement->condition) || !empty($requirement->condition))
        <div class="form-group  col-md-1 clearfix ">
           <a class="fa fa-external-link show-condition"  title="Show Condition" style="color:blue" id="" data-id="{!! $requirement->condition  !!}"> </a>
        </div>
        @else
        <div class="form-group col-md-1 clearfix {{($blue_count==1?'blue-trash':'')}}">
            <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
        </div>
        @endif
    </div>
@php $blue_count++; @endphp
@endforeach
@endif