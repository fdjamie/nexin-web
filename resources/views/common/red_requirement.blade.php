<h2 class="red col-md-3"><i class="fa fa-exclamation " aria-hidden="true"></i> Unacceptable</h2>
<div class="form-group col-md-2">
    <br>
    <div class="btn-group">
        <button id="red-add" class="btn sbold red-background white-text add" > <i class="fa fa-plus"></i></button>
    </div>
    <a  href="{!!  route('condition',['condition'=>'red', 'service_id'=>$id,'directorate_id'=>$directorate_id]) !!}" id="" class="btn sbold red-background white-text "  ><i class="fa fa-link" aria-hidden="true"></i></a>
</div>
<div class="clearfix"></div>
{!! Form::hidden('service_id', $id ); !!}
{!! Form::hidden('directorate_id', $directorate_id,['id'=>'directorate_id'] ); !!}
@if(!count($red_requirements))
<div class="red-form clearfix">
    <div class="form-group col-md-1">
        {!! Form::text('red[condition][]' , '',[ 'class'=>'form-control operator','id'=>'red_operator-1' ,'readonly'=>'']) !!}
    </div>
    <div class="form-group col-md-1">
        {!! Form::select('red[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'red_parameter-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('red[operator][]', $operators ,null, [ 'class'=>'form-control operator','id'=>'red_operator-1']); !!}
    </div>
    <div class="form-group col-md-4 clearfix value-div">
        {!! Form::select('red[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'red_value-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix value-2">
        {!! Form::select('red[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'red_value_2-1','disabled'=>'disabled']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('red[status][]', $status,null, [ 'class'=>'form-control status','id'=>'red_status-1']); !!}
    </div>
    <div class="form-group col-md-1 clearfix">
        {!! Form::number('red[number][]', 1, [ 'class'=>'form-control number','id'=>'red_number-1','min'=> 1]); !!}
    </div>
    <!--                            <div class="form-group col-md-1 clearfix ">
                                    {!! Form::select('red[condition][]' ,[''=>'select condition','and'=>'AND','or'=>'OR'], '',[ 'class'=>'form-control operator','id'=>'red_operator-1']) !!}
                                </div>-->
    <div class="form-group col-md-2 clearfix red-trash">
        <a class="fa fa-trash" ></a>
    </div>
</div>
@else
@php
$conditionIds =[];
@endphp
@foreach($red_requirements as $requirement)
    <div class="red-form clearfix">
        @php
        $conditionIds[$requirement->id]= $requirement->id;
        @endphp
        {!! Form::hidden('red[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'red_id-'.$red_count]); !!}
        <div class="form-group col-md-1">
            {!! Form::text('red[condition][]' ,($requirement->condition == '' || is_null($requirement->condition)?'':$requirement->id),[ 'class'=>'form-control condition','id'=>'red_operato' ,'readonly'=>'']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::select('red[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'red_parameter-'.$red_count]); !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('red[operator][]', $requirement->operators ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'red_operator-'.$red_count]); !!}
        </div>
        <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix value-div">
            @if($requirement->parameter == 'name')
            {!! Form::text('red[value][]' ,$requirement->value, [ 'class'=>'form-control value','id'=>'red_value-'.$red_count]); !!}
            @else
            {!! Form::select('red[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'red_value-'.$red_count]); !!}
            @endif
        </div>
        <div class="form-group col-md-2 clearfix value-2" style="{{((isset($requirement->value_2) && $requirement->value_2 !='')?'display:block;':'display:none;')}}" >
            {!! Form::select('red[value_2][]', ($requirement->operator=='between'?$requirement->values:[]) ,$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'red_value_2-'.$red_count]) !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('red[status][]', $status,$requirement->status, [ 'class'=>'form-control status','id'=>'red_status-'.$red_count]); !!}
        </div>
        <div class="form-group col-md-1 clearfix">
            {!! Form::number('red[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'red_number-'.$red_count,'min'=> 1]); !!}
        </div>
        @if(!is_null($requirement->condition) || !empty($requirement->condition))
       <div class="form-group  col-md-1 clearfix">
           <a class="fa fa-external-link show-condition" title="Show Condition" style="color:red" id="" data-id="{!! $requirement->condition  !!}"> </a>
        </div>
        @else
        <div class="form-group col-md-1 clearfix {{($red_count==1?'red-trash':'')}}">
            <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
        </div>
        @endif
    </div>
@php $red_count++; @endphp
@endforeach
@endif