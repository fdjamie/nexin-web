<div class="col-md-12">
    <h2 class="yellow col-md-3"><i class="fa fa-stop yellow-flip" aria-hidden="true"> </i> Adequate</h2>
    <div class="form-group col-md-2">
        <br>
        <div class="btn-group">
            <button id="yellow-add" class="btn sbold yellow-background white-text add" > <i class="fa fa-plus"></i></button>
        </div>
        <a  href="{!!  route('condition',['condition'=>'yellow', 'service_id'=>$id,'directorate_id'=>$directorate_id]) !!}" id="" class="btn sbold yellow-background white-text "  ><i class="fa fa-link" aria-hidden="true"></i></a>
    </div>
</div>
@if(!count($yellow_requirements))
<div class="yellow-form clearfix">
    <div class="form-group col-md-1">
        {!! Form::text('yellow[condition][]' , '',[ 'class'=>'form-control condition','id'=>'yellow_condition-1','readonly'=>'']) !!}
    </div>
    <div class="form-group col-md-1">
        {!! Form::select('yellow[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'yellow_parameter-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('yellow[operator][]',  $operators ,null, [ 'class'=>'form-control operator','id'=>'yellow_operator-1']); !!}
    </div>
    <div class="form-group col-md-4 clearfix">
        {!! Form::select('yellow[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'yellow_value-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix value-2">
        {!! Form::select('yellow[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'yellow_value_2-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('yellow[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'yellow_status-1']); !!}
    </div>
    <div class="form-group col-md-1 clearfix">
        {!! Form::number('yellow[number][]', 1, [ 'class'=>'form-control number','id'=>'yellow_number-1','min'=> 1]); !!}
    </div>
    <div class="form-group col-md-1 clearfix yellow-trash">
        <a class="fa fa-trash" ></a>
    </div>

</div>
@else
@foreach($yellow_requirements as $requirement)
    <div class="yellow-form clearfix">
        {!! Form::hidden('yellow[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'yellow_id-'.$yellow_count]); !!}
        <div class="form-group col-md-1">
            {!! Form::text('yellow[condition][]' , ($requirement->condition == '' || is_null($requirement->condition)?'':$requirement->id),[ 'class'=>'form-control condition','id'=>'yellow_condition-1','readonly'=>'']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::select('yellow[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'yellow_parameter-'.$yellow_count]); !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('yellow[operator][]', $requirement->operators ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'yellow_operator-'.$yellow_count]); !!}
        </div>
        <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
            @if($requirement->parameter == 'name')
            {!! Form::text('yellow[value][]',$requirement->value, [ 'class'=>'form-control value','id'=>'yellow_value-'.$yellow_count]); !!}
            @else
            {!! Form::select('yellow[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'yellow_value-'.$yellow_count]); !!}
            @endif
        </div>
        @if($requirement->operator == 'between')
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('yellow[value_2][]', $requirement->values ,$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'yellow_value_2-'.$yellow_count]); !!}
        </div>
        @endif
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('yellow[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'yellow_status-'.$yellow_count]); !!}
        </div>
        <div class="form-group col-md-1 clearfix">
            {!! Form::number('yellow[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'yellow_number-'.$yellow_count,'min'=> 1]); !!}
        </div>
        @if(!is_null($requirement->condition) || !empty($requirement->condition))
        <div class="form-group  col-md-1 clearfix ">
           <a class="fa fa-external-link show-condition"  title="Show Condition" style="color:yellow" id="" data-id="{!! $requirement->condition  !!}"> </a>
        </div>
        @else
        <div class="form-group col-md-1 clearfix {{($yellow_count==1?'yellow-trash':'')}}">
            <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
        </div>
        @endif
    </div>
@php $yellow_count++; @endphp
@endforeach
@endif