<div class="col-md-12                 ">
    <h2 class="orange col-md-3"><i class="fa fa-play orange-flip" aria-hidden="true"></i> Borderline</h2>
    <div class="form-group col-md-2">
        <br>
        <div class="btn-group">
            <button id="orange-add" class="btn sbold orange-background white-text add" > <i class="fa fa-plus"></i></button>
        </div>
        <a  href="{!!  route('condition',['condition'=>'orange', 'service_id'=>$id,'directorate_id'=>$directorate_id]) !!}" id="" class="btn sbold orange-background white-text "  ><i class="fa fa-link" aria-hidden="true"></i></a>
    </div>
</div>
@if(!count($orange_requirements))
<div class="orange-form clearfix">
    <div class="form-group col-md-1 ">
        {!! Form::text('orange[condition][]' , '',[ 'class'=>'form-control condition','id'=>'orange_condition-1' ,'readonly'=>'']) !!}
    </div>
    <div class="form-group col-md-1">
        {!! Form::select('orange[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'orange_parameter-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('orange[operator][]', $operators ,null, [ 'class'=>'form-control operator','id'=>'orange_operator-1']); !!}
    </div>
    <div class="form-group col-md-4 clearfix">
        {!! Form::select('orange[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'orange_value-1']); !!}
    </div>
    <div class="form-group col-md-2 clearfix value-2">
        {!! Form::select('orange[value_2][]', $values ,null, [ 'class'=>'form-control value','id'=>'orange_value-1','disabled'=>'disabled']); !!}
    </div>
    <div class="form-group col-md-2 clearfix">
        {!! Form::select('orange[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'orange_status-1']); !!}
    </div>
    <div class="form-group col-md-1 clearfix">
        {!! Form::number('orange[number][]', 1, [ 'class'=>'form-control number','id'=>'orange_number-1','min'=> 1]); !!}
    </div>
    <div class="form-group col-md-2 clearfix orange-trash">
        <a class="fa fa-trash" ></a>
    </div>
</div>
@else
@foreach($orange_requirements as $requirement)

    <div class="orange-form clearfix">
        {!! Form::hidden('orange[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'orange_id-'.$orange_count]); !!}
        <div class="form-group col-md-1">
            {!! Form::text('orange[condition][]' , ($requirement->condition == '' || is_null($requirement->condition)?'':$requirement->id),[ 'class'=>'form-control condition','id'=>'orange_operator','readonly'=>'']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::select('orange[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'orange_parameter-'.$orange_count]); !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('orange[operator][]', $requirement->operators ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'orange_operator-'.$orange_count]); !!}
        </div>
        <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix value-div">
            @if($requirement->parameter == 'name')
            {!! Form::text('orange[value][]' ,$requirement->value, [ 'class'=>'form-control value','id'=>'orange_value-'.$orange_count]); !!}
            @else
            {!! Form::select('orange[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'orange_value-'.$orange_count]); !!}
            @endif
        </div>
        <div class="form-group col-md-2 clearfix value-2" style="{{((isset($requirement->value_2) && $requirement->value_2 !='')?'display:block;':'display:none;')}}}" >
            {!! Form::select('orange[value_2][]', ($requirement->operator=='between'?$requirement->values:[]) ,$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'orange_value_2-'.$orange_count]); !!}
        </div>
        <div class="form-group col-md-2 clearfix">
            {!! Form::select('orange[status][]', $status,$requirement->status, [ 'class'=>'form-control status','id'=>'orange_status-'.$orange_count]); !!}
        </div>
        <div class="form-group col-md-1 clearfix">
            {!! Form::number('orange[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'orange_number-'.$orange_count,'min'=> 1]); !!}
        </div>
        @if(!is_null($requirement->condition) || !empty($requirement->condition))
        <div class="form-group  col-md-1 clearfix ">
           <a class="fa fa-external-link show-condition"  title="Show Condition" style="color:orange" id="" data-id="{!! $requirement->condition  !!}"> </a>
        </div>
        @else
        <div class="form-group col-md-1 clearfix {{($orange_count==1?'orange-trash':'')}}">
            <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
        </div>
        @endif
    </div>
@php $orange_count++; @endphp 
@endforeach
@endif
