@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>Transition Times 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-time'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-time') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-time'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-time') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="btn-group">

                                        @if(__authorize(config('module.transition-times'),'add'))
                                        <a href="{{url('/transition-time/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($directorates as $directorate)
                            @if($directorate->id == $directorates[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$directorate->id}}" data-toggle="tab">{{ $directorate->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($directorates as $directorate)
                            @if($directorate->id == $directorates[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$directorate->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$directorate->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Start Time </th>
                                                <th> End Time </th>
                                                <th class="text-center"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($directorate->transition_time as $time)
                                            <tr class="odd gradeX">
                                                <td width='25%'>{{$time->name}}</td>
                                                <td width='25%'>{{$time->start_time}}</td>
                                                <td width='25%'>{{$time->end_time}}</td>
                                                <td class="text-center">
                                                    @if(__authorize(config('module.transition-times'),'edit'))
                                                    <a href="{{url('/transition-time/edit/'.$time->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    @endif
                                                        @if(__authorize(config('module.transition-times'),'delete'))
                                                    <a data-toggle="modal" href="#small" id="{{$time->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        @endif
                                                </td>
                                            </tr>
                                            @empty
                                            <tr class="odd gradeX">
                                                <td colspan="4"> No Data Found.</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/transition-time/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection