<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <link rel="icon" type="image/png" href="assets/images/favicon.ico">
        <title>Nexin | @yield('title')</title>
        <!-- Title and Meta Tags Ends -->

        <!-- Google Font Begins -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
        <!-- Google Font Ends -->

        <!-- CSS Begins-->
        <link href='{{ asset('css/font-awesome.min.css')}}' rel='stylesheet' type='text/css'/>
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/animate.min.css')}}" rel="stylesheet" type="text/css"/>
        <noscript>
			<link rel="stylesheet" type="text/css" href="{{ asset('css/styleNoJS.css')}}" />
        </noscript>

        <!-- Main Style -->
        <link href="{{ asset('css/responsive.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
        
        @yield('extra-css')
        <script type="text/javascript"> var App_URL = {!! json_encode(url('/')) !!} </script>
        <script type="text/javascript" src="{{ asset('js/jquery-1.11.0.min.js')}}"></script>
        
    </head>
    <body>
        <!-- Header Begins -->
        @include('header')
        <!-- Header Ends -->
        @yield('content')
        <!-- Footer Widgets Section Begins -->
        @include('footer')
        <!-- Footer Widgets Section Ends -->
        <!-- Script Begins -->
        <script type="text/javascript">
            $.ajaxSetup({headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}});
        </script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>	

        @yield('extra-js')
        
        <script type='text/javascript' src='{{ asset('js/bootstrapValidator.min.js')}}'></script>
        <!-- Script Ends --> 
        
    </body>
</html>