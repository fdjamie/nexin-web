@extends('admin.admin')
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-h-square fa-2x"></i>Add New Speciality 
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['SpecialityController@update',$speciality->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $speciality->name,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('abbreviation', 'Abbreviation', ['class' => 'awesome']); !!}
                            {!! Form::text('abbreviation', $speciality->abbreviation, ['placeholder' => 'Enter Abbreviation', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$speciality->directorate_id, [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('status', 'status', ['class' => 'awesome']); !!}
                            {!! Form::select('status', \App\Http\helpers::booleanDropDown(),$speciality->status,['class' => 'form-control']); !!}
                        </div>
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
