@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<style>
    .color-box{
        height: 33px;
        width: 15%;
        border: 1px solid #000;
        float: left;
        margin-left: 10px;
    }
    .royg{
        width: 80%;
        float: left;
    }
</style>
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fas fa-object-group"></i>{{$priority_set->name}}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-priority'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-priority') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method'=>'put','action' => ['PrioritySetController@update', $priority_set->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $priority_set->name,['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!} 
                            {!! Form::select('directorate_id', $directorates, $priority_set->speciality->directorate_id,['class' => 'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}  <i class="fas fa-spinner fa-spin" id="sp_spin"></i>
                            {!! Form::select('directorate_speciality_id', $specialities, $priority_set->directorate_speciality_id,['class' => 'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!} 
                            {!! Form::text('start_date',$priority_set->start_date,['class' => 'form-control datepicker','id'=>'start_date']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!} 
                            {!! Form::text('end_date', $priority_set->end_date ,['class' => 'form-control datepicker','id'=>'end_date']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fas fa-object-group"></i>Configure Priority Set
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-priority'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-priority') }}
                    </div>
                    @endif
                    @if (Illuminate\Support\Facades\Session::has('error-priority'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-priority') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method'=>'post','action' => ['SpecialityPrioritySetController@store']]) !!}
                        <table class="table  table-bordered table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>level</th>
                                    <th>From</th>
                                    <th>Priority</th>
                                    <th>To</th>
                                </tr>
                            </thead>
                            <tbody id="body-{{$priority_set->id}}">
                                @if(isset($priority_set->priorities) && count($priority_set->priorities))
                                @php 
                                $count = 1;
                                @endphp
                                {!! Form::hidden('priority_set_id', $priority_set->id ); !!}
                                @foreach($priority_set->priorities as $set)
                                <tr class="copy-row">
                                    {!! Form::hidden('id[]', $set->id,['class'=>'row-id'] ); !!}
                                    <td class="add-row text-center">{!!($count==1?'<div class="btn-group"><button id="sample_editable_1_2_new" class="btn sbold blue add" > <i class="fa fa-plus"></i></button></div>':'<a class="fa fa-trash delete-row" id="'.$set->id.'"></a>')!!}</td>
                                    <td width="10%">{!! Form::number('level[]', $set->level ,[ 'class'=>'form-control level','id'=>'level-'.$count,'min'=>1]); !!}</td>
                                    <td width="25%">{!! Form::select('from[]', $royg ,$set->from, [ 'class'=>'form-control royg from','id'=>'from-'.$count.'_'.$priority_set->id]); !!}<div class="color-box" style="background:{{(isset($colors[$set->from])?$colors[$set->from]:'#D9534F')}}"></div></td>
                                    <td width="30%">{!! Form::select('priority_id[]', $dropdown ,$set->priority_id, [ 'class'=>'form-control priority','id'=>'priority_id-'.$count]); !!}</td>
                                    <td width="25%">{!! Form::select('to[]', $royg ,$set->to, [ 'class'=>'form-control royg to','id'=>'to-'.$count.'_'.$priority_set->id]); !!}<div class="color-box" style="background:{{(isset($colors[$set->to])?$colors[$set->to]:'#D9534F')}}"></div></td>
                                </tr>
                                @php $count++; @endphp
                                @endforeach
                                @else
                                {!! Form::hidden('priority_set_id', $priority_set->id ); !!}
                                <tr class="copy-row">
                                    <td class="add-row text-center"><div class="btn-group"><button id="sample_editable_1_2_new" class="btn sbold blue add" > <i class="fa fa-plus"></i></button></td>
                                    <td width="10%">{!! Form::number('level[]', 1 ,[ 'class'=>'form-control level','id'=>'level-1','min'=>1]); !!}</td>
                                    <td width="25%">{!! Form::select('from[]', $royg ,null, [ 'class'=>'form-control royg from','id'=>'from-1']); !!}<div class="color-box" style="background:{{(isset($colors[0])?$colors[0]:'#D9534F')}}"></div></td>
                                    <td width="30%">{!! Form::select('priority_id[]', $dropdown ,null, [ 'class'=>'form-control priority','id'=>'priority_id-1']); !!}</td>
                                    <td width="25%">{!! Form::select('to[]', $royg ,null, [ 'class'=>'form-control royg to','id'=>'to-1']); !!}<div class="color-box" style="background:{{(isset($colors[0])?$colors[0]:'#D9534F')}}"></div></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {!! Form::submit('Add !', ['class'=>'btn green']); !!}
                        {!! Form::close(); !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="{{url('/js/priority-set.js')}}"></script>
<script>
$(document).ready(function () {
    $('#sp_spin').hide();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#directorate_id').change(function () {
        updateSpeciality($(this).val());
    });
    var updateSpeciality = function (id) {
        $('#directorate_speciality_id').hide();
        $('#sp_spin').show();
        jQuery.ajax({
            url: APP_URL + '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {

                    speciality += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#directorate_speciality_id').html(speciality).show();
                $('#sp_spin').hide();
            }
        });
    };
});
</script>
@endsection
