@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fas fa-object-group"></i>Add Priority Set
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-priority'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-priority') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method'=>'post','action' => ['PrioritySetController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!} 
                            {!! Form::select('directorate_id', $directorates, null,['class' => 'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}  <i class="fas fa-spinner fa-spin" id="sp_spin"></i>
                            {!! Form::select('directorate_speciality_id', $specialities, null,['class' => 'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!} 
                            {!! Form::text('start_date',null,['class' => 'form-control datepicker','id'=>'start_date']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!} 
                            {!! Form::text('end_date', null ,['class' => 'form-control datepicker','id'=>'end_date']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script>
$(document).ready(function () {
    $('#sp_spin').hide();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#directorate_id').change(function(){
        updateSpeciality($(this).val());
    });
    var updateSpeciality = function (id) {
        $('#directorate_speciality_id').hide();
        $('#sp_spin').show();
        jQuery.ajax({
            url: APP_URL+ '/getdirectoratespeciality/' + id,
            type: 'get',
            success: function (data) {
                var speciality = '';
                $.each(data, function (index, value) {

                    speciality += "<option value='" + index + "'>" + value + "</option>";
                });
                $('#directorate_speciality_id').html(speciality).show();
                $('#sp_spin').hide();
            }
        });
    };
});
</script>
@endsection
