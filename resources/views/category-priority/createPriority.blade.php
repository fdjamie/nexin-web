@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
<link rel="stylesheet" href="{{url('/css/tree.min.css')}}"  />
<style>
    #sub-div,#service-div{
        display: none;
    }
</style>
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>Set Category Priority 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-priority'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-priority') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['CategoryPriorityController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,null, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('directorate_speciality_id', $specialities ,null, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('type', 'Type', ['class' => 'awesome']); !!}
                            {!! Form::select('type', $types ,null, [ 'class'=>'form-control','id'=>'type']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} <span id="ct_spin"><i class="fa fa-spin fa-refresh"></i></span> {!!Form::checkbox('category_check', 1, true,['id'=>'category_check','class'=>'check'])!!}
                            {!! Form::select('category_id', $categories ,null, [ 'class'=>'form-control','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group" id="sub-div">
                            <a data-toggle="modal" href="#small" id="modal-display">{!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!}</a> {!!Form::checkbox('category_check', 1, true,['id'=>'subcategory_check','class'=>'check'])!!}
                            {!! Form::select('sub_category_id', [],null,['class' => 'form-control serach-subcategory','id'=>'subcategory_id','disabled'=>'disabled','style'=>'width:100%;']); !!}
                        </div>
                        <div class="form-group" id="service-div">
                            {!! Form::label('service_id', 'Service', ['class' => 'awesome']); !!} <span id="sb_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('service_id', [],null,['class' => 'form-control','id'=>'service_id','disabled'=>'disabled']); !!}
                        </div>
                        <div class="form-group" style="margin-top: 15px">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Category Hierarchy</h4>
            </div>
            <div class="modal-body" id="tree"> {!! $tree !!} </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script src="{{url('/js/category-priority.js')}}" type="text/javascript"></script>
<script src="{{url('js/search-subcategory.js')}}" type="text/javascript"></script>

@endsection
