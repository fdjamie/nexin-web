@extends('admin.admin')
@php 
use App\Http\Helpers;
@endphp
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .color-box{
        height: 33px;
        width: 15%;
        border: 1px solid #000;
        float: left;
        margin-left: 10px;
    }
    .royg{
        width: 80%;
        float: left;
    }
    #colors-table{
        width: 50%;
    }
</style>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>Category Priorities 
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['CategoryPriorityController@index'],'id'=>'directorate_form']) !!}
                        <div class="form-group col-md-offset-4 col-md-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-priority'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-priority') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-priority'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-priority') }}
                    </div>
                    @endif
                    @if(empty($color_array))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ 'Please add Color before adding priority set.' }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <h3>COLOR</h3>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="colors-table">
                            <thead>
                                <tr>
                                    <th> Position</th>
                                    <th> Name </th>
                                    <th> Letter </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($color_array as $color)
                                <tr>
                                    <td>{{$color->position}}</td>
                                    <td>{{$color->name}}</td>
                                    <td>{{$color->letter}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <h3>Priority Type</h3>
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{{url('/category-priority/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_priority_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialities as $speciality_node)
                            <div class="tab-pane fade  {{($speciality_node->id == $specialities[0]->id?'active in':'')}}" id="tab_priority_{{$speciality_node->id}}">
                                @if(count($speciality_node->category_priority))
                                <table class="table  table-bordered table-checkable order-column" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="priority-body-{{$speciality_node->id}}">
                                        @foreach($speciality_node->category_priority as $priority)
                                        <tr>
                                            @if($priority->type == 'category')
                                            <td>{{$priority->category[0]->name}}</td>
                                            @elseif($priority->type == 'subcategory')
                                            <td>{{$priority->subcategory[0]->name}}</td>
                                            @elseif($priority->type == 'service')
                                            <td>{{$priority->service[0]->name}}</td>
                                            @else
                                            <td>{{$priority->directorate_service[0]->name}}</td>
                                            @endif
                                            <td>{{$priority->type}}</td>
                                            <td class="text-center">
                                                <a href="{{url('/category-priority/edit/'.$priority->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                <a data-toggle="modal" href="#small" id="{{$priority->id}}" data-url="category-priority" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <table class="table table-bordered" >
                                    <tr>
                                        <th class="text-center">No Category priority for this speciality.</th>
                                    </tr>
                                </table>
                                @endif
                            </div>
                            @endforeach
                        </div>
                        <h3>Priority Set</h3>
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{{url('/priority-set/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialities as $speciality_node)
                            <div class="tab-pane fade  {{($speciality_node->id == $specialities[0]->id?'active in':'')}}" id="tab_{{$speciality_node->id}}">
                                @if(count($speciality_node->priority_set))
                                <table class="table  table-bordered table-checkable order-column" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="body-{{$speciality_node->id}}">
                                        @if(isset($speciality_node->priority_set) && count($speciality_node->priority_set))
                                        @php 
                                        $count = 1;
                                        @endphp
                                        @foreach($speciality_node->priority_set as $set)
                                        <tr>
                                            <td>{!!$set->name!!}</td>
                                            <td>{!!$set->start_date!!}</td>
                                            <td>{!!$set->end_date!!}</td>
                                            <td class="text-center">
                                                <a href="{{url('/priority-set/edit/'.$set->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                <a data-toggle="modal" href="#small" id="{{$set->id}}" data-url="priority-set" class="delete-set"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else

                                        @endif

                                    </tbody>
                                </table>
                                @else
                                <table class="table table-bordered" >
                                    <tr class="text-center">
                                        <th class="text-center">No Category priority for this speciality.</th>
                                    </tr>
                                </table>
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/category-priority/delete")}}' + '/' + $(this).attr('id'));
    });
    $('.delete-set').click(function () {
        $('#delete-button').attr('href', '{{url("/priority-set/delete")}}' + '/' + $(this).attr('id'));
    });
</script>
<script src="{{url('/js/category-priority-index.js')}}"></script>
@endsection