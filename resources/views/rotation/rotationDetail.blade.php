@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<?php 
//echo '<pre>';
//print_r([$rotation,$templateRotations,$shifts]);
//die;
?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Rotation</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        {{--!! Breadcrumbs::render('rotation/detail',$id) !!--}}
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-repeat"></i>Rotations
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr class="text-center">
                                    <th>Week</th>
                                    <th> Monday </th>
                                    <th> Tuesday </th>
                                    <th> Wednesday </th>
                                    <th> Thursday </th>
                                    <th> Friday </th>
                                    <th> Saturday </th>
                                    <th> Sunday </th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @foreach($templateRotations as $templateRotation)
                                <tr>
                                    <td></td>
                                    @foreach($templateRotation as $value)
                                    <td>
                                    @foreach ($shifts as $shift)
                                    @if ($shift->id == $value)
                                    {{$shift->name}}<br>{{$shift->start_time}}
                                    @endif
                                    @endforeach
                                    </td>
                                @endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection