@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
<style>
    .hand{
        cursor: pointer!important;
    }
</style>
@endsection

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Rotation</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        {{--!! Breadcrumbs::render('rotation/new') !!--}}
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-repeat"></i>Add Rotation
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('directorate_speciality_id', $specialities ,$speciality, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-rotation'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-rotation') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['RotationController@store']]) !!}
                        <div class="form-group">
                            <a data-toggle="modal" data-target="#staff">{!! Form::label('staff_id', 'Staff Member', ['class' => 'awesome']); !!}</a>
                            {!! Form::select('staff_member', [] , old('staff') , [ 'class'=>'form-control search_directorate_staff','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('post_id', 'Post', ['class' => 'awesome']); !!} <span id="p_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('post_id', $posts, null , ['class' => 'form-control','id'=>'post_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Group', ['class' => 'awesome']); !!}
                            {!! Form::select('rota_group_id', $rota_groups, $selected_template, ['class' => 'form-control','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('template_id', 'Template', ['class' => 'awesome']); !!} <span id="rt_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('template_id', $rota_templates, $rota_group_id, ['class' => 'form-control','id'=>'template_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('assigned_week', 'Position', ['class' => 'awesome']); !!} <span id="spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::number('assigned_week', $current_position,[ 'class' => 'form-control', 'max'=>$position_max ,'min'=>1, 'id'=>'template_position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start date', ['class' => 'awesome']); !!}
                            {!! Form::text('start_date', date('Y-m-d'),[ 'class' => 'form-control datepicker']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('permanent', 'Permanent', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('permanent',1,[ 'class' => 'form-control','id'=>'permanent']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End date', ['class' => 'awesome']); !!}
                            {!! Form::text('end_date', null,[ 'class' => 'form-control datepicker', 'id'=>'endDate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-lg" id="staff" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Directorate Staff</h4>
            </div>
            <div class="modal-body"> 
                <div class="table-toolbar">
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Gmc</th>
                            <th>Grade</th>
                            <th>Role</th>
                            <th>Availabilty</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($directorate_staff as $staff)
                        <tr>
                            <td width="25%">{{$staff->name}}</td>
                            <td class="text-center">{{$staff->gmc}}</td>
                            <td class="text-center">{{(isset($staff->grade->name)?$staff->grade->name:'Not assigned')}}</td>
                            <td class="text-center">{{(isset($staff->role->name)?$staff->role->name:'Not assigned')}}</td>
                            <td class="text-center">
                                @if(! is_null($staff->post))
                                {{$staff->post->name}}
                                @else
                                Available
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script src="{{url('/js/rotations.js')}}" type="text/javascript"></script>
<script src="{{url('/js/search_staff.js')}}" type="text/javascript" ></script>
<script>
if ($(':checkbox').is(':checked')) {
    $('#endDate').attr("disabled", "disabled");
}
$(':checkbox').change(function () {
    if ($(this).is(':checked')) {
        $('#endDate').attr("disabled", "disabled");
        $('#endDate').val("");
    } else {
        $("#endDate").removeAttr("disabled");
    }
});
</script>
@endsection