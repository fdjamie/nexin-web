@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage Rotations</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-repeat"></i>Rotation  
                        </div>
                        {!! Form::open(['method' => 'PUT', 'autocomplete'=>'off', 'action' => ['RotationController@index']]) !!}
                        <div class="form-group col-md-3">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('directorate', 'Date', ['class' => 'awesome']); !!}
                            {!! Form::text('date_range', $date_range->copy()->format('Y-m-d') , [ 'class'=>'form-control datepicker','id'=>'date_range']); !!}
                        </div>
                        {!! Form::close() !!}

                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-rotation'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-rotation') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-rotation'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-rotation') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/rotations/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($specialities)==0)
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th colspan="5"> No Speciality for this Directorate</th>
                                </tr>
                            </thead>
                        </table>
                        @endif
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$speciality_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$speciality_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Description </th>
                                                <th> Staff Member </th>
                                                <th> Start Date </th>
                                                <th> End Date </th>
                                                <th> Rota tempalte </th>
                                                <th> Week </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($speciality_node->post as $post)
                                            @foreach($post->rotation as $rotation)
                                            @php
                                            $date_range = new Carbon ($date_range);
                                            $date_range = $date_range->format('Y-m-d');
                                            $start_date = new Carbon ($rotation->start_date);
                                            $start_date = $start_date->format('Y-m-d');
                                            $current_date = new Carbon ();
                                            if($rotation->end_date != null && $rotation->permanent != 1)
                                            $end_date = new Carbon ($rotation->end_date);
                                            else
                                            $end_date = new Carbon ('last day of next year');
                                            @endphp
                                            <tr class="odd gradeX">
                                                <td width='14%'>{{$post->name}}</td>
                                                <td width='14%'>{{$post->description}}</td>
                                                <td>

                                                    @if(isset($rotation->staff->name) && $rotation->staff->name != '' && $date_range<=$end_date)
                                                    {{$rotation->staff->name}}
                                                    @else
                                                    VACANT
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($rotation->start_date) && $rotation->start_date != '0000-00-00')
                                                    {{$rotation->start_date}}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{($rotation->permanent == 1?'Permanent':$rotation->end_date)}}
                                                </td>
                                                <td>
                                                    @if(isset($rotation->rota_template->name))
                                                    {{$rotation->rota_template->name}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($rotation->template_position) && count($rotation->template_position))
                                                    {{$rotation->template_position}}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{url('/rotations/edit/'.$rotation->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                    <a data-toggle="modal" href="#small" id="{{$rotation->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    @endsection
    @section('modal')
    @include('common.delete-confirmation-modal')
    @endsection
    @section('extra-js')
    @include('includes.scriptsViewLinks')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
    <script>
$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
$("#directorate,#date_range").change(function () {
    $("form").submit();
});
$('.delete').click(function () {
    $('#delete-button').attr('href', '{{url("/rotations/delete")}}' + '/' + $(this).attr('id'));
});
    </script>
    @endsection