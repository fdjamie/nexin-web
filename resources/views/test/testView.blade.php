@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css" integrity="sha256-avvOYh3iuZbeN1rcsni+ZBCq641kD8BMfypj56QtJMI=" crossorigin="anonymous" />
<link href="{{url('/css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
<link href="{{url('/css/scheduler.min.css')}}" rel='stylesheet' />
<style>
    .fc-license-message{
        display: none;
    }
</style>
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-4">
                <h1>Requirements view</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy"></i>Service Requirement view
                        </div>
                    </div>
                    <br><br>
                    <div class="portlet-body">
                        <div id="calendar">
                            {!! $calendar->calendar() !!}
                            {!! $calendar->script() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js" integrity="sha256-mpLtfGupKq1kFdhoOpC1h7vr/+ZH67BVVOUnb3beJiU=" crossorigin="anonymous"></script>-->
<script src='{{url('/js/fullcalendar.min.js')}}'></script>
<script src='{{url('/js/scheduler.min.js')}}'></script>
<script>
//    $(function () { // document ready
//
//        $('#calendar').fullCalendar({
//            now: '2017-11-07',
//            aspectRatio: 1.8,
//            scrollTime: '00:00', // undo default 6am scrollTime
//            header: {
//                left: '',
//                center: '',
//                right: ''
//            },
//            defaultView: 'timelineDay',
//            views: {
//                timelineThreeDays: {
//                    type: 'timeline',
//                    duration: {days: 3}
//                }
//            },
//            resourceLabelText: 'Rooms',
//            eventClick: function (event) {
//                $('.modal-body').html(event.title);
//                $('#small').modal({show: true});
//            },
//            resources: [
//                {id: 'a', title: 'Auditorium A'},
//                {id: 'b', title: 'Auditorium B', eventColor: 'green'},
//                {id: 'c', title: 'Auditorium C', eventColor: 'orange'},
//                {id: 'd', title: 'Auditorium D', children: [
//                        {id: 'd1', title: 'Room D1'},
//                        {id: 'd2', title: 'Room D2'}
//                    ]},
//                {id: 'e', title: 'Auditorium E'},
//                {id: 'f', title: 'Auditorium F', eventColor: 'red'},
//                {id: 'g', title: 'Auditorium G'},
//                {id: 'h', title: 'Auditorium H'},
//                {id: 'i', title: 'Auditorium I'},
//                {id: 'j', title: 'Auditorium J'},
//                {id: 'k', title: 'Auditorium K'},
//                {id: 'l', title: 'Auditorium L'},
//                {id: 'm', title: 'Auditorium M'},
//                {id: 'n', title: 'Auditorium N'},
//                {id: 'o', title: 'Auditorium O'},
//                {id: 'p', title: 'Auditorium P'},
//                {id: 'q', title: 'Auditorium Q'},
//                {id: 'r', title: 'Auditorium R'},
//                {id: 's', title: 'Auditorium S'},
//                {id: 't', title: 'Auditorium T'},
//                {id: 'u', title: 'Auditorium U'},
//                {id: 'v', title: 'Auditorium V'},
//                {id: 'w', title: 'Auditorium W'},
//                {id: 'x', title: 'Auditorium X'},
//                {id: 'y', title: 'Auditorium Y'},
//                {id: 'z', title: 'Auditorium Z'}
//            ],
//            events: [
//                {id: '1', resourceId: 'b', start: '2017-11-07T02:00:00', end: '2017-11-07T07:00:00', title: 'event 1'},
//                {id: '2', resourceId: 'c', start: '2017-11-07T05:00:00', end: '2017-11-07T22:00:00', title: 'event 2'},
//                {id: '3', resourceId: 'd', start: '2017-11-06', end: '2017-11-08', title: 'event 3'},
//                {id: '4', resourceId: 'e', start: '2017-11-07T03:00:00', end: '2017-11-07T08:00:00', title: 'event 4'},
//                {id: '5', resourceId: 'f', start: '2017-11-07T00:30:00', end: '2017-11-07T02:30:00', title: 'event 5'}
//            ]
//        });
//
//    });
</script>
@endsection