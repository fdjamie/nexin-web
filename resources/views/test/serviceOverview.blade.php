@extends('admin.admin')
@php
use Carbon\Carbon;
@endphp
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Services Overview</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy fa-2x"></i> Services Overview 
                        </div>
                        {!! Form::open(['method' => 'post', 'action' => ['ServiceOverviewController@index'],'id'=>'directorate_form']) !!}
                        <div class="form-group col-md-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control col-md-3','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::select('speciality_id', $specialities ,$speciality_id, [ 'class'=>'form-control col-md-3','id'=>'speciality_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="portlet-body">
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            @php
                            $selected_date = new Carbon(session()->get('date_time'));
                            @endphp
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">{{$selected_date->format('Y M d D')}}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    @if(!count($staff_array))
                                    <b>No Data found.</b>
                                    @endif
                                    @foreach($staff_array as $key => $staffs)
                                    <h4>{{$key}}</h4>
                                    @foreach($staffs as $staff)
                                    {{$staff}} <br>
                                    @endforeach
                                    @endforeach
                                </div>
                                <div class="col-md-6" style="border-left: 1px solid;">
                                    @if(!count($service_info))
                                    <b>No Data found.</b>
                                    @endif
                                    @foreach($service_info as $key => $services)
                                    <h4>{{$key}}</h4>
                                    @foreach($services as $service)
                                    {{$service->name}} <br>
                                    @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4 pull-right">
                                {!! Form::open(['method' => 'post', 'action' => ['ServiceOverviewController@index'],'id'=>'category_form']) !!}
                                {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!}
                                {!! Form::select('category_id', $categories ,$category_id, [ 'class'=>'form-control col-md-3','id'=>'category_id']); !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">Requirements</h3>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                                        <thead>
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Operator</th>
                                                <th>Value</th>
                                                <th>Status</th>
                                                <th>Number</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($requirments_array as $requirement)
                                            <tr>
                                                <td>{{$requirement->parameter}}</td>
                                                <td>{{$requirement->operator}}</td>
                                                <td>{{$requirement->value_name}}</td>
                                                <td>{{$requirement->status}}</td>
                                                <td>{{$requirement->number}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $(document).ready(function () {
        $("#speciality_id,#directorate_id").change(function () {
            $("#directorate_form").submit();
        });
        $("#category_id").change(function () {
            $("#category_form").submit();
        });
    });
</script>
@endsection