@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage Services</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    
                    @if (Illuminate\Support\Facades\Session::has('success-service'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-service') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-service'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-service') }}
                    </div>
                    @endif

                    @if (Illuminate\Support\Facades\Session::has('service-add'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('service-add') }}
                    </div>
                    @elseif(Illuminate\Support\Facades\Session::has('service-teamplate-deleted'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('service-teamplate-deleted') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{!!  route('operator-add') !!}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
            
                        <div class="tab-content">
                     
                                  
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contactable</th>
                                                <th>Min Standards</th>
                                                <th> Actions</th>
                                            </tr>
                                        </thead>
 
                                        <tbody>
                                        </tbody>
                                    </table>
                                
                        </div>
                    </div>
                </div>
      
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body"> You want to delete this record? </div>
            <div class="modal-footer">
                <a href="" id='delete-button'><button type="submit" class="btn btn-danger" >Delete</button></a>
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="{{url('/js/checkbox_update.js')}}"></script>
<script>
$(document).ready(function () {
    $('.delete').click(function () {
        id = $(this).attr('id').split('-');
        if (id[0] == 'd') {
            url = '{{url("/directorate-service/delete")}}' +'/'+ id[1];
        } else {
            url = '{{url("/service/delete")}}' +'/'+ id[1];
        }
        $('#delete-button').attr('href', url);
//        $('#delete-button').attr('href', '/service/delete/' + $(this).attr('id'));
    });
    $("#directorate_speciality_id,#directorate").change(function () {
        $("form").submit();
    });
//        $('.main-check-box .check-boxes').change(function () {
//            $(this).parent().find('.sub-category').toggleClass('hide');
//        });
});

</script>
@endsection