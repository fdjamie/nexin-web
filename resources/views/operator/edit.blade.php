@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}" rel="stylesheet" type="text/css" />
<style>
    .value-2{
        display: none;
    }
</style>
@endsection
@php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-6">
                <h1<i class="fa fa-life-bouy fa-2x"></i> {!!  $heading !!}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('success-requirement'))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('success-requirement')}}
                    </li>
                </ul>
            </div>
            @endif
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    {!! Form::open(['method' => 'put', 'action' => ['ServiceRequirementParameterController@update',$id]]) !!}
<!--                    <div class="form-group ">
                        {!! Form::label('parameter', 'Parameter', ['class' => 'awesome']) !!}
                        {!! Form::select('directorate_id', $directorates ,$directorates , [ 'class'=>'form-control']) !!}
                    </div>-->
                    <div class="form-group ">
                        {!! Form::label('Parameter', 'Parameter', ['class' => 'awesome']); !!}
                        {!! Form::select('name', $parameterDropdown,$parameterDropdown,['placeholder' => 'Parameter Name' , 'class' => 'form-control','required']); !!}
                    </div>
                    <div class="form-group ">
                        {!! Form::label('Type', 'Type', ['class' => 'awesome']); !!}
                        {!! Form::select('type', $type , $typeSelectet , [ 'class'=>'form-control']) !!}
                    </div>
                   <div class="form-group ">
                       <input class="btn green" type="submit" value="Add !" title="Update Value"/>     
                        <a  href="{!!  route('service.requirement.parameter.index') !!}" >
                            <button id="sample_editable_1_2_new" class="btn sbold green" title="Back to home"> Back <i class="fa fa-backward"></i></button>
                        </a>
                    </div>

                    {!! Form::close() !!}
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<!--<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>-->
@endsection