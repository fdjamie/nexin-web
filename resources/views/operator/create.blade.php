@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}" rel="stylesheet" type="text/css" />
<style>
    .value-2{
        display: none;
    }
</style>
@endsection
@php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-6">
                <h1<i class="fa fa-life-bouy fa-2x"></i> {!!  $heading !!}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('success-parameters'))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('success-parameters')}}
                    </li>
                </ul>
            </div>
            @elseif (Illuminate\Support\Facades\Session::has('error-parameters'))
            <div class='alert alert-danger alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                {{ session('error-parameters') }}
            </div>
            @endif

            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    {!! Form::open(['method' => 'post', 'url' => 'operator-store']) !!}
                    <div class="form-group ">
                        {!! Form::label('Directorates', 'Directorates', ['class' => 'awesome']) !!}
                        {!! Form::select('directorate_id',$directorates,'', [ 'class'=>'form-control directorate_id']) !!}
                    </div>
                    <div class="form-group ">
                        {!! Form::label('Operator', 'Operator', ['class' => 'awesome']); !!}
                        {!! Form::select('value',$operators,'', [ 'class'=>'form-control parameter']) !!}
                    </div>
                                        <div class="form-group checkboxs">
                                     </div>
                   
                    <div class="form-group ">
                        {!! Form::label('Type', 'Type', ['class' => 'awesome']); !!}
                        {!! Form::select('type',$types,'', [ 'class'=>'form-control']) !!}
                    </div>
                    <div class="form-group ">
                        <input class="btn green" type="submit" value="Add !"/>     
                        <a  href="{!!  route('service.requirement.parameter.index') !!}">
                            <button id="sample_editable_1_2_new" class="btn sbold green" > Back <i class="fa fa-backward"></i></button>
                        </a>
                    </div>


                    {!! Form::close() !!}

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
            <!--<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>-->
<script>
    $('.directorate_id').on('change', function () {
        var values = '';
        var directorate_id = $(".directorate_id option:selected").val();
        var send = {directorate_id: directorate_id};
            jQuery.ajaxSetup({async: false});
            var responseData;
            var url = APP_URL + '/operator-getParameter/'+directorate_id;
            $.get(url,  function (data) {
                responseData = data;
            });
            $.each(responseData, function (index, value) {
                 values +=  '<span style="display: inline-block;  margin-left: 10px;"><lable>'+value+'</lable>  <input type="checkbox" class = "checkbox"id="check_all_1" name="parameters[]" title="Select All" value="'+index+'"/></span>';
            });
//            $(".parameter-select").empty().append(values);
            $(".checkboxs").empty().append(values);
            $(".checkboxs").show();

       
    });
</script>
@endsection