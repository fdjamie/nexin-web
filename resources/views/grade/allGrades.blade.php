@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-star"></i>GRADES 
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@showAllGrade']]) !!}
                        <div class="form-group col-md-offset-4 col-md-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-grade'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-grade') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-grade'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-grade') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        @if(__authorize(config('module.grades'),'add'))
                                        <a href="{{url('/grade/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        @endif

                                        <a href="{{url('/grade/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Grades <i class="fa fa-file-excel-o"></i></button>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @if(!count($specialities))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                <tr>
                                    <th width='100%' class="text-center"><b> No Speciality for this directorate.</b></td>
                                </tr>    
                            </table>
                            @endif
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$speciality_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$speciality_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Position</th>
                                                <th> Name </th>
                                                <th> Role </th>
                                                <th> Additional Grade </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($speciality_node->directorate->grade))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($speciality_node->directorate->grade as $grade)
                                            <tr class="odd gradeX text-center">
                                                <td>{{$grade->position}}</td>
                                                <td>{{$grade->name}}</td>
                                                <td>{{$grade->role->name}}</td>
                                                <td>
                                                    @if(isset($grade->additional_grade->name) && $grade->additional_grade->directorate_speciality_id == $speciality_node->id)
                                                    {{$grade->additional_grade->name}}
                                                    @else
                                                    {{'-'}}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if(__authorize(config('module.grades'),'edit'))
                                                    <a href="{{url('/grade/edit/'.$grade->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    @endif
                                                    @if(__authorize(config('module.grades'),'delete'))
                                                    <a data-toggle="modal" href="#small" id="{{$grade->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/grade/delete")}}' +'/'+ $(this).attr('id'));
        });
        $('#directorate_id').change(function () {
            $('form').submit();
        });
    });

</script>
@endsection