@extends('admin.admin')

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-star"></i>{{$grade->name}} 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-grade'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{--count(session('error-grade'))--}}
                        @foreach(session('error-grade') as $errornode)
                        <ul>
                            @if(is_array($errornode))
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                            @else
                            <li>
                                {{$errornode}}
                            </li>
                            @endif
                            
                            
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@updateGrade', $grade->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$grade->directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $grade->name ,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('role_id', 'Role', ['class' => 'awesome']); !!} <span id="role_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('role_id', $roles ,$grade->role_id, [ 'class'=>'form-control','id'=>'role_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Grade', ['class' => 'awesome']); !!} <span id="position_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('position', $grades_position ,$grade->position, [ 'class'=>'form-control','id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!} 
                            {!! Form::select('place', [0 => 'Before', 1 => 'After'] ,null, [ 'class'=>'form-control','id'=>'place']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::hidden('current_position', $grade->position ,['class' => 'form-control']); !!}
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/grade.js')}}" type="text/javascript"></script>
@endsection
