@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-hospital-o"></i>{{$site->name}} 
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['SiteController@update', $site->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $site->name,['placeholder' => 'Enter New Name','class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('abbreviation', 'Abbreviation', ['class' => 'awesome']); !!}
                            {!! Form::text('abbreviation', $site->abbreviation,['placeholder' => 'Enter Abbrevation','class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('address', 'Address', ['class' => 'awesome']); !!}
                            {!! Form::text('address', $site->address,['placeholder' => 'Enter New Address' ,'class' => 'form-control']); !!}
                        </div>
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
