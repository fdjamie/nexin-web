@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-hospital-o fa-2x"></i>Sites 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-site'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-site') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-site'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-site') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        @if(__authorize(config('module.sites'),'add'))
                                        <a href="{{url('/site/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        @endif
                                        <a href="{{url('/site/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Sites <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th> Name </th>
                                    <th> Abbreviation</th>
                                    <th> Address </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($records as $record)
                                <tr>
                                    <td>{{$record->id}}</td>
                                    <td>{{$record->name}}</td>
                                    <td>{{$record->abbreviation}}</td>
                                    <td>{{$record->address}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/site/'.$record->id.'/services')}}"><i class="fa fa-life-bouy" aria-hidden="true"></i></a>
                                        @if(__authorize(config('module.sites'),'edit'))
                                        <a href="{{url('/site/edit/'.$record->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                         @endif
                                            @if(__authorize(config('module.sites'),'delete'))
                                        <a data-toggle="modal" href="#small" id="{{$record->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                           @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/site/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection