@extends('admin.admin')

@section('extra-css')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        tr,th{
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-users"></i>Users Requests
                            </div>
                        </div>

                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (\Session::has('error'))
                                <div class="alert alert-danger text-center">
                                    {{Session::get('error')}}
                                </div>
                            @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success text-center">
                                    {{Session::get('success')}}
                                </div>
                            @endif
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                                <thead>
                                <tr>
                                    {{-- <th>Id</th>--}}
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>EMAIL</th>
                                    <th>REQUEST TIME</th>
                                    <th>STATUS</th>
                                    <th>REQUEST PROCEEDED BY</th>
                                    <th>REQUEST PROCEEDED AT</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                               @if(!empty($requests))
                                @foreach ($requests as $K=>$request)
                                    <tr>
                                        <td>{{--{{ $request->id }}--}}{{$K+1}}</td>
                                        <td>{{ $request->name }}</td>
                                        <td>{{ $request->email }}</td>
                                        <td>{{ $request->created_at }}</td>
                                        <td>
                                          @if($request->status=='pending')
                                            <span class="badge badge-primary"><i class="fa fa-circle-o" aria-hidden="true"></i> {{ $request->status }}</span>
                                          @elseif($request->status=='verified')
                                                <span class="badge badge-success"><i class="fa fa-check" aria-hidden="true"></i> {{ $request->status }}</span>
                                              @else
                                                <span class="badge badge-danger"><i class="fa fa-times" aria-hidden="true"></i> {{ $request->status }}</span>
                                              @endif
                                        </td>

                                        <td>{{ $request->proceeded_by_user }}</td>
                                        <td>{{ $request->proceeded_at }}</td>

                                        <td class="text-center">

                                                  @if($request->status=='pending')
                                                 {{-- @if($request->status!='verified')--}}
                                                   <a onclick="getActionModel('{{$request->requestId}}','roleModal')" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>
                                               {{--  @endif--}}
                                                      {{--@if($request->status!='rejected')--}}
                                                   <a title="Cancel"onclick="getActionModel('{{$request->requestId}}','rejectModal')"   class="delete" ><i class="fa fa-times" aria-hidden="true"></i></a>
                                                @endif

                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                 <tr>

                                     <td colspan='8'>no record found</td>
                                 </tr>
@endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="roleModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Assign Role To User</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['method' => 'post', 'action' => ['SignupRequestController@updateRequest'],'id'=>'form-roleModal']) !!}
                                    <div class="form-group">
                                        {!! Form::label('Roles', 'Select Roles', ['class' => 'awesome']); !!}
                                        {{-- {!! Form::select('roles[]', $roles, null ,['class' => 'form-control','multiple' => 'multiple']); !!}--}}
                                        <input type="hidden" name="requestId" id="requestId">
                                        <input type="hidden" name="status" value="verified" >
                                        <select class="form-control" name="roles[]" multiple required>
                                            @foreach($roles as $r)
                                                <option value="{{@$r->pivot->id}}">{{$r->name}}</option>
                                            @endforeach

                                        </select>
                                </div>

                                    <div class="form-group text-center">
                                        {!! Form::submit('Assign Role And Verify', ['class'=> 'btn green ']); !!}
                                    </div>
                                    {!! Form::close() !!}



                            </div>

                        </div>
                    </div>



                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Confirmation </h4>
                                </div>
                                {!! Form::open(['method' => 'post', 'action' => ['SignupRequestController@updateRequest'],'id'=>'form-rejectModal']) !!}
                                <input type="hidden" name="requestId" id="requestId">
                                <input type="hidden" name="status" value="rejected" >
                                <div class="modal-body">

                                    <h4 class="modal-title">Are you sure To reject request  </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>

                                    {!! Form::submit('YES', ['class'=> 'btn btn-danger btn-ok ']); !!}

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
@section('modal')
    @include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
    @include('includes.scriptsViewLinks')
    <script>
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/user/delete")}}' +'/'+ $(this).attr('id'));
        });

        function getActionModel(i,modal) {

            $('#form-'+modal)[0].reset();
          /*  $('#requestId').val(i);*/
            $('#form-'+modal+' input[id=requestId]').val(i);
            $('#'+modal).modal('show');

        }


    </script>
@endsection