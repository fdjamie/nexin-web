@extends('admin.admin')
@php
use Carbon\Carbon;
@endphp
@section('extra-css')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy fa-2x"></i> Optimal Assignments 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            @php
                            $selected_date = new Carbon(session()->get('date_time'));
                            @endphp
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">{{$selected_date->format('Y M d D H:i')}}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel panel-primary event-timeline">
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Transition Times</h3>
                                    </div>
                                    <div class="panel-body panel-requirement">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                            <thead>
                                                <tr>
                                                    <th> Transition Times </th>
                                                    <th> Service </th>
                                                    <th> Staff </th>
                                                </tr>
                                            </thead>
                                            @foreach($assignments as $time => $values)
                                            <tr>
                                                <td>
                                                    {{ $time }}
                                                </td>
                                                <td>
                                                    <ul>
                                                        @if(isset($values))
                                                        @foreach($values as $value)
                                                        <li>{{$value['service']}}</li>
                                                        @endforeach
                                                        @endif
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @if(isset($values))
                                                        @foreach($values as $value)
                                                        <li>{{$value['staff']}}</li>
                                                        @endforeach
                                                        @endif
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="info-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Assign Staff</h4>
            </div>
            {!! Form::open(['method' => 'post', 'action' => ['ServiceOverviewController@assignService'],'id'=>'service-assign']) !!}
            <div class="modal-body">
                {!! Form::hidden('service_id', null ,['id'=>'service_id']); !!}
                {!! Form::hidden('directorate_staff_id', null ,['id'=>'staff_id']); !!}
                {!! Form::hidden('duration', null ,['id'=>'duration']); !!}
                {!! Form::hidden('start', null ,['id'=>'start']); !!}
                {!! Form::hidden('end', null ,['id'=>'end']); !!}
                {!! Form::hidden('end_day', null ,['id'=>'endday']); !!}
                {!! Form::hidden('day', null ,['id'=>'day']); !!}
                {!! Form::hidden('date', null ,['id'=>'assign_date']); !!}
                <b>Assign staff member to this service?</b>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes',['class'=>'btn btn-success']); !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close(); !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-sm" id="update-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Staff</h4>
            </div>
            {!! Form::open(['method' => 'put', 'action' => ['ServiceOverviewController@updateAssignService'],'id'=>'service-assign']) !!}
            <div class="modal-body">
                {!! Form::hidden('assign_id', null ,['id'=>'assign_id']); !!}
                {!! Form::hidden('end', null ,['id'=>'ending']); !!}
                <b>Remove staff member from this service?</b>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes',['class'=>'btn btn-success']); !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close(); !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{!! url('/js/fullcalendar.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('/js/scheduler.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('/js/service-overview.js') !!}" type="text/javascript"></script>
@endsection