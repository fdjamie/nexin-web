@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}" rel="stylesheet" type="text/css" />
<style>
    .value-2{
        display: none;
    }
</style>
@endsection
@php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-6">
                <h1<i class="fa fa-life-bouy fa-2x"></i> {!! $service->name !!}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('success-requirement'))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('success-requirement')}}
                    </li>
                </ul>
            </div>
            @endif
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    {!! Form::open(['method' => 'post', 'action' => ['ServiceController@storeRequirement']]) !!}
                    <div class="portlet-title">
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('parameter', 'Parameter', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('operator', 'Operator', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-4">
                            <b>{!! Form::label('rota_status', 'Value', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('rota_status', 'Rota Status', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('number', 'Number', ['class' => 'awesome']); !!}</b>
                        </div>
                    </div>
                    <div class="row red-box">
                        <h2 class="red col-md-3"><i class="fa fa-exclamation " aria-hidden="true"></i> Unacceptable</h2>
                        <div class="form-group col-md-2">
                            <br>
                            <div class="btn-group">
                                <button id="red-add" class="btn sbold red-background white-text add" > <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::hidden('service_id', $id ); !!}
                        {!! Form::hidden('directorate_id', $directorate_id,['id'=>'directorate_id'] ); !!}
                        @if(!count($red_requirements))
                        <div class="red-form clearfix">
                            <div class="form-group col-md-2">
                                {!! Form::select('red[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'red_parameter-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('red[operator][]', $operator ,null, [ 'class'=>'form-control operator','id'=>'red_operator-1']); !!}
                            </div>
                            <div class="form-group col-md-4 clearfix value-div">
                                {!! Form::select('red[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'red_value-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::select('red[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'red_value_2-1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('red[status][]', $status,null, [ 'class'=>'form-control status','id'=>'red_status-1']); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('red[number][]', 1, [ 'class'=>'form-control number','id'=>'red_number-1','min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix red-trash">
                                <a class="fa fa-trash" ></a>
                            </div>
                        </div>
                        @else
                        @foreach($red_requirements as $requirement)
                        <div class="red-form clearfix">
                            {!! Form::hidden('red[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'red_id-'.$red_count]); !!}
                            <div class="form-group col-md-2">
                                {!! Form::select('red[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'red_parameter-'.$red_count]); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('red[operator][]', $operator ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'red_operator-'.$red_count]); !!}
                            </div>
                            <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix value-div">
                                @if($requirement->parameter == 'name')
                                {!! Form::text('red[value][]' ,$requirement->value, [ 'class'=>'form-control value','id'=>'red_value-'.$red_count]); !!}
                                @else
                                {!! Form::select('red[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'red_value-'.$red_count]); !!}
                                @endif
                            </div>
                            <div class="form-group col-md-2 clearfix value-2" style="{{((isset($requirement->value_2) && $requirement->value_2 !='')?'display:block;':'display:none;')}}}" >
                                {!! Form::select('red[value_2][]', ($requirement->operator=='between'?$requirement->values:[]) ,$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'red_value_2-'.$red_count]); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('red[status][]', $status,$requirement->status, [ 'class'=>'form-control status','id'=>'red_status-'.$red_count]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('red[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'red_number-'.$red_count,'min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix {{($red_count==1?'red-trash':'')}}">
                                <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
                            </div>
                        </div>
                        @php $red_count++; @endphp
                        @endforeach
                        @endif
                    </div>
                    <div class="row orange-box">
                        <div class="col-md-12">
                            <h2 class="orange col-md-3"><i class="fa fa-play orange-flip" aria-hidden="true"></i> Borderline</h2>
                            <div class="form-group col-md-2">
                                <br>
                                <button id="orange-add" class="btn sbold orange-background white-text add" > <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        @if(!count($orange_requirements))
                        <div class="orange-form clearfix">
                            <div class="form-group col-md-2">
                                {!! Form::select('orange[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'orange_parameter-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('orange[operator][]', $operator ,null, [ 'class'=>'form-control operator','id'=>'orange_operator-1']); !!}
                            </div>
                            <div class="form-group col-md-4 clearfix">
                                {!! Form::select('orange[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'orange_value-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::select('orange[value_2][]', $values ,null, [ 'class'=>'form-control value','id'=>'orange_value-1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('orange[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'orange_status-1']); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('orange[number][]', 1, [ 'class'=>'form-control number','id'=>'orange_number-1','min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix orange-trash">
                                <a class="fa fa-trash" ></a>
                            </div>
                        </div>
                        @else
                        @foreach($orange_requirements as $requirement)
                        <div class="orange-form clerfix">
                            {!! Form::hidden('orange[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'orange_id-'.$red_count]); !!}
                            <div class="form-group col-md-2">
                                {!! Form::select('orange[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'orange_parameter-'.$orange_count]); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('orange[operator][]', $operator ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'orange_operator-'.$orange_count]); !!}
                            </div>
                            <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
                                @if($requirement->parameter == 'name')
                                {!! Form::text('orange[value][]',$requirement->value, [ 'class'=>'form-control value','id'=>'orange_value-'.$orange_count]); !!}
                                @else
                                {!! Form::select('orange[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'orange_value-'.$orange_count]); !!}
                                @endif
                            </div>
                            @if($requirement->operator == 'between')
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('orange[value_2][]', $requirement->values, $requirement->value_2, [ 'class'=>'form-control value_2','id'=>'orange_value_2-'.$orange_count]); !!}
                            </div>
                            @endif
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('orange[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'orange_status-'.$orange_count]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('orange[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'orange_number-'.$orange_count,'min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix {{($orange_count==1?'orange-trash':'')}}">
                                <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
                            </div>
                        </div>
                        @php $orange_count++; @endphp 
                        @endforeach
                        @endif
                    </div>
                    <div class="row yellow-box">
                        <div class="col-md-12">
                            <h2 class="yellow col-md-3"><i class="fa fa-stop yellow-flip" aria-hidden="true"> </i> Adequate</h2>
                            <div class="form-group col-md-2">
                                <div class="btn-group">
                                    <br>
                                    <button id="yellow-add" class="btn sbold yellow-background white-text add" > <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        @if(!count($yellow_requirements))
                        <div class="yellow-form clearfix">
                            <div class="form-group col-md-2">
                                {!! Form::select('yellow[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'yellow_parameter-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('yellow[operator][]', $operator ,null, [ 'class'=>'form-control operator','id'=>'yellow_operator-1']); !!}
                            </div>
                            <div class="form-group col-md-4 clearfix">
                                {!! Form::select('yellow[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'yellow_value-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::select('yellow[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'yellow_value_2-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('yellow[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'yellow_status-1']); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('yellow[number][]', 1, [ 'class'=>'form-control number','id'=>'yellow_number-1','min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix yellow-trash">
                                <a class="fa fa-trash" ></a>
                            </div>
                        </div>
                        @else
                        @foreach($yellow_requirements as $requirement)
                        <div class="yellow-form clearfix">
                            {!! Form::hidden('yellow[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'yellow_id-'.$red_count]); !!}
                            <div class="form-group col-md-2">
                                {!! Form::select('yellow[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'yellow_parameter-'.$yellow_count]); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('yellow[operator][]', $operator ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'yellow_operator-'.$yellow_count]); !!}
                            </div>
                            <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
                                @if($requirement->parameter == 'name')
                                {!! Form::text('yellow[value][]',$requirement->value, [ 'class'=>'form-control value','id'=>'yellow_value-'.$yellow_count]); !!}
                                @else
                                {!! Form::select('yellow[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'yellow_value-'.$yellow_count]); !!}
                                @endif
                            </div>
                            @if($requirement->operator == 'between')
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('yellow[value_2][]', $requirement->values ,$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'yellow_value_2-'.$yellow_count]); !!}
                            </div>
                            @endif
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('yellow[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'yellow_status-'.$yellow_count]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('yellow[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'yellow_number-'.$yellow_count,'min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix {{($yellow_count==1?'yellow-trash':'')}}">
                                <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
                            </div>
                        </div>
                        @php $yellow_count++; @endphp
                        @endforeach
                        @endif
                    </div>
                    <div class="row green-box">
                        <div class="col-md-12">
                            <h2 class="green col-md-3"><i class="fa fa-star" aria-hidden="true"></i> Ideal</h2>
                            <div class="form-group col-md-2">
                                <div class="btn-group">
                                    <br>
                                    <button id="green-add" class="btn sbold green-background white-text add" > <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        @if(!count($green_requirements))
                        <div class="green-form clearfix">
                            <div class="form-group col-md-2">
                                {!! Form::select('green[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'green_parameter-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('green[operator][]', $operator ,null, [ 'class'=>'form-control operator','id'=>'green_operator-1']); !!}
                            </div>
                            <div class="form-group col-md-4 clearfix">
                                {!! Form::select('green[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'green_value-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::select('green[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'green_value_2-1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('green[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'green_status-1']); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('green[number][]', 1, [ 'class'=>'form-control number','id'=>'green_number-1','min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix green-trash">
                                <a class="fa fa-trash" ></a>
                            </div>
                        </div>
                        @else
                        @foreach($green_requirements as $requirement)
                        <div class="green-form clearfix">
                            {!! Form::hidden('green[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'green_id-'.$red_count]); !!}
                            <div class="form-group col-md-2">
                                {!! Form::select('green[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'green_parameter-'.$green_count]); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('green[operator][]', $operator ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'green_operator-'.$green_count]); !!}
                            </div>
                            <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
                                @if($requirement->parameter == 'name')
                                {!! Form::text('green[value][]',$requirement->value, [ 'class'=>'form-control value','id'=>'green_value-'.$green_count]); !!}
                                @else
                                {!! Form::select('green[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'green_value-'.$green_count]); !!}
                                @endif
                            </div>
                            @if($requirement->operator == 'between')
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::text('green[value_2][]',$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'green_value_2-'.$green_count]); !!}
                            </div>
                            @endif
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('green[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'green_status-'.$green_count]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('green[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'green_number-'.$green_count,'min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix {{($green_count==1?'green-trash':'')}}">
                                <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
                            </div>
                        </div>
                        @php $green_count++; @endphp 
                        @endforeach
                        @endif
                    </div>
                    <div class="row blue-box">
                        <div class="col-md-12">
                            <h2 class="blue col-md-3"><i class="fa fa-exclamation" aria-hidden="true"> </i> OverStaffed</h2>
                            <div class="form-group col-md-2">
                                <div class="btn-group">
                                    <br>
                                    <button id="blue-add" class="btn sbold blue-background white-text add" > <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        @if(!count($blue_requirements))
                        <div class="blue-form clearfix">
                            <div class="form-group col-md-2">
                                {!! Form::select('blue[parameter][]', $parameters ,null, [ 'class'=>'form-control parameter','id'=>'blue_parameter-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('blue[operator][]', $operator ,null, [ 'class'=>'form-control operator','id'=>'blue_operator-1']); !!}
                            </div>
                            <div class="form-group col-md-4 clearfix">
                                {!! Form::select('blue[value][]', $values ,null, [ 'class'=>'form-control value','id'=>'blue_value-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::select('blue[value_2][]', $values ,null, [ 'class'=>'form-control value_2','id'=>'blue_value_2-1']); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('blue[status][]', $status ,null, [ 'class'=>'form-control status','id'=>'blue_status-1']); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('blue[number][]', 1, [ 'class'=>'form-control number','id'=>'blue_number-1','min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix blue-trash">
                                <a class="fa fa-trash" ></a>
                            </div>
                        </div>
                        @else
                        @foreach($blue_requirements as $requirement)
                        <div class="blue-form">
                            {!! Form::hidden('blue[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'blue_id-'.$red_count]); !!}
                            <div class="form-group col-md-2">
                                {!! Form::select('blue[parameter][]', $parameters ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'blue_parameter-'.$blue_count]); !!}
                            </div>
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('blue[operator][]', $operator ,$requirement->operator, [ 'class'=>'form-control operator','id'=>'blue_operator-'.$blue_count]); !!}
                            </div>
                            <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix">
                                @if($requirement->parameter == 'name')
                                {!! Form::text('blue[value][]', $requirement->value ,[ 'class'=>'form-control value','id'=>'blue_value-'.$blue_count]); !!}
                                @else
                                {!! Form::select('blue[value][]', $requirement->values ,$requirement->value, [ 'class'=>'form-control value','id'=>'blue_value-'.$blue_count]); !!}
                                @endif
                            </div>
                            @if($requirement->operator == 'between')
                            <div class="form-group col-md-2 clearfix value-2">
                                {!! Form::text('blue[value_2][]',$requirement->value_2, [ 'class'=>'form-control value_2','id'=>'blue_value_2-'.$blue_count]); !!}
                            </div>
                            @endif
                            <div class="form-group col-md-2 clearfix">
                                {!! Form::select('blue[status][]', $status ,$requirement->status, [ 'class'=>'form-control status','id'=>'blue_status-'.$blue_count]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix">
                                {!! Form::number('blue[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'blue_number-'.$blue_count,'min'=> 1]); !!}
                            </div>
                            <div class="form-group col-md-1 clearfix {{($blue_count==1?'blue-trash':'')}}">
                                <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
                            </div>
                        </div>
                        @php $blue_count++; @endphp
                        @endforeach
                        @endif
                    </div>
                    <div class="row submit-requirement">
                        <div class="form-group col-md-12">
                            {!! Form::submit('Submit!', ['class'=> 'btn green pull-right']); !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>
@endsection