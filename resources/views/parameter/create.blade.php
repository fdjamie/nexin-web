@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}" rel="stylesheet" type="text/css" />
<style>
    .value-2{
        display: none;
    }
</style>
@endsection
@php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('success-parameters'))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('success-parameters')}}
                    </li>
                </ul>
            </div>
            @elseif (Illuminate\Support\Facades\Session::has('error-parameters'))
            <div class='alert alert-danger alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                @foreach(session('error-parameters') as $errornode)
                <ul>
                    @foreach($errornode as $error)
                    <li>
                        {{$error}}
                    </li>
                    @endforeach
                </ul>
                @endforeach
            </div>
            @endif

            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-asterisk"></i>{!!  $heading !!}
                        </div>
                    </div>
                    {!! Form::open(['method' => 'post', 'url' => 'service/requirement/parameter']) !!}
                    <div class="form-group ">
                        {!! Form::label('Directorates', 'Directorates', ['class' => 'awesome']) !!}
                        {!! Form::select('directorate_id',$directorates,'', [ 'class'=>'form-control directorate_id']) !!}
                    </div>
                    <div class="form-group ">
                        {!! Form::label('Parameter', 'Parameter', ['class' => 'awesome']); !!}
                        {!! Form::select('parameters',$parameters,'', [ 'class'=>'form-control parameter']) !!}
                    </div>
                    <div class="form-group addinal_parameter" hidden="">
                        {!! Form::label('Parameter', 'Parameter', ['class' => 'awesome']); !!}
                        {!! Form::select('name',[],'', [ 'class'=>'form-control parameter-select']) !!}
                    </div>
                    <div class="form-group row checkboxsAjax" hidden="">
                    </div>
                    <div class="form-group row checkboxs" >
                        @foreach($operators as $key =>$operator)
                        <div class="col-md-3 ">
                            {!! Form::label('Operator', $operator, ['class' => 'awesome ']) !!}
                            {!!  Form::checkbox('operator[]', $key) !!}
                        </div>
                        @endforeach
                    </div>

                    <div class="form-group ">
                        {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                    </div>
                    {!! Form::close() !!}
                    <!--<div class="form-group ">-->
                    <!--<a  href="{--!! route('service.requirement.parameter.index')!!--}">-->
<!--                        <button id="sample_editable_1_2_new" class="btn sbold green" > Back <i class="fa fa-backward"></i></button>
                    </a>
                     </div>-->
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
            <!--<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

<script>
$('.parameter').on('change', function () {
    var values = '';
    var directorate_id = $(".directorate_id option:selected").val();
    var send = {directorate_id: directorate_id};
    if ($(this).val() === 'additional_parameter') {
        jQuery.ajaxSetup({async: false});
        var responseData;
        var url = APP_URL + '/additional-parameter';
        $.get(url, send, function (data) {
            responseData = data;
        });
        $.each(responseData, function (index, value) {
            values += "<option value='" + value + "'>" + value + "</option>";
        });
    
//            test = $(".checkboxs").html();
        $(".checkboxs").hide();
        $(".checkboxs").empty();
        $(".parameter-select").empty().append(values);
        $(".addinal_parameter").show();
//            $(".checkboxsAjax").show();
var operatorURL = APP_URL + '/service/requirement/parameter/create';
      getOperator($(this).val()  ,operatorURL);
        $(".checkboxsAjax").show();

    } else {
    var operatorDatas ='';
     $(".checkboxsAjax").empty();
      $(".checkboxs").empty();
      var operatorURL = APP_URL + '/service/requirement/parameter/create';
      getOperator($(this).val()  ,operatorURL);
//        var parameter = {parameter: $(this).val()};
//        $.get(APP_URL + '/service/requirement/parameter/create', parameter, function (data) {
//            operatorDatas = data;
//        });
//        console.log(operatorDatas);
//        $.each(operatorDatas, function (index, value) {
//            $(".checkboxsAjax").append("<div class='col-md-3'><label for='Operator'class='awesome'>" + value + "</label><input  type='checkbox' name='operator[]' value='" + index + "' /></div");
//        });
        $(".checkboxsAjax").show();
//        $(".checkboxsAjax").hide();
//        $(".checkboxsAjax").empty();
//        $(".checkboxs").append($.cookie("test"));
         $(".checkboxs").hide();
        $(".parameter-select").empty();
        $(".addinal_parameter").hide();
    }
});
 function getOperator(parameter ,url){
        var operatorData;
    jQuery.ajaxSetup({async: false});
        var parameters = {parameter: parameter};
        $.get(url, parameters, function (data) {
            operatorData = data;
        });
//        console.log(operatorData);
    $.each(operatorData, function (index, value) {
            $(".checkboxsAjax").append("<div class='col-md-3'><label for='Operator'class='awesome'>" + value + "</label><input  type='checkbox' name='operator[]' value='" + index + "' /></div");
        });
}
</script>
@endsection