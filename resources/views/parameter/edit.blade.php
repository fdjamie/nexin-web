@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}" rel="stylesheet" type="text/css" />
<style>
    .value-2{
        display: none;
    }
</style>
@endsection
@php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('success-requirement'))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('success-requirement')}}
                    </li>
                </ul>
            </div>
            @endif
            @if (Illuminate\Support\Facades\Session::has('error-parameters'))
            <div class='alert alert-danger alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                @foreach(session('error-parameters') as $errornode)
                <ul>
                    @foreach($errornode as $error)
                    <li>
                        {{$error}}
                    </li>
                    @endforeach
                </ul>
                @endforeach
            </div>
            @endif
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-asterisk"></i>{!!  $heading !!}
                        </div>
                    </div>
                    {!! Form::open(['method' => 'put', 'action' => ['ServiceRequirementParameterController@update',$id]]) !!}

                    <div class="form-group ">
                        {!! Form::label('Parameter', 'Parameter', ['class' => 'awesome']); !!}
                        {!! Form::select('name', $parameterDropdown,$parameterDropdown,['placeholder' => 'Parameter Name' , 'class' => 'form-control','required']); !!}
                    </div>
                    <div class="form-group row">
                        @foreach($operators as $operateKey =>$operator)
                        @foreach($parameters as $key => $parameter)
                        <div class="col-md-3">
                        {!! Form::label('Operator', $operator, ['class' => 'awesome']) !!}
                        {!!  Form::checkbox('operator[]', $operateKey ,((in_array($operateKey,$parameter->operator)?true:false)),['id'=> $operateKey ,'class'=>'check-boxes ']) !!}
                        </div>
                        @endforeach
                        @endforeach
                    </div>
                    <div class="form-group ">
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                    </div>

                    {!! Form::close() !!}
<!--                    <div class="form-group">
                        <a  href="{--!! route('service.requirement.parameter.index') !!--}" >-->
<!--                            <button id="sample_editable_1_2_new" class="btn sbold green" title="Back to home"> Back <i class="fa fa-backward"></i></button>
                        </a>
                    </div>-->
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<!--<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>-->
@endsection