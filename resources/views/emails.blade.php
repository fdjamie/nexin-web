@extends('admin.admin')

@section('extra-css')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        tr,th{
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-envelope-o"></i>Emails
                            </div>
                        </div>

                        <div class="portlet-body">

                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                                <thead>
                                <tr>

                                    <th>#</th>
                                    <th>EMAIL</th>
                                    <th>Type</th>
                                    <th>Sent At</th>

                                     </tr>
                                </thead>
                                <tbody>
                           @foreach($emails as $k=>$m)
                               <tr>
                               <td>{{$k+1}}</td>
                               <td style="width: 50% !important;">
                                   <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#email_{{$k}}">view</button>
                                   <div id="email_{{$k}}" class="collapse">
                                       {!! $m->email !!}
                                   </div>

                                   </td>
                               <td>{{$m->type}}</td>
                               <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($m->created_at))->diffForHumans()}} AT {{$m->created_at}}</td>
                               </tr>

                           @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
@section('extra-js')
    @include('includes.scriptsViewLinks')
    <script>
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/user/delete")}}' +'/'+ $(this).attr('id'));
        });
    </script>
@endsection