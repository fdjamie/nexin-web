

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Nexin Trust</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #4 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('/assets/global/plugins/bootstrap/bootstrap.min.css')}}" type="text/css" >
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{asset('/assets/layouts/layout4/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/assets/layouts/layout4/css/themes/default.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{asset('/assets/layouts/layout4/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" integrity="sha256-SMGbWcp5wJOVXYlZJyAXqoVWaE/vgFA5xfrH3i/jVw0=" crossorigin="anonymous" />-->
    <script src="{{asset('/assets/global/scripts/jquery-2.2.3.min.js')}}"></script>
    <script type="text/javascript"> APP_URL ={!! json_encode(url('/')) !!};</script>
    <script src="{{asset('/assets/global/plugins/bootstrap/bootstrap.min.js')}}" ></script>
    <link href="{{asset('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{url('/assets/images/favicon.ico')}}" />

    <link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
    @yield('extra-css')
</head>
<!-- END HEAD -->
<style>
    .get-date{
        padding: 20px 0px !important;
    }
    #wrapper {
        font-family: Lato;
        font-size: 1.5rem;
        text-align: center;
        box-sizing: border-box;
        color: #333;
    }
    #wrapper #dialog {
        border: solid 1px #ccc;
        margin: 10px auto;
        padding: 20px 30px;
        display: inline-block;
        box-shadow: 0 0 4px #ccc;
        background-color: #FAF8F8;
        overflow: hidden;
        position: relative;
      /*  max-width: 450px;*/
        max-width: 698px;
    }
    #wrapper #dialog h3 {
        margin: 0 0 10px;
        padding: 0;
        line-height: 1.25;
    }
    #wrapper #dialog span {
        font-size: 90%;
    }
    #wrapper #dialog #form {
        max-width: 240px;
        margin: 25px auto 0;
    }
    #wrapper #dialog #form input {
        margin: 0 5px;
        text-align: center;
        line-height: 80px;
        font-size: 50px;
        border: solid 1px #ccc;
        box-shadow: 0 0 5px #ccc inset;
        outline: none;
        width: 20%;
        transition: all 0.2s ease-in-out;
        border-radius: 3px;
    }
    #wrapper #dialog #form input:focus {
        border-color: #007bff;
        box-shadow: 0 0 5px #007bff inset;
    }
    #wrapper #dialog #form input::-moz-selection {
        background: transparent;
    }
    #wrapper #dialog #form input::selection {
        background: transparent;
    }
    #wrapper #dialog #form button {
        margin: 30px 0 50px;
        width: 100%;
        padding: 6px;
        background-color: #007bff;
        border: none;
        text-transform: uppercase;
    }
    #wrapper #dialog button.close {
        border: solid 2px;
        border-radius: 30px;
        line-height: 19px;
        font-size: 120%;
        width: 22px;
        position: absolute;
        right: 5px;
        top: 5px;
    }
    #wrapper #dialog div {
        position: relative;
        z-index: 1;
    }
    #wrapper #dialog img {
        position: absolute;
        bottom: -70px;
        right: -63px;
    }


</style>
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
@include('admin.adminHeader')
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->

<!-- BEGIN CONTENT -->
<div class="clearfix"> </div>
    <div class="container">
        <div class="row" style="margin-top: 86px;">


            <div id="wrapper">
                <div id="dialog">
                    @if (session('error'))
                        <div class="alert alert-danger text-center">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success text-center">
                            {{ session('success') }}
                        </div>
                    @endif
                    <h3>Please enter the 4-digit verification code we sent via Email:</h3>
                   {{-- <span>(we want to make sure it's you before we contact our movers)</span>--}}
                    <form method="POST" class="form-horizontal" role="form" id="form"   action="{{ route('verify-email-code') }}">
                        <input type="text" name="code[]" maxLength="1" size="1" min="0" max="9"  required />
                        <input type="text" name="code[]" maxLength="1" size="1" min="0" max="9"  required /><input type="text" name="code[]" maxLength="1" size="1" min="0" max="9"  required /><input type="text" name="code[]" maxLength="1" size="1" min="0" max="9"   />
                        {{ csrf_field() }}
                        <button class="btn btn-primary btn-embossed" type="submit">Verify</button>
                    </form>

                    <div>
                        Didn't receive the code?<br />
                        <a href="{{route('send-email-verification-code')}}">Send code again</a><br />

                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

<script>
    $(function() {
        'use strict';

        var formInput = $('#form');

        function goToNextInput(e) {
            var key = e.which,
                t = $(e.target),
                sib = t.next('input');

          /*  if (key != 9 && (key < 48 || key > 57)) {
                e.preventDefault();
                return false;
            }

            if (key === 9) {
                return true;
            }
*/
            if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
                if (!sib || !sib.length) {

                    sib = formInput.find('input').eq(0);
                }
                sib.select().focus();
            }

        }

        function onKeyDown(e) {
            var key = e.which;

            if ((key == 8) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
                return ture;
            }
            e.preventDefault();
            return false;
        }

        function onFocus(e) {
            $(e.target).select();
        }

        formInput.on('keyup', 'input', goToNextInput);
        formInput.on('keydown', 'input', onKeyDown);
        formInput.on('click', 'input', onFocus);

    })
</script>