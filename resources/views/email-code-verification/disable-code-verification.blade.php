@extends ('admin.admin')
@section('content')
    <div class="container">
        <div class="row" style="margin-top: 24px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Email Code Verification</strong></div>
                    <div class="panel-body">


                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                            <div class="alert alert-success">
                                Email Code Verification  is Currently <strong>Enabled</strong> for your account.
                            </div>
                            <p>If you are looking to disable Email code Verification. Please confirm your password and
                                Click Disable  Button.</p>
                            <form class="form-horizontal" method="POST" action="{{ route('email-verification-disable') }}">
                                <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                    <label for="change-password" class="col-md-4 control-label">Current Password</label>
                                    <div class="col-md-6">
                                        <input id="current-password" type="password" class="form-control"
                                               name="current-password" required>
                                        @if ($errors->has('current-password'))
                                            <span class="help-block">
                                         <strong>{{ $errors->first('current-password') }}</strong>
                                       </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-5">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-primary ">Disable</button>
                                </div>
                            </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection