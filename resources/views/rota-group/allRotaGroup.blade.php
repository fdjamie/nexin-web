@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-object-group" aria-hidden="true"></i>Rota Groups 
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['RotaGroupController@index']]) !!}
                        <div class="form-group col-md-3 pull-right">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates,$directorate,['class' => 'form-control','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-rota-group'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-rota-group') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-rota-group'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-rota-group') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        @if(__authorize(config('module.rota-groups'),'add'))
                                        <a href="{{url('/rota-group/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        @endif

                                        <a href="{{url('/rota-group/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Rota Groups <i class="fa fa-file-excel-o"></i></button>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th> Name </th>
                                    <th> Description </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rota_groups as $rota_group)
                                <tr class="odd gradeX">
                                    <td>{{$rota_group->id}}</td>
                                    <td>{{$rota_group->name}}</td>
                                    <td>{{$rota_group->description}}</td>
                                    <td class="text-center">
                                        @if(__authorize(config('module.rota-groups'),'edit'))
                                        <a href="{{url('/rota-group/edit/'.$rota_group->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        @endif
                                         @if(__authorize(config('module.rota-groups'),'delete'))
                                        <a data-toggle="modal" href="#small" id='{{$rota_group->id}}' class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/rota-group/delete")}}' +'/'+ $(this).attr('id'));
        });
        $('#directorate_id').change(function(){
            $('form').submit();
        });
    });
</script>
@endsection