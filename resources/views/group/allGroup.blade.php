@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-object-group fa-2x"></i>Groups 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-group'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-group') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-group'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-group') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        @if(__authorize(config('module.groups'),'add'))
                                        <a href="{{url('/group/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        @endif

                                        <a href="{{url('/group/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Group <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($directorates as $directorates_node)
                            @if($directorates_node->id == $directorates[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$directorates_node->id}}" data-toggle="tab">{{ $directorates_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($directorates as $directorates_node)
                            @if($directorates_node->id == $directorates[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$directorates_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$directorates_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Id</th>
                                                <th> Name </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($directorates_node->group))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($directorates_node->group as $group)
                                            <tr class="odd gradeX">
                                                <td width='25%'>{{$group->id}}</td>
                                                <td width='25%'>{{$group->name}}</td>
                                                <td class="text-center">
                                                    @if(__authorize(config('module.groups'),'edit'))
                                                    <a href="{{url('/group/edit/'.$group->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                     @endif
                                                        @if(__authorize(config('module.groups'),'delete'))
                                                        <a data-toggle="modal" href="#small" id="{{$group->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        @endif

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/group/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection