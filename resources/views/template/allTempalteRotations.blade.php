@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage Rotation Template</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
<!--        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/rotation-template/">Rotation Template</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-repeat fa-2x"></i>Rotation Templates 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="/rotation-template/new">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" data-toggle="modal" href="#basic"> Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead class="text-center">
                                <tr>
                                    <th>Id</th>
                                    <th> Rotation Name </th>
                                    <th> Weeks Cycle </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($rotations as $rotation)
                                @if(isset($rotation) && $rotation->type == 'rotation')
                                <tr>
                                    <td>{{ $rotation->id }}</td>
                                    <td>{{ $rotation->name }}</td>
                                    <td>{{ count($rotation->contents->shift) }}</td>
                                    <td class="text-center">
                                        <a href="/rotation-template/edit/{{$rotation->id}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id="{{$rotation->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endif
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '/rotation-template/delete/' + $(this).attr('id'));
    });
</script>
@endsection
