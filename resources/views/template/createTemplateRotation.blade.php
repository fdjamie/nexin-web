@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Rotation Template</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
<!--        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/rotation-template/">Rotation Template</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-repeat"></i>Add Rotation Template
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['HomeController@storeTemplateRotation']]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <table class="table" id="empty-container">
                            <tr>
                                @foreach($weekdays as $key=>$value)
                                <th>{!! Form::label('name', $value); !!}</th>
                                @endforeach
                            </tr>
                            <tr class="template-contents">
                                @for ($i = 0; $i < $total_weeks; $i++)
                                @foreach($weekdays as $key=>$value)
                                <td>{!! Form::select('contents[shift]['.$i.']['.$key.']', $shifts, '',['placeholder' => 'Off' ]); !!}</td>
                                @endforeach
                                @endfor
                                <td><a class="fa fa-trash" style="display:none;cursor: pointer;"></a></td>
                            </tr>
                        </table>
                        <div class="form-group">
                            {!! Form::button('Add Week!', ['class'=> 'btn btn-success addweek']); !!}
                            {!! Form::submit('Add Template!', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script type="text/javascript" src="/js/cloneweek.js"></script>
@endsection