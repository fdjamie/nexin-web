@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
@php
$bleep = explode('^',$staff->individual_bleep);
if(!isset($bleep[1]))$bleep[1]=1;
$qualifications = explode('^',$staff->qualifications);
if(!isset($qualifications[1]))$qualifications[1]=1;
$mobile = explode('^',$staff->mobile);
if(!isset($mobile[1]))$mobile[1]=1;
@endphp
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>{{$staff->name}}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/staff">Edit Staff Member</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user fa-2x"></i>{{$staff->name}}
                        </div>
                    </div>
                    <div class="portlet-body row ">
                        <div class="row">
                            <div class="col-md-6 profile-sidebar">
                                <div class="profile-userpic">
                                    <img src="/assets/images/staff_profile/{{$staff->profile_pic}}" class="img-responsive"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-3">GMC : </div>          <div class="col-md-9">{{$staff->gmc}}</div>
                                <div class="col-md-3">Speciality : </div>   <div class="col-md-9">{{$speciality->name}}</div>
                                <div class="col-md-3">Grade : </div>        <div class="col-md-9">{{$grade->name}}</div>
                                <div class="col-md-3">Role : </div>         <div class="col-md-9">{{$role->name}}</div>
                                @if ($qualifications[1] == 1)
                                <div class="col-md-3">Qualifactions : </div><div class="col-md-9">{{$qualifications[0]}}</div>
                                @endif
                                @if ($bleep[1] == 1)
                                <div class="col-md-3">Individual Bleep :</div><div class="col-md-9">{{$bleep[0]}}</div>
                                @endif
                                <div class="col-md-3">Email : </div>        <div class="col-md-9">{{$staff->email}}</div>
                                @if ($mobile[1] == 1)
                                <div class="col-md-3">Mobile : </div>       <div class="col-md-9">{{$mobile[0]}}</div>
                                @endif
                            </div>
                        </div>

                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
@ensection