@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Current Working Status Dashboard</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>ID</th><th>Entity Name</th><th>Status</th><th>% age</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><a href="{{ url('/speciality') }}">Speciality</a></td>
                                <td>DONE & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><a href="{{ url('/trust') }}">Trust</a></td>
                                <td>DONE & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><a href="/division">Division</a></td>
                                <td>DONE & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><a href="/directorate">Directorate</a></td>
                                <td>DONE & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><a href="/site">Sites</td>
                                <td>Done & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td><a href="/location">Locations</td>
                                <td>Done & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td><a href="/grade/">Grades</td>
                                <td>Done & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td><a href="/role/">Roles</td>
                                <td>Done & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td><a href="/staff/">Staff</td>
                                <td>Done & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td><a href="/shift/">Shift</td>
                                <td>Done & Ready to Test</td>
                                <td>100%</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td><a href="/admin/">Tree Navigation View</td>
                                <td>sample ready for feedback</td>
                                <td>100%</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
