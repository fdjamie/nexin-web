<!-- Copyright Section Begins -->
<div class="container">
    <div class="row">
        <?php
        if (Illuminate\Support\Facades\Session::has('example')) {
            echo "<div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            <em><pre>";
            print_r(json_decode(session('example'), TRUE));
            echo "</pre></em></div>";
        }
        ?>
    </div>
</div>
<div class="well container-fluid"> 
    <div class="">
        <!-- Copyright Title -->
        <div class="col-md-12">
            <div class="col-md-6">
                <ul>
                    <a href="#">About Us</a><br/>
                    <a href="#">Privacy Policy</a><br/>
                    <a href="#">Terms & Conditions</a><br/>
                    <a href="#">Contact Us</a>
                </ul>
            </div>
            <div class="col-md-6 text-right"></div>
        </div>
    </div>
</div>
<section class="copyright" id="copyright">
    <div class="container">
        <div class="col-md-12 no-padding">
            <p class="col-md-3 no-padding text-right"></p>
            <p class="col-md-6 no-padding">copyrights reserved by &copy; sitedirect.co</p>
            <p class="col-md-3 no-padding text-right"></p>
        </div>        
    </div>
</section>
<!-- Copyright Section Ends -->