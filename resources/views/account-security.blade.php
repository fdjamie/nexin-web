<style>
    th,td{
        text-align: center;
    }

    </style>
@extends ('admin.admin')
@section('content')
    <div class="container">
        <div class="row" style="margin-top: 24px;">

                @if (session('error'))
                    <div class="alert alert-danger text-center alert-dismissable">
                        {{ session('error') }}

                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success text-center alert-dismissable">
                        {{ session('success') }}

                    </div>
                @endif


            <div class="col-md-6 ">

                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Secure Your Account</strong></div>
                    <div class="panel-body">

                        <div class="row" >

                            <div class="col-md-6">
                                Google Authenticator App
                            </div>
                            <div class="col-md-6">
                                @if(!empty(Auth::user()->google2Fauth) && Auth::user()->google2Fauth->enable)
                                    <span class="badge badge-primary"><i class="fa fa-check"></i> Enabaled</span>
                                    <a href="{{route('google-2f-authenticate')}}" class="btn-sm  btn-danger" >Clike Here To Disable</a>
                                @else

                                    <span class="badge badge-danger"><i class="fa fa-times"></i> Disabled</span>
                                    <a href="{{route('google-2f-authenticate')}}" class="btn-sm btn-primary" >Clike Here To Enable</a>
                                    @endif



                            </div>

                        </div>
                        <hr />
                        <div class="row" >
                            <div class="col-md-6">
                                Email Code Verification
                            </div>


                            <div class="col-md-6">

                                @if(!empty(Auth::user()->emailSecurity) && Auth::user()->emailSecurity->enable)
                                    <span class="badge badge-primary"><i class="fa fa-check"></i> Enabaled</span>
                                    <a href="{{route('email-verification-disable')}}" class="btn-sm btn-danger" >Clike Here To Disable</a>
                                @else

                                    <span class="badge badge-danger"><i class="fa fa-times"></i> Disabled</span>
                                    <a href="{{route('email-verification-enable')}}" class="btn-sm btn-primary" >Clike Here To Enable</a>
                                @endif

                            </div>
                        </div>
                        <hr />
                        <div class="row" >
                            <div class="col-md-6">
                                SMS Code Verification
                            </div>


                            <div class="col-md-6">

                                @if(!empty(Auth::user()->smsSecurity) && Auth::user()->smsSecurity->enable)
                                    <span class="badge badge-primary"><i class="fa fa-check"></i> Enabaled</span>
                                    <a href="{{route('sms-verification-disable')}}" class="btn-sm btn-danger" >Clike Here To Disable</a>
                                @else

                                    <span class="badge badge-danger"><i class="fa fa-times"></i> Disabled</span>
                                    <a href="{{route('sms-verification-enable')}}" class="btn-sm btn-primary" >Clike Here To Enable</a>
                                @endif

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Account Login Details</strong></div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover  table-bordered" >
                        <thead>
                        <tr>
                            <td>IP</td>
                            <td>Device Type</td>
                            <td>Device</td>
                            <td>Browser</td>
                            <td>Verified By</td>
                        </tr>
                        </thead>
                        <tbody>

                        @if(!empty($loginDetails=Auth::user()->loginDetail))

                            @foreach($loginDetails as $d)
                               <tr >
                                <td>{{$d->ip}}</td>
                                <td>{{$d->device_type}}</td>
                                <td>{{empty($d->device)?'WebKit':$d->device}}</td>
                                <td>{{$d->browser}}</td>
                                <td>

                                @if(empty($d->verified_by))
                                        <span class="badge badge-danger"><i class="fa fa-times-circle"></i> Not Verified Yet</span>

                                    @else
                                    @if(!empty($d->verified_by['login_verified_by']))
                                    @if(in_array('sms',$d->verified_by['login_verified_by']))
                                                <span class="badge badge-success"><i class="fa fa-check"></i> SMS </span>
                                        @endif
                                        @if(in_array('email',$d->verified_by['login_verified_by']))
                                            <span class="badge badge-success"><i class="fa fa-check"></i> Email </span>
                                        @endif
                                        @if(in_array('googleApp',$d->verified_by['login_verified_by']))
                                            <span class="badge badge-success"><i class="fa fa-check"></i> Google Auth App </span>
                                        @endif
                                        @endif
                                    @endif

                                </td>
                               </tr>

                                @endforeach





                            @endif


                        </tbody>

                    </table>


                </div>
            </div>
            </div>
        </div>
    </div>





@endsection