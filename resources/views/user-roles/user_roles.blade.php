@extends('admin.admin')

@section('extra-css')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        td,th{
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-shield"></i>USER ROLES
                            </div>
                        </div>

                        @if (!empty($errors))
                            @foreach($errors as $err)
                            <div class='alert alert-danger alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                {{ $err }}
                            </div>
                            @endforeach

                        @endif
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a href="{{url('/user-roles/new')}}">
                                                <button id="sample_editable_1_2_new" class="btn sbold green" data-toggle="modal" href="#basic"> Add New     <i class="fa fa-plus"></i></button>
                                            </a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-content">


                                                    <table class="table table-striped table-bordered table-hover table-checkable" >
                                                        <thead>
                                                        <tr>
                                                            <th> # </th>
                                                            <th> Role</th>
                                                            <th> Actions </th>
                                                            <th> Permissions </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($trustRoles))

                                                            @foreach($trustRoles as $K=>$r)
                                                               <tr>
                                                                <td> {{$K+1}} </td>
                                                                <td> {{$r->name}} </td>
                                                                <td>
{{--
                                                                    <a href="{{url('/trust-roles/edit/'.$r->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
--}}
{{--                                                        <a data-toggle="modal" href="#small" id="{{$r->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
--}}
                                                                </td>
                                                                   <td>
                                                                      {{-- The Pivote Id is basically trust role id from role_trust table --}}
                                                                       <a href="{{url('/role/permissions/'.base64_encode($r->pivot->id))}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                                               </tr>
                                                         @endforeach
                                                            @endif

                                                        </tbody>
                                                    </table>
                                                </div>

                                        </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
@section('modal')
    @include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
    @include('includes.scriptsViewLinks')
    <script>
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/role/delete")}}' +'/'+ $(this).attr('id'));
        });
    </script>
@endsection