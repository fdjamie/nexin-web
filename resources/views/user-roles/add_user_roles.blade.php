@extends('admin.admin')

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa fa-shield"></i>Add User Role
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        <div class="portlet-body">
                            {!! Form::open(['method' => 'post', 'action' => ['UserRolesController@store']]) !!}
                            <div class="form-group">
                                {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                                {!! Form::text('name', '',['placeholder' => 'Enter Role' , 'class' => 'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('extra-js')
    <script src="{{url('/js/role.js')}}" type="text/javascript"></script>
@endsection
