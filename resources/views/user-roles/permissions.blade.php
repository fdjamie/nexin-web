@extends('admin.admin')
@section('extra-css')
    <style>
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }

        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-3 ">

                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa fa-shield"></i> All  Roles
                            </div>
                        </div>


                        <div class="portlet-body">
                            <table class="table table-condensed table-bordered">
                                <tbody>
                                <tr>
                                    <td>Role</td>
                                    <td class="text-center">Permissions</td>
                                </tr>
                                @foreach($trustRoles as $r)
                                    <tr>
                                        <td>{{$r->name}}</td>
                                        <td class="text-center">
                                            @if(base64_decode($trustRoleId)==$r->pivot->id)
                                                <span class="badge badge-success"> Active  <i class="fa fa-arrow-right"></i></span>
                                            @else
                                                <a href="{{url('/role/permissions/'.base64_encode($r->pivot->id))}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
                <div class="col-md-9 ">

                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa fa-shield"></i>Add Permissions To Role
                            </div>
                        </div>
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                @foreach(session()->get('error') as $e)
                                {{ $e }}
                                    @endforeach
                            </div>
                        @endif

                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        <div class="portlet-body">

                              <form action="{{route('add-role-permissions')}}" method="post">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="trustRoleId" value="{{$trustRoleId}}">
                            <table class="table table-condensed table-bordered">
                                <tbody>

                                <tr>
                                    <td>
                                        Module
                                    </td>
                                    @if(!empty($permissions))
                                        @foreach($permissions as $p)
                                            <td>CAN {{strtoupper($p->name)}}</td>
                                        @endforeach
                                    @endif
                                </tr>
                                 @if(!empty($modules))
                                        @foreach($modules as $k=>$m)
                                            <tr id="{{$k}}">
                                            <td>{{$m->name}}</td>
                                            @if(!empty($permissions))
                                                @foreach($permissions as $pk=>$p)
                                                    <td><!-- Material checked -->
                                                        <div class="material-switch ">
                                                            <input onchange="checkForViewPermission(this)" data-name="{{strtolower($p->name)}}" id="someSwitchOptionPrimary_{{$k.$pk}}" value="{{base64_encode($m->id."_".$p->id)}}" name="permission[]"
                                                                   type="checkbox" {{(in_array($m->id."_".$p->id,$rolePermissions))?'checked':''}}/>
                                                            <label for="someSwitchOptionPrimary_{{$k.$pk}}" class="label-success"></label>
                                                        </div>
                                                    </td>
                                                    @endforeach
                                                @endif
                                            </tr>

                                @endforeach
                                @endif
                                </tbody>
                            </table>

                                  <button class="btn btn-primary " type="submit">Save</button>
                              </form>
                        </div>


                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->





            </div>

        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
@endsection
@section('extra-js')
    <script src="{{url('/js/role.js')}}" type="text/javascript"></script>
@endsection
