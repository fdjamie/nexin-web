@extends('admin.admin')
<!-- START CONTENT -->
@section('extra-css')
<style>
    .fontBig{
        font-size: 17px;
    }
</style>
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Working Linkss</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        {{--!! Breadcrumbs::render('rotation/new') !!--}}
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-list"></i>List of All Linkss
                        </div>
                    </div>
                    <div class="portlet-body">
                        <h3>1. Trust Links </h3>
                        <ul class="fontBig">
                            <li><a href="/trust">All Trusts</a></li>
                            <li><a href="/trust/new">Add Trust</a></li>
                            <li><b>List of all trusts Urls : </b>
                                <ul>
                                    @foreach($trusts as $trust)
                                    <li><a href="/trust/edit/{{$trust->id}}">{{$trust->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>2. Divisions Links </h3>
                        <ul class="fontBig">
                            <li><a href="/division">All Divisions</a></li>
                            <li><a href="/division/new">Add Division</a></li>
                            <li><b>List of all divisions Urls : </b>
                                <ul>
                                    @foreach($divisions as $division)
                                    <li><a href="division/edit/{{$division->id}}">{{$division->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>3. Directorates Links </h3>
                        <ul class="fontBig">
                            <li><a href="/directorate">All Directorate</a></li>
                            <li><a href="/directorate/new">Add Directorate</a></li>
                            <li><b>List of all directorate Urls : </b>
                                <ul>
                                    @foreach($directorates as $directorate)
                                    <li><a href="directorate/edit/{{$directorate->id}}">{{$directorate->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>4. Directorate Staff Links </h3>
                        <ul class="fontBig">
                            <li><a href="/directorate-staff">All Directorate Staff</a></li>
                            <li><a href="/directorate-staff/new">Add Directorate Staff</a></li>
                            <li><b>List of all directorate staff Urls : </b>
                                <ul>
                                    @foreach($staffs as $staff)
                                    <!--<li><a href="directorate-staff/edit/{staff->member->id}">{staff->member->name}</a></li>-->
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>5. Directorate Specialities Links </h3>
                        <ul class="fontBig">
                            <li><a href="/directoratespeciality">All Directorate Speciality</a></li>
                            <li><a href="/directoratespeciality/new">Add Directorate Speciality</a></li>
                            <li><b>List of all directorate speciality Urls : </b>
                                <ul>
                                    @foreach($specialties as $specialty)
<!--                                    <li><a href="directoratespeciality/edit/{specialty->id}">{specialty->name}</a></li>-->
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>6. Services Links </h3>
                        <ul class="fontBig">
                            <li><a href="/service">All Services</a></li>
                            <li><a href="/service/new">Add Service</a></li>
                            <li><b>List of all services Urls : </b>
                                <ul>
                                    @foreach($services as $service)
                                    <li><a href="service/edit/{{$service->id}}">{{$service->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>7. Services Template Links </h3>
                        <ul class="fontBig">
                            <li>Service Template can be viewed On all Service Page, each service has a option to view its regarding template.</li>
<!--                            <li><a href="/service/template/add/{services[0]->id}">Add Service Template</a></li>-->
                            <li><b>List of all service template Urls : </b>
                                <ul>
                                    @foreach($services as $service)
                                    <li><a href="service/template/edit/{{$service->id}}">{{$service->name}} - Service Template</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>8. Post Links </h3>
                        <ul class="fontBig">
                            <li><a href="/post">All Posts</a></li>
                            <li><a href="/post/new">Add Post</a></li>
                            <li><b>List of all directorate speciality Urls : </b>
                                <ul>
                                    @foreach($posts as $post)
                                    <li><a href="post/edit/{{$post->id}}">{{$post->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>9. Rotations Links </h3>
                        <ul class="fontBig">
                            <li><a href="/rotation">All Rotations</a></li>
                            <li><a href="/rotation/new">Add Rotation</a></li>
                            <br>
                        </ul>
                        <h3>10. Sites Links </h3>
                        <ul class="fontBig">
                            <li><a href="/site">All Sites</a></li>
                            <li><a href="/site/new">Add Site</a></li>
                            <li><b>List of all directorate speciality Urls : </b>
                                <ul>
                                    @foreach($sites as $site)
                                    <li><a href="site/edit/{{$site->id}}">{{$site->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>11. Locations Links </h3>
                        <ul class="fontBig">
                            <li><a href="/location">All Locations</a></li>
                            <li><a href="/location/new">Add Location</a></li>
                            <li><b>List of all directorate location Urls : </b>
                                <ul>
                                    @foreach($locations as $location)
                                    <li><a href="location/edit/{{$location->id}}">{{$location->name}}</a> </li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>12. Locations Links </h3>
                        <ul class="fontBig">
                            <li><a href="/location">All Locations</a></li>
                            <li><a href="/location/new">Add Location</a></li>
                            <li><b>List of all directorate location Urls : </b>
                                <ul>
                                    @foreach($locations as $location)
                                    <li><a href="location/edit/{{$location->id}}">{{$location->name}}</a> </li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>13. Rota Template Links </h3>
                        <ul class="fontBig">
                            <li><a href="/rota-template">All Rota Templates</a></li>
                            <li><a href="/rota-template/new">Add Rota Template</a></li>
                            <li><b>List of all Rota Template Urls : </b>
                                <ul>
                                    @foreach($rota_templates as $rota_template)
                                    <li><a href="rota-template/edit/{{$rota_template->id}}">{{$rota_template->name}}</a> </li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>14. Shift Types Links </h3>
                        <ul class="fontBig">
                            @foreach($rota_templates as $rota_template)
                            <li>{{$rota_template->name}}
                                <ul>
                                    <li><a href="/shift-type/{{$rota_template->id}}">{{$rota_template->name}} - Shift Types</a></li>
                                    <li><a href="/shift-type/new/{{$rota_template->id}}">{{$rota_template->name}} - New Shift Type</a></li>
                                </ul>
                            </li>
                            @endforeach
                            <li><b>List of all shift types Urls : </b>
                                <ul>
                                    @foreach($shift_types as $shift_type)
                                    <li><a href="shift_type/edit/{{$shift_type->id}}">{{$shift_type->name}}</a> </li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>15. Profile Links </h3>
                        <ul class="fontBig">
                            <li><a href="/profile">All Profiles</a></li>
                            <li><b>List of all profiles Urls : </b>
                                <ul>
                                    @foreach($staffs as $staff)
<!--                                    <li><a href="profile/{staff->member->id}">{staff->member->name}</a></li>-->
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>16. Users Links </h3>
                        <ul class="fontBig">
                            <li><a href="/user">All User</a></li>
                            <li><a href="/user/new">Add User</a></li>
                            <li><b>List of all Users Urls : </b>
                                <ul>
                                    @foreach($users as $user)
                                    <li><a href="user/edit/{{$user->id}}">{{$user->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>17. Grades Links </h3>
                        <ul class="fontBig">
                            <li><a href="/grade">All Grades</a></li>
                            <li><a href="/grade/new">Add Grade</a></li>
                            <li><b>List of all Users Urls : </b>
                                <ul>
                                    @foreach($grades as $grade)
                                    <li><a href="grade/edit/{{$grade->id}}">{{$grade->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>18. Roles Links </h3>
                        <ul class="fontBig">
                            <li><a href="/role">All Roles</a></li>
                            <li><a href="/role/new">Add Role</a></li>
                            <li><b>List of all Role Urls : </b>
                                <ul>
                                    @foreach($roles as $role)
                                    <li><a href="role/edit/{{$role->id}}">{{$role->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                        <h3>19. Staff Links </h3>
                        <ul class="fontBig">
                            <li><a href="/staff">All Staff</a></li>
                            <li><a href="/staff/new">Add Staff</a></li>
                            <li><b>List of all Staff Urls : </b>
                                <ul>
                                    @foreach($staffs as $staff)
<!--                                    <li><a href="staff/edit/{staff->member->id}">{staff->member->name}</a></li>-->
                                    @endforeach
                                </ul>
                            </li>
                            <br>
                        </ul>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
<!-- END CONTENT -->
