@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css" integrity="sha256-WiW45+2MJLXlf9nO+kdeRR8mV+OUBMF6VwS/4/IX2Fc=" crossorigin="anonymous" />
<style>
    .color-picker{
        width: 85%;
        float: left;
    }
    .color-box{
        height: 33px;
        width: 6%;
        border: 1px solid #000;
        float: left;
        margin-left: 10px;
        margin-top: 5px;
    }
</style>
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-paint-brush" aria-hidden="true"></i>{{$color->name}} 
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['ColorController@update', $color->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $color->name,['class' => 'form-control','id'=>'name']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('letter', 'Letter', ['class' => 'awesome']); !!}
                            {!! Form::text('letter', $color->letter,['class' => 'form-control','id'=>'letter']); !!}
                        </div>
                        <div class="form-group color-picker">
                            {!! Form::label('code', 'Color', ['class' => 'awesome']); !!}
                            {!! Form::text('code', $color->code,['class' => 'form-control','id'=>'code']); !!}
                        </div>
                        <br>
                        <div class="color-box" style="background:{{$color->code}};"></div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Color', ['class' => 'awesome']); !!}
                            {!! Form::select('position', $color_array, $color->position, ['class' => 'form-control','max'=>5,'min'=>1,'id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!}
                            {!! Form::select('place', [0=>'Less then',1=>'Greater then'],null,['class' => 'form-control','id'=>'place']); !!}
                        </div>
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js" integrity="sha256-/48jq3JSvRjSX+/bZosYmT29RkZk4lPukj1HKRfABU4=" crossorigin="anonymous"></script>
<script>
$('#code').colorpicker()
        .on('changeColor', function () {
            $('.color-box').css('background',$(this).val());
        });
</script>
@endsection
