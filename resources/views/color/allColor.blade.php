@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-paint-brush" aria-hidden="true"></i>Colros
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-color'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-color') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-color'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-color') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="/color/new">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
<!--                                <div class="col-md-3">
                                    <div class="btn-group">
                                        <a href="/site/export">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning"> Export Sites <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                            <thead>
                                <tr>
                                    <th> Position</th>
                                    <th> Name </th>
                                    <th> Letter </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($colors as $color)
                                <tr>
                                    <td>{{$color->position}}</td>
                                    <td>{{$color->name}}</td>
                                    <td>{{$color->letter}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/color/edit/'.$color->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id="{{$color->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/color/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection