@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css" integrity="sha256-WiW45+2MJLXlf9nO+kdeRR8mV+OUBMF6VwS/4/IX2Fc=" crossorigin="anonymous" />
<style>
    .color-picker{
        width: 85%;
        float: left;
    }
    .color-box{
        height: 33px;
        width: 6%;
        border: 1px solid #000;
        float: left;
        margin-left: 10px;
        margin-top: 5px;
    }
</style>
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-paint-brush" aria-hidden="true"></i> Add Color
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-color'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-color') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['ColorController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['class' => 'form-control','id'=>'name']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('letter', 'Letter', ['class' => 'awesome']); !!}
                            {!! Form::text('letter', '',['class' => 'form-control','id'=>'letter']); !!}
                        </div>
                        <div class="form-group color-picker">
                            {!! Form::label('code', 'Color', ['class' => 'awesome']); !!}
                            {!! Form::text('code', '#D9534F',['class' => 'form-control','id'=>'code']); !!}
                        </div>
                        <br>
                        <div class="color-box" style="background:#D9534F;"></div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Color', ['class' => 'awesome']); !!}
                            {!! Form::select('position', $color_array, null, ['class' => 'form-control','max'=>5,'min'=>1,'id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!}
                            {!! Form::select('place', [0=>'Less then',1=>'Greater then'],null,['class' => 'form-control','id'=>'place']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!--            <div class="col-md-6 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-home"></i>Upload Sites
                                    </div>
                                </div>
                                @if($errors->has())
                                <div class='alert alert-danger alert-dismissible' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="portlet-body">
                                    {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['SiteController@import']]) !!}
                                    <div class="form-group">
                                        {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                                        {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>-->
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js" integrity="sha256-/48jq3JSvRjSX+/bZosYmT29RkZk4lPukj1HKRfABU4=" crossorigin="anonymous"></script>
<script>
$('#code').colorpicker()
        .on('changeColor', function () {
            $('.color-box').css('background',$(this).val());
        });
</script>
@endsection
