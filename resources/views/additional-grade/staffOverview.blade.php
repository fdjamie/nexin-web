@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .grade-scale span {
        border: 1px solid black;
        display: inline-block;
        /*width: auto;*/
    }
    .grade-scale span p {
        margin: 0px;
        padding: 3px;
        word-wrap: break-word;
    }
    .roles {
        margin-top: 10px;
    }
    .roles span {
        border: 1px solid black;
        display: inline-block;
        width: auto;
    }
    .roles span p {
        margin: 0px;
        padding: 5px 10px;
        word-wrap: break-word;
    }

</style>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage Grades</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-users"></i>Staff Overview
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@showAllAdditionalGradeTest'],'id'=>'directorate_form']) !!}
                        <div class="form-group col-md-offset-4 col-md-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-grade'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-grade') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-grade'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-grade') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            @foreach($specialties as $speciality)
                            @if($speciality->id == $specialties[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality->id}}" data-toggle="tab">{{ $speciality->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialties as $speciality)
                            @php 
                            $additional_grade = ''; 
                            @endphp
                            @if($speciality->id == $specialties[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$speciality->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$speciality->id}}">
                                    @endif
                                    @foreach($speciality->additional_grade as $grade)
                                    @php
                                    $additional_grade = $additional_grade.$grade->name.',';
                                    @endphp
                                    @endforeach
                                    <div class="form-group">
                                        {!! Form::label('directorate_grades', 'Grades', ['class' => 'awesome']); !!}
                                        {!! Form::text('directorate_grades', implode(', ',$grades_text ), [ 'class'=>'form-control','id'=>'directorate_grades']); !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('role_id', 'Roles', ['class' => 'awesome']); !!}
                                        {!! Form::text('directorate_roles', implode(', ', $role_text), [ 'class'=>'form-control','id'=>'directorate_grades']); !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('role_id', 'Additional Grades', ['class' => 'awesome']); !!}
                                        {!! Form::text('directorate_grades', $additional_grade, [ 'class'=>'form-control','id'=>'directorate_grades']); !!}
                                    </div>
                                </div>
                                @endforeach
                                <div class="grade-scale">
                                    @foreach($directorate_grades as $grade)
                                    <span style="width: {{$grade_width}}%;background: {{$grade['color']}};color:#fff"><p>{{$grade['name']}}</p></span>
                                    @endforeach
                                </div>
                                <div class="roles">
                                    @foreach($directorate_roles as $role)
                                    <span style="width: {{$role_width}}%;background: {{$role['color']}};color:#fff"><p>{{$role['name']}}</p></span>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body"> You want to delete this record? </div>
            <div class="modal-footer">
                <a href="" id='delete-button'><button type="submit" class="btn btn-danger" >Delete</button></a>
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $(document).ready(function () {
        $('#directorate_id').change(function () {
            $('#directorate_form').submit();
        });
    });
    </script>
@endsection