@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit Grade</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-star"></i>{{$grade->name}} 
                        </div>
                        <div class="form-group col-md-offset-4 col-md-4">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::text('directorate',$grade->directorate_speciality->directorate->name, [ 'class'=>'form-control','id'=>'directorate','disabled'=>'disabled']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-grade'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-grade') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@updateAdditionalGrade', $grade->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_speciality_id', $specialties ,$grade->directorate_speciality_id, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                            {!! Form::hidden('directorate_id',$grade->directorate_speciality->directorate->id, ['id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $grade->name ,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('grade_id', 'Grade', ['class' => 'awesome']); !!}
                            {!! Form::select('grade_id', $grades ,$grade->grade_id, [ 'class'=>'form-control','id'=>'grade_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Grade', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('position', $additional_grades ,$grade->position, [ 'class'=>'form-control','id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!} 
                            {!! Form::select('place', [0 => 'Before', 1 => 'After'] ,null, [ 'class'=>'form-control','id'=>'place']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::hidden('current_position',$grade->position ); !!}
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/additional-grade.js')}}" type="text/javascript"></script>
@endsection
