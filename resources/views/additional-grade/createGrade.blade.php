@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Grade</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa fa-star"></i>Add Grade
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@createAdditionalGrade'],'id'=>'directorate_form']) !!}
                        <div class="form-group col-md-offset-4 col-md-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-grade'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-grade') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['HomeController@storeAdditionalGrade']]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!} 
                            {!! Form::select('directorate_speciality_id', $specialties ,null, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('grade_id', 'Grade', ['class' => 'awesome']); !!}
                            {!! Form::select('grade_id', $grades ,null, [ 'class'=>'form-control','id'=>'grade_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Grade', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('position', $additional_grades ,null, [ 'class'=>'form-control','id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!} 
                            {!! Form::select('place', [0 => 'Before', 1 => 'After'] ,null, [ 'class'=>'form-control','id'=>'place']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-star"></i>Upload Grades
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['HomeController@importAdditionalGrade']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file'); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script>
    $(document).ready(function () {
        $('#directorate_id').change(function () {
            $('#directorate_form').submit();
        });
    });
</script>
<script src="{{url('/js/additional-grade.js')}}" type="text/javascript"></script>
@endsection
