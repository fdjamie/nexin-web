@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Staff Members</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-users fa-2x"></i>Staff Members 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <table class="table table-bordered">
                            <thead class="text-center">
                                <tr>
                                    <th> Name </th>
                                    <th> Grade </th>
                                    <th> Role </th>
                                    <th> GMC </th>
                                    <th> Result </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($view_data as $staff)
                                @if(isset($staff['error_gmc']) || isset($staff['error_grade']) || isset($staff['error_role']))
                                <tr class="bg-danger">
                                    @else
                                <tr class="odd gradeX">
                                    @endif    
                                    <td>{{ $staff['name'] }}</td>
                                    <td>{{ $staff['grade'] }}</td>
                                    <td>{{ $staff['role'] }}</td>
                                    <td>{{ $staff['gmc'] }}</td>
                                    <td class="table-danger" width='25%'>
                                        <ul>
                                            @if(isset($staff['error_gmc']))<li class="list-group-item-danger"> {{$staff['error_gmc']}}   </li>@endif 
                                            @if(isset($staff['error_role']))<li class="list-group-item-danger"> {{$staff['error_role']}}   </li>@endif 
                                            @if(isset($staff['error_grade']))<li class="list-group-item-danger"> {{$staff['error_grade']}}   </li>@endif 
                                        </ul>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/staff/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['HomeController@uploadStaffData'] ]) !!}
                                        {!! Form::hidden('replace',$replace); !!}
                                        {!! Form::hidden('file_name',$file_name); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green']); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection