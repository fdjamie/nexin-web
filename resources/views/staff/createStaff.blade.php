@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Staff Memnber</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!--        <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="/staff/">Staff Members</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa fa-shield"></i>Add Staff Member
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-staff'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-staff') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['HomeController@storeStaff'] ]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name','class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gmc', 'GMC', ['class' => 'awesome']); !!}
                            {!! Form::text('gmc', '',['placeholder' => 'Enter GMC','class' => 'form-control']); !!}
                        </div>
                        <!--<div class="form-group">-->
                        {{--!! Form::label('speciality_id', 'Speciality', ['class' => 'awesome']); !!--}}
                        {{--!! Form::select('speciality_id', $specialities , null, ['placeholder' => 'Select Speciality', 'class'=>'form-control']); !!--}}
                        <!--</div>-->
                        <div class="form-group">
                            {!! Form::label('grade_id', 'Grade', ['class' => 'awesome']); !!}
                            {!! Form::select('grade_id', $grades , null, ['placeholder' => 'Select Grade', 'class'=>'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('role_id', 'Role', ['class' => 'awesome']); !!}
                            {!! Form::select('role_id', $roles , null, ['placeholder' => 'Select Role', 'class'=>'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('qualifications', 'Qualifications', ['class' => 'awesome']); !!}
                            {!! Form::text('qualifications', '',['placeholder' => 'Enter Qualifications','class' => 'form-control']); !!}
                            {!! Form::label('qualifications_public', 'Public', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('qualifications_public', 1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('individual_bleep', 'Individual Bleep', ['class' => 'awesome']); !!}
                            {!! Form::text('individual_bleep', '',['placeholder' => 'Individual Bleep','class' => 'form-control']); !!}
                            {!! Form::label('individual_bleep_public', 'Public', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('individual_bleep_public', 1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email', ['class' => 'awesome']); !!}
                            {!! Form::text('email', '',['placeholder' => 'Enter Email','class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('mobile', 'Mobile#', ['class' => 'awesome']); !!}
                            {!! Form::text('mobile', '',['placeholder' => 'Enter Mobile Number','class' => 'form-control']); !!}
                            {!! Form::label('mobile_public', 'Public', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('mobile_public', 1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('profile_pic', 'Profile Image', ['class' => 'awesome']); !!}
                            {!! Form::file('profile_pic', null,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa fa-shield"></i>Upload Staff Members
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['HomeController@storeStaff']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Replace', ['class' => 'awesome']); !!}
                            {!! Form::select('replace', [0 => 'append data', 1 => 'replace all data'], 0, ['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::file('file'); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
