@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('content')
@php
$bleep = explode('^',$staff->individual_bleep);
if(!isset($bleep[1]))$bleep[1]=1;
$qualifications = explode('^',$staff->qualifications);
if(!isset($qualifications[1]))$qualifications[1]=1;
$mobile = explode('^',$staff->mobile);
if(!isset($mobile[1]))$mobile[1]=1;
@endphp
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>{{$staff->name}}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
<!--        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/staff">Edit Staff Member</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-users fa-2x"></i>Update Staff Member 
                        </div>
                    </div>
                    <div class="portlet-body row">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-4 profile-sidebar">
                                <div class="profile-userpic">
                                    {!! Html::image($staff->profile_pic,null,['class' => 'img-responsive']); !!}
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name"> {{$staff->name}} </div>
                                    <div class="profile-usertitle-job"> {{$roles[$staff->role_id] }}</div>
                                </div>
                            </div>
                        </div>
                        {!! Form::open(['method' => 'put' ,'files' => true , 'action' => ['HomeController@updateStaff',$staff->id] ]) !!}
                        <div class='form-group col-md-6'>
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $staff->name,['placeholder' => 'Enter Name','class' => 'form-control']); !!}
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('gmc', 'GMC', ['class' => 'awesome']); !!}
                            {!! Form::text('gmc', $staff->gmc,['placeholder' => 'Enter GMC','class' => 'form-control']); !!}
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::select('speciality_id', $specialities , $staff->speciality_id, ['class'=>'form-control']); !!}
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('role_id', 'Role', ['class' => 'awesome']); !!}
                            {!! Form::select('role_id',$roles , $staff->role_id,['class' => 'form-control']); !!}
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('grade_id', 'Grade', ['class' => 'awesome']); !!}
                            {!! Form::select('grade_id', $grades , $staff->grade_id,['class' => 'form-control']); !!}
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('qualifications', 'Qualifications', ['class' => 'awesome']); !!}
                            {!! Form::text('qualifications', $qualifications[0],['class' => 'form-control']); !!}
                            {!! Form::label('qualifications_public', 'Public', ['class' => 'awesome']); !!}
                            @if($qualifications[1] == 1)
                            {!! Form::checkbox('qualifications_public', 1,true) !!}
                            @else
                            {!! Form::checkbox('qualifications_public', 1) !!}
                            @endif
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('individual_bleep', 'Individual Bleep', ['class' => 'awesome']); !!}
                            {!! Form::text('individual_bleep', $bleep[0],['class' => 'form-control']); !!}
                            {!! Form::label('individual_bleep_public', 'Public', ['class' => 'awesome']); !!}
                            @if($bleep[1] == 1 )
                            {!! Form::checkbox('individual_bleep_public', 1, $bleep[1]) !!}
                            @else
                            {!! Form::checkbox('individual_bleep_public', 1) !!}
                            @endif
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('email', 'Email', ['class' => 'awesome']); !!}
                            {!! Form::text('email', $staff->email,['class' => 'form-control']); !!}
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('mobile', 'Mobile#', ['class' => 'awesome']); !!}
                            {!! Form::text('mobile', $mobile[0] ,['class' => 'form-control']); !!}
                            {!! Form::label('mobile_public', 'Public', ['class' => 'awesome']); !!}
                            @if($mobile[1] == 1)
                            {!! Form::checkbox('mobile_public', 1,true) !!}
                            @else
                            {!! Form::checkbox('mobile_public', 1) !!}
                            @endif
                        </div>
                        <div class='form-group col-md-6'>
                            {!! Form::label('profile_pic', 'Profile Image', ['class' => 'awesome']); !!}
                            {!! Form::file('profile_pic', '',['class' => 'form-control']); !!}
                        </div>
                        <div class='form-group row'>
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
@ensection