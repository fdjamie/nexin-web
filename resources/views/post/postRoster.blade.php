@extends('admin.admin')

@section('content')

@php
use \Carbon\Carbon;

$flag=true;
$pd = new Carbon($post->start_date);
$post_week = $pd->weekOfYear;
$assigned_week = $post->assigned_week;
$sub_weeks = $total_weeks = count($post->rota_template->content->shift_type);
//$cd = new Carbon('first day of this month');
$selected_date = new Carbon(Session::get('date_time'));
$cd = $selected_date->copy()->startOfMonth();
//$cd = $selected_date->copy();
if($cd->year > $post_week)
{
$year_difference = $cd->year - $pd->year;
$current_week = ($year_difference*52)+$selected_date->copy()->weekOfYear;
}else
$current_week = $selected_date->copy()->weekOfYear;
$pm = $selected_date->copy()->subMonth()->endOfMonth();
//$pm = new Carbon ('last day of last month');
$week_diff = $current_week - $post_week;
//echo 'total ='.$total_weeks.', diff = '.$week_diff;
//die;
if($pd->year == -1)
$week=$assigned_week;
else
$week = ($week_diff%$total_weeks)+$assigned_week;
//echo "current weekday = [$selected_date->dayOfWeek] Post Week {$post_week}, Current Week {$current_week}, Assigned Week {$assigned_week} Week {$week}";die;
//die;
if($selected_date < $pd)
$flag=false;
@endphp
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Post Roster</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!--        <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="/post/">Posts</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-file-o"></i>Post Roster
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#view_1">View 1</a></li>
                            <li><a data-toggle="tab" href="#view_2">View 2</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="view_1" class="tab-pane fade in active">
                                <h3>View 1</h3>
                                <table class="table table-bordered" >
                                    <thead>
                                        <tr>
                                            <th width="14%">  </th>
                                            <th width="14%"> Week </th>
                                            <th > Monday </th>
                                            <th> Tuesday </th>
                                            <th> Wednesday </th>
                                            <th> Thursday </th>
                                            <th> Friday </th>
                                            <th> Saturday </th>
                                            <th> Sunday </th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @if($flag)
                                        @for($i=0;$i<=($total_weeks*7)*2;$i++)
                                        @if($i%7==1)
                                        @if((($week-1)%$total_weeks)+1 == $assigned_week)
                                        <tr class="bg-success">
                                            @else
                                        <tr>
                                            @endif
                                            <td>@if($i >= $cd->dayOfWeek && $i < $cd->dayOfWeek+$cd->daysInMonth)
                                                {{str_pad($i-$cd->dayOfWeek+1, 2, "0", STR_PAD_LEFT)}}/{{str_pad($cd->month, 2, "0", STR_PAD_LEFT)  }}
                                                @elseif($i<30)
                                                {{$pm->addDays($i-$cd->dayOfWeek+1)->format('d/m')}}
                                                @elseif($i>30)
                                                {{str_pad(($i-$cd->dayOfWeek+1)-$cd->daysInMonth,2,'0',STR_PAD_LEFT)}}/{{str_pad($cd->month+1, 2, "0", STR_PAD_LEFT)  }}
                                                @endif</td>
                                            <td>{{(($week-1)%$total_weeks)+1}}</td>
                                            @endif
                                            @if($i > 0)
                                            <td>
                                                @if( $template[ (($week-1)%$total_weeks)][abs(($i-1)%7)] =='')
                                                OFF<br/>
                                                @else
                                                {{$shift[$template[(($week-1)%$total_weeks)][abs(($i-1)%7)]]->name}}<br/>
                                                {{ date('H:i',strtotime($shift[$template[(($week-1)%$total_weeks)][abs(($i-1)%7)]]->start_time)) }} - {{ date('H:i',strtotime($shift[$template[(($week-1)%$total_weeks)][abs(($i-1)%7)]]->finish_time))}}
                                                @endif
                                                @else
                                                &nbsp
                                            </td>
                                            @endif
                                            @if($i%7==0 && $i!=0)
                                            @php $week++; @endphp
                                        </tr>
                                        @endif
                                        @endfor
                                        @else
                                        <tr class="bg-danger">
                                            <td colspan="9">
                                            <li class="list-group-item-danger">Selected date is before the starting date of post.</li>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div id="view_2" class="tab-pane fade">
                                <h3>View 2</h3>
                                <table class="table table-bordered" >
                                    <thead>
                                        <tr>
                                            <th width="14%">  </th>
                                            <th width="14%"> Week </th>
                                            <th > Monday </th>
                                            <th> Tuesday </th>
                                            <th> Wednesday </th>
                                            <th> Thursday </th>
                                            <th> Friday </th>
                                            <th> Saturday </th>
                                            <th> Sunday </th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @if($flag)
                                        @for($i=0;$i<=$total_weeks*7;$i++)
                                        @php $index= $total_weeks - $sub_weeks; @endphp
                                        @if($i%7==1)
                                        @if(($index+1) == $assigned_week)
                                        <tr class="bg-success">
                                            @else
                                        <tr>
                                            @endif
                                            <td>@if($i >= $cd->dayOfWeek && $i < $cd->dayOfWeek+$cd->daysInMonth)
                                                {{str_pad($i-$cd->dayOfWeek+1, 2, "0", STR_PAD_LEFT)}}/{{str_pad($cd->month, 2, "0", STR_PAD_LEFT)  }}
                                                @elseif($i<30)
                                                {{$pm->addDays($i-$cd->dayOfWeek+1)->format('d/m')}}
                                                @elseif($i>30)
                                                {{str_pad(($i-$cd->dayOfWeek+1)-$cd->daysInMonth,2,'0',STR_PAD_LEFT)}}/{{str_pad($cd->month+1, 2, "0", STR_PAD_LEFT)  }}
                                                @endif</td>
                                            <td>@if($index < $total_weeks) {{($index)+1}} @endif</td>
                                            @endif
                                            @if($i > 0)
                                            <td>
                                                @if($index < $total_weeks)
                                                @if($template[$index][abs(($i-1)%7)] == '')
                                                OFF
                                                @else
                                                {{$shift[$template[$index][abs(($i-1)%7)]]->name }}<br>
                                                {{date('H:i',strtotime($shift[$template[$index][abs(($i-1)%7)]]->start_time))}} - {{date('H:i',strtotime($shift[$template[$index][abs(($i-1)%7)]]->finish_time))}}
                                                @endif
                                                @endif

                                            </td>
                                            @endif
                                            @if($i%7==0 && $i!=0)
                                            @php 
                                            $week++; 
                                            @endphp
                                        </tr>
                                        @endif
                                        @if($sub_weeks > 0 && $i%7==0 && $i!=0)
                                        @php $sub_weeks--; @endphp
                                        @endif
                                        @endfor
                                        @else
                                        <tr class="bg-danger">
                                            <td colspan="9">
                                            <li class="list-group-item-danger">Selected date is before the starting date of post.</li>
                                            </td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
