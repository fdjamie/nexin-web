@extends('admin.admin')
@section ('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Post</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        {{-- !! Breadcrumbs::render('post/new') !!--}}
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-file-o"></i>Add Post
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['PostController@create'],'id'=>'directorate_form']) !!}
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                            {!! Form::close() !!}

                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['PostController@store']]) !!}
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('directorate_speciality_id', $specialties, null, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-post'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-post') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description', ['class' => 'awesome']); !!}
                            {!! Form::text('description', '',['placeholder' => 'Enter Description' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Group', ['class' => 'awesome']); !!} <span id="rg_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('rota_group_id',$rota_groups, null,[ 'class' => 'form-control','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_template_id', 'Rotation Template', ['class' => 'awesome']); !!} <span id="rt_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('rota_template_id',$rota_templates, null,[ 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('assigned_week', 'Assigned Week', ['class' => 'awesome']); !!} <span id="spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::number('assigned_week',1, ['class' => 'form-control','id'=>'assigned_week','min'=>1,'max'=> isset($week_cycle)?$week_cycle:1]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!}
                            {!! Form::text('start_date', date('Y-m-d'),[ 'class' => 'form-control datepicker']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('permanent', 'Permanent', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('permanent',1,[ 'class' => 'form-control','id'=>'permanent']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!}
                            {!! Form::text('end_date', null,[ 'class' => 'form-control datepicker','id'=>'endDate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!-- Import File-->
             <div class="col-md-4 col-sm-4">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-home"></i>Upload Divisions
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{-- $error --}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'url' => route('post-import')]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']) !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script src="{{url('/js/post-details.js')}}"></script>
@endsection