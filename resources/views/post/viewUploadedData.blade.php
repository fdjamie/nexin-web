@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="/css/custom.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Category</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user-md"></i>Category 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <div class="clearfix overflow-scroll">
                            <table class="table table-responsive  table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th> Directorate Name </th>
                                        <th> Post Name </th>
                                        <th> Description </th>
                                        <th> Speciality Name </th>
                                        <th> Rota Group Name </th>
                                        <th> Rota Template Name </th>
                                        <th> Start Date  </th>
                                        <th> End Date  </th>
                                        <th> Assigned Week </th>
                                        <th> Result </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($viewData as $directorate)
                                        
                                    <tr class="odd gradeX">
                                        <td>{{ $directorate['directorate'] }}</td>
                                        <td>{{ $directorate['name'] }}</td>
                                        <td>{{ $directorate['description'] }}</td>
                                        <td>{{ $directorate['speciality'] }}</td>
                                        <td>{{ $directorate['rota_group'] }}</td>
                                        <td>{{ $directorate['rota_template'] }}</td>
                                        <td>{{ $directorate['start_date'] }}</td>
                                        <td>{{ $directorate['end_date'] }}</td>
                                        <td>{{ $directorate['assigned_week'] }}</td>
                                        
                                        <td class="table-danger">
                                            @foreach($directorate['errors'] as $key=>$error)
                                            <ul>
                                                @if(isset($error))<li class=" {!! $key === 'new_data'?'': 'list-group-item-danger' !!}"> {!! $key === 'new_data'?'': $error!!} </li>@endif 
                                            </ul>

                                        @endforeach
                                        </td>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-toolbar">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{!!  route('post-index') !!}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'url' => route('post-upload') ]) !!}
                                        {!! Form::hidden('fileName',$fileName); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green']); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection