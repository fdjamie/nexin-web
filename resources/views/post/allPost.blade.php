@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-file-o"></i>Post 
                        </div>
                        <div class="form-group col-md-3 pull-right">
                            {!! Form::open(['method' => 'POST', 'autocomplete'=>'off', 'action' => ['PostController@index']]) !!}
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate']); !!}
                            {!! Form::close() !!}
                        </div>

                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-post'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-post') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-post'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-post') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{{url('/post/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
<!--                                <div class="col-md-3">
                                    <div class="btn-group">
                                        <a href="{!!route('post-export') !!}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning"> Export Posts <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        @if(count($specialities)==0)
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th colspan="5"> No Speciality for this Directorate</th>
                                </tr>
                            </thead>
                        </table>
                        @endif
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$speciality_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$speciality_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Description </th>
                                                <th> Staff Member </th>
                                                <th> Start Date </th>
                                                <th> End Date </th>
                                                <th> Rota tempalte </th>
                                                <th> Week </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($speciality_node->post))
                                            <tr>
                                                <td></td>
                                                <td colspan="7" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($speciality_node->post as $post)

                                            <tr class="odd gradeX">
                                                <td width='14%'>{{$post->name}}</td>
                                                <td width='14%'>{{$post->description}}</td>
                                                <td>{{(isset($post->rotation[0]->staff->name)?$post->rotation[0]->staff->name:'Vacant')}}</td>
                                                <td>
                                                    @if(isset($post->start_date) && $post->start_date != '0000-00-00')
                                                    {{$post->start_date}}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{(isset($post->permanent)?'Permanent':$post->end_date)}}
                                                </td>
                                                <td>
                                                    @if(isset($post->rota_template->name))
                                                    {{$post->rota_template->name}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($post->assigned_week))
                                                    {{$post->assigned_week}}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="{!! url('/post-rotation/'.$post->id) !!}" ><i class="fa fa-list-alt" aria-hidden="true"></i></a> 
                                                    <a href="{!! url('/post-roster/'.$post->id) !!}" ><i class="fa fa-calendar-times-o" aria-hidden="true"></i></a> 
                                                    <a href="{!! url('/post/edit/'.$post->id) !!}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                    <a data-toggle="modal" href="#small" id="{{$post->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>

                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                                <div class="table-toolbar">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>Posts Statistics of {{$directorte_name}} directorate.</h4>
                                            <ul>
                                                <li>Assigned Posts = <b> {{$assigned_posts}}</b></li>
                                                <li>Vacant Posts = <b> {{$vacant_posts}}</b></li>
                                                <li>Total Posts = <b> {{$total_posts}}</b></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    @endsection
    @section('modal')
    @include('common.delete-confirmation-modal')
    @endsection
    @section('extra-js')
    <script>
        $(document).ready(function () {
            $("#directorate, #directorate_speciality_id").change(function () {
                $("form").submit();
            });
//            $('#directorate').select2({placeholder: "Select a directorate",allowClear: true});
            $('.delete').click(function () {
                $('#delete-button').attr('href', APP_URL+"/post/delete/" + $(this).attr('id'));
            });
        });
    </script>
    @include('includes.scriptsViewLinks')
    @endsection