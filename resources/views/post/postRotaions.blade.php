@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
<?php
//echo '<pre>';
//print_r($rotations);
//die;
?>
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Post Rotations</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-file-o"></i>{{$post->name}} - Post Rotations
                        </div>

                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th> Staff Member </th>
                                    <th> start Time </th>
                                    <th> End Time </th>
                                    <th> Rota Template </th>
                                    <th> Assigned Weed </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($post->rotation == null)
                                <tr>
                                    <td colspan="5">
                                        <b>No Rotations for this post</b>
                                    </td> 
                                </tr>
                                @endif
                                @foreach($post->rotation as $rotation)
                                <tr class="odd gradeX">
                                    @if($rotation->staff != null)
                                    <td width='20%'>{{$rotation->staff->name}}</td>
                                    @else
                                    <td>Not Assigened Yet</td>
                                    @endif
                                    <td>
                                        @if(isset($rotation->start_date) && $rotation->start_date != '0000-00-00')
                                        {{$rotation->start_date}}
                                        @endif
                                    </td>
                                    <td>
                                        @if(isset($rotation->end_date) && $rotation->end_date != '0000-00-00')
                                        @if($rotation->end_date == '1111-00-00')
                                        Permanent
                                        @else
                                        {{$rotation->end_date}}
                                        @endif
                                        @endif
                                    </td>
                                    <td>{{$rotation->rota_template->name}}</td>
                                    <td>{{$rotation->template_position}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    @endsection
    @section('extra-js')
    @include('includes.scriptsViewLinks')
    @endsection