@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />-->
<link rel="stylesheet" href="{{url('/select2/css/select2.min.css')}}" />
<link rel="stylesheet" href="{{url('/select2/css/select2-bootstrap.min.css')}}"/>
@endsection
@section('content')
@php

@endphp
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit Post</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-12">
                            <i class="fa fa-file-o"></i>{{$post->name}}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['PostController@edit',$post->id],'id'=>'directorate_form']) !!}
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::open(['method' => 'put', 'autocomplete'=>'off', 'action' => ['PostController@update',$post->id]]) !!}
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('directorate_speciality_id', $specialties, $post->directorate_speciality_id, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-post'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-post') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $post->name,['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description', ['class' => 'awesome']); !!}
                            {!! Form::text('description', $post->description,['placeholder' => 'Enter Description' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Group', ['class' => 'awesome']); !!} <span id="rg_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('rota_group_id',$rota_groups, isset($post->rota_template->rota_group_id)?$post->rota_template->rota_group_id:null,[ 'class' => 'form-control','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_template_id', 'Rotation Template', ['class' => 'awesome']); !!} <span id="rt_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('rota_template_id',$rota_templates, $post->rota_template_id,[ 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('assigned_week', 'Assigned Week', ['class' => 'awesome']); !!} <span id="spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::number('assigned_week',$post->assigned_week, ['class' => 'form-control','id'=>'assigned_week','min'=>1,'max'=> isset($week_cycle)?$week_cycle:1]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!}
                            @if($post->start_date != '0000-00-00')
                            {!! Form::text('start_date', $post->start_date,[ 'class' => 'form-control datepicker']); !!}
                            @else
                            {!! Form::text('start_date', null,[ 'class' => 'form-control datepicker']); !!}
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('permanent', 'Permanent', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('permanent',1,$post->permanent,[ 'id'=>'permanent']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!}
                            {!! Form::text('end_date', $post->end_date,[ 'class' => 'form-control datepicker','id'=>'endDate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-file-o"></i>{{$post->name}}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-parameter'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            <li>{{session('success-parameter')}}</li>
                        </ul>
                    </div>
                    @endif
                    @if (Illuminate\Support\Facades\Session::has('error-parameter'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            <li>{{session('error-parameter')}}</li>
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['PostController@addAdditionalParameters',$post->id]]) !!}
                        <div class="parameters-form">
                            <div class="form-group col-md-12">
                                <div class="btn-group">
                                    <br>
                                    <button id="new-parameter" class="btn sbold green add"> <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            @if(is_null($post->additional_parameters))
                            <div class="copy-row">
                                <div class="form-group col-md-5">
                                    {!! Form::label('parameter', 'Parameter', ['class' => 'awesome']); !!}
                                    {!! Form::select('parameter[]', $parameters, null,['class' => 'form-control parameter','id'=>'parameter-1']); !!}
                                </div>
                                <div class="form-group col-md-5 value-field">
                                    {!! Form::label('value', 'Value', ['class' => 'awesome']); !!}
                                    {!! Form::select('value[0][]', $values, null,['class' => 'form-control value select2-multiple','id'=>'value-1', 'multiple'=>'multiple']); !!}
                                </div>
                                <div class="form-group col-md-2">
                                    <br><br>
                                    <a class="fa fa-trash delete" id="trash-1"></a>
                                </div>
                            </div>
                            @else
                            @for($index =0; $index < count($post_parameters['parameter']); $index++)
                            @php $id=$index+1; @endphp
                            <div class="copy-row">
                                <div class="form-group col-md-5">
                                    {!! Form::label('parameter', 'Parameter', ['class' => 'awesome']); !!}
                                    {!! Form::select('parameter[]', $post_parameters['parameter'][$index], $post_parameters['selected_parameter'][$index],['class' => 'form-control parameter','id'=>'parameter-'.$id]); !!}
                                </div>
                                <div class="form-group col-md-5 value-field">
                                    {!! Form::label('value', 'Value', ['class' => 'awesome']); !!}
                                    {!! Form::select('value['.$index.'][]', $post_parameters['values'][$index], $post_parameters['selected_value'][$index],['class' => 'form-control value select2-multiple','id'=>'value-'.$id, 'multiple'=>'multiple']); !!}
                                </div>
                                <div class="form-group col-md-2">
                                    <br><br>
                                    <a class="fa fa-trash delete" id="trash-{{$id}}"></a>
                                </div>
                            </div>
                            @endfor
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save Chagnes !', ['class'=> 'btn btn-warning']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>-->
<script src="{{url('/select2/js/select2.full.js')}}"></script>
<script src="{{url('/js/components-select2.min.js')}}"></script>
<script src="{{url('/js/post-details.js')}}"></script>
<script>
//$(document).on('DOMNodeInserted', 'select', function () {
//    console.log('yes');
//});
</script>
@endsection
