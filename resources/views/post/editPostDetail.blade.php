@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit Post</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
<!--                        <div class="caption col-md-4">
                            <i class="fa fa-anchor" aria-hidden="true"></i>
                        </div>-->
                        <div class="form-group col-md-4">
                            {!! Form::open(['method' => 'put', 'autocomplete'=>'off', 'action' => ['HomeController@updatePostDetails',$postdetail->id]]) !!}
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::text('directorate', $directorate , [ 'class'=>'form-control','id'=>'directorate','disabled'=>true]); !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('speciality', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::text('speciality', $speciality, [ 'class'=>'form-control','id'=>'speciality','disabled'=>true]); !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('post_name', 'Post', ['class' => 'awesome']); !!}
                            {!! Form::text('post_name', $post->name, [ 'class'=>'form-control','id'=>'post_name','disabled'=>true]); !!}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('staff_member', 'Staff Member', ['class' => 'awesome']); !!}
                            {!! Form::select('staff_member',$staff, $staff,[ 'class' => 'form-control search_directorate_staff']); !!}
                            {!! Form::hidden('directorate_id',$directorate_id,[ 'id' => 'directorate_id']); !!}
                            {{--!! Form::text('staff_member', $postdetail->staff_member,[ 'class' => 'form-control']); !!--}}
                            {!! Form::hidden('post_id',$postdetail->post_id); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_template_id', 'Rota Template', ['class' => 'awesome']); !!}
                            {!! Form::select('rota_template_id',\App\Http\Helpers::getDropDownData($rota_template), $postdetail->rota_template_id,[ 'class' => 'form-control','id'=>'rota_template_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('assigned_week', 'Assigned Week', ['class' => 'awesome']); !!} <span id="fa_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::number('assigned_week',$postdetail->assigned_week, ['class' => 'form-control','id'=>'assigned_week','min'=>1,'max'=>isset($postdetail->rota_template->content->shift_type)?count($postdetail->rota_template->content->shift_type):1]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!}
                            @if($postdetail->start_date == '0000-00-00')
                            {!! Form::text('start_date', null,[ 'class' => 'form-control datepicker']); !!}
                            @else
                            {!! Form::text('start_date', $postdetail->start_date,[ 'class' => 'form-control datepicker']); !!}
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'Permanent', ['class' => 'awesome']); !!}
                            @if($postdetail->end_date == '1111-00-00')
                            {!! Form::checkbox('end_date','1111-00-00',true); !!}
                            @else
                            {!! Form::checkbox('end_date','1111-00-00',false); !!}
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!}
                            @if($postdetail->end_date == '1111-00-00' || $postdetail->end_date == '0000-00-00')
                            {!! Form::text('end_date', null,[ 'class' => 'form-control datepicker','id'=>'endDate']); !!}
                            @else
                            {!! Form::text('end_date', $postdetail->end_date,[ 'class' => 'form-control datepicker','id'=>'endDate']); !!}
                            @endif
                        </div>
                        <div class="form-group row">
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6 col-sm-6">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> Rota Template Info 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover order-column" >
                            <thead>
                                <tr>
                                    <th width="33%"> Name </th>
                                    <th> Weeks </th>
                                    <th> Description</th>
                                </tr>
                            </thead>
                            <tbody id='rota-info'>
                                @foreach($rota_template as $template)
                                <tr><td>{{$template->name}}</td><td>{{count($template->content->shift_type)}}</td><td>{{$template->description}}</td></tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{url('/js/search_staff.js')}}"></script>
<script src="{{url('/js/post-details.js')}}"></script>
@endsection