<div class="header-section"> 
    <!-- Top Section Begins -->
    <div class="sticky-wrapper is-sticky" id="sticky-section-sticky-wrapper">
        <div class="sticky-navigation" id="sticky-section">
            <div class="top-container absolute container" id="top-section">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 no-padding logo-container"> 
                            <!-- Logo Begins -->
                            <div class="site-logo col-md-3 no-padding"></div>
                            <!-- Logo Ends -->
                        </div>
                        <!-- Navigation Menu Begins -->
                        <div class="col-md-6 no-padding navbar top-navbar" id="navigation"> 

                            <!-- Mobile Nav Toggle Begins -->
                            <div class="navbar-header nav-respons"></div>
                            <!-- Mobile Nav Toggle Ends --> 

                            <div class="top-social-media-icons col-md-12 no-padding clearfix">
								<div class="col-md-6 app-nav no-padding"></div>                                
                            </div> 

                            <!-- Menu Begins -->
                            <nav class="collapse navbar-collapse bs-navbar-collapse no-padding" role="navigation" id="topnav"></nav>
                            <!-- Menu Ends --> 
                        </div>
                        <!-- Navigation Menu Ends --> 
                    </div>
                </div>
                <div class="dropdown-box">
                    @if (Auth::guest())
                    @include('login')
                    @include('signup')
                    <div id="bottom-section" class="bottom-section"></div>
                    @else
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Top Section Ends --> 
</div>