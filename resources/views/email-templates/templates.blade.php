@extends('admin.admin')

@section('extra-css')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        tr, th {
            text-align: center;
        }

        .loader {
            text-align: center;
            padding: 62px;
            display: none;
            position: absolute;
            z-index: 99999;
            width: 100%;

        }
        .alert{
            margin-bottom: 2px !important;
        }


        .error {
            display: none;
        }

        .loader img {
            width: 19%;
        }
        .modal .modal-header {
            background: #007bff;
            border-bottom: 1px solid #EFEFEF;
            color: #ffff;
        }

    </style>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-envelope"></i>Email Template
                            </div>
                        </div>

                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (\Session::has('error'))
                                <div class="alert alert-danger text-center">
                                    {{Session::get('error')}}
                                </div>
                            @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success  alert-success col-sm-6 col-sm-offset-3 text-center">
                                    {{Session::get('success')}}
                                </div>
                            @endif

                             {{--<a data-toggle="modal" data-target="#addNewTemplate" class="btn btn-info pull-right"> <i class="fa fa-plus"></i>--}}
                             <a href="{{url('/create-new-template')}}" class="btn btn-info pull-right"> <i class="fa fa-plus"></i>
                                Add New Template</a>

                            <table class="table table-striped table-bordered table-hover table-checkable order-column"
                                   id="sample_3">
                                <thead>
                                <tr>
                                    {{-- <th>Id</th>--}}
                                    <th>#</th>
                                    <td>Email Type</td>
                                    <td>Active Template</td>
                                    <td>Change</td>

                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($templates))
                                    @foreach ($templates as $k=>$t)
                                        <tr>
                                            <td> {{$k+1}}</td>
                                            <td>{{$t->name}}</td>
                                            {{--
                                                                                        <td><a data-html="{{json_encode($t->templateHtml)}}}}" onclick='viewTemplateHtml(this)' href="javascript:;"><i class="fa fa-eye"></i> View</a></td>
                                            --}}
                                            <td><a onclick='viewTemplateHtml({{$t->templateId}})' href="javascript:;"><i
                                                            class="fa fa-eye"></i> View</a></td>
                                            {{--     <td><a data-templates="{{json_encode($t->templates)}}}}"  onclick="changeTemplate(this)"> <i class="fa fa-edit"></i> Change</a></td>--}}
                                            <td><a onclick="changeTemplate('{{$t->id}}','{{$t->templateId}}')"> <i class="fa fa-edit"></i>
                                                    Change</a></td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>

                                        <td colspan='5'>no record found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="modal fade" id="viewTemplate" style="z-index: 99999999" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">



                                <div class="modal-body">
                                <div class="loader"><img src="{{asset('assets/images/load.gif')}}"></div>
                                <div class="error alert text-center alert-danger"></div>
                                <div id="templateHtml" style="width:100%;overflow:hidden;"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="changeTemplate" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">

                            <div class="modal-content"  >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Select Template</h4>
                                </div>
                                <div class="loader"><img src="{{asset('assets/images/load.gif')}}"></div>
                                <div class="error alert text-center alert-danger"></div>
                                <div class="modal-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                                           id="changeTemplateTable" data-active-template="">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <td>Subject</td>
                                            <td>From</td>
                                            <td>View</td>
                                            <td>Select</td>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>




                </div>


                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection


@section('extra-js')

    <script>


        var selectTemplateRequest=true;



        function viewTemplateHtml(id) {
            $('#templateHtml').html("");
            $('.error').hide();
            showLoader('#viewTemplate');

            var url = "{{url('get-trust-template')}}";
            var data = {id: id};
            var get = ajaxCall(url, 'GET', data);

            get.success(function (data) {
                hideLoader('#viewTemplate');
                var obj = jQuery.parseJSON(JSON.stringify(data));
                if (obj.status) {
                    if (obj.apiResponseData.status) {
                        $('#templateHtml').html(obj.apiResponseData.html);
                    }
                    else {
                        showError('Unknow Error Occured.Try Again!', '#viewTemplate');
                    }
                }
                else {
                    showError('Unknow Error Occured.Try Again!', '#viewTemplate');
                }
            });

            get.error(function (error) {
                hideLoader('#viewTemplate');
                showError('Unknow Error Occured.Try Again');
            });
            $('#viewTemplate').modal('show');

        }

        function changeTemplate(id,selected) {


            $('#changeTemplateTable tbody').html("");
            $('#changeTemplate .error').hide();
            showLoader('#changeTemplate');

            var url = "{{url('get-email-templates')}}";
            var data = {id: id};
            var get = ajaxCall(url, 'GET', data);

            get.success(function (data) {
                var obj = jQuery.parseJSON(JSON.stringify(data));
                hideLoader('#changeTemplate');
                if (obj.status) {
                    if (obj.apiResponseData.status) {
                        return templateHtml(obj.apiResponseData.data,selected);
                    }
                    else {
                        showError('Unknow Error Occured.Try Again!', '#changeTemplate');

                    }
                }
                else {
                    showError('Unknow Error Occured.Try Again!', '#changeTemplate');
                }
            });

            get.error(function (error) {
                hideLoader('#changeTemplate');
                showError('Unknow Error Occured.Try Again', '#changeTemplate');
            });

            $('#changeTemplate').modal('show');

        }


        function templateHtml(data,selected) {

            var html = "";
            for (let i = 0; i <= data.length - 1; i++) {
                    var span = "";
                    var select = "<a id='select_"+data[i].id+"' href='javascript:;' onclick=\"selectTemplate('" + data[i].id + "','" + data[i].email_type_id + "')\"> <i class=\"fa fa-arrow-circle-o-right\"></i> Select</a>";
                    if (data[i].id == selected) {
                        /*span = "<span class='badge badge-success'>Active Now</span>";*/
                        span = "";
                       /* select = "<span class='badge badge-success'> <i class=\"fa fa-check\"></i> Selected</span>";*/
                        select = "<span class='badge badge-success'><i class=\"fa fa-check\"> Active Now</span>";
                    }
                    html = html + "<tr><td>" + (i + 1) + "</td>\n" + "<td>"+data[i].subject+"</td><td>"+data[i].from+"</td><td><a   onclick='viewTemplateHtml(\"" + data[i].id + "\")' href=\"javascript:;\"><i class=\"fa fa-eye\"></i> View " + span + "</a></td>\n" +
                        "<td>" + select + "</td><tr>";
                }

          /*  $('#changeTemplateTable').attr('data-active-template',selected);*/
            $('#changeTemplateTable tbody').html(html);
        }


        function selectTemplate(templtId,selectedId) {

            $('#changeTemplate .error').hide();
            var url = "{{url('update-template')}}";
            var data = {templateId: templtId,emailTypeId:selectedId};
            if(!selectTemplateRequest) {
                return ;
            }

            selectTemplateRequest=false;
            $('#select_'+templtId).append(' <i class="fa fa-circle-o-notch fa-spin" style="font-size:20px"></i>');
            var post = ajaxCall(url, 'POST', data);
            post.success(function (data) {
                selectTemplateRequest=true;
                $('#select_'+templtId).show();
                var obj = jQuery.parseJSON(JSON.stringify(data));
                hideLoader('#changeTemplate');
                if (obj.status) {

                    if (obj.apiResponseData.status) {
                        $('#changeTemplate .alert').toggleClass('alert-danger alert-success');
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                        showError('Template Successfully Changed', '#changeTemplate');
                    }
                    else {
                        showError('Unknow Error Occured.Try Again!', '#changeTemplate');

                    }
                }
                else {
                    showError('Unknow Error Occured.Try Again!', '#changeTemplate');
                }
            });
             post.error(function (error) {
                 selectTemplateRequest=true;
                 $('#select_'+templtId).show();
                 hideLoader('#changeTemplate');
                showError('Unknow Error Occured.Try Again', '#changeTemplate');
            });


        }



        function showLoader(dv) {

            $(dv + ' .loader').show();
        }

        function hideLoader(dv) {

            $(dv + ' .loader').hide();
            $('.fa-spin').remove();

        }

        function showError(msg, dv) {

            $(dv + ' .error').html(msg);
            $(dv + ' .error').show();
        }

        function ajaxCall(url, type, data) {
            return $.ajax({
                url: url,
                type: type,
                data: data,

            });
        }

    </script>

@endsection
