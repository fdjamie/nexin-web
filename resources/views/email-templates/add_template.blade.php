@extends('admin.admin')

@section('extra-css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
@endsection

@section('content')

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-offset-1 col-md-6 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-h-md fa-2x"></i>Add New Email Template
                            </div>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-success col-sm-6 col-sm-offset-3">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        <div class="portlet-body">
                            <form  action="{{url('/create-template')}}" method="post" class="form">

                                <div class="form-group">
                                    <select class="form-control" name="templateType" value="{{ old('templateType') }}" required>
                                        <option value="" >Select Template Type</option>
                                        @foreach ($templates as $k=>$t)
                                            <option value="{{$t->id}}">{{$t->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ old('subject') }}" name="subject" placeholder="Subject"  >

                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" value="{{ old('from') }}" name="from" placeholder="From"  >

                                </div>
                                <div class="alert alert-info">
                                    * <strong>For Verification Template</strong> Write <strong>&name&</strong> and <strong>&code&</strong> where you want to display name and code.
                                </div>
                                <div class="alert alert-info">
                                    * <strong>For Request Accept Or Reject Template</strong> Write <strong>&name&</strong> and <strong>&trust&</strong> where you want to display name and Trust name.
                                </div>

                                <div class="form-group">
                                    <textarea id="createTemplate" class="form-control" name="template" required> {{ old('template') }} </textarea>
                                </div>


                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" >Add Template </button>
                                </div>



                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>


            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('extra-js')

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#createTemplate').summernote(
                {
                    tabsize: 1,
                    height: 350,
                    callbacks: {
                        onImageUpload: function (image) {
                            uploadImage(image[0]);
                        }
                    }
                }
            );


            function uploadImage(image) {

                var data = new FormData();
                data.append("image",image);
                $.ajax ({
                    data: data,
                    type: "POST",
                    url: "{{url('/uploadEmailTemplateImage')}}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(url) {
                        console.log(url);

                        $('#createTemplate').summernote('insertImage', url);

                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }


        });
        </script>
@endsection
