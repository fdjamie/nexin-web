@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-map-marker"></i>{{$location->name}} 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-location'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{--{{ session('error-location') }}--}}
                        @foreach(session('error-location') as $error)
                            @foreach($error as $errormsg)
                                {{$errormsg}}
                                <br />
                            @endforeach
                            {{--@php
                                echo '<pre>';
                                print_r(session('error-location'));
                            @endphp--}}
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['LocationController@update', $location->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Site', ['class' => 'awesome']); !!}
                            {!! Form::select('site_id', $sites,$location->site_id,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $location->name,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('service', 'service', ['class' => 'awesome']); !!}
                            {!! Form::select('service', \App\Http\Helpers::booleanDropDown(), $location->service , ['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('limit', 'Limit', ['class' => 'awesome']); !!}
                            {!! Form::number('limit', $location->limit,['class' => 'form-control', 'min'=>'1', 'max'=>'10']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates, isset($location->group->directorate_id)?$location->group->directorate_id:null, ['class' => 'form-control','directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('group_id', 'Group', ['class' => 'awesome']); !!} <span id="gp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('group_id', $groups, $location->group_id , ['class' => 'form-control','group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('subgroup_id', 'Sub Group', ['class' => 'awesome']); !!} <span id="sg_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('subgroup_id', $sub_groups, $location->subgroup_id , ['class' => 'form-control','subgroup_id']); !!}
                        </div>
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/location.js')}}" type="text/javascript"></script>
@endsection
