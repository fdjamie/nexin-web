@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-map-marker"></i>Locations 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-location'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-location') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-location'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-location') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/location/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        <a href="{{url('/location/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Locations <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead class="text-center">
                                <tr>
                                    <th >Id</th>
                                    <th> Site Name </th>
                                    <th> Name </th>
                                    <th> Service </th>
                                    <th> Limit </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($sites as $site)
                                @if(isset($site->location))
                                @foreach ($site->location as $location)
                                <tr>
                                    <td>{{ $location->id }}</td>
                                    <td>{{ $site->name }}</td>
                                    <td>{{ $location->name }}</td>
                                    <td>{{ $location->service?"Enabled":"Disabled" }}</td>
                                    <td>{{ $location->limit }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('/location/'.$location->id.'/services') }}"><i class="fa fa-life-bouy" aria-hidden="true"></i></a>
                                        <a href="{{url('/location/edit/'.$location->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id="{{$location->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/location/delete")}}' +'/' +$(this).attr('id'));
    });
</script>
@endsection