@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-map-marker"></i>Add Location
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-location'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-location') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['LocationController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Site', ['class' => 'awesome']); !!}
                            {!! Form::select('site_id', $sites, null ,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('service', 'service', ['class' => 'awesome']); !!}
                            {!! Form::select('service', \App\Http\helpers::booleanDropDown(), null , ['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('limit', 'Limit', ['class' => 'awesome']); !!}
                            {!! Form::number('limit', '1',['class' => 'form-control', 'min'=>'1', 'max'=>'10']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates, null , ['class' => 'form-control','directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('group_id', 'Group', ['class' => 'awesome']); !!} <span id="gp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('group_id', $groups, null , ['class' => 'form-control','group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('subgroup_id', 'Sub Group', ['class' => 'awesome']); !!} <span id="sg_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('subgroup_id', $sub_groups, null , ['class' => 'form-control','subgroup_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-map-marker"></i>Upload Locations
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['LocationController@import']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="{{url('/js/location.js')}}" type="text/javascript"></script>
@endsection
