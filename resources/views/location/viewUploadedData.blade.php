@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Locations</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-map-marker"></i>Locations 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <div class="clearfix overflow-scroll">
                            <table class="table table-responsive  table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th> Name </th>
                                        <th> Service </th>
                                        <th> Limit </th>
                                        <th> Site </th>
                                        <th> Result </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($view_data as $location)
                                    @if(isset($location['location_name_error']) || isset($location['location_error']) || isset($location['site_error']) || isset($location['service_error']))
                                    <tr class="bg-danger">
                                        @else
                                    <tr class="odd gradeX">
                                        @endif    
                                        <td>{{ $location['name'] }}</td>
                                        <td>{{ $location['service'] }}</td>
                                        <td>{{ (isset($location['limit'])?$location['limit']:'') }}</td>
                                        <td>{{ $location['site'] }}</td>
                                        <td class="table-danger">
                                            <ul>
                                                @if(isset($location['location_name_error']))<li class="list-group-item-danger"> {{$location['location_name_error']}}</li>@endif 
                                                @if(isset($location['site_error']))<li class="list-group-item-danger"> {{$location['site_error']}}</li>@endif 
                                                @if(isset($location['location_error']))<li class="list-group-item-danger"> {{$location['location_error']}}</li>@endif 
                                                @if(isset($location['service_error']))<li class="list-group-item-danger"> {{$location['service_error']}}</li>@endif 
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-toolbar">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/location/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['LocationController@upload'] ]) !!}
                                        {!! Form::hidden('file_name',$file_name); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green']); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection