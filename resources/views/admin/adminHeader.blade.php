@php
use \Carbon\Carbon;
@endphp
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <h1 class="font-blue-sharp bold">Nexin</h1> </a>
            <!-- <div class="menu-toggler sidebar-toggler">
                DOC: Remove the above "hide" to enable the sidebar toggler button on header
            </div> -->
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->

        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="page-top">
                <!-- BEGIN TOP NAVIGATION MENU -->

                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                      @if(Auth::user()->is_admin!=2)
                        <li class="dropdown dropdown-user get-date input-append ">
                            <h4 style="color: white;font-weight: 500;">Trusts:</h4>
                          </li>
                        <li class="dropdown dropdown-user get-date input-append " style="margin: 0px 5px 0px 1px;">
                            <select class="form-control" onchange="getTrust(this.value)">
                                @if(!empty(session('userTrusts') && session('userTrusts')['status']))
                                   @foreach(session('userTrusts')['trusts'] as $trust)
                                  <option value="{{base64_encode($trust->id)}}"   {{(session('activeTrust')==$trust->id)?'selected':''}} >{{$trust->name}}</option>
                                    @endforeach
                                    @else
                                    <option value="">No Any Trust Assigned To You.</option>
                                    @endif
                            </select>
                        </li>
@endif
                        <li class="dropdown dropdown-user get-date input-append date form_datetime"> 
                            {{--!! Form::text('date', (is_null(session()->get('date_time'))?new Carbon():session()->get('date_time')),[ 'class' => 'form-control date','id'=>'date']); !!--}}
                            {!! Form::text('date', (is_null(session()->get('date_time'))?new Carbon():session()->get('date_time')),[ 'class' => 'form-control date-header','id'=>'date-header']); !!}
                        </li>
                        <li class="dropdown dropdown-user get-date"> 
                            {!! Form::submit('Reload !', ['class'=> 'btn sbold green','id'=>'get-date']); !!}
                        </li>
                        <li class="dropdown dropdown-user dropdown-dark">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                <img alt="" class="img-circle" src="{{url ('assets/images/staff_profile/default.png')}}" /> </a>
                            <ul class="dropdown-menu dropdown-menu-default">

                              {{--  @if(empty(Auth::user()->google2Fauth->enable) ||(session('google2fa') && session('google2fa')['auth_passed']==1))
                                <li>
                                    <a href="{{ url('/google-2f-authenticate') }}">
                                        <i class="icon-key"></i>2 Factors Authentication </a>
                                </li>
                              @endif--}}

                                <li>
                                    <a href="{{ url('/logout') }}">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>

                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION MENU -->


        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
    @if (Illuminate\Support\Facades\Session::has('Exception'))
        <div class='alert alert-danger alert-dismissible col-sm-4 col-sm-offset-4 text-center fade in' style="position: absolute" role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            {{ session('Exception') }}
        </div>
    @endif
</div>


