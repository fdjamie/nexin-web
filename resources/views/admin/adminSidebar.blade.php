<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <h3 class="menu-title ">Features</h3>
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            @if(!empty(session('userTrusts') && session('userTrusts')['status']))
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link main-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">Trust</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="{{url('/trust/')}}" class="nav-link ">
                                <span class="title"><i class="fa fa-search"></i> List All</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href="javascript:;" class="nav-link ">
                                <span class="title"><i class="fa fa-ambulance"></i> Divisions</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="{{url('/divisions/')}}" class="nav-link ">
                                        <span class="title"><i class="fa fa-search"></i> List All</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="javascript:;" class="nav-link ">
                                        <span class="title"><i class="fa fa-user-md"></i> Directorates</span>
                                        <span class="arrow"></span>
                                    </a>

                                    <ul class="sub-menu">
                                        @if(__authorize(config('module.directorates'),'view'))
                                            <li class="nav-item ">
                                                <a href="{{url('/directorates/')}}" class="nav-link ">
                                                    <span class="title"><i class="fa fa-search"></i> List All</span>
                                                </a>
                                            </li>
                                        @endif
                                        @if(__authorize(config('module.general-services'),'view'))
                                            <li class="nav-item ">
                                                <a href="{{url('/directorate-services')}}" class="nav-link ">
                                                    <span class="title"><i class="fa fa-life-bouy"></i> General Services</span>
                                                </a>
                                            </li>
                                        @endif
                                            @if(__authorize(config('module.additional-parameters'),'view'))
                                        <li class="nav-item ">
                                            <a href="{{url('/additional-parameter')}}" class="nav-link ">
                                                <span class="title"><i class="fa fa-tags" aria-hidden="true"></i> Additinal Parameter</span>
                                            </a>
                                        </li>
                                           @endif
                                            @if(__authorize(config('module.transition-times'),'view'))
                                        <li class="nav-item ">
                                            <a href="{{url('/transition-times')}}" class="nav-link ">
                                                <span class="title"><i class="fa fa-clock-o" aria-hidden="true"></i> Transition Times</span>
                                            </a>
                                        </li>
                                            @endif

                                        <li class="nav-item">
                                            <a href="javascript:;" class="nav-link ">
                                                <span class="title"><i class="fa fa-h-square"></i>Specialities</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">

                                                @if(__authorize(config('module.specialities'),'view'))
                                                <li class="nav-item ">
                                                    <a href="{{url('/specialities')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-search"></i> List All</span>
                                                    </a>
                                                </li>


                                                <li class="nav-item ">
                                                    <a href="" class="nav-link ">
                                                        <span class="title"><i
                                                                    class="fa fa-life-bouy"></i>Services</span>
                                                        <span class="arrow"></span>
                                                    </a>
                                                    <ul class="sub-menu">
                                                        <li class="nav-item ">
                                                            <a href="{{url('/service')}}" class="nav-link ">
                                                                <span class="title"><i class="fa fa-search"></i> List All</span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item ">
                                                            <a href="{{url('/service-overview')}}" class="nav-link ">
                                                                <span class="title"><i class="fa fa-search"></i> Over View</span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item ">
                                                            <a href="{{url('/service-post-view')}}" class="nav-link ">
                                                                <span class="title"><i class="fa fa-file-o"></i> Service Post View</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class="nav-item">
                                                    <a href="{{url('/category-priority')}}" class="nav-link">
                                                        <span class="title"><i class="fa fa-sort-amount-asc"></i>Category Priority</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a href="{{url('/post')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-file-o"></i>Post</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a href="{{url('/rotations')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-repeat"></i>Rotations</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{url('/additional-grade/')}}" class="nav-link nav-toggle">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <span class="title">Additional Grades</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{url('/staff-overview/')}}" class="nav-link nav-toggle">
                                                        <i class="fa fa-users" aria-hidden="true"></i>
                                                        <span class="title">Staff Overview</span>
                                                    </a>
                                                </li>
                                                @endif
                                            </ul>
                                        </li>

                                            @if(__authorize(config('module.parameters'),'view'))
                                        <li class="nav-item ">
                                            <a href="{{url('/service/requirement/parameter')}}" class="nav-link ">
                                                <span class="title"><i class="fa fa-asterisk"></i> Parameters</span>
                                            </a>
                                        </li>
                                            @endif



                                        <li class="nav-item ">
                                            <a href="{{url('/rota-group')}}" class="nav-link ">
                                                <span class="title"><i
                                                            class="fa fa-object-group"></i> Rota Groups</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                @if(__authorize(config('module.rota-groups'),'view'))
                                                <li class="nav-item ">
                                                    <a href="{{url('/rota-group')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-search"></i> List All</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a href="{{url('/shift-type')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-clock-o"></i> Shifts</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a href="{{url('/rota-template')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-calendar-check-o"></i> Templates</span>
                                                    </a>
                                                </li>
                                                    @endif
                                            </ul>
                                        </li>
                                            @if(__authorize(config('module.staff'),'view'))
                                        <li class="nav-item ">
                                            <a href="{{url('/directorate-staff')}}" class="nav-link ">
                                                <span class="title"><i class="fa fa-user-md"></i> Staff</span>
                                            </a>
                                        </li>
                                            @endif

                                            @if(__authorize(config('module.grades'),'view'))
                                        <li class="nav-item">
                                            <a href="{{url('/grades/')}}" class="nav-link nav-toggle">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <span class="title">Grades</span>
                                            </a>
                                        </li>
                                            @endif

                                            @if(__authorize(config('module.roles'),'view'))
                                        <li class="nav-item">
                                            <a href="{{url('/roles/')}}" class="nav-link nav-toggle">
                                                <i class="fa fa-shield" aria-hidden="true"></i>
                                                <span class="title">Roles</span>
                                            </a>
                                        </li>
                                            @endif

                                            @if(__authorize(config('module.groups'),'view'))
                                        <li class="nav-item ">
                                            <a href="#" class="nav-link ">
                                                <span class="title"><i class="fa fa-object-group"></i> Groups</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item ">
                                                    <a href="{{url('/groups/')}}" class="nav-link ">
                                                        <span class="title"><i class="fa fa-search"></i> List All</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a href="{{url('/subgroups/')}}" class="nav-link ">
                                                        <span class="title"><i
                                                                    class="fa fa-sitemap"></i>Subgroups</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                            @endif
                                            @if(__authorize(config('module.category'),'view'))
                                        <li class="nav-item">
                                            <a href="javascript:;" class="nav-link ">
                                                <span class="title"><i i class="fa fa-snowflake-o"
                                                                       aria-hidden="true"></i> Category</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item">
                                                    <a href="{{url('/category/')}}" class="nav-link nav-toggle">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                        <span class="title">List All</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{url('/sub-category/')}}" class="nav-link nav-toggle">
                                                        <i class="fa fa-sitemap" aria-hidden="true"></i>
                                                        <span class="title">Sub Category</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                                @endif
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @if(__authorize(config('module.sites'),'view'))
                        <li class="nav-item ">
                            <a href="#" class="nav-link ">
                                <span class="title"><i class="fa fa-hospital-o"></i> Sites</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="{{url('/sites/')}}" class="nav-link ">
                                        <span class="title"><i class="fa fa-search"></i> List All</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{url('/locations/')}}" class="nav-link ">
                                        <span class="title"><i class="fa fa-map-marker"></i>Locations</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <!--                    <li class="nav-item">
                                                <a href="javascript:;" class="nav-link nav-toggle">
                                                    <i class="fa fa-object-ungroup"></i>
                                                    <span class="title">Templates</span>
                                                    <span class="selected"></span>
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li class="nav-item ">
                                                        <a href="/rota-template" class="nav-link ">
                                                            <span class="title"><i class="fa fa-calendar-check-o"></i>Rota</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>-->
                    </ul>
                </li>
            @endif
            <li class="nav-item">
                <a href="{{url('/profile')}}" class="nav-link main-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">Profiles</span>
                </a>
            </li>

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link main-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Setting</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu"
                    style="display: {{ in_array(Route::currentRouteName(),['user','create-new-template','update-template','user-emails','email-templates','trust-requests','permissions','add-role-permissions','user-roles','account-security','google-2f-authenticate','email-verification-disable','sms-verification-disable'])  ? 'block' : 'none' }}">

                    <li    class="nav-item {{ in_array(Route::currentRouteName(),['account-security','google-2f-authenticate','email-verification-disable','sms-verification-disable'])  ? 'active' : '' }}">
                        <a href="{{route('account-security')}}" class="nav-link  ">
                            <span class="title"><i class="fa fa-lock"></i> Account Security</span>
                        </a>
                    </li>

                    @if(__authorize(config('module.user'),'view'))
                    <li class="nav-item ">
                        <a href="{{url('/user')}}" class="nav-link ">
                            <span class="title"><i class="fa fa-users"></i> Manage Users</span>
                        </a>
                    </li>
                    @endif

                    @if(auth()->user()->is_admin)


                        <li class="nav-item ">
                            <a href="{{url('/user-roles')}}" class="nav-link ">
                                <span class="title"><i class="fa fa-users"></i> User Roles</span>
                            </a>
                        </li>

                        <li class="nav-item ">
                            <a href="{{url('/trust-requests')}}" class="nav-link ">
                                <span class="title"><i class="fa fa-users"></i> User Requests</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href="{{url('/email-templates')}}" class="nav-link ">
                                <span class="title"><i class="fa fa-envelope"></i> Email Templates</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href="javascript:;" class="nav-link ">
                                <span class="title"><i class="fa fa-cog"></i> Global Settings</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="{{url('/qualifications/')}}" class="nav-link ">
                                        <span class="title"><i class="fa fa-trophy" aria-hidden="true"></i> Qulifications </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{url('/colors/')}}" class="nav-link ">
                                        <span class="title"><i class="fa fa-paint-brush" aria-hidden="true"></i> Colors </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item ">
                            <a href="{{url('/links')}}" class="nav-link ">
                                <span class="title"><i class="fa fa-list"></i> Working Links</span>
                            </a>
                        </li>
                    @endif
                    <li class="nav-item ">
                        <a href="{{url('/user-emails')}}" class="nav-link ">
                            <span class="title"><i class="fa fa-envelope-o"></i> Emails</span>
                        </a>
                    </li>
                </ul>
            </li>

            {{--<li class="nav-item">
                <a target="_blank" href="{{env('CRM_LINK')}}" class="nav-link main-link nav-toggle">
                    <i class="fa fa-copyright"></i>
                    <span class="title">Nexin CRM</span>
                </a>
            </li>--}}

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
