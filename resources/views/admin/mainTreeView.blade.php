@extends('admin.admin')

@section('extra-css')
<!--<link rel="stylesheet" href="/css/MultiNestedList.css" />-->
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        @if(!empty($accountStatus))

           @if($accountStatus->status=="pending")
               <div class="col-sm-12 col-md-6  col-md-offset-3 text-center alert alert-success">
            <h2 class="form-title font-green">REQUEST PENDING FOR APPROVAL</h2>
            <h5 style="    line-height: 1.4;">Your Request has been farwarded to Trust Admin.You will be inform by email once your request accepted or rejected.</h5>
               </div>



           @elseif($accountStatus->status=="rejected")

                <div class="col-sm-12 col-md-6 col-md-offset-3 text-center alert alert-danger">
                    <h2 class="form-title font-green"><i class="fa fa-times">REQUEST REJECTED</i></h2>
                    <h5 style="    line-height: 1.4;">Sorry your request has been rejected by Trust Admin.</h5>
                </div>
            @endif

            @else
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Tree View of Nexin</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- <ul class="page-breadcrumb breadcrumb">
            {!! Breadcrumbs::render('home') !!}
        </ul> -->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row">
            <div class="col-md-12">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sort-amount-asc "></i>
                            <span class="caption-subject  bold uppercase">Trust Tree</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="tree">
                            <ul>

                                @if(!empty($trust->id))
                                <li><a href="{{url('/trust/edit/'.$trust->id)}}"> {{$trust->name}} </a>
                                    <ul>
                                        @if(isset ($trust->directorate) && count($trust->directorate))
                                        <li><a href="{{url('/directorates')}}"> Directorate </a>
                                            <ul>
                                                @foreach($trust->directorate as $directorate)
                                                <li><a href="{{url('/directorate/edit/'.$directorate->id)}}"> {{$directorate->name}} </a> - <a href="{{(isset($directorate->division)?'/division/edit/'.$directorate->division->id:'/divisions')}}"> ({{(isset($directorate->division)?$directorate->division->name:'N/A')}})</a>
                                                    <ul>
                                                        @if(isset ($directorate->speciality) && count($directorate->speciality))
                                                        <li><a href="{{url('/specialities')}}"> Speciality </a>
                                                            <ul>
                                                                @foreach($directorate->speciality as $speciality)
                                                                <li><a href="{{url('/speciality/edit/'.$speciality->id)}}"> {{$speciality->name}} </a>
                                                                    <ul>
                                                                        @if(isset ($speciality->service) && count($speciality->service))
                                                                        <li><a href="{{url('/service')}}"> Service </a>
                                                                            <ul>
                                                                                @foreach($speciality->service as $service)
                                                                                <li><a href="{{url('/service/edit/'.$service->id)}}"> {{$service->name}} </a></li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </li>
                                                                        @else
                                                                        <li> no service available </li>
                                                                        @endif
                                                                    </ul>
                                                                    <ul>
                                                                        @if(isset ($speciality->post) && count($speciality->post))
                                                                        <li><a href="{{url('/post')}}"> Post </a>
                                                                            <ul>
                                                                                @foreach($speciality->post as $post)
                                                                                <li><a href="{{url('/post/edit/'.$post->id)}}"> {{$post->name}} </a></li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </li>
                                                                        @else
                                                                        <li> no post available </li>
                                                                        @endif
                                                                    </ul>
                                                                    <ul>
                                                                        @if(isset ($speciality->additional_grade) && count($speciality->additional_grade))
                                                                        <li><a href="{{url('/additional-grade')}}"> Additional Grade </a>
                                                                            <ul>
                                                                                @foreach($speciality->additional_grade as $grade)
                                                                                <li><a href="{{url('/additional-grade/edit/'.$grade->id)}}"> {{$grade->name}} </a></li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </li>
                                                                        @else
                                                                        <li> no Additional Grade available </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no speciality available </li>
                                                        @endif
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->services) && count($directorate->services))
                                                        <li><a href="{{url('/directorate-services')}}"> Directorate Services </a>
                                                            <ul>
                                                                @foreach($directorate->services as $service)
                                                                <li><a href="{{url('/directorate-service/edit/'.$service->id)}}">{{$service->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Service available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->rota_group) && count($directorate->rota_group))
                                                        <li><a href="{{url('/rota-group')}}"> Rota Group </a>
                                                            <ul>
                                                                @foreach($directorate->rota_group as $rota_group)
                                                                <li><a href="{{url('/rota-group/edit/'.$rota_group->id)}}">{{$rota_group->name}}</a>
                                                                    <ul>
                                                                        @if(isset ($rota_group->rota_template) && count($rota_group->rota_template))
                                                                        <li><a href="{{url('/rota-template')}}"> Rota Template </a>
                                                                            <ul>
                                                                                @foreach($rota_group->rota_template as $template)
                                                                                <li><a href="{{url('/rota-group/edit/'.$template->id)}}">{{$template->name}}</a></li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </li>
                                                                        @else
                                                                        <li> no Rota Template available </li>
                                                                        @endif                                                                       
                                                                    </ul>
                                                                    <ul>
                                                                        @if(isset ($rota_group->shift_type) && count($rota_group->shift_type))
                                                                        <li><a href="{{url('/shift-type')}}"> Shift Type </a>
                                                                            <ul>
                                                                                @foreach($rota_group->shift_type as $shift_type)
                                                                                <li><a href="{{url('/shift-type/edit/'.$shift_type->id)}}">{{$shift_type->name}}</a></li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </li>
                                                                        @else
                                                                        <li> no Shift Type available </li>
                                                                        @endif                                                                       
                                                                    </ul>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Rota Group available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->rota_template) && count($directorate->rota_template))
                                                        <!--                                                                        <li><a href="/rota-template"> Rota Template </a>
                                                                                                                                    <ul>
                                                                                                                                        @foreach($directorate->rota_template as $rotatemplate)
                                                                                                                                        <li><a href="/rota-template/edit/{{$rotatemplate->id}}"> {{$rotatemplate->name}} </a>
                                                                                                                                            <ul>
                                                                                                                                                @if(isset ($rotatemplate->shift_type) && count($rotatemplate->shift_type))
                                                                                                                                                <li><a href="/shift-type/{{$rotatemplate->id}}"> Shift </a>
                                                                                                                                                    <ul>
                                                                                                                                                        @foreach($rotatemplate->shift_type as $shifttype)
                                                                                                                                                        <li><a href="/shift-type/edit/{{$shifttype->id}}"> {{$shifttype->name}} </a></li>
                                                                                                                                                        @endforeach
                                                                                                                                                    </ul>
                                                                                                                                                </li>
                                                                                                                                                @else
                                                                                                                                                <li> no shift available </li>
                                                                                                                                                @endif
                                                                                                                                            </ul>
                                                                                                                                        </li>
                                                                                                                                        @endforeach
                                                                                                                                    </ul>
                                                                                                                                </li>-->
                                                        @else
                                                        <li> no rota template available </li>
                                                        @endif
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->staff) && count($directorate->staff))
                                                        <li><a href="{{url('/directorate-staff')}}"> Staff </a>
                                                            <ul>
                                                                @foreach($directorate->staff as $staff)
                                                                <li><a href="{{url('/directorate-staff/edit/'.$staff->id)}}">GMC# {{$staff->gmc}} [{{$staff->name}}]</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Staff available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->grade) && count($directorate->grade))
                                                        <li><a href="{{url('/grades')}}"> Grade </a>
                                                            <ul>
                                                                @foreach($directorate->grade as $grade)
                                                                <li><a href="{{url('/grade/edit/'.$grade->id)}}">{{$grade->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Grade available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->role) && count($directorate->role))
                                                        <li><a href="{{url('/roles')}}"> Role </a>
                                                            <ul>
                                                                @foreach($directorate->role as $role)
                                                                <li><a href="{{url('/role/edit/'.$role->id)}}">{{$role->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Role available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->group) && count($directorate->group))
                                                        <li><a href="{{url('/groups')}}"> Group </a>
                                                            <ul>
                                                                @foreach($directorate->group as $group)
                                                                <li><a href="{{url('/group/edit/'.$group->id)}}">{{$group->name}}</a></li>
                                                                <ul>
                                                                    @if(isset ($group->subgroup) && count($group->subgroup))
                                                                    <li><a href="{{url('/subgroups')}}"> Sub Group </a>
                                                                        <ul>
                                                                            @foreach($group->subgroup as $subgroup)
                                                                            <li><a href="{{url('/subgroup/edit/'.$subgroup->id)}}">{{$subgroup->name}}</a></li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                    @else
                                                                    <li> no Sub Group available </li>
                                                                    @endif                                                                       
                                                                </ul>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Group available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                    <ul>
                                                        @if(isset ($directorate->category) && count($directorate->category))
                                                        <li><a href="{{url('/category')}}"> Category </a>
                                                            <ul>
                                                                @foreach($directorate->category as $category)
                                                                <li><a href="{{url('/category/edit/'.$category->id)}}">{{$category->name}}</a></li>
                                                                <ul>
                                                                    @if(isset ($category->sub_category) && count($category->sub_category))
                                                                    <li><a href="{{url('/sub-category')}}"> Sub Category </a>
                                                                        <ul>
                                                                            @foreach($category->sub_category as $subcategory)
                                                                            <li><a href="{{url('/sub-category/edit/'.$subcategory->id)}}">{{$subcategory->name}}</a></li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                    @else
                                                                    <li> no Sub Category available </li>
                                                                    @endif                                                                       
                                                                </ul>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no Category available </li>
                                                        @endif                                                                       
                                                    </ul>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        @else
                                        <li> no directorate available </li>                                        
                                        @endif
                                        @if(isset ($trust->site) && count($trust->site))
                                        <li><a href="{{url('/sites')}}"> Site </a>
                                            <ul>
                                                @foreach($trust->site as $site)
                                                <li><a href="{{url('/site/edit/'.$site->id)}}"> {{$site->name}} </a>
                                                    <ul>
                                                        @if(isset ($site->location) && count($site->location))
                                                        <li><a href="{{url('/locations')}}"> Location </a>
                                                            <ul>
                                                                @foreach($site->location as $location)
                                                                <li><a href="{{url('/location/edit/'.$location->id)}}"> {{$location->name}} </a></li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li> no location available </li>
                                                        @endif
                                                    </ul>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                    @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
            <!-- END PAGE BASE CONTENT -->

   @endif

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@endsection

@section('extra-js')
<script type="text/javascript" src="{{url('/js/MultiNestedList.js')}}"></script>
@endsection
