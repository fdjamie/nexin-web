<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <h3 class="menu-title ">Features</h3>
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link main-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">Users</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu"
                    style="">

                    <li class="nav-item ">
                        <a href="" class="nav-link  ">
                            <span class="title"><i class="fa fa-lock"></i> List All</span>
                        </a>
                    </li>


                </ul>
            </li>
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link main-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">User Groups</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu"
                    style="">

                    <li class="nav-item ">
                        <a href="" class="nav-link  ">
                            <span class="title"><i class="fa fa-lock"></i> List All</span>
                        </a>
                    </li>



                </ul>
            </li>
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link main-link nav-toggle">
                    <i class="fa fa-home"></i>
                    <span class="title">Trusts</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu"
                    style="">

                    <li class="nav-item ">
                        <a href="" class="nav-link  ">
                            <span class="title"><i class="fa fa-lock"></i> List All</span>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="" class="nav-link  ">
                            <span class="title"><i class="fa fa-lock"></i> Assign Trust</span>
                        </a>
                    </li>


                </ul>
            </li>
            <li class="nav-item">
                <a href="{{url('/profile')}}" class="nav-link main-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">Profiles</span>
                </a>
            </li>

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link main-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Setting</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu"
                    style="display: {{ in_array(Route::currentRouteName(),['user','permissions','add-role-permissions','user-roles','account-security','google-2f-authenticate','email-verification-disable','sms-verification-disable'])  ? 'block' : 'none' }}">

                    <li class="nav-item {{ in_array(Route::currentRouteName(),['account-security','google-2f-authenticate','email-verification-disable','sms-verification-disable'])  ? 'active' : '' }}">
                        <a href="{{route('account-security')}}" class="nav-link  ">
                            <span class="title"><i class="fa fa-lock"></i> Account Security</span>
                        </a>
                    </li>


                </ul>
            </li>


        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
