

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Nexin Trust</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #4 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('/assets/global/plugins/bootstrap/bootstrap.min.css')}}" type="text/css" >
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{asset('/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{asset('/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{asset('/assets/layouts/layout4/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/assets/layouts/layout4/css/themes/default.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{asset('/assets/layouts/layout4/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" integrity="sha256-SMGbWcp5wJOVXYlZJyAXqoVWaE/vgFA5xfrH3i/jVw0=" crossorigin="anonymous" />-->
        <script src="{{asset('/assets/global/scripts/jquery-2.2.3.min.js')}}"></script>
        <script type="text/javascript"> APP_URL ={!! json_encode(url('/')) !!};</script>
        <script src="{{asset('/assets/global/plugins/bootstrap/bootstrap.min.js')}}" ></script>
        <link href="{{asset('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{url('/assets/images/favicon.ico')}}" /> 

        <link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
        @yield('extra-css')
    </head>
    <!-- END HEAD -->
    <style>
        .get-date{
            padding: 20px 0px !important;
        }
    </style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        @include('admin.adminHeader')
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->

        <!-- BEGIN CONTENT -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
        @if(Auth::user()->is_admin==2)
            @include('admin.systemAdminSidebar')
            @else
            @include('admin.adminSidebar')
        @endif

            <!-- END SIDEBAR -->
            @yield('content')
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner col-md-offset-4 col-md-4 text-center"> 2016 &copy; Nexin Trust</div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        @yield('modal')
        <script src="{{asset('/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script> 
        <script src="{{asset('/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/assets/layouts/layout4/scripts/layout.min.js')}}" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js" integrity="sha256-1hjUhpc44NwiNg8OwMu2QzJXhD8kcj+sJA3aCQZoUjg=" crossorigin="anonymous"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                jQuery("#date-header").datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#get-date').click(function () {
                    jQuery.ajax({
                        url: APP_URL+'/set-date',
                        type: 'post',
                        data: {date: $('#date-header').val()},
                        success: function (data) {
                            console.log(data);
                            location.reload();
                        }
                    });
                });
                
            });
            function getTrust(v) {

                if(v)
                {
                    window.location.href = "{{url('')}}/switch-trust/"+v;
                }
            }


            function checkForViewPermission(t) {
                  var name=$(t).attr('data-name');
                  if(name=="view") {
                      if (!$(t).is(":checked")) {
                          $(t).closest('tr').find('input:checkbox').removeAttr('checked');
                      }

                  }
                  else
                  {
                      var s=$(t).closest('tr').find('input[data-name*="view"]');
                      if(!$(s).is(":checked")){
                          $(t).removeAttr('checked');
                      }
                  }
                  }




        </script>
        @yield('extra-js')
        
    </body>

</html>