@extends ('layouts.admin')
@section('extra-css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.1/themes/default/style.min.css">
@endsection

@section('content')
<div class="container">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Menu Tree</div>
            <div class="panel-body">
                <div class="portlet light bordered col-md-6">
                    <div class="portlet-title"></div>
                    <div class="portlet-body">
                        <div id="tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Details</div>
            <div class="panel-body">
                <div class="portlet light bordered col-md-12">
                    <div class="portlet-title"><h3>Details of the contents</h3></div>
                    <div class="container">Actual Contents will be updated here...</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('extra-js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.1/jstree.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tree').jstree({'core': {data: {dataType: 'json', url: '/test', "check_callback": true}, "plugins": ["contextmenu","wholerow"], }}).on('loaded.jstree', function () {
            $('#tree').jstree('open_all');
        }).bind("select_node.jstree", function (e, data) {
            var id = data.node.id;
            var node = id.split("-");
            console.log(id); 
//            console.log(data); 
//            var inst = data.inst;
//            var level = inst.get_path().length;
//            var selected = inst.get_selected();
//            var id = selected.attr('id');
//            var name = selected.prop('tagName');
//            console.log(name, id, level);
        });
    });




</script>
@endsection