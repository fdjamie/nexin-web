@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<?php 
//echo '<pre>';
//print_r($divisions);
//die;
?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage Divisions</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
<!--        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/division">Divisions</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-ambulance fa-2x"></i>Divisions 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="/division/restore">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Restore All <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th> Name </th>
                                    <th> Trust Name </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($divisions as $division)
                                <tr class="odd gradeX">
                                    <td>{{$division->id}}</td>
                                    <td>{{$division->name}}</td>
                                    <td>{{$division->trust->name}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/division/edit/'.$division->id)}}"><i class="fa fa-repeat" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id="{{$division->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body"> You want to delete this record? </div>
            <div class="modal-footer">
                <a href="" id='delete-button'><button type="submit" class="btn btn-danger" >Delete</button></a>
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '/division/delete/' + $(this).attr('id'));
    });
</script>
@endsection