@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-ambulance fa-2x"></i>{{$division->name}} 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-location'))
                        <div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            @foreach(session('error-location') as $errornode)
                                <ul>
                                    @foreach($errornode as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                    @endforeach
                                </ul>
                            @endforeach
                        </div>
                    @endif
                    <div class="portlet-body">
                            {!! Form::open(['method' => 'put', 'action' => ['DivisionController@update', $division->id]]) !!}
                            <div class="form-group">
                                {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                                {!! Form::text('name', $division->name, ['class' => 'form-control']); !!}
                            </div>
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
