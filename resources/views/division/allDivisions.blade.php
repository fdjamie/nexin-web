@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-ambulance fa-2x"></i>Divisions 
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-division'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-division') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-division'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-division') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/division/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn green bold" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        <a href="{{url('/division/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn green bold"> Export Division <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th style="width: 50px">Id</th>
                                    <th> Name </th>
                                    <th> Trust Name </th>
                                    <th style="width: 100px;text-align: center;"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($trusts->division))
                                @forelse ($trusts->division as $division)
                                <tr class="odd gradeX">
                                    <td>{{$division->id}}</td>
                                    <td>{{$division->name}}</td>
                                    <td>{{$trusts->name}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/division/edit/'.$division->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id="{{$division->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/division/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection