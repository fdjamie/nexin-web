@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" integrity="sha256-lBtf6tZ+SwE/sNMR7JFtCyD44snM3H2FrkB/W400cJA=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.min.css" integrity="sha256-om6WdVA9jl2Y5FpRyiHQWboI3PRXcY9SDh9UuYOxRtA=" crossorigin="anonymous" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Multi Location -  {{$shift_type->name}}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="clearfix portlet light">
                <div class="col-md-offset-6 col-md-6">
                    <table class="table table-striped table-bordered table-hover" >
                        <tbody>
                            <tr><td width="50%"><b>Name</b></td><td width="50%">{{$shift_type->name}}</td></tr>
                            <tr><td width="50%"><b>Rota Group</b></td><td width="50%">{{$shift_type->rota_group->name}}</td></tr>
                            <tr><td width="50%"><b>Start Time</b></td><td width="50%">{{$shift_type->start_time}}</td></tr>
                            <tr><td width="50%"><b>End Time</b></td><td width="50%">{{$shift_type->finish_time}}</td></tr>
                            <tr><td width="50%"><b>Over Rides Teaching</b></td><td width="50%">{{(($shift_type->overrides_teaching==0)? 'No' : 'Yes')}}</td></tr>
                            <tr><td width="50%"><b>Number Of Locations</b><i class="fa fa-refresh fa-spin" aria-hidden="true" id="splitSpin"></i></td><td width="50%">{!! Form::number('splits', 0,['class' => 'form-control','id'=>'splits','min'=>0,'max'=>2]); !!}</td></tr>
                            <tr id="split_one_time"><td width="50%" ><b>Location 1 time</b></td><td width="50%">{!! Form::text('split_one_time', null,['class' => 'form-control time','id'=>'split_1_time','disabled'=>'disabled']); !!}</td></tr>
                            <tr id="split_two_time"><td width="50%" ><b>Location 2 time</b></td><td width="50%">{!! Form::text('split_two_time', null,['class' => 'form-control time','id'=>'split_2_time','disabled'=>'disabled']); !!}</td></tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                @if (Illuminate\Support\Facades\Session::has('error-shift-detail'))
                <div class='alert alert-danger alert-dismissible' role='alert'>
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                    @foreach(session('error-shift-detail') as $errornode)
                    <ul>
                        @foreach($errornode as $error)
                        <li>
                            {{$error}}
                        </li>
                        @endforeach
                    </ul>
                    @endforeach
                </div>
                @endif
                <div class="clearfix"></div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                {!! Form::open(['method' => 'post', 'id'=>'addRotaTemplate','action'=>['HomeController@storeShiftDetail']]) !!}
                <div class="col-md-4 col-sm-4">
                    <div class="portlet light bordered ">
                        <div class="portlet-title">
                            <div class="caption">Location 1</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group">
                                {!! Form::label('shift', 'Shift', ['class' => 'awesome']); !!}
                                {!! Form::text('shift', $shift_type->name,['class' => 'form-control','id'=>'name','disabled'=>'disabled']); !!}
                                {!! Form::hidden('shift_id',$shift_type->id,['id'=>'shift_id']); !!}
                                {!! Form::hidden('directorate_id',$shift_type->directorate_id,['id'=>'directorate_id']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                                {!! Form::select('directorate_speciality_id[0]', $specialities,$shift_type->directorate_speciality_id,['class' => 'form-control directorate_speciality_id','id'=>'directorate_speciality_id']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('category_check', 'Category', ['class' => 'awesome']); !!} {!! Form::checkbox('category_check',1,true, ['id'=>'category_check','class'=>'category_check']); !!}
                                {!! Form::select('category_id[0]', $categories ,null, [ 'class'=>'form-control category_id','id'=>'category_id']); !!}
                            </div>
                            <div class="form-group">
                                <!--{!! Form::label('sub_category_check', 'Sub Category', ['class' => 'awesome']); !!}-->
                                 <a data-toggle="modal" href="#small" id="modal-display">{!! Form::label('sub_category_check', 'Sub Category', ['class' => 'awesome']); !!} 
                                  </a>
                                {!! Form::checkbox('sub_category_check',1,true, ['id'=>'sub_category_check','class'=>'sub_category_check']); !!}
                                {!! Form::select('sub_category_id[0]', $sub_categories ,null, [ 'class'=>'form-control sub_category_id','id'=>'sub_category_id']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('service_id', 'Service', ['class' => 'awesome']); !!} {!! Form::checkbox('service_check',1,true, ['id'=>'service_check','class'=>'service_check']); !!}
                                {!! Form::select('service_id[0]', $combined_services, $shift_type->service_type.'-'.$shift_type->service_id,['class' => 'form-control service_id','id'=>'service_id']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                                {!! Form::text('start_time[0]', $shift_type->start_time,['class' => 'form-control time','id'=>'start_time']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                                {!! Form::text('end_time[0]', $shift_type->finish_time,['class' => 'form-control time','id'=>'end_time']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('overrides_teaching', 'Overrides Teaching ?', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('overrides_teaching[0]',1,true,['id'=>'overrides_teaching','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('nroc', 'Non residential on call?', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('nroc[0]',1,true,['id'=>'nroc','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('on_site', 'On Site', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('on_site[0]',1,true,['id'=>'on_site','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="col-md-4 col-sm-4">
                    <div class="portlet light bordered ">
                        <div class="portlet-title">
                            <div class="caption">Location 2</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group">
                                {!! Form::label('shift', 'Shift', ['class' => 'awesome']); !!}
                                {!! Form::text('shift', $shift_type->name,['class' => 'form-control','id'=>'name','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                                {!! Form::select('directorate_speciality_id[1]', $specialities,$shift_type->directorate_speciality_id,['class' => 'form-control split_2 directorate_speciality_id','id'=>'directorate_speciality_id_1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('category_check_1', 'Category', ['class' => 'awesome']); !!} 
                                {!! Form::checkbox('category_check_1',1,true, ['id'=>'category_check_1','class'=>'split_2 category_check']); !!}
                                {!! Form::select('category_id[1]', $categories ,null, [ 'class'=>'form-control category_id split_2','id'=>'category_id_1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                <!--{!! Form::label('sub_category_check_1', 'Sub Category', ['class' => 'awesome']); !!}-->
                                <a data-toggle="modal" href="#small" id="modal-display">{!! Form::label('sub_category_check_1', 'Sub Category', ['class' => 'awesome']); !!} 
                                  </a>
                                {!! Form::checkbox('sub_category_check_1',1,true, ['id'=>'sub_category_check_1','class'=>'split_2 sub_category_check']) !!}
                                {!! Form::select('sub_category_id[1]', $sub_categories ,null, [ 'class'=>'form-control sub_category_id split_2','id'=>'sub_category_id_1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group"> 
                                {!! Form::label('service_id', 'Service', ['class' => 'awesome']); !!} {!! Form::checkbox('service_check_1',1,true, ['id'=>'service_check_1','class'=>'service_check split_2']); !!}
                                {!! Form::select('service_id[1]', $combined_services, $shift_type->service_type.'-'.$shift_type->service_id,['class' => 'form-control split_2 service_id','id'=>'service_id_1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                                {!! Form::text('start_time[1]', '',['class' => 'form-control split_2 time','id'=>'start_time_1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                                {!! Form::text('end_time[1]', $shift_type->finish_time,['class' => 'form-control split_2 time','id'=>'end_time_1','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('overrides_teaching_1', 'Overrides Teaching ?', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('overrides_teaching[1]',1,true,['id'=>'overrides_teaching_1','class'=>'make-switch split_2', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('nroc_1', 'Non residential on call?', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('nroc[1]',1,true,['id'=>'nroc_1','class'=>'make-switch split_2', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('on_site', 'On Site', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('on_site[1]',1,true,['id'=>'on_site','class'=>'split_2 make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="col-md-4 col-sm-4">
                    <div class="portlet light bordered ">
                        <div class="portlet-title">
                            <div class="caption">Location 3</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group">
                                {!! Form::label('shift', 'Shift', ['class' => 'awesome']); !!}
                                {!! Form::text('shift', $shift_type->name,['class' => 'form-control','id'=>'name','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                                {!! Form::select('directorate_speciality_id[2]', $specialities,$shift_type->directorate_speciality_id,['class' => 'form-control split_3 directorate_speciality_id','id'=>'directorate_speciality_id_2','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('category_check_2', 'Category', ['class' => 'awesome']); !!} {!! Form::checkbox('category_check_2',1,true, ['id'=>'category_check_2','class'=>'split_3 category_check']); !!}
                                {!! Form::select('category_id[2]', $categories ,null, [ 'class'=>'form-control category_id split_3','id'=>'category_id_2','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                 <a data-toggle="modal" href="#small" id="modal-display">
                                     {!! Form::label('sub_category_check_2', 'Sub Category', ['class' => 'awesome']); !!} 
                                  </a>
                                {!! Form::checkbox('sub_category_check_2',1,true, ['id'=>'sub_category_check_2','class'=>'split_3 sub_category_check']); !!}
                                {!! Form::select('sub_category_id[2]', $sub_categories ,null, [ 'class'=>'form-control sub_category_id split_3','id'=>'sub_category_id_2','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('service_id', 'Service', ['class' => 'awesome']); !!} {!! Form::checkbox('service_check_2',1,true, ['id'=>'service_check_2','class'=>'service_check split_3']); !!}
                                {!! Form::select('service_id[2]', $combined_services, $shift_type->service_type.'-'.$shift_type->service_id,['class' => 'form-control split_3 ','id'=>'service_id_2','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                                {!! Form::text('start_time[2]', '',['class' => 'form-control split_3 time','id'=>'start_time_2','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                                {!! Form::text('end_time[2]', $shift_type->finish_time,['class' => 'form-control split_3 time','id'=>'end_time_2','disabled'=>'disabled']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('overrides_teaching_2', 'Overrides Teaching ?', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('overrides_teaching[2]',1,true,['id'=>'overrides_teaching_2','class'=>'make-switch split_3', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('nroc_2', 'Non residential on call?', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('nroc[2]',1,true,['id'=>'nroc_2','class'=>'make-switch split_3', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('on_site', 'On Site', ['class' => 'awesome']); !!}
                                {!! Form::checkbox('on_site[2]',1,true,['id'=>'on_site','class'=>'make-switch split_3', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.categoryTreeModal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.min.js" integrity="sha256-LPgEyZbedErJpp8m+3uasZXzUlSl9yEY4MMCEN9ialU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js" integrity="sha256-AKUJYz2DyEoZYHh2/+zPHm1tTdYb4cmG8HC2ydmTzM4=" crossorigin="anonymous"></script>
<script src="{{url('/js/shift_detail.js')}}" type="text/javascript"></script>
@endsection
