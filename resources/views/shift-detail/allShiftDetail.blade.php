@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>{{$shift_type->name}}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-clock-o"></i>{{$shift_type->name}}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-shift-detail'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-shift-detail') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-shift-detail'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-shift-detail') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/shift/'.$shift_type->id.'/shift-detail/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" data-toggle="modal" href="#basic"> Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th> Id </th>
                                    <th> Start Time </th>
                                    <th> End Time </th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($shift_detail as $detail)
                                <tr>
                                    <td width="15%"> {{$detail->id}} </td>
                                    <td width="15%"> {{$detail->start_time}} </td>
                                    <td width="15%"> {{$detail->end_time}} </td>
                                    <td class="text-center"> 
                                        <a href="{{url('/shift-detail/edit/'.$detail->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id="{{$detail->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/shift-detail/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection