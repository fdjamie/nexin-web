@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="/css/custom.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Category</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user-md"></i>Category 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <div class="clearfix overflow-scroll">
                            <table class="table table-responsive  table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th> Directorate Name </th>
                                        <th> Parameter Name </th>
                                        <th> Values </th>
                                        <th> Result </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($viewData as $directorate)
                                    @if(isset($directorate['diractorate_err']) || isset($directorate['diractorate_not_exits'])|| isset($directorate['directorate_required'])|| isset($directorate['parameter_required']) || isset($directorate['values_required']))
                                    <tr class="bg-danger">
                                        @else
                                    <tr class="odd gradeX">
                                        @endif    
                                        <td>{{ $directorate['name'] }}</td>
                                        <td>{{ $directorate['values'] }}</td>
                                        <td>{{ $directorate['directorate'] }}</td>
                                        <td class="table-danger">
                                            <ul>
                                                @if(isset($directorate['diractorate_err']))<li class="list-group-item-danger"> {{$directorate['diractorate_err']}}</li>@endif 
                                                @if(isset($directorate['diractorate_not_exits']))<li class="list-group-item-danger"> {{$directorate['diractorate_not_exits']}}</li>@endif 
                                                @if(isset($directorate['directorate_required']))<li class="list-group-item-danger"> {{$directorate['directorate_required']}}</li>@endif 
                                                @if(isset($directorate['parameter_required']))<li class="list-group-item-danger"> {{$directorate['parameter_required']}}</li>@endif 
                                                @if(isset($directorate['values_required']))<li class="list-group-item-danger"> {{$directorate['values_required']}}</li>@endif 
                                            </ul>
                                                
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-toolbar">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="/category/new">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'url' => route('additional-parameter-upload') ]) !!}
                                        {!! Form::hidden('fileName',$fileName); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green']); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection