@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tags" aria-hidden="true"></i>Add Additional Parameter
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-parameter'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-parameter') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['AdditionalParameterController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates, null,['class' => 'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group clearfix">
                            {!! Form::label('parameter', 'Parameter', ['class' => 'awesome']); !!}
                            {!! Form::text('parameter', '',['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group clearfix">
                            {!! Form::label('value', 'value', ['class' => 'awesome']); !!}
                            {!! Form::text('value', '',['class' => 'form-control','id'=>'value']); !!}
                            {!! Form::label('value', 'Please enter comma seprated values in value field.', ['class' => 'awesome']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
        
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script>
    $(document).ready(function () {
        $('#sp_spin').hide();
        var updateSpeciality = function (id) {
            $('#directorate_speciality_id').hide();
            $('#sp_spin').show();
            jQuery.ajax({
                url: APP_URL+'/getdirectoratespeciality/' + id,
                type: 'get',
                success: function (data) {
                    var speciality = '';
                    $.each(data, function (index, value) {
                        speciality += "<option value='" + index + "'>" + value + "</option>";
                    });
                    $('#directorate_speciality_id').html(speciality).show();
                    $('#sp_spin').hide();
                }
            });

            };
            $('#directorate_id').change(function () {
                updateSpeciality($(this).val());
            });
        });
    </script>
    @endsection