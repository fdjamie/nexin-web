@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title col-md-12">
                        <div class="caption">
                            <i class="fa fa-tags" aria-hidden="true"></i>Additional Parameter 
                        </div>
                    </div>

                    <div class="portlet-body">
                        @if (Illuminate\Support\Facades\Session::has('success-parameter'))
                        <div class='alert alert-success alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            {{ session('success-parameter') }}
                        </div>
                        @elseif (Illuminate\Support\Facades\Session::has('error-parameter'))
                        <div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            {{ session('error-parameter') }}
                        </div>
                        @endif
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        @if(__authorize(config('module.additional-parameters'),'add'))
                                        <a href="{!! url('/additional-parameter/new')!!}">
                                            <button id="sample_editable_1_2_new" class="btn green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">

                                        <a href="{!!  route('additional-parameter-export') !!}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning"> Export Additional Parameters <i class="fa fa-file-excel-o"></i></button>
                                        </a>


                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($directorates)==0)
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                    <thead>
                                        <tr>
                                            <th colspan="5"> No Directorate added in the system.</th>
                                        </tr>
                                    </thead>
                                </table>
                                @endif
                                <ul class="nav nav-tabs">
                                    @foreach($directorates as $directorate)
                                    @if($directorate->id == $directorates[0]->id)
                                    <li class="active">
                                        @else
                                    <li>
                                        @endif
                                        <a href="#tab_{{$directorate->id}}" data-toggle="tab">{{ $directorate->name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    @foreach($directorates as $directorate)
                                    @if($directorate->id == $directorates[0]->id)
                                    <div class="tab-pane fade active in" id="tab_{{$directorate->id}}">
                                        @else
                                        <div class="tab-pane fade" id="tab_{{$directorate->id}}">
                                            @endif
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Value</th>
                                                        <th class="text-center">Parameter</th>
                                                        <th class="text-center"> Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($directorate->additional_parameter as $parameter)
                                                    <tr>
                                                        <td class="text-center">{{$parameter->parameter}}</td>
                                                        <td class="text-center">{{$parameter->value}}</td>
                                                        <td class="text-center">
                                                            @if(__authorize(config('module.additional-parameters'),'edit'))
                                                            <a href="additional-parameter/edit/{{$parameter->id}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                            @endif
                                                                @if(__authorize(config('module.additional-parameters'),'delete'))
                                                            <a data-toggle="modal" href="#small" id="{{$parameter->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                             @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', 'additional-parameter/delete/' + $(this).attr('id'));
    });
    $('#directorate_id').change(function () {
        $('form').submit();
    });
</script>
@endsection