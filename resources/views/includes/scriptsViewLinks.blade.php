<script src="{{url('/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>