@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        tr,th{
            text-align: center;
        }
        </style>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-users"></i>Users 
                        </div>
                    </div>
                    @if ($errors->has('msg'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <span class="help-block">
                            @foreach ($errors->all() as $error)
                            {{ $error }} 
                            @endforeach
                        </span>
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        @if(__authorize(config('module.user'),'add'))
                                        <a href="{{url('/user/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                   {{-- <th>Id</th>--}}
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>EMAIL</th>
                                  {{--  <th>TRUST</th>--}}
                                    <th>Roles</th>
                                   {{-- <th>ADMIN</th>--}}
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse ($users as $K=>$user)
                                <tr>
                                    <td>{{--{{ $user->user->id }}--}}{{$K+1}}</td>
                                    <td>{{ $user->user->name }}</td>
                                    <td>{{ $user->user->email }}</td>
                                    <td>

                                        @if(empty($user->user_trust_rules))
                                            @if($user->user->is_admin)

                                                Trust Admin
                                                @else
                                                -
                                            @endif
                                        @else
                                            @foreach($user->user_trust_rules as $ur)

                                                ({{$ur->roles->name}})

                                                @endforeach

                                        @endif

                                    </td>
{{--
                                    <td>{{ $user->is_admin?"Enable":"Disable" }}</td>
--}}
                                    <td class="text-center">
                                        @if(!$user->user->is_admin)
                                        @if(__authorize(config('module.user'),'edit'))
                                        <a href="{{url('/user/edit/'.base64_encode($user->id))}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        @endif
                                            @if(__authorize(config('module.user'),'delete'))
                                         <a data-toggle="modal" href="#small" id="{{$user->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        @endif
                                        @endif

                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/user/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection