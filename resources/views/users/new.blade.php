@extends('admin.admin')

@section('content')

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-user"></i>Add User
                            </div>
                        </div>
                        @if ($errors->has('msg'))
                            <div class='alert alert-danger alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <span class="help-block">
                            @foreach ($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                        </span>
                            </div>
                        @endif
                        <div class="portlet-body">
                            {!! Form::open(['method' => 'post', 'action' => ['HomeController@storeUser']]) !!}
                           {{-- <div class="form-group">
                                {!! Form::label('is_admin', 'Administrator', ['class' => 'awesome']); !!}
                                {!! Form::select('is_admin', \App\Http\Helpers::booleanDropDown(), 1 ,['class' => 'form-control']); !!}
                            </div>--}}
                            {{--<div class="form-group">
                                {!! Form::label('trust', 'Trust', ['class' => 'awesome']); !!}
                                {!! Form::select('trust_id', $trusts, null ,['class' => 'form-control']); !!}
                            </div>--}}
                            <div class="form-group {{ $errors->has('roles') ? ' has-error' : '' }}">
                                {!! Form::label('Roles', 'Select Roles', ['class' => 'awesome']); !!}
                               {{-- {!! Form::select('roles[]', $roles, null ,['class' => 'form-control','multiple' => 'multiple']); !!}--}}
                                 <select class="form-control" name="roles[]" multiple>
                                     @foreach($roles as $r)
                                         <option value="{{@$r->pivot->id}}">{{$r->name}}</option>
                                         @endforeach

                                 </select>

                            @if ($errors->has('roles'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('roles') }}</strong>
                            </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::label('name', 'Full Name', ['class' => 'awesome']); !!}
                                {!! Form::text('name', old('name'),['placeholder' => 'Full Name' , 'class' => 'form-control']); !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                                {!! Form::label('email', 'Email', ['class' => 'awesome']); !!}
                                {!! Form::text('email', old('email'),['placeholder' => 'Email' , 'class' => 'form-control']); !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                                {!! Form::label('phone', 'Phone', ['class' => 'awesome']); !!}
                                {!! Form::text('phone', old('phone'),['placeholder' => 'Phone' , 'class' => 'form-control']); !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {!! Form::label('password', 'Password', ['class' => 'awesome']); !!}
                                {!! Form::password('password', ['placeholder' => 'Password' , 'class' => 'form-control']); !!}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'awesome']); !!}
                                {!! Form::password('password_confirmation', ['placeholder' => 'Re-type Your Password' , 'class' => 'form-control']); !!}
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
