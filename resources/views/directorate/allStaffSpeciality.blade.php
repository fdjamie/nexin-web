

@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <!--<h1>Manage Directorate Staff</h1>-->
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        {{--!! Breadcrumbs::render('directoratespeciality') !!--}}
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title col-md-3">
                        <div class="caption">
                            <i class="fa fa-md"></i>Directorate Staff Members 
                        </div>
                    </div>
                    {!! Form::open(['method' => 'post', 'action' => ['HomeController@indexDirectorateSpecialityStaff']]) !!}
                    <div class="form-group col-md-3">
                        {!! Form::label('start_date', 'Start date', ['class' => 'awesome']); !!}
                        {!! Form::text('start_date', $start_date,[ 'class' => 'form-control datepicker', 'id'=>'start_date']); !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('end_date', 'End date', ['class' => 'awesome']); !!}
                        {!! Form::text('end_date',$end_date ,[ 'class' => 'form-control datepicker', 'id'=>'end_date']); !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                        {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate']); !!}
                    </div>
                    <div class="clearfix"></div>
                    <h5><b>Grades</b></h5>
                    <div class="col-md-12">
                        {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!}
                        {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                    </div>
                    @foreach($grades as $grade)
                    <div class="form-group col-md-3">
                        @if(in_array($grade->id,$selected_grades))
                        {!! Form::checkbox('selected_grades[]', $grade->id ,true, ['class'=>'check-boxes','id'=>$grade->name]); !!}
                        @else
                        {!! Form::checkbox('selected_grades[]', $grade->id ,false, ['class'=>'check-boxes','id'=>$grade->name]); !!}
                        @endif
                        {!! Form::label($grade->name, $grade->name, ['class' => 'awesome']); !!}
                    </div>
                    @endforeach
                    <div class="clearfix"></div>
                    <h5><b>Roles</b></h5>
                    <div class="col-md-12">
                        {!! Form::checkbox('check_all', 1,false,['id'=>'check_all_roles']); !!}
                        {!! Form::label('check_all_roles', 'All', ['class' => 'awesome']); !!}
                    </div>
                    @foreach($roles as $role)
                    <div class="form-group col-md-3">
                        @if(in_array($role->id,$selected_roles))
                        {!! Form::checkbox('selected_roles[]', $role->id ,true, ['class'=>'check-boxes-roles','id'=>$role->name]); !!}
                        @else
                        {!! Form::checkbox('selected_roles[]', $role->id ,false, ['class'=>'check-boxes-roles','id'=>$role->name]); !!}
                        @endif
                        {!! Form::label($role->name, $role->name, ['class' => 'awesome']); !!}
                    </div>
                    @endforeach
                    <div class="clearfix"></div>
                    <div class="form-group">
                        {!! Form::submit('Search !', ['class'=> 'btn green']); !!}
                    </div>
                    {!! Form::close() !!}

                    @if (Illuminate\Support\Facades\Session::has('success-staff'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-staff') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-staff'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-staff') }}
                    </div>
                    @endif

                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialities as $speciality_node)
                            @php
                            $total_staff = 0;
                            $free_staff = 0;
                            $busy_staff = 0;
                            @endphp
                            @if($speciality_node->id == $specialities[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$speciality_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$speciality_node->id}}">
                                    @endif
                                    <!--<table class="table table-striped table-bordered table-hover table-checkable order-column" >-->
                                    <table class="table table-striped table-bordered table-hover table-checkable"  >
                                        <thead>
                                            <tr>
                                                <th> Id</th>
                                                <th> Post </th>
                                                <th> Staff </th>
                                                <th> GMC </th>
                                                <th> Grade </th>
                                                <th> Role </th>
                                                <th> Appointed From  </th>
                                                <th> Appointed Till </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($speciality_node->staff))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($speciality_node->staff as $staff)
                                            @php
                                            if($staff->appointed_from == '0000-00-00' || $staff->appointed_from == '1111-00-00')
                                            {
                                            $start = new Carbon ();
                                            $start = $start->format('Y-m-d');
                                            }else{
                                            $start = new Carbon($staff->appointed_from);
                                            $start = $start->format('Y-m-d');
                                            }
                                            if($staff->appointed_till == '0000-00-00' || $staff->appointed_till == '1111-00-00')
                                            {
                                            $end = new Carbon ('last day of next year');
                                            $end = $end->format('Y-m-d');
                                            }else{
                                            $end = new Carbon($staff->appointed_till);
                                            $end = $end->format('Y-m-d');
                                            }
                                            $total_staff = count($speciality_node->staff);
                                            if(count($staff->post))
                                            $busy_staff ++;
                                            $free_staff =   $total_staff -  $busy_staff; 
                                            @endphp
                                            @if((($start_date <= $start && $end_date >= $start) || ($start_date <= $end && $end_date >= $end)) && ((in_array($staff->grade_id,$selected_grades)) || (in_array($staff->role_id,$selected_roles))))
                                            <tr class="odd gradeX">
                                                <td width='14%'>{{$staff->id}}</td>
                                                <td>
                                                    @if(isset($staff->post))
                                                    {{$staff->post->name}}
                                                    @else
                                                    AVAILABLE
                                                    @endif
                                                </td>
                                                <td width='14%'>{{$staff->name}}</td>
                                                <td width='14%'>{{$staff->gmc}}</td>
                                                <td width='14%'>{{$staff->grade->name}}</td>
                                                <td width='14%'>{{$staff->role->name}}</td>
                                                <td>
                                                    @if(isset($staff->appointed_from) && $staff->appointed_from != '0000-00-00')
                                                    {{$staff->appointed_from}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($staff->appointed_till) && $staff->appointed_till != '0000-00-00')
                                                    {{$staff->appointed_till}}
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>working staff members =<b> {{$busy_staff}}</b></li>
                                                    <li>free staff members =<b> {{$free_staff}}</b></li>
                                                    <li>total staff members =<b> {{$total_staff}}</b></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@php 
//echo '<pre>';
//print_r($dates);
//die;
@endphp
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="{{url('/js/checkbox_update.js')}}"></script>
<script>
$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#directorate,#end_date,#start_date').change(function () {
        $('form').submit()
    });
});
                                </script>
@endsection
