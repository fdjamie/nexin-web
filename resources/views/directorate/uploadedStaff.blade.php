@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Staff Members</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-users fa-2x"></i>Staff Members 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <div class="clearfix overflow-scroll">
                            <table class="table table-responsive  table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th> Name </th>
                                        <th> Gmc </th>
                                        <th> Grade </th>
                                        <th> Role </th>
                                        <th> Qualifications </th>
                                        <th> Individual Bleep </th>
                                        <th> Email </th>
                                        <th> Mobile </th>
                                        <th> Directorate </th>
                                        <th> Speciality </th>
                                        <th> Appointed from </th>
                                        <th> Appointed till</th>
                                        <th> Result </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($view_data as $staff)
                                    @if(
                                        isset($staff['name_error']) || 
                                        isset($staff['directorate_error']) || 
                                        isset($staff['grade_error']) || 
                                        isset($staff['role_error']) || 
                                        isset($staff['speciality_error']) || 
                                        isset($staff['grade_role_error']) || 
                                        isset($staff['gmc_error']) ||
                                        isset($staff['qualifications_public_error']) ||
                                        isset($staff['mobile_public_error']) ||
                                        isset($staff['individual_bleep_public_error']) 
                                        )
                                    <tr class="bg-danger">
                                        @else
                                    <tr class="odd gradeX">
                                        @endif    
                                        <td>{{ $staff['name'] }}</td>
                                        <td>{{ $staff['gmc'] }}</td>
                                        <td>{{ $staff['grade'] }}</td>
                                        <td>{{ $staff['role'] }}</td>
                                        <td>{{ $staff['qualifications'] }}</td>
                                        <td>{{ $staff['individual_bleep'] }}</td>
                                        <td>{{ $staff['email'] }}</td>
                                        <td>{{ $staff['mobile'] }}</td>
                                        <td>{{ $staff['directorate'] }}</td>
                                        <td>{{ $staff['speciality'] }}</td>
                                        <td>{{ $staff['appointed_from'] }}</td>
                                        <td>{{@($staff['appointed_till']=='0000-00-00')? 'permanent' : $staff['appointed_till']}}</td>
                                        <td class="table-danger">
                                            <ul>
                                                @if(isset($staff['name_error']))<li class="list-group-item-danger"> {{$staff['name_error']}}   </li>@endif 
                                                @if(isset($staff['directorate_error']))<li class="list-group-item-danger"> {{$staff['directorate_error']}}   </li>@endif 
                                                @if(isset($staff['grade_error']))<li class="list-group-item-danger"> {{$staff['grade_error']}}   </li>@endif 
                                                @if(isset($staff['role_error']))<li class="list-group-item-danger"> {{$staff['role_error']}}   </li>@endif 
                                                @if(isset($staff['speciality_error']))<li class="list-group-item-danger"> {{$staff['speciality_error']}}   </li>@endif 
                                                @if(isset($staff['grade_role_error']))<li class="list-group-item-danger"> {{$staff['grade_role_error']}}   </li>@endif 
                                                @if(isset($staff['gmc_error']))<li class="list-group-item-danger"> {{$staff['gmc_error']}}   </li>@endif 
                                                @if(isset($staff['qualifications_public_error']))<li class="list-group-item-danger"> {{$staff['qualifications_public_error']}}   </li>@endif 
                                                @if(isset($staff['mobile_public_error']))<li class="list-group-item-danger"> {{$staff['mobile_public_error']}}   </li>@endif 
                                                @if(isset($staff['individual_bleep_public_error']))<li class="list-group-item-danger"> {{$staff['individual_bleep_public_error']}}   </li>@endif 
                                                @if(isset($staff['qualification_error']))
                                                    @foreach($staff['qualification_error'] as $error)
                                                    <li class="list-group-item-danger"> {{$error}}   </li>
                                                    @endforeach
                                                @endif 
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-toolbar">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="/directorate-staff/new">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['DirectorateStaffController@upload'] ]) !!}
                                        {!! Form::hidden('file_name',$file_name); !!}
                                        {!! Form::hidden('update_flag',$update_flag); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green']); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endsection