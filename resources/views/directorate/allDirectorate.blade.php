@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user-md"></i>Directorates 
                        </div>
                    </div>
                    @if ($errors->any())

                        @foreach ($errors->all() as $error)
                            <div class='alert alert-danger alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                {{$error}}
                            </div>
                        @endforeach
                    @endif
                    @if (Illuminate\Support\Facades\Session::has('success-directorate'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-directorate') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-directorate'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-directorate') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        @if(__authorize(config('module.directorates'),'add'))
                                        <a href="{{url('/directorate/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        @endif

                                        <a href="{{url('/directorate/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Export Directorates <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th style="width:50px;">Id</th>
                                    <th>Name </th>
                                    <th> Trust Name </th>
                                    <th> Division Name </th>
                                    <th> Directorate Service </th>
                                    <th style="width: 100px;text-align: center;"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($directorates as $directorate)
                                <tr class="odd gradeX">
                                    <td>{{$directorate->id}}</td>
                                    <td>{{$directorate->name}}</td>
                                    <td>{{$directorate->trust->name}}</td>
                                    <td>{{isset($directorate->division)?$directorate->division->name:'N/A'}}</td>
                                    <td>{{($directorate->service == 1)?'Yes':'No'}}</td>
                                    <td class="text-center">
                                        @if(__authorize(config('module.directorates'),'edit'))
                                            <a href="{{url('/directorate/edit/'.$directorate->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        @endif
                                            @if(__authorize(config('module.directorates'),'delete'))
                                                 <a data-toggle="modal" href="#small" id="{{$directorate->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/directorate/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection