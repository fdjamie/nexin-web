@extends('admin.admin')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user-md"></i>{{$directorate->name}}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-directorate'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-directorate') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['DirectorateController@update', $directorate->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $directorate->name, ['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('division_check', 'Division', ['class' => 'awesome']); !!} {!! Form::checkbox('division_check', 1, ($directorate->division_id == 0 ? false: true),['id'=>'division_check'] ); !!}
                            {!! Form::select('division_id', $divisions, ($directorate->division_id != 0 ? $directorate->division_id : null),['class' => 'form-control','id'=>'division_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('service', 'Directorate service ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('service', 1 , $directorate->service ,[]); !!}
                        </div>
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script>
    $(document).ready(function () {
        var directorateCheck = function () {
            if ($('#division_check').is(':checked'))
                $('#division_id').removeAttr("disabled");
            else
                $('#division_id').attr("disabled", "disabled");
        };
        $('#division_check').change(function () {
            directorateCheck();
        });
        directorateCheck();
    });
</script>
@endsection
