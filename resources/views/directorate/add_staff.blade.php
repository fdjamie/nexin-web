@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
<!--<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.css"></script>-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" integrity="sha256-xJOZHfpxLR/uhh1BwYFS5fhmOAdIRQaiOul5F/b7v3s=" crossorigin="anonymous" />-->
<!--<link rel="stylesheet" href="{{url('/select2/css/select2.min.css')}}" />-->
<link rel="stylesheet" href="{{url('/select2/css/select2.min.css')}}" />
<link rel="stylesheet" href="{{url('/select2/css/select2-bootstrap.min.css')}}"/>
<!--<style rel="stylesheet">
.select2-selection__rendered, .select2-search--inline,.select2-search__field{
        width: 100% !important;
    }  
    .select2-search__field{
        padding: 5px;
        height: 39px;
        border: 1px solid #CCC;
    }
</style>-->
@endsection

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-h-md fa-2x"></i>Add Staff Member
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-staff'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-staff') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post','files' => true ,'enctype' => 'multipart/form-data', 'action' => ['DirectorateStaffController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate', 'directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates , old('directorate') , [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('speciality_id', 'Speciality', ['class' => 'awesome']); !!} <i class="fa fa-circle-o-notch fa-spin" id='spin' aria-hidden="true"></i>
                            {!! Form::select('directorate_speciality_id', $specialties , null, ['class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('staff', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', old('staff') , [ 'class'=>'form-control search_staff','id'=>'name']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gmc', 'GMC', ['class' => 'awesome']); !!}
                            {!! Form::text('gmc', '',['placeholder' => 'Enter GMC','class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('grade_id', 'Grade', ['class' => 'awesome']); !!} <i class="fa fa-circle-o-notch fa-spin" id='gd_spin' aria-hidden="true"></i>
                            {!! Form::select('grade_id', $grades , null, ['class'=>'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('role_id', 'Role', ['class' => 'awesome']); !!} <i class="fa fa-circle-o-notch fa-spin" id='rl_spin' aria-hidden="true"></i>
                            {!! Form::select('role_id', $roles , null, ['class'=>'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('qualifications', 'Qualifications', ['class' => 'awesome']); !!}
                            {!! Form::select('qualifications[]', $qualifications,[],['class' => 'select2-multiple form-control', 'multiple'=>'multiple']); !!}
                            {!! Form::label('qualifications_public', 'Public', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('qualifications_public', 1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('individual_bleep', 'Individual Bleep', ['class' => 'awesome']); !!}
                            {!! Form::text('individual_bleep', '',['placeholder' => 'Individual Bleep','class' => 'form-control']); !!}
                            {!! Form::label('individual_bleep_public', 'Public', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('individual_bleep_public', 1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email', ['class' => 'awesome']); !!}
                            {!! Form::text('email', '',['placeholder' => 'Enter Email','class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('mobile', 'Mobile#', ['class' => 'awesome']); !!}
                            {!! Form::text('mobile', '',['placeholder' => 'Enter Mobile Number','class' => 'form-control']); !!}
                            {!! Form::label('mobile_public', 'Public', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('mobile_public', 1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('profile_pic', 'Profile Image', ['class' => 'awesome']); !!}
                            {!! Form::file('profile_pic', null,['class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('from', 'Appointed From', ['class' => 'awesome']); !!}
                            {!! Form::text('appointed_from', (old('appointed_from')!=null)?old('appointed_from'):date('Y-m-d') , ['placeholder' => 'Enter Appointment Date', 'class' => 'form-control datepicker']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('till', 'Appointed Till', ['class' => 'awesome']); !!}
                            {!! Form::text('appointed_till', (old('appointed_from')!=null)?old('appointed_from'):'', ['placeholder' => 'left blank if permanent', 'class' => 'form-control datepicker']); !!}
                        </div>
                        {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa fa-shield"></i>Upload Directorate Staff
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['DirectorateStaffController@import']]) !!}
                        <div class="form-group">
                            {!! Form::label('update', 'Update data', ['class' => 'awesome','id'=>'overrides_teaching']); !!}
                            {!! Form::checkbox('update_flag', 1,false,['id'=>'update']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-textext/1.3.0/jquery.textext.min.js" integrity="sha256-cbSFs5QoIt8BenP43wacr/rpsL3hLBMVme9xrrc9lp0=" crossorigin="anonymous"></script>-->
<!--<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>-->
<script src="{{url('/select2/js/select2.full.js')}}"></script>
<script src="{{url('/js/components-select2.min.js')}}"></script>
<!--<script type="text/javascript" src="/js/search_staff.js"></script>-->
<script src="{{url('/js/staff.js')}}"></script>

@endsection
