

@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-md"></i>Directorate Staff Members 
                        </div>
                        <!--<div class="form-group col-md-3 pull-right">-->
                        {{--!! Form::open(['method' => 'post', 'action' => ['HomeController@indexDirectorateStaff']]) !!--}}
                        {{--!! Form::label('directorate', 'directorate', ['class' => 'awesome']); !!--}}
                        {{--!! Form::select('directorate', $directorates ,$directorate_id, [ 'class'=>'form-control','id'=>'directorate']); !!--}}
                        {{--!! Form::close() !!--}}
                        <!--</div>-->
                    </div>

                    @if (Illuminate\Support\Facades\Session::has('success-staff'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-staff') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-staff'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-staff') }}
                    </div>
                    @endif

                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">

                                        @if(__authorize(config('module.staff'),'add'))
                                        <a href="{{url('/directorate-staff/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Add New <i class="fa fa-plus"></i></button>
                                        </a>

                                        @endif

                                        <a href="{{url('/directorate-staff/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Data <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($directorates as $directorates_node)
                            @if($directorates_node->id == $directorates[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$directorates_node->id}}" data-toggle="tab">{{ $directorates_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @if(!empty($directorates))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                <tr>
                                    <th width='100%' class="text-center"><b> No directorate exist in the system.</b></td>
                                </tr>    
                            </table>
                            @endif
                            @foreach($directorates as $directorates_node)
                            @php
                            $total_staff = 0;
                            $free_staff = 0;
                            $busy_staff = 0;
                            @endphp
                            @if($directorates_node->id == $directorates[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$directorates_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$directorates_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Id</th>
                                                <th> Staff </th>
                                                <th> GMC </th>
                                                <th> Appointed From  </th>
                                                <th> Appointed Till </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($directorates_node->staff))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($directorates_node->staff as $staff)
                                            @php
                                            $total_staff = count($directorates_node->staff);
                                            if(empty($staff->post))
                                            $busy_staff ++;
                                            $free_staff =   $total_staff -  $busy_staff; 

                                            @endphp
                                            <tr class="odd gradeX">
                                                <td width='14%'>{{$staff->id}}</td>
                                                <td width='14%'>{{$staff->name}}</td>
                                                <td width='14%'>{{$staff->gmc}}</td>
                                                <td>
                                                    @if(isset($staff->appointed_from) && $staff->appointed_from != '0000-00-00')
                                                    {{$staff->appointed_from}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($staff->appointed_till) && $staff->appointed_till != '0000-00-00')
                                                    {{$staff->appointed_till}}
                                                    @endif
                                                </td>
                                                <td class="text-center">

                                                    @if(__authorize(config('module.staff'),'edit'))
                                                    <a href="{{url('/directorate-staff/edit/'.$staff->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    @endif
                                                     @if(__authorize(config('module.staff'),'delete'))
                                                    <a data-toggle="modal" href="#small" id="{{$staff->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>working staff members =<b> {{$busy_staff}}</b></li>
                                                    <li>free staff members =<b> {{$free_staff}}</b></li>
                                                    <li>total staff members =<b> {{$total_staff}}</b></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body"> You want to delete this record? </div>
            <div class="modal-footer">
                <a href="" id='delete-button'><button type="submit" class="btn btn-danger" >Delete</button></a>
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('#directorate').change(function () {
        $('form').submit()
    });
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/directorate-staff/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection