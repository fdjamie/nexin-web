@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                {!! Form::open(['method' => 'post', 'autocomplete'=>'off', 'action' => ['HomeController@storeRotaTemplate']]) !!}
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-calendar-check-o"></i>Add Rota Template
                        </div>
                        <div class="form-group col-md-3 pull-right">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-rotatemplate'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-rotatemplate') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description', ['class' => 'awesome']); !!}
                            {!! Form::text('description', '',['placeholder' => 'Enter Description' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Groups', ['class' => 'awesome']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="rotaSpin"></i>
                            {!! Form::select('rota_group_id', $rota_groups,null,['class' => 'form-control','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!}
                            {!! Form::text('start_date', date('Y-m-d'),[ 'class' => 'form-control datepicker']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('permanent', 'Permanent', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('permanent',1,[ 'class' => 'form-control','id'=>'permanent']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('endDate', 'End Date', ['class' => 'awesome']); !!}
                            {!! Form::text('end_date', null,[ 'class' => 'form-control datepicker','id'=>'endDate']); !!}
                        </div>
                        <table class="table" id="empty-container">
                            <tr>
                                @foreach($weekdays as $key=>$value)
                                <th>{!! Form::label('name', $value); !!}</th>
                                @endforeach
                            </tr>
                            <tr class="template-contents">
                                @for ($i = 0; $i < $total_weeks; $i++)
                                @foreach($weekdays as $key=>$value)
                                
                                <td>
                                    <i class="fa fa-refresh fa-spin shiftSpin" aria-hidden="true" ></i>
                                    {!! Form::select('content[shift_type]['.$i.']['.$key.']', $shift_types, '',['placeholder'=>'Off','class'=>'shift_type']); !!}
                                </td>
                                @endforeach
                                @endfor
                                <td><a class="fa fa-trash" style="display:none;cursor: pointer;"></a></td>
                            </tr>
                        </table>
                        <div class="form-group">
                            {!! Form::button('Add Week!', ['class'=> 'btn btn-success addweek']); !!}
                            {!! Form::submit('Add Template!', ['class'=> 'btn green addTemplate']); !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
                {!! Form::close() !!}
            </div>
        </div>
<!--        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-home"></i>Upload Sites
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'url' => route('import-rota-template')]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>-->
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="{{url('/js/rota_template.js')}}" type="text/javascript"></script>
@endsection