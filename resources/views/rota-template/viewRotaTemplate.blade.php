@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1> Shifts</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
<!--        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/rota-template">Rota Template</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-object-ungroup"></i>{{$templatename}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th class="text-center">Week</th>
                                    @foreach($weekdays as $key=>$value)
                                    <th class="text-center">{{$value}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @foreach($shiftTypesData as $shift)
                                    @php $shift = (object) $shift; $i++;@endphp
                                    @if($i%7==1)
                                    <tr>
                                        <td>{{intval($i/7)+1}}</td> 
                                    @endif
                                    <td class="text-center">
                                        {{$shift->name }} <br>
                                        @if($shift->start_time != '')
                                        {{ date('H:i', strtotime($shift->start_time)) }} - {{ date('H:i', strtotime($shift->finish_time)) }}
                                        @endif
                                    </td>
                                    @if($i%7==0)
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
//$('#directorate').change(function(){
//    $('form').submit();
//});
</script>
@endsection