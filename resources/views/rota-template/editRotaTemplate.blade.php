@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" integrity="sha256-5ad0JyXou2Iz0pLxE+pMd3k/PliXbkc65CO5mavx8s8=" crossorigin="anonymous" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                {!! Form::open(['method' => 'put', 'autocomplete'=>'off', 'action' => ['HomeController@updateRotaTemplate',$rotaTemplate->id]]) !!}
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-calendar-check-o"></i>{{$rotaTemplate->name}}
                        </div>
                        <div class="form-group col-md-3 pull-right">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$rotaTemplate->directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $rotaTemplate->name,['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description', ['class' => 'awesome']); !!}
                            {!! Form::text('description', $rotaTemplate->description,['placeholder' => 'Enter Description' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Groups', ['class' => 'awesome']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="rotaSpin"></i>
                            {!! Form::select('rota_group_id', $rota_groups,$rotaTemplate->rota_group_id,['class' => 'form-control','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Start Date', ['class' => 'awesome']); !!}
                            @if($rotaTemplate->start_date == '0000-00-00')
                            {!! Form::text('start_date', null,[ 'class' => 'form-control datepicker']); !!}
                            @else
                            {!! Form::text('start_date', $rotaTemplate->start_date,[ 'class' => 'form-control datepicker']); !!}
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('permanent', 'Permanent', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('permanent',$rotaTemplate->permanent,[ 'class' => 'form-control','id'=>'permanent']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'awesome']); !!}
                            {!! Form::text('end_date', $rotaTemplate->end_date,[ 'class' => 'form-control datepicker','id'=>'endDate']); !!}
                        </div>
                        <table class="table" id="empty-container">
                            <tr>
                                @foreach($weekdays as $key=>$value)
                                <th>{!! Form::label('name', $value); !!}</th>
                                @endforeach
                            </tr>
                            @if(isset($rotaTemplate->content->shift_type))
                            @foreach($rotaTemplate->content->shift_type as $week =>$value)
                            <tr class="template-contents">
                                @foreach($weekdays as $key=>$weekday)
                                <td>
                                    <i class="fa fa-refresh fa-spin shiftSpin" aria-hidden="true" ></i>
                                    {!! Form::select('content[shift_type]['.$week.']['.$key.']', $shift_types, $value[$key],['placeholder' => 'Off','class'=>'shift_type' ]); !!}
                                </td>
                                @endforeach
                                @if($week==0)
                                <td><a class="fa fa-trash" style="display:none;cursor: pointer;"></a></td>
                                @else
                                <td><a class="fa fa-trash" style="cursor: pointer;"></a></td>
                                @endif
                            </tr>
                            @endforeach
                            @else
                            <tr class="template-contents">
                                @for ($i = 0; $i < $total_weeks; $i++)
                                @foreach($weekdays as $key=>$value)
                                
                                <td>
                                    <i class="fa fa-refresh fa-spin shiftSpin" aria-hidden="true" ></i>
                                    {!! Form::select('content[shift_type]['.$i.']['.$key.']', $shift_types, '',['placeholder'=>'Off','class'=>'shift_type']); !!}
                                </td>
                                @endforeach
                                @endfor
                                <td><a class="fa fa-trash" style="display:none;cursor: pointer;"></a></td>
                            </tr>
                            @endif
                        </table>
                        <div class="form-group">
                            {!! Form::button('Add Week!', ['class'=> 'btn btn-success addweek']); !!}
                            {!! Form::submit('Update Template!', ['class'=> 'btn green addTemplate']); !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
                {!! Form::close() !!}
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" integrity="sha256-urCxMaTtyuE8UK5XeVYuQbm/MhnXflqZ/B9AOkyTguo=" crossorigin="anonymous"></script>
<script src="{{url('/js/rota_template.js')}}" type="text/javascript"></script>
@endsection