@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-calendar-check-o"></i>Rota Template
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@indexRotaTemplate']]) !!}
                        <div class="form-group col-md-3 pull-right">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/rota-template/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" data-toggle="modal" href="#basic"> Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        <a href="{!!route('export-rota-template') !!}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Rota Template <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            @foreach($rota_groups as $rota_group)
                            @if($rota_group->id == $rota_groups[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$rota_group->id}}" data-toggle="tab">{{ $rota_group->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @if(!count($rota_groups))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                <tr>
                                    <th width='100%' class="text-center"><b> No rota group for this directorate.</b></td>
                                </tr>    
                            </table>
                            @endif
                            @foreach($rota_groups as $rota_group)
                            @if($rota_group->id == $rota_groups[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$rota_group->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$rota_group->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th width="20%"> Name </th>
                                                <th> Description </th>
                                                <th> Week Cycle </th>
                                                <th> Start Date </th>
                                                <th> End Date </th>
                                                <th> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($rota_group->rota_template))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($rota_group->rota_template as $rotaTemplate)
                                            <tr>
                                                <td> {{$rotaTemplate->name}} </td>
                                                <td> {{$rotaTemplate->description}} </td>
                                                <td> {{(isset($rotaTemplate->content->shift_type)?count($rotaTemplate->content->shift_type):0)}} </td>
                                                <td>
                                                    @if(isset($rotaTemplate->start_date) && $rotaTemplate->start_date != '0000-00-00')
                                                    {{$rotaTemplate->start_date}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($rotaTemplate->end_date) && $rotaTemplate->end_date != '0000-00-00')
                                                    @if($rotaTemplate->end_date == '1111-00-00')
                                                    Permanent
                                                    @else
                                                    {{$rotaTemplate->end_date}}
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="text-center"> 
                                                    <a href="{{url('/rota-template/view/'.$rotaTemplate->id)}}" ><i class="fa fa-eye" aria-hidden="true"></i></a> 
                                                    <a href="{{url('/rota-template/edit/'.$rotaTemplate->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                    <a data-toggle="modal" href="#small" id="{{$rotaTemplate->id}}" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('#directorate_id,#directorate_speciality_id').change(function () {
        $('form').submit();
    });
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/rota-template/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection