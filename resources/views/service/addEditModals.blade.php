@section('modal')
<div class="modal fade" id="add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Event</h4>
                {!! Form::open(['method' => 'post', 'id'=>'addEvent']) !!}
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    {!! Form::label('site_id', 'Site', ['class' => 'awesome']); !!}
                    {!! Form::select('site_id', $sites , null,['class' => 'form-control','id'=>'site_id']); !!}
                </div>
                <i class="fa fa-circle-o-notch fa-spin fa-fw" id='spinn'></i>
                <div class="form-group" id="locations">
                    {!! Form::label('location_id', 'Location', ['class' => 'awesome']); !!} <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                    {{--!! Form::select('location_id', [],null,['class' => 'form-control','id'=>'location_id']); !!--}}
                </div>
                <div class="form-group">
                    {!! Form::hidden('service_id', $service->id,['class' => 'form-control','id'=>'service_id']); !!}
                </div>
                <div class="form-group">
                    {{--!! Form::label('day', 'Day', ['class' => 'awesome']); !!--}}
                    {{--!! Form::text('day', '',['class' => 'form-control','id'=>'day']); !!--}}
                </div>
                <div class="form-group">
                    {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                    <!--                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control" id='start_time' />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>-->
                    {!! Form::text('start_time', '',['class' => 'form-control','id'=>'start_time']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                    <!--                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control" id='end_time' />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>-->
                    {!! Form::text('end_time', '',['class' => 'form-control','id'=>'end_time']); !!}
                    {!! Form::hidden('event_start_date', '',['class' => 'form-control','id'=>'event_start_date']); !!}
                    {!! Form::hidden('event_end_date', '',['class' => 'form-control','id'=>'event_end_date']); !!}
                    {!! Form::hidden('title', $service->name,['id'=>'title']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Event</h4>
                {!! Form::open(['method' => 'post', 'id'=>'updateEvent']) !!}
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    {!! Form::label('site_id', 'Site', ['class' => 'awesome']); !!}
                    {!! Form::select('site_id', $sites , null,['class' => 'form-control','id'=>'edit_site_id']); !!}
                </div>
                <i class="fa fa-circle-o-notch fa-spin fa-fw" id='spin'></i>
                <div class="form-group" id="locations_edit">
                    {!! Form::label('location_id', 'Location', ['class' => 'awesome']); !!} 
                    {{--!! Form::select('location_id', [],null,['class' => 'form-control','id'=>'location_id']); !!--}}
                </div>
                <div class="form-group">
                    {!! Form::hidden('service_id', $service->id,['class' => 'form-control','id'=>'edit_service_id']); !!}
                </div>
                <div class="form-group">
                    {{--!! Form::label('day', 'Day', ['class' => 'awesome']); !!--}}
                    {{--!! Form::text('day', '',['class' => 'form-control','id'=>'day']); !!--}}
                </div>
                <div class="form-group">
                    {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                    <!--                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control" id='edit_start_time' />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>-->
                    {!! Form::text('edit_start_time', '',['class' => 'form-control','id'=>'edit_start_time']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                    <!--                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control" id='edit_end_time' />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>-->
                    {!! Form::text('edit_end_time', '',['class' => 'form-control','id'=>'edit_end_time']); !!}
                    {!! Form::hidden('event_start_date_edit', '',['class' => 'form-control','id'=>'event_start_date_edit']); !!}
                    {!! Form::hidden('event_end_date_edit', '',['class' => 'form-control','id'=>'event_end_date_edit']); !!}
                    {!! Form::hidden('title', $service->name,['id'=>'edit_title']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection