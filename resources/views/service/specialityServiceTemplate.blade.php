@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-h-square"></i>Select Services
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::text('directorate',$speciality->directorate->name, ['class' => 'form-control','id'=>'directorate','disabled'=>'true']); !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('speciality', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::text('speciality',$speciality->name, ['class' => 'form-control','id'=>'directorate_speciality','disabled'=>'true']); !!}
                        </div>
                    </div>
                    @if($services)
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!}
                        </div>
                        {!! Form::open(['method' => 'post', 'action' => ['HomeController@viewSpecialitySerivceTemplate',$id]]) !!}
                        @foreach($services as $service)
                        <div class="form-group col-md-3">
                            {!! Form::label('services', $service->name, ['class' => 'awesome']); !!}
                            @if(in_array($service->id,$selected_services))
                            {!! Form::checkbox('services[]', $service->id,true,['class'=>'check-boxes']) !!}
                            @else
                            {!! Form::checkbox('services[]', $service->id,false,['class'=>'check-boxes']) !!}
                            @endif
                        </div>
                        @endforeach
                        <div class="form-group col-md-12">
                            {!! Form::submit('Search !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div id='calendar-service'>
                            {!! $calendar->calendar() !!}
                            {!! $calendar->script() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('extra-js')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="{{url('/js/checkbox_update.js')}}"></script>
<!--
<script>
$(document).ready(function () {
    var id = 0;
    var day = '';
    var url = window.location.href;
    url = url.split('/');
    var service_id = url[url.length - 1];
    var events = [];
    jQuery.ajax({
        type: "GET",
        url: '/servicetemplate',
        data: {serviceId: service_id},
        success: function (data) {
            console.log(data.cycle_length);
            if (data != '') {
                var assignedWeek = moment(data.events[0].start_date).week();
                var startingWeek = moment(data.start_date, 'YYYY-MM-DD').isoWeek();
                var endingWeek = moment(data.end_date, 'YYYY-MM-DD').isoWeek();
//        console.log('assigned week = ' + (assignedWeek));
//        console.log('starting week = ' + (startingWeek));
//        console.log('ending week = ' + (endingWeek));
                var j = 1;
                for (i = startingWeek; i <= endingWeek; i++) {
                    $.each(data.events, function (key, value) {
                        if (i < assignedWeek) {
                            start = moment(value.start).subtract(j, 'weeks').format('YYYY-MM-DDTHH:mm');
                            end = moment(value.end).subtract(j, 'weeks').format('YYYY-MM-DDTHH:mm');
                        }
                        if (i == assignedWeek) {
                            start = moment(value.start).format('YYYY-MM-DDTHH:mm');
                            end = moment(value.end).format('YYYY-MM-DDTHH:mm');
                            j = 0;
                        }
                        if (i > assignedWeek) {
                            start = moment(value.start).add(j, 'weeks').format('YYYY-MM-DDTHH:mm');
                            end = moment(value.end).add(j, 'weeks').format('YYYY-MM-DDTHH:mm');
                        }
                        events.push({
                            title: value.title,
                            start: start,
                            end: end,
                            id: value.id,
                            icon: 'times-circle-o',
                        });
                        id = value.id;
                        cycleLength = value.cycleLength;
                    });
                    j++;
                }
            }

            $('#week_view').attr('max', cycleLength);
            $('#calendar-service').fullCalendar({
                header: {
                    left: 'prev,next',
                    center: '',
                    right: 'month,agendaWeek,twoWeeks'
                },
                views: {
                    twoWeeks: {
                        type: 'agenda',
                        duration: {weeks: 2},
                        buttonText: '2 weeks'
                    }
                },
                firstDay: 1,
                defaultView: 'agendaWeek',
                allDaySlot: false,
                slotDuration: '01:00',
                columnFormat: 'M/DD dd ',
                contentHeight: 'auto',
                events: events,
                timeFormat: 'HH:mm',
                axisFormat: 'HH:mm',
            });
        }
    });
    weekView = $('#week_view').val();
    $('#week_view').bind('change click', function (e) {
        if (weekView < $('#week_view').val()) {
            $('#calendar-service').fullCalendar('next');
            weekView = $('#week_view').val();
        } else if (weekView > $('#week_view').val()) {
            $('#calendar-service').fullCalendar('prev');
            weekView = $('#week_view').val();
        }
    });
});
</script>-->
@endsection