@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<style>
    #staff-table{
        width: 25%;
        transform: rotate(270deg);
        float: left;

    } 
    #requirement-table{
        margin-top: 11px;
        width: auto;
        float: left;
    }
    #status-table{
        margin-top: 11px;
        width: auto;
        float: left;
    }

    .bg-success{
        background: #090;
        color: #FFF;
    }
    .bg-danger{
        background: #cc0001;
        color: #FFF;
    }
    .bg-warning{
        background: #e89505;
        color: #FFF;
    }
    .red{
        color: #D9534F;
    }
    .orange-flip{
        color: #F0AD4E;
        transform: rotate(30deg);
    }
    .yellow-flip{
        color: #F5E348;
        transform: rotate(45deg);
    }
    .green{
        color: #449D44; 
    }
    .blue{
        color: #5BC0DE;
    }
    .blue, .green,.yellow-flip,.orange-flip,.red{
        font-size: 20px;
    }
    .portlet.light.bordered{
        float: left;
        width: 100%;
    }
</style>
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-4">
                <h1>Requirements view</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy"></i>Service Requirement view
                        </div>
                        {!! Form::open(['method' => 'post', 'action' => ['ServiceOverviewController@testing']]) !!}
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                                {!! Form::select('directorate_id', $directorates , $directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                                {!! Form::select('speciality_id', $specialities , $speciality_id, [ 'class'=>'form-control','id'=>'speciality_id']); !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <br><br>
                    <div class="portlet-body">
                        @if(isset($requirements_data) && count($requirements_data))
                        <div class="table-toolbar">
                            @if(isset($staffs) && count($staffs))
                            <table class="table table-responsive  table-bordered" id="staff-table">
                                <thead class="text-center">
                                    <tr>
                                        <th> Staff </th>
                                        <th> Role </th>
                                        <th> Grade </th>
                                        <th> Additional </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($staffs as $record)
                                    <tr>
                                        <td>{{$record->name}}</td>
                                        <td>{{$record->role->name}}</td>
                                        <td>{{$record->grade->name}}</td>
                                        <td>
                                            @if(isset($record->additional_parameters->parameter))
                                            @foreach($record->additional_parameters->parameter as $key => $addtional)
                                            {!!  $key !!}(
                                            @foreach($addtional as $parameter)
                                             {!!  $parameter !!} ,
                                            @endforeach
                                            )
                                            @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <table class="table table-responsive  table-bordered" >
                                <thead class="text-center">
                                    <tr>
                                        <th class="text-center"> No Staff members for this speciality </th>
                                    </tr>
                                </thead>
                            </table>
                            @endif
                        </div>
                        @if(isset($staffs) && count($staffs))
                        <table class="table table-responsive  table-bordered " id='requirement-table'>
                            <thead class="text-center">
                                <tr>
                                    <th> Service </th>
                                    <th> Statement </th>
                                    <th> number </th>
                                    @for($i=0; $i< count($staffs) ;  $i++)
                                    <th> - </th>
                                    @endfor
                                </tr>
                            </thead>
                            <tbody>

                                @php $count = array_fill(0, count($staffs), 0);@endphp

                                @foreach($requirements_data as $color => $color_value)
                                @foreach($color_value as  $value)
                                @if ($color == 'red')
                                @php $icon = 'fa-exclamation red'; @endphp
                                @elseif ($color == 'orange')
                                @php $icon = 'fa-play orange-flip'; @endphp
                                @elseif ($color == 'yellow')
                                @php $icon = 'fa-stop yellow-flip yellow'; @endphp
                                @elseif($color == 'green')
                                @php $icon = 'fa-star green'; @endphp
                                @elseif($color == 'blue')
                                @php $icon = 'fa-exclamation blue'; @endphp
                                @endif
                                <tr>
                                    <td>{{$value->name}}  <i class="fa {{$icon}}" aria-hidden="true"> </i></td>
                                    <td>{{$value->parameter}} {{$value->operator}} {{$value->value_name}} {{(isset($value->value2_name)?', '.$value->value2_name:'')}}</td>
                                    <td>{{$value->number}}</td>
                                    @if(isset($value->checks) && count($value->checks))
                                    @for($i=0;$i< count($value->checks);$i++)
                                    @php
                                    $count[$i]= ($value->checks[$i] != -1 ?$count[$i]+$value->checks[$i]:$count[$i]+0);
                                    @endphp 
                                    @if($value->checks[$i] == 1 )
                                    @php 
                                    $bg_color = 'bg-success'; 
                                    $check_icon = 'fa-check';
                                    @endphp
                                    @elseif($value->checks[$i] == 0 )
                                    @php 
                                    $bg_color = 'bg-danger'; 
                                    $check_icon = 'fa-times';
                                    @endphp
                                    @else
                                    @php 
                                    $bg_color = 'bg-warning'; 
                                    $check_icon = 'fa-exclamation-triangle';
                                    @endphp
                                    @endif
                                    
                                    <td style="width: 37px; " class="{{$bg_color}}"><i class="fa {{$check_icon}}" aria-hidden="true"></i>
                                    </td>
                                    @endfor
                                    @endif
                                </tr>
                                @endforeach
                                @endforeach
                                <tr>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    @for($j=0; $j< count($count); $j++)
                                    <td style="width: 37px;" class="bg-primary"><b>{{$count[$j]}}</b></td>
                                    @endfor
                                </tr>
                            </tbody>
                        </table>
                        @endif
                        @else
                        <div class="table-toolbar">
                            <table class="table table-responsive  table-bordered " >
                                <thead class="text-center">
                                    <tr>
                                        <th>  </th>
                                        <th colspan="5" class="text-center"> No Data to display </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        @endif
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script>
    $('#staff-table').css('margin-left', $('#requirement-table').width() - 199);
    $('#directorate_id,#speciality_id').change(function () {
        $('form').submit();
    });
</script>
@include('includes.scriptsViewLinks')
@endsection