@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-12">
                            <i class="fa fa-life-bouy"></i>Add Services
                        </div>
                        {!! Form::open(['method' => 'post', 'action' => ['ServiceController@store']]) !!}
                        <div class="form-group col-md-6">
                            {!! Form::label('directorate', 'directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorate ,null, [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('directorate_speciality_id', 'speciality', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('directorate_speciality_id', $specialites, null, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-service'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-service') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} <span id="ct_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('category_id', $categories,null,['class' => 'form-control','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group">
                            <a data-toggle="modal" href="#small" id="modal-display">
                                {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!} <span id="sb_spin"><i class="fa fa-spin fa-refresh"></i></span> </a>
                            {!! Form::select('sub_category_id', [],null,['class' => 'form-control serach-subcategory','id'=>'sub_category_id']); !!}
                        </div>
                        <div id="child-dropdown">

                        </div>
                        <div class="form-group">
                            {!! Form::label('contactable', 'Contactable ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('contactable', 1,true); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('min_standards', 'Min Standards ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('min_standards', 1,true); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-life-bouy"></i>Upload Services
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['ServiceController@import']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fas fa-chess-queen"></i> Upload Master Data
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body ">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['ServiceController@importReference']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@endsection
@section('modal')
@include('common.categoryTreeModal')
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{url('/js/MultiNestedList.js')}}"></script>
<script src="{{url('/js/service.js')}}" type="text/javascript"></script>
<script src="{{url('/js/search-subcategory.js')}}" type="text/javascript"></script>
@endsection