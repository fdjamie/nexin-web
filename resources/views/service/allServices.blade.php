@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy fa-2x"></i> Service 
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['ServiceController@index']]) !!}
                        <div class="form-group col-md-4 col-md-offset-4">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control col-md-3','id'=>'directorate']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-service'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-service') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-service'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-service') }}
                    </div>
                    @endif
                    <div class="clearfix"> <h5><b>Select Category </b></h5></div>
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!}
                            {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                        </div>
                        @foreach($categories as $category)
                        <div class="form-group col-md-3 main-check-box">
                            @if(in_array($category->id,$selected_category))
                            {!! Form::checkbox('selected_category[]', $category->id ,true,['id'=>$category->id,'class'=>'check-boxes']); !!}
                            @else
                            {!! Form::checkbox('selected_category[]', $category->id ,false,['id'=>$category->id,'class'=>'check-boxes']); !!}
                            @endif
                            {!! Form::label($category->id, $category->name, ['class' => 'awesome']); !!}
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group clearfix">
                        {!! Form::submit('Search !', ['class'=> 'btn sbold green']); !!}
                    </div>
                    {!! Form::close() !!}
                    @if (Illuminate\Support\Facades\Session::has('service-add'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('service-add') }}
                    </div>
                    @elseif(Illuminate\Support\Facades\Session::has('service-teamplate-deleted'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('service-teamplate-deleted') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/service/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        <a href="{{url('/service/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Services <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($specialities)==0)
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                            <thead>
                                <tr>
                                    <th colspan="5"> No Speciality for this Directorate</th>
                                </tr>
                            </thead>
                        </table>
                        @endif
                        <ul class="nav nav-tabs">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$speciality_node->id}}" data-toggle="tab">{{ $speciality_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($specialities as $speciality_node)
                            @if($speciality_node->id == $specialities[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$speciality_node->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$speciality_node->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contactable</th>
                                                <th>Min Standards</th>
                                                <th>Category</th>
                                                <th> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($speciality_node->service as $service)
                                            @if(in_array($service->category_id,$selected_category) || $check_all ==1)
                                            <tr>
                                                <td width="25%">{{$service->name}}</td>
                                                <td class="text-center">
                                                    @if($service->contactable == true)
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    @else
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                    @endif
                                                </td>
                                                <td class="text-center">{!!($service->min_standards == true?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>')!!}</td>
                                                <td class="text-center">{!!(isset($service->category)?$service->category->name:'N/A')!!}</td>
                                                <td class="text-center">
                                                    <a href="{{url('service/'.$service->id.'/requirements')}}"><i class="fa fa-list" aria-hidden="true"></i></a> 
                                                    <a href="{{url('/service/template/'.$service->id)}}" ><i class="fa fa-calendar" aria-hidden="true"></i></a> 
                                                    <a href="{{url('/service/edit/'.$service->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                    <a data-toggle="modal" href="#small" id="{{'s-'.$service->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-life-bouy"></i> Directorate Services
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            @foreach($directorates_services as $directorates_node)
                            @if($directorates_node->id == $directorates_services[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$directorates_node->id}}_service" data-toggle="tab">{{ $directorates_node->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @if(!count($directorates))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                <tr>
                                    <th width='100%' class="text-center"><b> No directorate exist in the system.</b></td>
                                </tr>    
                            </table>
                            @endif
                            @foreach($directorates_services as $directorates_node)
                            @if($directorates_node->id == $directorates_services[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$directorates_node->id}}_service">
                                @else
                                <div class="tab-pane fade" id="tab_{{$directorates_node->id}}_service">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Specialities </th>
                                                <th> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($directorates_node->services))
                                            <tr>
                                                <td></td>
                                                <td colspan="5" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($directorates_node->services as $service)
                                            <tr class="odd gradeX">
                                                <td width='25%'>{{$service->name}}</td>
                                                <td width='50%'>
                                                    @if(!is_null($service->specialities))
                                                    @foreach($directorates_speciality as $directorate)
                                                    @foreach($directorate->speciality as $speciality)
                                                    @if(in_array($speciality->id,$service->specialities))
                                                    {{$speciality->name . ','}}
                                                    @endif
                                                    @endforeach
                                                    @endforeach
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{url('/directorate-service/template/'.$service->id)}}" ><i class="fa fa-calendar" aria-hidden="true"></i></a> 
                                                    <a href="{{url('/directorate-service/edit/'.$service->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                    <a data-toggle="modal" href="#small" id="{{'d-'.$service->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="{{url('/js/checkbox_update.js')}}"></script>
<script>
$(document).ready(function () {
    $('.delete').click(function () {
        id = $(this).attr('id').split('-');
        if (id[0] == 'd') {
            url = '{{url("/directorate-service/delete")}}' + '/' + id[1];
        } else {
            url = '{{url("/service/delete")}}' + '/' + id[1];
        }
        $('#delete-button').attr('href', url);
//        $('#delete-button').attr('href', '/service/delete/' + $(this).attr('id'));
    });
    $("#directorate_speciality_id,#directorate").change(function () {
        $("form").submit();
    });
//        $('.main-check-box .check-boxes').change(function () {
//            $(this).parent().find('.sub-category').toggleClass('hide');
//        });
});

</script>
@endsection