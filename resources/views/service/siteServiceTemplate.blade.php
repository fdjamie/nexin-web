@extends('admin.admin')

@section('extra-css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Services Calender View</h1>
            </div>

            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <!--        <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="/service">Services</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>-->
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-h-square"></i>Select Locations
                        </div>
                        <div class="col-md-3 col-md-offset-3">
                            {!! Form::label('site', 'Site', ['class' => 'awesome']); !!}
                            {!! Form::text('site',$site->name, ['class' => 'form-control','id'=>'directorate','disabled'=>'true']); !!}
                        </div>
                        <!--<div class="col-md-3">-->
                        {{--!! Form::label('speciality', 'Speciality', ['class' => 'awesome']); !!--}}
                        {{--!! Form::text('speciality',$speciality->name, ['class' => 'form-control','id'=>'directorate_speciality','disabled'=>'true']); !!--}}
                        <!--</div>-->

                    </div>
                    @if($locations)
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('check_all', 'All', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('check_all', 1,false,['id'=>'check_all']); !!}
                        </div>
                        {!! Form::open(['method' => 'post', 'action' => ['HomeController@viewSiteSerivceTemplate',$id]]) !!}
                        @foreach($locations as $key => $value)
                        <div class="form-group col-md-3">
                            {!! Form::label('locations', $value, ['class' => 'awesome']); !!}
                            @if(in_array($key,$selected_locations))
                            {!! Form::checkbox('locations[]', $key,true,['class'=>'check-boxes']) !!}
                            @else
                            {!! Form::checkbox('locations[]', $key,false,['class'=>'check-boxes']) !!}
                            @endif
                        </div>
                        @endforeach
                        <div class="form-group col-md-12">
                            {!! Form::submit('Search !', ['class'=> 'btn green']); !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div id='calendar-service'>
                            {!! $calendar->calendar() !!}
                            {!! $calendar->script() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('extra-js')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="{{url('/js/checkbox_update.js')}}"></script>
@endsection