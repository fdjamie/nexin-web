@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}"  rel="stylesheet" type="text/css" />
<style>
    .value-2{
        display: none;
    }
</style>
@endsection
@php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-6">
                <h1<i class="fa fa-life-bouy fa-2x"></i> {!! $service->name !!}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('success-requirement'))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('success-requirement')}}
                    </li>
                </ul>
            </div>
            @endif
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    {!! Form::open(['method' => 'post', 'action' => ['ServiceController@storeRequirement'] ]) !!}
                    <div class="portlet-title">
                        <div class="form-group col-md-1">
                            <b>{!! Form::label('condition', 'Condition', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-1">
                            <b>{!! Form::label('parameter', 'Parameter', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('operator', 'Operator', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-4">
                            <b>{!! Form::label('rota_status', 'Value', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('rota_status', 'Rota Status', ['class' => 'awesome']); !!}</b>
                        </div>
                        <div class="form-group col-md-2">
                            <b>{!! Form::label('number', 'Number', ['class' => 'awesome']); !!}</b>
                        </div>
                    </div>
                    <div class="row red-box">
                        @include('common.red_requirement')
                    </div>
                    <div class="row orange-box">
                        @include('common.orange_requirement')
                    </div>
                    <div class="row yellow-box">
                        @include('common.yellow_requirement') 
                    </div>
                    <div class="row green-box">
                        @include('common.green_requirement')
                    </div>
                    <div class="row blue-box">
                        @include('common.blue_requirement')
                    </div>
                    <div class="row submit-requirement">
                        <div class="form-group col-md-12">
                            {!! Form::submit('Submit!', ['class'=> 'btn green pull-right']); !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bd-example-modal-small" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Condition</h4>
            </div>
            <div class="modal-body">
                <h3> Coditions </h3>
                
            </div>
            <div class="modal-footer">
                <!--<a href="" id='condition-button'><button type="submit" class="btn btn-success" >Submit</button></a>-->
                <button type="button" class="btn green" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>
<script>
$('.show-condition').on('click',function(e){
    e.preventDefault();
    $('.modal-body').empty();
    $('.modal-body').append($(this).data('id'));
    $('#small').modal('show');
});
</script>
@endsection