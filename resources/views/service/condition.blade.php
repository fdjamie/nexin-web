@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/service-requirement.css')}}" rel="stylesheet" type="text/css" />
<style>
</style>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title col-md-6">
                <h1<i class="fa fa-life-bouy fa-2x"></i> Condition</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            @if (Illuminate\Support\Facades\Session::has('error-condition'))
            <div class='alert alert-danger alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    <li>
                        {{Session('error-condition')}}
                    </li>
                </ul>
            </div>
            @endif
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        {!!  $red !!}
                    </div>
                    @php $red_count = $orange_count = $yellow_count = $green_count = $blue_count = 1 ; @endphp
                    <div class="row submit-requirement">
                        {!! Form::open(['method' => 'post', 'url' => 'condition' ,'id'=>'condition-form']) !!}
                        <div class="form-group col-md-12">
                            @foreach($requirements->requirement as $requirement)
                            <div class="red-form clearfix">
                                {!! Form::hidden('red[id][]' ,$requirement->id, [ 'class'=>'id','id'=>'red_id-'.$red_count]); !!}
                                <div class="form-group col-md-1">
                                    {!! Form::text('red[condition][]' ,$requirement->condition,[ 'class'=>'form-control condition','id'=>'red_operato' ,'readonly'=>'']) !!}
                                </div>
                                <div class="form-group col-md-1">
                                    {!! Form::text('red[parameter][]' ,$requirement->parameter, [ 'class'=>'form-control parameter','id'=>'red_parameter-'.$red_count, 'readonly'=>'']); !!}
                                </div>
                                <div class="form-group col-md-2 clearfix">
                                    {!! Form::text('red[operator][]', $requirement->operator, [ 'class'=>'form-control operator','id'=>'red_operator-'.$red_count,'readonly'=>'']); !!}
                                </div>
                                <div class="form-group {{($requirement->operator=='between'?'col-md-2':'col-md-4')}} clearfix value-div">
                                    @if($requirement->parameter == 'name')
                                    {!! Form::text('red[value][]' ,$requirement->value, [ 'class'=>'form-control value','id'=>'red_value-'.$red_count ,'readonly'=>'']); !!}
                                    @else
                                    {!! Form::text('red[value][]' ,$requirement->value, [ 'class'=>'form-control value','id'=>'red_value-'.$red_count,'readonly'=>'']); !!}
                                    @endif
                                </div>
                                <div class="form-group col-md-2 clearfix value-2" style="{{((isset($requirement->value_2) && $requirement->value_2 !='')?'display:block;':'display:none;')}}" >
                                    {!! Form::text('red[value_2][]', $requirement->value_2, [ 'class'=>'form-control value_2','id'=>'red_value_2-'.$red_count ,'readonly'=>'']) !!}
                                </div>
                                <div class="form-group col-md-2 clearfix">
                                    {!! Form::text('red[status][]', $requirement->status, [ 'class'=>'form-control status','id'=>'red_status-'.$red_count,'readonly'=>'']); !!}
                                </div>
                                <div class="form-group col-md-1 clearfix">
                                    {!! Form::number('red[number][]', $requirement->number, [ 'class'=>'form-control number','id'=>'red_number-'.$red_count,'min'=> 1,'readonly'=>'']); !!}
                                </div>
                                <!--                                <div class="form-group col-md-1 clearfix {{($red_count==1?'red-trash':'')}}">
                                                                    <a class="fa fa-trash delete" id="{{$requirement->id}}"></a>
                                                                </div>-->
                                @if($requirement->condition == null)
                                <div class="form-group col-md-1 clearfix fa fa-add">
                                    <span>Brackets</span><a class="fa fa-chevron-left brecket" id="{{$requirement->id}}" chevron=" ( "></a>
                                    <a class="fa fa-chevron-right brecket" id="{{$requirement->id}}" chevron=" ) "></a>
                                    <span >Requirement</span><input type="checkbox" class="add-condition-id" id="{{$requirement->id}}"  data-bind="  <?php echo $requirement->parameter?>"/>
                                    <span>AND</span><input type="checkbox" class="condition-make" id=""  value=" && "/>
                                    <span>OR</span><input type="checkbox" class="condition-make" id="" value=" || " />

                                </div>
                                @else
                                <div class="form-group col-md-1 clearfix">
                                    @php
                                    $array = implode(',', $requirement->condition_id);                                    
                                    @endphp
                                    <a class="fa fa-trash delete-condition" id="{{$requirement->id}}" deleteCondition="<?php   echo $array ;?>"></a>
                                    
                                </div>
                                @endif

                            </div>
                            @endforeach

                            <div class="form-group pull-right">
                                {!! Form::submit('Submit!', ['class'=> 'btn green pull-right']); !!}
                            </div>
                            <div><span>Condition Make</span></div>
                            <div class="saveCondition" hidden="" ></div>
                            <div class="parameter" style= "font-size: 30px;"  ></div>
                            <span>
                                <a class="fa fa-remove remove" id="">Remove Condition</a>
                            </span>
<!--                            <span>
                                <a class="fa fa-plus-circle getdiv" id=""></a>
                            </span>-->
                            <input type="text" value="" name="ids" class="ids" hidden=""/>
                            <input type="text" value="" name="condition_value" class="condition_value" hidden=""/>
                            {!!  Form::close() !!}
                        </div>

                    </div>

                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')

@endsection
@section('extra-js')
<script src="{{url('/js/service-requirement.js')}}" type="text/javascript"></script>
<script>
$('#condition-form').submit(function (e) {
    // set our summary div inputs values with our form values
    $('.condition_value').val($.trim($('.saveCondition').text()));


});

$(document).ready(function () {
    $('.delete-condition').on('click',function(e){
    e.preventDefault();
    console.log($(this).attr('deleteCondition') ,($(this).attr('id')));
    var idst;
     idst =  $(this).attr('deleteCondition');
    var url = APP_URL + '/service/delete-requirement-condition';
    console.log(idst);
    $.get(url, {ids:idst}, function (data) {
        location.reload();
    });
});
    var ids = [];
    $('.add-condition-id').on('change', function (e) {
        e.preventDefault();
//        var val = this.checked ? this.id : '';
        if (this.checked) {
            var val = this.id;
            var parameters = $(this).attr('data-bind');
            $('.saveCondition').append(val);
            $('.parameter').append(parameters);
            ids.push(val);
            $('.ids').empty();
            $('.ids').val(ids);
        }
    });
    $('.condition-make').on('change', function (e) {
        e.preventDefault();
        if (this.checked) {
            var val = this.value;
            $('.saveCondition').append(val);
            $('.parameter').append(val);
        }
    });
    $('.brecket').on('click', function (e) {
        e.preventDefault();
        var val = $(this).attr('chevron');
        $('.saveCondition').append(val);
        $('.parameter').append(val);
    });
    $('.remove').on('click', function (e) {
        e.preventDefault();
        $('.saveCondition').empty();
         $('.parameter').empty();
        ids = [];
        $('.ids').val('');
        $('.add-condition-id').prop('checked', false);
        $('.condition-make').prop('checked', false);
    });
//    $('.getdiv').on('click', function (e) {
//        e.preventDefault();
//        console.log($.trim($('.checking').text()));
//    });

});
</script>
@endsection