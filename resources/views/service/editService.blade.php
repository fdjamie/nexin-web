@extends('admin.admin')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
@endsection
@section('content')

@endphp
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-life-bouy"></i>{{$service->name}}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::open(['method' => 'put', 'action' => ['ServiceController@update',$service->id]]) !!}
                            {!! Form::label('directorate', 'directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate', $directorates ,$directorate, [ 'class'=>'form-control','id'=>'directorate']); !!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('directorate_speciality_id', 'speciality', ['class' => 'awesome']); !!} <span id="sp_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('directorate_speciality_id', $specialty_data, $service->directorate_speciality_id, [ 'class'=>'form-control','id'=>'directorate_speciality_id']); !!}
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-location'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-location') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $service->name,['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} <span id="ct_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('category_id', $categories,$main_category,['class' => 'form-control','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group">
                            <a data-toggle="modal" href="#small" id="modal-display">{!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!} <span id="sb_spin"><i class="fa fa-spin fa-refresh"></i></span></a>
                            {!! Form::select('sub_category_id', $dropdown_text ,$dropdown_text ,['class' => 'form-control serach-subcategory','id'=>'sub_category_id']); !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('contactable', 'Contactable ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('contactable', 1,$service->contactable); !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('min_standards', 'Min Standards ?', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('min_standards', 1,$service->min_standards); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Category Hierarchy</h4>
            </div>
            <div class="modal-body" id="tree"> {!! $tree !!} </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{url('/js/MultiNestedList.js')}}"></script>
<script src="{{url('/js/service.js')}}" type="text/javascript"></script>
<script src="{{url('/js/search-subcategory.js')}}" type="text/javascript"></script>
@endsection