@extends('admin.admin')
@php
use Carbon\Carbon;
@endphp
@section('extra-css')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('css/custom.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css" integrity="sha256-avvOYh3iuZbeN1rcsni+ZBCq641kD8BMfypj56QtJMI=" crossorigin="anonymous" />
<link href="{{url('/css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
<link href="{{url('/css/scheduler.min.css')}}" rel='stylesheet' />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
<style>
    .color-box{
        height: 40px;
        width: 40px;
        border: 1px solid #000;
        float: left;
        margin-left: 10px;
    }
    .fc-license-message{
        display: none;
    }
    #sample_3_info{
        display: none;
    }
    .events{
        border-bottom: 1px solid #337AB7;
        list-style-type: none;
        display: inline-flex;
        width: 100%;
    }
    .events li b{
        font-size: 21px;
        border-left: 1px solid #337AB7;
        padding: 0px 5px;
    }

    #staff-table{
        width: 37%;
        transform: rotate(270deg);
        float: left;
        margin-top: 10%;
    } 
    #requirement-table{
        width: auto;
        float: left;
        margin-left: 2%;
        margin-top: 7%;
    }
    #status-table{
        margin-top: 11px;
        width: auto;
        float: left;
    }

    .bg-success{
        background: #090;
        color: #FFF;
    }
    .bg-danger{
        background: #cc0001;
        color: #FFF;
    }
    .bg-warning{
        background: #e89505;
        color: #FFF;
    }
    .red{
        color: #D9534F;
    }
    .orange-flip{
        color: #F0AD4E;
        transform: rotate(30deg);
    }
    .yellow-flip{
        color: #F5E348;
        transform: rotate(45deg);
    }
    .green{
        color: #449D44; 
    }
    .blue{
        color: #5BC0DE;
    }
    .blue, .green,.yellow-flip,.orange-flip,.red{
        font-size: 20px;
    }
    .portlet.light.bordered{
        float: left;
        width: 100%;
    }
    /*    .timeline{
            height: 20%;
        }*/
</style>
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Services Overview</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            <i class="fa fa-life-bouy fa-2x"></i> Services Overview 
                        </div>
                        {!! Form::open(['method' => 'post', 'action' => ['ServiceOverviewController@index'],'id'=>'directorate_form']) !!}
                        <div class="form-group col-md-4">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates_dropdown ,$directorate_id, [ 'class'=>'form-control col-md-3','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::select('speciality_id', $specialities_dropdown ,$speciality_id, [ 'class'=>'form-control col-md-3','id'=>'speciality_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-service-overview'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-service-overview') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            @php
                            $selected_date = new Carbon(session()->get('date_time'));
                            @endphp
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">{{$selected_date->format('Y M d D H:i')}}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel panel-primary staff">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Staff</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div id="staff"></div>
                                    </div>
                                </div>
                                <div class="panel panel-primary service">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Services</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div id="services"></div>
                                    </div>
                                </div>
                                <div class="panel panel-primary event-timeline">
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Transition Times</h3>
                                    </div>
                                    <div class="panel-body panel-requirement">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                            <thead>
                                                <tr>
                                                    <th> Transition Times </th>
                                                    <th> Service </th>
                                                    <th> Staff </th>
                                                </tr>
                                            </thead>
                                            @if(isset($transition_times[$directorate_id]))
                                                @foreach($transition_times[$directorate_id] as $time)
                                                <tr>
                                                    <td>
                                                        {{ $time['name'] .' ('.$time['start_time'].' - '.$time['end_time'].')' }}
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            @if(isset($transition_times_services[$time['id']]))
                                                                @foreach($transition_times_services[$time['id']] as $service)
                                                                    <li>{{$service['name']}}</li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            @if(isset($transition_times_staff[$time['id']]))
                                                                @foreach($transition_times_staff[$time['id']] as $staff)
                                                                    <li>{{$staff['name']}}</li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endif
                                        </table>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="btn-group">
                                                    <a href="{!! url('/optimal-assignment') !!}">
                                                        <button class="btn green bold" > View Optimal Assignments </button>
                                                    </a>
                                                    <a>
                                                        <button id="optimal-assignment" class="btn green bold" > Assign Optimal Assignments </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary event-timeline">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Event Timline</h3>
                                    </div>
                                    <div class="panel-body text-center panel-requirement">
                                        @php //dd($events); @endphp
                                        @if(isset($events) && count($events))
                                        <ul class="events">
                                            @foreach($events as $key=> $time )
                                            <li><b><a data="{!!$key!!}" class="event">{!! $key!!}</a></b></li>
                                            @endforeach
                                        </ul>
                                        @else
                                        <b>No Data found.</b>
                                        @endif
                                        <h4 id="event-time"></h4>
                                        <b id="no-data"></b>
                                        <br>
                                        <i class="fa fa-spinner fa-3x fa-spin" aria-hidden="true"></i>
                                        <div class="col-md-12" id="testing-div">
                                            <table class="table table-responsive  table-bordered" id="staff-table">

                                            </table>
                                            <table class="table table-responsive  table-bordered " id='requirement-table'>

                                            </table>
                                        </div>

                                    </div>
                                </div>
                                <div class="panel panel-primary service">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Priority Set</h3>
                                    </div>
                                    <div class="panel-body">
                                        @if(isset($priority_set) && count($priority_set))
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column " >
                                            <thead>
                                                <tr>
                                                    <th>Level</th>
                                                    <th>From</th>
                                                    <th>Priority</th>
                                                    <th>To</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($priority_set as $priority) 
                                                <tr>
                                                    <th width="25%">{!! $priority['level'] !!}</th>
                                                    <th width="15%"><div class="color-box" style="background:{{ $priority['from'] }}"></div></th>
                                                    <th >{!! $priority['priority'] !!}</th>
                                                    <th width="15%"><div class="color-box" style="background:{{ $priority['to'] }}"></div></th>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @else
                                        <b>No Data found.</b>
                                        @endif
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Requirements</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            @if(isset($requirements_array) && count($requirements_array))
                                            @foreach($requirements_array as $key => $level)
                                            <h2>{{$key}}</h2>
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column level-table" id="{!!$key!!}">
                                                <thead>
                                                    <tr>
                                                        <th>Service</th>
                                                        <th>Parameter</th>
                                                        <th>Operator</th>
                                                        <th>Value</th>
                                                        <th>Status</th>
                                                        <th>Number</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($level as $requirement)
                                                    <tr>

                                                        <td width="20%">{{$requirement->name}}</td>
                                                        <td>{{$requirement->parameter}}</td>
                                                        <td>{{$requirement->operator}}</td>
                                                        <td>{{$requirement->value_name}}</td>
                                                        <td>{{$requirement->status}}</td>
                                                        <td>{{$requirement->number}}</td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                            @endforeach
                                            @else
                                            <b>No Data found</b>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
<div class="modal fade bs-modal-sm" id="info-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Assign Staff</h4>
            </div>
            {!! Form::open(['method' => 'post', 'action' => ['ServiceOverviewController@assignService'],'id'=>'service-assign']) !!}
            <div class="modal-body">
                {!! Form::hidden('service_id', null ,['id'=>'service_id']); !!}
                {!! Form::hidden('directorate_staff_id', null ,['id'=>'staff_id']); !!}
                {!! Form::hidden('duration', null ,['id'=>'duration']); !!}
                {!! Form::hidden('start', null ,['id'=>'start']); !!}
                {!! Form::hidden('end', null ,['id'=>'end']); !!}
                {!! Form::hidden('end_day', null ,['id'=>'endday']); !!}
                {!! Form::hidden('day', null ,['id'=>'day']); !!}
                {!! Form::hidden('date', null ,['id'=>'assign_date']); !!}
                <b>Assign staff member to this service?</b>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes',['class'=>'btn btn-success']); !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close(); !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-sm" id="update-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Staff</h4>
            </div>
            {!! Form::open(['method' => 'put', 'action' => ['ServiceOverviewController@updateAssignService'],'id'=>'service-assign']) !!}
            <div class="modal-body">
                {!! Form::hidden('assign_id', null ,['id'=>'assign_id']); !!}
                {!! Form::hidden('end', null ,['id'=>'ending']); !!}
                <b>Remove staff member from this service?</b>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes',['class'=>'btn btn-success']); !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close(); !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{!! url('/js/fullcalendar.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('/js/scheduler.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('/js/service-overview.js') !!}" type="text/javascript"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
$('#optimal-assignment').click(function () {
    jQuery.ajax({
        url: APP_URL + '/command',
        type: 'get',
        success: function (data) {
            if (data.success) {
                toastr.success(data.success, "Success");
            } else {
                toastr.error(data.error, "Error");
            }
        }
    });
});
</script>
@endsection