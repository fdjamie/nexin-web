@extends('admin.admin')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Services and Posts view </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-4">
                            Services and Posts view 
                        </div>
                        {!! Form::open(['method' => 'POST', 'autocomplete'=>'off', 'action' => ['HomeController@servicePostView']]) !!}
                        <div class="form-group col-md-4">
                            {!! Form::label('directorate', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('directorate', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::select('speciality_id', $specialities ,$speciality_id, [ 'class'=>'form-control','id'=>'speciality_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="portlet-body">
                        <h3>Directorate Services</h3>
                        <table class="table table-striped table-bordered table-hover" id="directorate_service">
                            <thead>
                                <tr>
                                    <th> Category </th>
                                    <th> Name </th>
                                    <th> Contactable </th>
                                    <th> Min Standards </th>
                                    <th> Sites </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($directorate_services as $service)
                                <tr class="odd gradeX">
                                    <td>{{$service['category']}}</td>
                                    <td>{{$service['name']}}</td>
                                    <td><i class="fa  {{($service['contactable'] == 1 ?'fa-check-square-o':'fa fa-square-o')}}" aria-hidden="true"></i></td>
                                    <td><i class="fa  {{($service['min_standards'] == 1 ?'fa-check-square-o':'fa fa-square-o')}}" aria-hidden="true"></i></td>
                                    <td>{{$service['sites']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <h3>Speciality Services</h3>
                        <table class="table table-striped table-bordered table-hover" id="speciality_service">
                            <thead>
                                <tr>
                                    <th> Category </th>
                                    <th> Name </th>
                                    <th> Contactable </th>
                                    <th> Min Standards </th>
                                    <th> Sites </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($speciality_services as $service)
                                <tr class="odd gradeX">
                                    <td>{{$service['category']}}</td>
                                    <td>{{$service['name']}}</td>
                                    <td><i class="fa  {{($service['contactable'] == 1 ?'fa-check-square-o':'fa fa-square-o')}}" aria-hidden="true"></i></td>
                                    <td><i class="fa  {{($service['min_standards'] == 1 ?'fa-check-square-o':'fa fa-square-o')}}" aria-hidden="true"></i></td>
                                    <td>{{$service['sites']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <h3>Posts</h3>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id='posts'>
                            <thead>
                                <tr>
                                    <th> Name </th>
                                    <th> Staff </th>
                                    <th> Description </th>
                                    <th> Week </th>
                                    <th> Additional Parameter (values) </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($post_array as $post)
                                <tr class="odd gradeX">
                                    <td>{{$post->name}}</td>
                                    <td>{{(isset($post->staff)?$post->staff->name:'-')}}</td>
                                    <td width='30%'>{{($post->description)}}</td>
                                    <td>{{$post->assigned_week}}</td>
                                    <td>{{(isset($post->parameter)?implode(', ',$post->parameter):'')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!--<script src="/js/service_post_view.js" type="text/javascript"></script>-->
<script>
$(document).ready(function () {
    $('#directorate_service,#speciality_service,#posts').DataTable();
    $('#directorate_id,#speciality_id').change(function () {
        $('form').submit();
    });
});
</script>
@endsection