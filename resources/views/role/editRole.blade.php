@extends('admin.admin')

@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shield"></i>{{$role->name}} 
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'action' => ['HomeController@updateRole', $role->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$role->directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $role->name ,['placeholder' => 'Enter Name' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Role', ['class' => 'awesome']); !!} <span id="position_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('position', $roles_position ,$role->position, [ 'class'=>'form-control','id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!} 
                            {!! Form::select('place', [0 => 'Before', 1 => 'After'] ,null, [ 'class'=>'form-control','id'=>'place']); !!}
                        </div>
                        {!! Form::hidden('current_position', $role->position ,['class' => 'form-control']); !!}
                        {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
<script src="{{url('/js/role.js')}}" type="text/javascript"></script>
@endsection
