@extends('admin.admin')

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa fa-shield"></i>Add Role
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-role'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-role') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'action' => ['HomeController@storeRole']]) !!}
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,null, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', '',['placeholder' => 'Enter Role' , 'class' => 'form-control']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('position', 'Select Role', ['class' => 'awesome']); !!} <span id="position_spin"><i class="fa fa-spin fa-refresh"></i></span>
                            {!! Form::select('position', $roles_position ,null, [ 'class'=>'form-control','id'=>'position']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Position', ['class' => 'awesome']); !!} 
                            {!! Form::select('place', [0 => 'Before', 1 => 'After'] ,null, [ 'class'=>'form-control','id'=>'place']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shield"></i>Upload Roles
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['HomeController@importRole']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('extra-js')
<script src="{{url('/js/role.js')}}" type="text/javascript"></script>
@endsection
