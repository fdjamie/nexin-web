@extends('admin.admin')
@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-home fa-2x"></i>TRUSTS 
                        </div>
                    </div>

                    @if ($errors->any())

                        @foreach ($errors->all() as $error)
                        <div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                           {{$error}}
                        </div>
                        @endforeach
                    @endif
                    @if (Illuminate\Support\Facades\Session::has('success-trust'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-trust') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-trust'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-trust') }}
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                      {{--  <a href="{{url('/trust/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" > Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        <a href="{{url('/trust/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Trust <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                        --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th style="width: 50px;"> Id</th>
                                    <th> Name </th>

                                  {{--  <th style="width:100px;text-align: center;"> Actions </th>--}}
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($trusts))
                                @forelse ($trusts as $trust)
                                <tr class="odd gradeX">
                                    <td>{{$trust->id}}</td>
                                    <td>{{$trust->name}}
                                        {!! ($trust->id==session('activeTrust'))?'<span class="badge badge-success"><i class="fa fa-check"></i> Active Now</span>':''!!}
                                    </td>
                                    {{--<td class="text-center">
                                        <a href="{{url('/trust/edit/'.$trust->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                        <a data-toggle="modal" href="#small" id='{{$trust->id}}' class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>--}}
                                </tr>
                                @empty
                               {{-- <tr>
                                    <td></td>
                                    <td colspan='4'>no record found</td>
                                </tr>--}}
                                @endforelse
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $('.delete').click(function () {
        $('#delete-button').attr('href', '{{url("/trust/delete")}}' +'/'+ $(this).attr('id'));
    });
</script>
@endsection