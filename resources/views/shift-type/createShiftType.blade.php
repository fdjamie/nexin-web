@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" integrity="sha256-lBtf6tZ+SwE/sNMR7JFtCyD44snM3H2FrkB/W400cJA=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.min.css" integrity="sha256-om6WdVA9jl2Y5FpRyiHQWboI3PRXcY9SDh9UuYOxRtA=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha256-Zlen06xFBs47DKkjTfT2O2v/jpTpLyH513khsWb8aSU=" crossorigin="anonymous" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-clock-o"></i>Define Shift
                        </div>
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('error-shifttype'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        @foreach(session('error-shifttype') as $errornode)
                        <ul>
                            @foreach($errornode as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                        @endforeach
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'id'=>'addRotaTemplate','action'=>['ShiftTypeController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', old('name'),['class' => 'form-control','id'=>'name']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,old('directorate_id'), [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Group', ['class' => 'awesome']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="rotaSpin"></i>
                            {!! Form::select('rota_group_id', $rota_groups ,old('rota_group_id'), [ 'class'=>'form-control directorate_speciality_id','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="specialitySpin"></i>
                            {!! Form::select('directorate_speciality_id', $specialities ,old('directorate_speciality_id'), [ 'class'=>'form-control directorate_speciality_id','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} {!! Form::checkbox('category_check', 1 ,true, ['id'=>'category_check']); !!}<i class="fa fa-refresh fa-spin" aria-hidden="true" id="categorySpin"></i>
                            {!! Form::select('category_id', $categories ,old('category_id'), [ 'class'=>'form-control category_id','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group">
                           <a data-toggle="modal" href="#small" id="modal-display">
                               {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!}  </a>
                            {!! Form::checkbox('sub_category_check', 1 ,true, ['id'=>'sub_category_check']); !!}
                            <i class="fa fa-refresh fa-spin" aria-hidden="true" id="subSpin"></i>
                            {!! Form::select('sub_category_id', [] ,old('sub_category_id'), [ 'class'=>'form-control serach-subcategory','id'=>'sub_category_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('service_id', 'Service', ['class' => 'awesome']); !!} {!! Form::checkbox('service_check', 1 ,true, ['id'=>'service_check']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="serviceSpin"></i>
                            {!! Form::select('service_id', $combined_services ,old('service_id'), [ 'class'=>'form-control service_id','id'=>'service_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                            {!! Form::text('start_time', old('start_time'),['class' => 'form-control time','id'=>'start_time']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_time', 'End Time', ['class' => 'awesome time']); !!}
                            {!! Form::text('finish_time', old('finish_time'),['class' => 'form-control time','id'=>'end_time']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('overrides_teaching', 'Overrides Teaching ?', ['class' => 'awesome','id'=>'overrides_teaching']); !!}
                            {!! Form::checkbox('overrides_teaching', 1,true,['id'=>'overrides_teaching','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('nroc', 'Non residential on call ?', ['class' => 'awesome','id'=>'overrides_teaching']); !!}
                            {!! Form::checkbox('nroc', 1,true,['id'=>'nroc','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('on_site', 'On Site', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('on_site',1,true,['id'=>'on_site','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Add !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-clock-o"></i> Upload ShiftTypes
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['ShiftTypeController@import']]) !!}
                        <div class="form-group">
                            {!! Form::label('update', 'Update data', ['class' => 'awesome','id'=>'overrides_teaching']); !!}
                            {!! Form::checkbox('update_flag', 1,false,['id'=>'update']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-clock-o"></i> Upload Master Data
                        </div>
                    </div>
                    @if($errors->has())
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'post', 'files' => true , 'action' => ['ShiftTypeController@importReferenceData']]) !!}
                        <div class="form-group">
                            {!! Form::label('replace', 'Choose file', ['class' => 'awesome']); !!}
                            {!! Form::file('file',['accept'=>".xls,.xlsx,.csv"]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Upload !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.categoryTreeModal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.min.js" integrity="sha256-LPgEyZbedErJpp8m+3uasZXzUlSl9yEY4MMCEN9ialU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js" integrity="sha256-AKUJYz2DyEoZYHh2/+zPHm1tTdYb4cmG8HC2ydmTzM4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha256-WO6QcQSEM5vwHL4eANUd/mzxRqRyxP3RWj+r6FS5qXk=" crossorigin="anonymous"></script>
<script src="{{url('/js/shift_type.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('/js/MultiNestedList.js')}}"></script>
<script src="{{url('/js/search-subcategory.js')}}" type="text/javascript"></script>
@endsection