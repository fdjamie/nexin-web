@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" integrity="sha256-lBtf6tZ+SwE/sNMR7JFtCyD44snM3H2FrkB/W400cJA=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.min.css" integrity="sha256-om6WdVA9jl2Y5FpRyiHQWboI3PRXcY9SDh9UuYOxRtA=" crossorigin="anonymous" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-clock-o"></i>{{$shifttype->name}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['method' => 'put', 'id'=>'addRotaTemplate','action'=>['ShiftTypeController@update',$shifttype->id]]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
                            {!! Form::text('name', $shifttype->name,['class' => 'form-control','id'=>'name']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$shifttype->directorate_id, [ 'class'=>'form-control','id'=>'directorate_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rota_group_id', 'Rota Group', ['class' => 'awesome']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="rotaSpin"></i>
                            {!! Form::select('rota_group_id', $rota_groups ,$shifttype->rota_group_id, [ 'class'=>'form-control directorate_speciality_id','id'=>'rota_group_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('directorate_speciality_id', 'Speciality', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_speciality_id', $specialities ,$shifttype->directorate_speciality_id, [ 'class'=>'form-control directorate_speciality_id','id'=>'directorate_speciality_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category', ['class' => 'awesome']); !!} {!! Form::checkbox('category_check', 1 ,($category_id != null ?true:false ), ['id'=>'category_check']); !!}<i class="fa fa-refresh fa-spin" aria-hidden="true" id="categorySpin"></i>
                            {!! Form::select('category_id', $categories ,($shifttype->service_id != 0 ? $shifttype->service->category_id : 0), [ 'class'=>'form-control category_id','id'=>'category_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'awesome']); !!} {!! Form::checkbox('sub_category_check', 1 ,($subcategory_id != null ?true:false ), ['id'=>'sub_category_check']); !!}<i class="fa fa-refresh fa-spin" aria-hidden="true" id="subSpin"></i>
                            {!! Form::select('sub_category_id', $subcategories ,($shifttype->service_id != 0 ? $shifttype->service->sub_category_id :0), [ 'class'=>'form-control','id'=>'sub_category_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('service_id', 'Service', ['class' => 'awesome']); !!} {!! Form::checkbox('service_check', 1 ,(($shifttype->service_id != 0 && $shifttype->service_type != 'none')?true:false), ['id'=>'service_check']); !!} <i class="fa fa-refresh fa-spin" aria-hidden="true" id="serviceSpin"></i>
                            {!! Form::select('service_id', $combined_services ,$shifttype->service_type.'-'.$shifttype->service_id, [ 'class'=>'form-control service_id','id'=>'service_id']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_time', 'Start Time', ['class' => 'awesome']); !!}
                            {!! Form::text('start_time', $shifttype->start_time,['class' => 'form-control time','id'=>'start_time']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_time', 'End Time', ['class' => 'awesome']); !!}
                            {!! Form::text('finish_time', $shifttype->finish_time,['class' => 'form-control time','id'=>'end_time']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('overrides_teaching', 'Overrides Teaching ?', ['class' => 'awesome','id'=>'overrides_teaching']); !!}
                            {!! Form::checkbox('overrides_teaching', 1,$shifttype->overrides_teaching,['id'=>'overrides_teaching','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('nroc', 'Non residential on call ?', ['class' => 'awesome','id'=>'overrides_teaching']); !!}
                            {!! Form::checkbox('nroc', 1,$shifttype->nroc,['id'=>'nroc','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('on_site', 'On Site', ['class' => 'awesome']); !!}
                            {!! Form::checkbox('on_site',1,$shifttype->on_site,['id'=>'on_site','class'=>'make-switch', 'data-on-color'=>'success','data-off-color'=>'danger']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Update !', ['class'=> 'btn green']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection

@section('extra-js')
@include('includes.scriptsViewLinks')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.min.js" integrity="sha256-LPgEyZbedErJpp8m+3uasZXzUlSl9yEY4MMCEN9ialU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js" integrity="sha256-AKUJYz2DyEoZYHh2/+zPHm1tTdYb4cmG8HC2ydmTzM4=" crossorigin="anonymous"></script>
<script src="{{url('/js/shift_type.js')}}" type="text/javascript"></script>
<script>
if ($('#category_check').is(':checked'))
    $('#category_id').removeAttr("disabled");
else
    $('#category_id').attr("disabled", "disabled");

if ($('#sub_category_check').is(':checked'))
    $('#sub_category_id').removeAttr("disabled");
else
    $('#sub_category_id').attr("disabled", "disabled");

if ($('#service_check').is(':checked'))
    $('#service_id').removeAttr("disabled");
else
    $('#service_id').attr("disabled", "disabled");
</script>
@endsection