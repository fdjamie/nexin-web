@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-6">
                            <i class="fa fa-clock-o"></i>Shift Types
                        </div>
                        {!! Form::open(['method' => 'put', 'action' => ['ShiftTypeController@index']]) !!}
                        <div class="form-group col-md-3 pull-right">
                            {!! Form::label('directorate_id', 'Directorate', ['class' => 'awesome']); !!}
                            {!! Form::select('directorate_id', $directorates ,$directorate, [ 'class'=>'form-control col-md-3','id'=>'directorate_id']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if (Illuminate\Support\Facades\Session::has('success-shifttype'))
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('success-shifttype') }}
                    </div>
                    @elseif (Illuminate\Support\Facades\Session::has('error-shifttype'))
                    <div class='alert alert-danger alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        {{ session('error-shifttype') }}
                    </div>
                    @endif
                    @if (Illuminate\Support\Facades\Session::has('info-shift'))
                    <div class='alert alert-info alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <ul>
                        @foreach(session('info-shift') as $info)
                        <li class='list-group-item-info'>{{ $info }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/shift-type/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green" data-toggle="modal" href="#basic"> Add New <i class="fa fa-plus"></i></button>
                                        </a>
                                        <a href="{{url('/shift-type/export')}}">
                                            <button id="sample_editable_1_2_new" class="btn sbold green"> Export Shift type <i class="fa fa-file-excel-o"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-tabs">
                            @foreach($rota_groups as $rota_group)
                            @if($rota_group->id == $rota_groups[0]->id)
                            <li class="active">
                                @else
                            <li>
                                @endif
                                <a href="#tab_{{$rota_group->id}}" data-toggle="tab">{{ $rota_group->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @if(!count($rota_groups))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                <tr>
                                    <th width='100%' class="text-center"><b> No rota group for this directorate.</b></td>
                                </tr>    
                            </table>
                            @endif
                            @foreach($rota_groups as $rota_group)
                            @if($rota_group->id == $rota_groups[0]->id)
                            <div class="tab-pane fade active in" id="tab_{{$rota_group->id}}">
                                @else
                                <div class="tab-pane fade" id="tab_{{$rota_group->id}}">
                                    @endif
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                        <thead>

                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>NROC (Non Residential On Call)</th>
                                                <th> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!count($rota_group->shift_type))
                                            <tr>
                                                <td></td>
                                                <td colspan="3" class="text-center"><b> No data entered yet.</b></td>
                                            </tr>
                                            @endif
                                            @foreach($rota_group->shift_type as $shift_type)
                                            <tr>
                                                <td width="25%">{{$shift_type->id}}</td>
                                                <td width="25%">{{$shift_type->name}}</td>
                                                <td width="25%">{{(($shift_type->nroc == 1)? 'Yes':'No')}}</td>
                                                <td class="text-center">
                                                    <a href="{{url('/shift/'.$shift_type->id.'/shift-detail')}}" ><i class="fa fa-clock-o" aria-hidden="true"></i></a> 
                                                    <a href="{{url('/shift-type/edit/'.$shift_type->id)}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                    <a data-toggle="modal" href="#small" id="{{$shift_type->id}}" class="delete" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('modal')
@include('common.delete-confirmation-modal')
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            $('#delete-button').attr('href', '{{url("/shift-type/delete")}}' +'/'+ $(this).attr('id'));
        });
        $('#directorate_id').change(function () {
            $('form').submit();
        });
    });

</script>
@endsection