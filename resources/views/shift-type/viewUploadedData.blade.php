@extends('admin.admin')

@section('extra-css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Uploaded Directorates</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-clock-o"></i>Shift Types 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                        </div>
                        <div class="clearfix overflow-scroll">
                            <table class="table table-responsive  table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th> Name </th>
                                        <th> directorate </th>
                                        <th> speciality </th>
                                        <th> service </th>
                                        <th> Result </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($view_data as $shifttype)
                                    @if(isset($shifttype['directorate_error']) || isset($shifttype['speciality_error']) || isset($shifttype['rotagroup_error']) || isset($shifttype['rotatemplate_error']) || isset($shifttype['shifttype_error']) || isset($shifttype['teaching_error']) || isset($shifttype['nroc_error'])
                                                )
                                    <tr class="bg-danger">
                                        @else
                                    <tr class="odd gradeX">
                                        @endif    
                                        <td>{{ $shifttype['name'] }}</td>
                                        <td>{{ $shifttype['directorate'] }}</td>
                                        <td>{{ (isset($shifttype['speciality'])?$shifttype['speciality']:'') }}</td>
                                        <td>{{ (isset($shifttype['service'])?$shifttype['service']:'') }}</td>
                                        <td class="table-danger">
                                            <ul>
                                                @if(isset($shifttype['directorate_error']))<li class="list-group-item-danger"> {{$shifttype['directorate_error']}}</li>@endif 
                                                @if(isset($shifttype['speciality_error']))<li class="list-group-item-danger"> {{$shifttype['speciality_error']}}</li>@endif 
                                                @if(isset($shifttype['rotagroup_error']))<li class="list-group-item-danger"> {{$shifttype['rotagroup_error']}}</li>@endif 
                                                @if(isset($shifttype['rotatemplate_error']))<li class="list-group-item-danger"> {{$shifttype['rotatemplate_error']}}</li>@endif 
                                                @if(isset($shifttype['shifttype_error']))<li class="list-group-item-danger"> {{$shifttype['shifttype_error']}}</li>@endif 
                                                @if(isset($shifttype['teaching_error']))<li class="list-group-item-danger"> {{$shifttype['teaching_error']}}</li>@endif 
                                                @if(isset($shifttype['nroc_error']))<li class="list-group-item-danger"> {{$shifttype['nroc_error']}}</li>@endif 
                                                @if(isset($shifttype['service_error']))<li class="list-group-item-danger"> {{$shifttype['service_error']}}</li>@endif 
                                                @if(isset($shifttype['servicetype_error']))<li class="list-group-item-danger"> {{$shifttype['servicetype_error']}}</li>@endif 
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-toolbar">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{url('/shift-type/new')}}">
                                            <button id="sample_editable_1_2_new" class="btn btn-warning" > Upload Again <i class="fa fa-repeat"></i></button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        @if(isset($flag))
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['ShiftTypeController@uploadReferenceData'] ]) !!}
                                        @else
                                        {!! Form::open(['method' => 'post' ,'files' => true , 'action' => ['ShiftTypeController@upload'] ]) !!}
                                        @endif
                                        {!! Form::hidden('file_name',$file_name); !!}
                                        {!! Form::submit('Proceed !', ['class'=> 'btn green','disabled'=>(isset($master_error) && $master_error == true ?true:false)]); !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('extra-js')
@include('includes.scriptsViewLinks')
@endsection