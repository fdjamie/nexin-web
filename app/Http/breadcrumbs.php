<?php

Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Home /', route('home'));
});

//############################## DIRECTORATES BREACBRUMBS ########################################

Breadcrumbs::register('directorates', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Directorates /', route('directorates'));
});

Breadcrumbs::register('directorate/new', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Directorates /', route('directorates'));
    $breadcrumbs->push('Add Directorate /', route('directorate/new'));
});

Breadcrumbs::register('directorate/edit', function($breadcrumbs,$directorate) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Directorates /', route('directorates'));
    $breadcrumbs->push($directorate->name, route('directorate/edit',$directorate->id));
});

//############################## DIVISIONS BREACBRUMBS ########################################

Breadcrumbs::register('divisions', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Divisions /', route('divisions'));
});

Breadcrumbs::register('division/new', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Divisions /', route('divisions'));
    $breadcrumbs->push('Add Division /', route('division/new'));
});

Breadcrumbs::register('division/edit', function($breadcrumbs,$directorate) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Divisions /', route('divisions'));
    $breadcrumbs->push($division->name, route('division/edit',$division->id));
});

//############################## SPECIALITY BREACBRUMBS ########################################

Breadcrumbs::register('directoratespeciality', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Speciality /', route('directoratespeciality'));
});

Breadcrumbs::register('directoratespeciality/new', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Speciality /', route('directoratespeciality'));
    $breadcrumbs->push('Add Speciality /', route('directoratespeciality/new'));
});

Breadcrumbs::register('directoratespeciality/edit', function($breadcrumbs,$speciality) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Speciality /', route('directoratespeciality'));
    $breadcrumbs->push($speciality->name, route('directoratespeciality/edit',$speciality->id));
});
//############################## SERVICES BREACBRUMBS ########################################

Breadcrumbs::register('services', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Services /', route('services'));
});

Breadcrumbs::register('service/new', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Services /', route('services'));
    $breadcrumbs->push('Add Service /', route('service/new'));
});

Breadcrumbs::register('service/edit', function($breadcrumbs,$service) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Services /', route('services'));
    $breadcrumbs->push($service->name, route('service/edit',$service->id));
});

//############################## ROTATION BREACBRUMBS ########################################

Breadcrumbs::register('rotations', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Rotations /', route('rotations'));
});

Breadcrumbs::register('rotation/new', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Rotations /', route('rotations'));
    $breadcrumbs->push('Add Rotation /', route('rotation/new'));
});

Breadcrumbs::register('rotation/edit', function($breadcrumbs,$rotation) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Rotations /', route('rotations'));
    $breadcrumbs->push('Rotation', route('rotation/edit',$rotation->id));
});

Breadcrumbs::register('rotation/detail', function($breadcrumbs,$id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Rotations /', route('rotations'));
    $breadcrumbs->push('Rotation Details', route('rotation/detail',$id));
});

//############################## POST BREACBRUMBS ########################################
//Breadcrumbs::register('posts', function($breadcrumbs) {
//    $breadcrumbs->parent('home');
//    $breadcrumbs->push('Posts /', route('posts'));
//});
//
//Breadcrumbs::register('post/new', function($breadcrumbs) {
//    $breadcrumbs->parent('home');
//    $breadcrumbs->push('Posts /', route('posts'));
//    $breadcrumbs->push('Add Post /', route('post/new'));
//});
//
//Breadcrumbs::register('post/edit', function($breadcrumbs,$post) {
//    $breadcrumbs->parent('home');
//    $breadcrumbs->push('Posts /', route('posts'));
//    $breadcrumbs->push($post->name, route('post/edit',$post->id));
//});

//############################## LOCATIONS BREACBRUMBS ########################################

Breadcrumbs::register('locations', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Locations', route('locations'));
});
