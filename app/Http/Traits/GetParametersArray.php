<?php

namespace App\Http\Traits;

trait GetParametersArray {
   
    public function getParametersArray($parameters) {
        $result = [];
        foreach ($parameters as $parameter) {
            $result['select'] = 'Select Parameter';
            $result[lcfirst($parameter->name)] = $parameter->name;
        }
        return $result;
    }
    public function getOperator($operator) {
         $operators = [];
        foreach ($operator as $node) {
            $operators[$node->value] = $node->name;
        }
        return $operators;
    }
}
