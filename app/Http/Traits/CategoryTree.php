<?php

namespace App\Http\Traits;

trait CategoryTree {
  private $child = '';
    public function getTree($categories) {
        $tree = '<ul>';
        foreach ($categories as $category) {
            $tree = $tree . '<li><a href=#>' . $category->name;
            if (isset($category->sub_category)) {
                $tree = $tree . '<ul>';
                foreach ($category->sub_category as $subcategory) {
                    $this->child = '';
                    $tree = $tree . '<li><a href=#>' . $subcategory->name;
                    if (isset($subcategory->child_recursive)) {

                        $tree = $tree . $this->getChild($subcategory->child_recursive);
                    }
                    $tree = $tree . '</li>';
                }
                $tree = $tree . '</ul>';
            }
            $tree = $tree . '</li>';
        }
        $tree = $tree . "</ul>";
        return $tree;
    }
     protected function getChild($nodes) {
        $this->child = $this->child . '<ul>';
        foreach ($nodes as $child_node) {
            $this->child = $this->child . '<li><a href=#>' . $child_node->name;
            if (isset($child_node->child_recursive))
                $this->getChild($child_node->child_recursive);
            $this->child = $this->child . '</li>';
        }
        $this->child = $this->child . '</ul>';
        return $this->child;
    }
}
