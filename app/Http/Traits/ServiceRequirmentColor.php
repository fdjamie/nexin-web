<?php

namespace App\Http\Traits;

use App\Http\Helpers;

trait ServiceRequirmentColor {

    public function getRequirementByColor($requirements, $color, $directorate_id, $operatorData, $directorateParameter) {
        $operatorCheck = $operatorById = $operatorByName = [];
        $result = $test = [];
        foreach ($operatorData as $operator) {
            $operatorById[$operator->id] = $operator->value;
            $operatorByName[$operator->id] = $operator->name;
        }
        foreach ($requirements as $key => $requirement) {
            if ($requirement->color == $color) {
                $requirement->values = $this->getValues($requirement->parameter, $directorate_id);
               
                if (!empty($directorateParameter)) {
                    foreach ($directorateParameter as $operaterKey => $parameter) {
                        $operatorArray = [];
                        $operatorCheck[$operaterKey] = $parameter->name;
                        if (in_array(ucfirst($requirement->parameter), $operatorCheck)) {
                            if (ucfirst($requirement->parameter) === $parameter->name) {

                                foreach ($parameter->operator as $operatorId) {
                                    if ($operatorById[$operatorId]) {
                                        $operatorArray[$operatorById[$operatorId]] = $operatorByName[$operatorId];
                                        $requirements[$key]->operators = $operatorArray;
                                    }
                                }
                            }
                        } else {
                            $requirements[$key]->operators = [];
                        }
                    }
                } else {
                    $requirements[$key]->operators = [];
                }
                
                   
                        $result[] = $requirement;
                   
                
            }
        }

        return $result;
    }

    public function getValue($value, $d_id) {
        switch ($value) {
            case 'grade':
                $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
                $directorates = json_decode($response->getBody()->getContents());
                $grades = Helpers::getAllNodesByIdFromMainNode($directorates, 'grade', $d_id, 'directorate_id');
                $grades = Helpers::getDropDownData($grades);
                return $grades;
                break;

            case 'role':
                $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
                $directorates = json_decode($response->getBody()->getContents());
                $roles = Helpers::getAllNodesByIdFromMainNode($directorates, 'role', $d_id, 'directorate_id');
                $roles = Helpers::getDropDownData($roles);
                return $roles;
                break;

            case 'qualification':
                $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('qualification'), '/'));
                $qualifications = json_decode($response->getBody()->getContents());
                $qualifications = Helpers::getDropDownData($qualifications);
                return $qualifications;
                break;

            case 'site':
                $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
                $sites = json_decode($response->getBody()->getContents());
                $sites = Helpers::getDropDownData($sites);
                return $sites;
                break;

            case 'location':
                $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
                $sites = json_decode($response->getBody()->getContents());
                $locations = Helpers::getAllNodesFromMainNode($sites, 'location');
                $locations = Helpers::getDropDownData($locations);
                return $locations;
                break;
            default:
                $response = $this->api_client->request("post", Helpers::getAPIUrl('additional-parameter') . $value . '/' . $d_id);
                $additionalParameter = json_decode($response->getBody()->getContents());
                $additionalParameters = [];
                if (isset($additionalParameter)) {
                    $array = explode(',', $additionalParameter->value);
                    foreach ($array as $additionalParameterValue) {
                        $additionalParameters[$additionalParameterValue] = $additionalParameterValue;
                    }
                }
                return $additionalParameters;
        }
    }

}
