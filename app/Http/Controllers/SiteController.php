<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class SiteController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.sites');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        $records = $trusts->site;
        return view('site.allSites', compact('records', 'trusts'));
    }

    public function create() {
        __authorize($this->module,'add',true);
        return view('site.createSite');
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('site'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-site', $result->error);
            return Redirect::back();
        }
        Session::flash('success-site', $request->name . ' , site Added successfully');
        return redirect('/sites');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('site') . $id);
        $site = json_decode($response->getBody()->getContents());
        return view('site.editSite', compact('site'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('site') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && $result->error != '') {
            Session::flash('error', $result->error);
            return redirect('/sites');
        } else {
            Session::flash('success-site', $result->updated);
            return redirect('/sites');
        }
    }
    
    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('site') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-site', $result->error);
        return redirect('/sites');
    }
    
    public function export() {
        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'abbreviation', 'address', 'trust'];
        $ref_data[] = ['name', 'abbreviation', 'address', 'trust'];
        $ref_data[] = ['Dummy Site', 'DS', 'Dummy address', 'Dummy Trust'];
        foreach ($sites as $site) {
            if (isset($site) && count($site))
                array_push($data, [$site->name, $site->abbreviation, $site->address, $site->trust->name]);
        }
        ExcelHelper::exportExcel('Site_backup', $data, $ref_data);
        Session::flash('success-Site', 'Sites data exported successfully.');
        return redirect('/sites');
    }

    public function import(Request $request) {
        __authorize($this->module,'add',true);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust'));
            $all_trust = json_decode($response->getBody()->getContents());
            $all_trust_array = Helpers::getDropDownData($all_trust);
            $trust_names = Helpers::gettingArrayOfName($all_trust);
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
            $sites = json_decode($response->getBody()->getContents());
            $trust_name = $all_trust_array[auth()->user()->trust_id];
            $sites = Helpers::gettingArrayOfName($sites);
            foreach ($data as $data_node) {
                if (!isset($data_node['name'])) {
                    $data_node['name'] = '';
                    $data_node['site_name_error'] = 'Site name is required';
                }
                if (!in_array($data_node['trust'], $trust_names))
                    $data_node['trusts_error'] = 'This trust does not exist in the system';
                if ($data_node['trust'] != $trust_name && in_array($data_node['trust'], $trust_names))
                    $data_node['trust_error'] = 'You are not authorized for this trust';
                if ($data_node['trust'] == $trust_name && in_array($data_node['name'], $sites))
                    $data_node['site_error'] = 'This site already exist for this trust';
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('site.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records,'trust_id'=>auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('site') .'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-site', $result->error);
            return redirect('/sites');
        } else {
            Session::flash('success-site', $result->success);
            return redirect('/sites');
        }
    }

}
