<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use Carbon\Carbon;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Client;
use App\Http\Traits\CategoryTree;
use App\Http\Traits\GetParametersArray;
use App\Http\Traits\ServiceRequirmentColor;

class ServiceController extends Controller {

    use CategoryTree,
        GetParametersArray,
        ServiceRequirmentColor;

    private $api_token, $api_url, $api_client;
    private $operators = [
        ['name' => 'Equal to', 'id' => '=', 'type' => 'integer'],
        ['name' => 'Greater or Equal to', 'id' => '>=', 'type' => 'integer'],
        ['name' => 'Greater than', 'id' => '>', 'type' => 'integer'],
        ['name' => 'Less or Equal to', 'id' => '<=', 'type' => 'integer'],
        ['name' => 'Less than', 'id' => '<', 'type' => 'integer'],
        ['name' => 'Not equal to', 'id' => '!=', 'type' => 'integer'],
        ['name' => 'Between', 'id' => 'between', 'type' => 'integer'],
        ['name' => 'Any', 'id' => 'any', 'type' => 'string'],
        ['name' => 'Is', 'id' => 'is', 'type' => 'string'],
        ['name' => 'Is not', 'id' => 'is_no', 'type' => 'string'],
    ];

    public function __construct() {
        $this->middleware('auth');
         $this->module=config('module.general-services');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token], 'timeout' => 5]);
    }

    public function index(Request $request) {
         __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates)) {
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        } else {
            $directorate = null;
        }
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate, 'directorate_id');
        if (count($categories)) {
            $selected_category = isset($request->selected_category) ? $request->selected_category : array_keys(Helpers::getDropDownData($categories));
        } else {
            $selected_category = [];
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/service');
        $directorates_speciality = json_decode($response->getBody()->getContents());
//        return [$directorates_speciality];
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates_speciality, 'speciality', $directorate, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates_speciality);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/service');
        $directorates_services = json_decode($response->getBody()->getContents());
        $check_all = $request->check_all;
        return view('service.allServices', compact('directorates', 'specialities', 'directorate', 'categories', 'selected_category', 'directorates_services', 'directorates_speciality', 'check_all'));
    }

    public function create() {
         __authorize($this->module,'add',true);
        $directorates = $sub_categories = $categories = $specialites = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
        $result = json_decode($response->getBody()->getContents());
        $categories = $sub_categories = $child_subcategory = $subcategories = [];
        if (count($result))
            $specialites = Helpers::getAllNodesByIdFromMainNode($result, 'speciality', $result[0]->id, 'directorate_id');
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorates[0]->id, 'directorate_id');
        if (count($categories))
            $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $categories[0]->id, 'category_id');
        $sub_categories[0] = 'Select Subcategory';
        foreach ($subcategories as $node) {
            if (isset($node->specialities) && in_array($specialites[0]->id, $node->specialities)) {
                $sub_categories[$node->id] = $node->name;
            }
        }
        $tree = $this->getTree($categories);
        $directorate = Helpers::getDropDownData($directorates);
        $specialites = Helpers::getDropDownData($specialites);
        $categories = Helpers::getDropDownData($categories);
        return view('service.createService', compact('directorate', 'categories', 'sub_categories', 'specialites', 'tree'));
    }

    public function store(Request $request) {
         __authorize($this->module,'add',true);
        if (!isset($request->min_standards)) {
            $request->merge(['min_standards' => 0]);
        }
        if (!isset($request->contactable)) {
            $request->merge(['contactable' => 0]);
        }
        if (!isset($request->sub_category_id)) {
            $request->merge(['sub_category_id' => 0]);
        }
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('service'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-service', $result->error);
            return redirect()->back();
        } else {
            Session::flash('service-add', $request->name . ' added succesfully.');
            return $this->index($request);
        }
    }

    public function edit($id) {
         __authorize($this->module,'edit',true);
        $sub_categories = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . $id);
        $service = json_decode($response->getBody()->getContents());
        $directorate = $service->directorate_speciality->directorate_id;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/service');
        $directorates = json_decode($response->getBody()->getContents());
        $specialty_data = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id');
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate, 'directorate_id');
        $tree = $this->getTree($categories);
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id');
        $main_category = (($service->category_id == 0) ? $categories[0]->id : $service->category_id );
        $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $main_category, 'category_id');
        foreach ($subcategories as $node) {
            if (isset($node->specialities) && in_array($specialities[0]->id, $node->specialities)) {
                $sub_categories[$node->id] = $node->name;
            }
        }
        $dropdown_text = [];
        $sub_category = (($service->sub_category_id == 0 && count($sub_categories)) ? array_keys($sub_categories)[0] : $service->sub_category_id);
        $categories = Helpers::getDropDownData($categories);
        if (isset($service->sub_category)) {
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('sub-category') . $service->sub_category_id . '/parents');
            $dropdown = json_decode($response->getBody()->getContents());
            $dropdown = array_reverse($dropdown);
            $dropdown_text [$service->sub_category_id] = $service->sub_category->name . ' (child of subcategory "' . $dropdown[0] . '")';
        } else {
            $dropdown_text [0] = 'Select Subcategory';
        }

        $specialty_data = Helpers::getDropDownData($specialty_data);
        $directorates = Helpers::getDropDownData($directorates);
        return view('service.editService', compact('service', 'directorate', 'directorates', 'specialty_data', 'sub_category', 'main_category', 'categories', 'sub_categories', 'tree', 'dropdown_text'));
    }

    public function update(Request $request, $id) {
         __authorize($this->module,'edit',true);
        if (!isset($request->min_standards)) {
            $request->merge(['min_standards' => 0]);
        }
        if (!isset($request->contactable)) {
            $request->merge(['contactable' => 0]);
        }
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('service') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/service/');
    }

    public function destroy($id) {
         __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('service') . $id);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/service/');
    }

    public function import(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $data = ExcelHelper::importExcelNew(Input::file('file'), 0);
        if ($data[0] == null) {
            return Redirect::back()->withErrors('No data found in uploaded file.');
        }
        $view_data = $template = $events = $formated_data = [];
        $previous_service_name = NULL;
        $i = -1;
        foreach ($data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $formated_data[$i] = $this->getService($data_node);
                $formated_data [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEvent($data_node);
            if ($formated_data [$i]['template'] != NULL && $event != NULL) {
                $formated_data [$i]['template']['events'][] = $event;
            }
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_names = Helpers::gettingArrayOfName($directorates);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $site_names = Helpers::gettingArrayOfName($sites);
        $i = 0;
        foreach ($formated_data as $data_node) {
            $i++;
            $category_names = $categories = $subcategory_names = $service_names = $location_names = [];
            if (!in_array($data_node['directorate'], $directorate_names))
                $data_node['directorate_error'] = "This directorate do not exist in the system or in this trust.";
            foreach ($directorates as $directorate) {
                if ($directorate->name == $data_node['directorate']) {
                    $specialties = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate->id, 'directorate_id');
                    $speciality_names = Helpers::gettingArrayOfName($specialties);
                    if ($data_node['category'] != 'N/A') {
                        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate->id, 'directorate_id');
                        $category_names = Helpers::gettingArrayOfName($categories);
                        if ($data_node['subcategory'] != 'N/A') {
                            foreach ($categories as $category_node) {
                                if ($category_node->name == $data_node['category']) {
                                    $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $category_node->id, 'category_id');
                                    $subcategory_names = Helpers::gettingArrayOfName($subcategories);
                                    foreach ($subcategories as $subcategory) {
                                        if ($subcategory->name == $data_node['subcategory']) {
                                            $services = Helpers::getAllNodesByIdFromMainNode($subcategories, 'service', $subcategory->id, 'sub_category_id');
                                            $service_names = Helpers::gettingArrayOfName($services);
                                            if (!in_array($data_node['name'], $service_names))
                                                $data_node['service_error'] = 'This service does not exist in this subcategory.';
                                        }
                                    }
                                }
                            }
                        } else {
                            foreach ($categories as $category_node) {
                                if ($category_node->name == $data_node['category']) {
                                    $services = Helpers::getAllNodesByIdFromMainNode($categories, 'service', $category_node->id, 'category_id');
                                    $service_names = Helpers::gettingArrayOfName($services);
                                    if (!in_array($data_node['name'], $service_names))
                                        $data_node['service_error'] = 'This service does not exist in this category.';
                                }
                            }
                        }
                    } else {
                        foreach ($specialties as $speciality) {
                            if ($data_node['speciality'] == $speciality->name) {
                                $services = Helpers::getAllNodesByIdFromMainNode($specialties, 'service', $speciality->id, 'directorate_speciality_id');
                                $service_names = Helpers::gettingArrayOfName($services);
                                if (!in_array($data_node['name'], $service_names))
                                    $data_node['service_error'] = 'This service does not exist in system.';
                            }
                        }
                    }
                }
            }
            if (!in_array($data_node['directorate'], $directorate_names))
                $data_node['directorate_error'] = "This directorate do not exist in the system or in this trust.";
            if (!in_array($data_node['speciality'], $speciality_names))
                $data_node['speciality_error'] = "This speciality do not exist in the system or in this directorate.";
            if ($data_node['category'] != 'N/A' && !in_array($data_node['category'], $category_names))
                $data_node['category_error'] = "This category do not exist in the system or in this directorate.";
            if ($data_node['subcategory'] != 'N/A' && !in_array($data_node['subcategory'], $subcategory_names))
                $data_node['subcategory_error'] = "This subcategory do not exist in the system or in this category.";
            if ($data_node['contactable'] != 'Yes' && $data_node['contactable'] != 'No')
                $data_node['contactable_error'] = "Invalid value for contactable.";
            if ($data_node['min_standards'] != 'Yes' && $data_node['min_standards'] != 'No')
                $data_node['standards_error'] = "Invalid value for min standards.";
            if (isset($data_node['template']['cycle_length']) && $data_node['template']['cycle_length'] <= 0)
                $data_node['cycle_length_error'] = "Invalid value for cycle length, Cycle length value should be greater then 0";
            if (isset($data_node['template']['starting_week']) && ($data_node['template']['starting_week'] <= 0 || $data_node['template']['starting_week'] > $data_node['template']['cycle_length']))
                $data_node['starting_week_error'] = "Invalid value for Starting week, Starting Week value should be greater then 0 and less or equal to cycle length";
            if (isset($data_node['template']['events']) && count($data_node['template']['events']) > 1) {
                $j = 1;
                foreach ($data_node['template']['events'] as $event) {
                    foreach ($sites as $site) {
                        if ($site->name == $event['site']) {
                            $location_names = Helpers::gettingArrayOfName($site->location);
                        }
                    }
                    if ($event['week_no'] <= 0 || $event['week_no'] > $data_node['template']['cycle_length'])
                        $data_node['events_error']['week_no'][] = $data_node['name'] . ', event ' . $j . ' week no has invalid value.';
                    if ($event['day'] < 0 || $event['day'] > 6)
                        $data_node['events_error']['day'][] = $data_node['name'] . ', event ' . $j . ' day has invalid value, day value be between 0 to 6.';
                    if (!is_numeric($event['duration']))
                        $data_node['events_error']['duration'][] = $data_node['name'] . ', event ' . $j . ' duration has invalid value, duration should be a numeric value.';
                    if (!in_array($event['site'], $site_names))
                        $data_node['events_error']['site'][] = $data_node['name'] . ', event ' . $j . ' "' . $event['site'] . '"  do not exist in the system or in this trust.';
                    if (isset($event['locations'])) {
                        $event_locations = explode(',', $event['locations']);
                        foreach ($event_locations as $location_node) {
                            if ($location_node != '') {
                                if (!in_array($location_node, $location_names))
                                    $data_node['events_error']['location'][] = $data_node['name'] . ', event ' . $j . ' "' . $location_node . '" do not exist in the system or in this site.';
                            }
                        }
                    }
                    $j++;
                }
            }
            $view_data [] = $data_node;
        }
        $file_name = Input::file('file')->getClientOriginalName();
        return view('service.viewUploadedData', compact('view_data', 'file_name'));
    }

    public function importReference(Request $request) {
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $view_data = Excel::selectSheetsByIndex(1)->load($request->file, function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $master_data = Excel::selectSheetsByIndex(2)->load($request->file, function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        list($view_data, $error) = $this->checkMaterReferencData($master_data, $view_data);
        $reference = 'reference';
        $file_name = Input::file('file')->getClientOriginalName();
        return view('service.viewUploadedData', compact('view_data', 'file_name', 'reference', 'error'));
    }

    private function checkMaterReferencData($master_data, $view_data, $error = false) {
        $master_data_directorate_name = $master_data_service_name = $master_data_speciality_name = $master_data_category_name = $master_data_subcat_name = [];
        foreach ($master_data as $service_name) {
            if (array_key_exists('name', $service_name)) {
                $master_data_service_name[] = $service_name['name'];
            }
            if (array_key_exists('directorate', $service_name)) {
                $master_data_directorate_name[] = $service_name['directorate'];
            }
            if (array_key_exists('speciality', $service_name)) {
                $master_data_speciality_name[] = $service_name['speciality'];
            }
            if (array_key_exists('category', $service_name)) {
                $master_data_category_name[] = $service_name['category'];
            }
            if (array_key_exists('subcategory', $service_name)) {
                $master_data_subcat_name[] = $service_name['subcategory'];
            }
        }
        foreach ($view_data as $key => $service_name) {
            if (!in_array($service_name['directorate'], $master_data_directorate_name)) {
                $view_data[$key]['name_error'] = "This directorate is miss match with master data ";
                $view_data['error'] = true;
            }
            if (!in_array($service_name['name'], $master_data_service_name)) {
                $view_data[$key]['name_error'] = "This name is miss match with master data ";
                $view_data['error'] = true;
            }
            if (!in_array($service_name['speciality'], $master_data_speciality_name)) {
                $view_data[$key]['speciality_error'] = "This speciality is miss match with master data ";
                $view_data['error'] = true;
            }
            if (!in_array($service_name['category'], $master_data_category_name)) {
                $view_data[$key]['category_error'] = "This category is miss match with master data ";
                $view_data['error'] = true;
            }
            if (!in_array($service_name['subcategory'], $master_data_subcat_name)) {
                $view_data[$key]['subcategory_error'] = "This subcategory is miss match with master data ";
                $view_data['error'] = true;
            }
        }
        if ((array_key_exists('error', $view_data))) {
            $error = true;
        }
        return [$view_data, $error];
    }

    public function referenceUpload(Request $request) {
        $data = Excel::selectSheetsByIndex(1)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $events_data = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $previous_service_name = null;
        $i = 0;
        foreach ($events_data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $formated_data[$i] = $this->getService($data_node);
                $formated_data [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEvent($data_node);
            if ($formated_data [$i]['template'] != NULL && $event != NULL) {
                $formated_data [$i]['template']['events'][] = $event;
            }
        }
        $request->merge(['trust_id' => auth()->user()->trust_id, 'data' => $data, 'service_data' => $formated_data]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('service') . 'upload-reference', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-service', $result->error);
            return redirect('/service');
        } else {
            Session::flash('success-service', $result->success);
            return redirect('/service');
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $data = $this->getFormatedData($records);
        $request->merge(['trust_id' => auth()->user()->trust_id, 'data' => $data]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('service') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-service', $result->error);
            return redirect('/service');
        } else {
            Session::flash('success-service', $result->success);
            return redirect('/service');
        }
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/service');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('deleted') . 'service/' . auth()->user()->trust_id);
        $master_data = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $data[] = ['directorate', 'speciality', 'category', 'subcategory', 'name', 'contactable', 'min_standards', 'start_date', 'end_date', 'cycle_length', 'starting_week', 'week_no', 'start_day', 'start_time', 'duration', 'end_time', 'end_day', 'site', 'locations'];
        $ref_data[] = ['id', 'directorate', 'speciality', 'category', 'subcategory', 'name', 'contactable', 'min_standards', 'operations'];
        $master_data_ref = [];
        foreach ($directorates as $directorate) {
            $site_name = null;
            if (isset($directorate->speciality)) {
                foreach ($directorate->speciality as $speciality) {
                    if (isset($speciality->service)) {
                        foreach ($speciality->service as $service) {
                            if (isset($service->template->events) && count($service->template->events)) {
                                foreach ($service->template->events as $event) {
                                    $start = new Carbon($event->start);
                                    $time = ($event->duration / 60) + $start->copy()->hour;
                                    $end = date('H:i', mktime(0, $time * 60));
                                    $end_day = ($time > 24 ? $event->day + 1 : $event->day);
                                    $location = '';
                                    $location_array = [];
                                    foreach ($sites as $site) {
                                        if ($event->site_id == $site->id) {
                                            $site_name = $site->name;
                                            if (isset($event->location_id)) {
                                                $location_array = Helpers::getDropDownData($site->location);
                                                foreach ($event->location_id as $location_id) {
                                                    $location = $location . $location_array[$location_id] . (($location_id != end($event->location_id)) ? ',' : '');
                                                }
                                            }
                                        }
                                    }
                                    array_push($data, [$directorate->name, $speciality->name, (($service->category_id == 0) ? 'N/A' : $service->category->name), (($service->sub_category_id == 0) ? 'N/A' : $service->sub_category->name), $service->name, ($service->contactable == 1 ? 'Yes' : 'No'), ($service->min_standards == 1 ? 'Yes' : 'No'), (isset($service->template->start_date) ? $service->template->start_date : 'N/A'), (isset($service->template->end_date) ? $service->template->end_date : 'N/A'), (isset($service->template->cycle_length) ? $service->template->cycle_length : 'N/A'), (isset($service->template->starting_week) ? $service->template->starting_week : 'N/A'), $event->week_no, $event->day, $event->start, $event->duration, $end, $end_day, $site_name, $location]);
                                }
                            } else {
                                array_push($data, [$directorate->name, $speciality->name, (($service->category_id == 0) ? 'N/A' : $service->category->name), (($service->sub_category_id == 0) ? 'N/A' : $service->sub_category->name), $service->name, ($service->contactable == 1 ? 'Yes' : 'No'), ($service->min_standards == 1 ? 'Yes' : 'No'), '', '', '', '', '', '', '', '', '', '', '', '']);
                            }
                        }
                    }
                }
            }
        }
        foreach ($master_data as $directorate) {
            foreach ($directorate->speciality as $speciality) {
                foreach ($speciality->service as $service) {
                    $ref_data[] = [$service->id, $directorate->name, $speciality->name, (isset($service->category->name) ? $service->category->name : 'N/A'), (isset($service->sub_category->name) ? $service->sub_category->name : 'N/A'), $service->name, ($service->contactable == 0 ? 'No' : 'Yes'), ($service->min_standards == 0 ? 'No' : 'Yes'), ($service->deleted_at == '' ? '' : 'Deleted')];
                }
            }
        }
        $master_data_ref = $this->getMasterData($master_data);
        ExcelHelper::exportExcel('Services_backup', $data, $ref_data, $master_data_ref);
        return redirect('/directorate-staff');
    }

    public function createServiceTemplate($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . $id);
        $service = json_decode($response->getBody()->getContents());
        if (!is_null($service->template) && count($service->template)) {
            return redirect('/service/template/edit/' . $id);
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $sites = Helpers::getDropDownData($trust->site);
        $events = [];
        return view('service.addServiceTemplate', compact('service', 'sites'));
    }

    public function storeServiceTemplate(Request $request) {
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('service/template'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return response()->json($result);
    }

    public function editServiceTemplate($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . $id);
        $service = json_decode($response->getBody()->getContents());
        if (is_null($service->template) || !count($service->template)) {
            return redirect('/service/template/add/' . $id);
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $sites = Helpers::getDropDownData($trust->site);

        $start_date = (isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? $service->template->start_date : date('Y-m-d');
        $end_date = (isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? $service->template->end_date : date('Y-m-d');
        $cycle_length = (isset($service->template->cycle_length) ) ? $service->template->cycle_length : 1;
        $starting_week = (isset($service->template->starting_week) ) ? $service->template->starting_week : 1;

        return view('service.editServiceTemplate', compact('sites', 'service', 'cycle_length', 'starting_week', 'start_date', 'end_date'));
    }

    public function updateServiceTemplate(Request $request) {
//        return response()->json($request->all());
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('service/template') . $request->service_id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return response()->json($result);
    }

    public function requirement($id) {
        $parameters = $operators = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . $id);
        $service = json_decode($response->getBody()->getContents());
        $directorate_id = $service->directorate_speciality->directorate_id;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'requirement/get-parameter/test/' . $directorate_id);
        $directorate = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('service') . 'get-operators-value');
        $operator = json_decode($response->getBody()->getContents());
        $operators = $this->getOperator($operator);
        $parameters = $this->getParametersArray($directorate->parameters);
        $values = Helpers::getAllNodesByIdFromMainNode($directorates, 'role', $directorate_id, 'directorate_id');
        $values = Helpers::getDropDownData($values);
        $status = ['present' => 'Present', 'mostly_present' => 'Mostly Present', 'contactable' => 'Contactable'];
        $red_requirements = $orange_requirements = $yellow_requirements = $green_requirements = $blue_requirements = [];
        if (isset($service->requirement) && count($service->requirement)) {
            $red_requirements = $this->getRequirementByColor($service->requirement, 'red', $directorate_id, $operator, $directorate->parameters);
            $orange_requirements = $this->getRequirementByColor($service->requirement, 'orange', $directorate_id, $operator, $directorate->parameters);
            $yellow_requirements = $this->getRequirementByColor($service->requirement, 'yellow', $directorate_id, $operator, $directorate->parameters);
            $green_requirements = $this->getRequirementByColor($service->requirement, 'green', $directorate_id, $operator, $directorate->parameters);
            $blue_requirements = $this->getRequirementByColor($service->requirement, 'blue', $directorate_id, $operator, $directorate->parameters);
        }
        return view('service.serviceRequirment', compact('parameters', 'values', 'status', 'id', 'service', 'red_requirements', 'orange_requirements', 'yellow_requirements', 'green_requirements', 'blue_requirements', 'directorate_id', 'operators'));
    }

    public function storeRequirement(Request $request) {
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('color'), '/'));
        $colors = json_decode($response->getBody()->getContents());
        $colors = $this->getColorsArray($colors);
        $requiremetns['red'] = $request->red;
        $requiremetns['orange'] = $request->orange;
        $requiremetns['yellow'] = $request->yellow;
        $requiremetns['green'] = $request->green;
        $requiremetns['blue'] = $request->blue;
        $store = [];
        foreach ($requiremetns as $key => $requirement) {
            for ($index = 0; $index < count($requirement['number']); $index++) {
                if ($requirement['parameter'][$index] != 'select')
                    $store[] = [
                        'service_id' => $request->service_id,
                        'color' => $key,
                        'parameter' => $requirement['parameter'][$index],
                        'operator' => $requirement['operator'][$index],
                        'value' => isset($requirement['value'][$index]) ? $requirement['value'][$index] : '',
                        'value_2' => (isset($requirement['value_2'][$index])) ? $requirement['value_2'][$index] : '',
                        'priority' => $colors[$key],
                        'status' => $requirement['status'][$index],
                        'number' => $requirement['number'][$index],
                        'id' => (isset($requirement['id'][$index]) ? $requirement['id'][$index] : 0)
                    ];
            }
        }
        //return [$store];
        /*echo '<pre>';
        print_r($store);
        print_r(rtrim(Helpers::getAPIUrl('service'), '/') . '-requirement');
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('service'), '/') . '-requirement', ['form_params' => $store]);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('success-requirement', $result->success);
        return Redirect::back();
    }

    public function destroyRequirement($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('service') . 'requirement/' . $id);
        $result = json_decode($response->getBody()->getContents());
        return [$result];
    }

    public function condition(Request $request) {
        $request->merge(["color" => $request->condition]);
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'get-requirement-data', ['form_params' => $request->all()]);
        $requirements = json_decode($response->getBody()->getContents());
        $role_array = $this->getGradeRoleArray($requirements->roles);
        $grade_array = $this->getGradeRoleArray($requirements->grades);
        $qualifications_array = $this->getGradeRoleArray($requirements->qualifications);
        $sites_array = $this->getGradeRoleArray($requirements->sites);
        $locations_array = $this->getGradeRoleArray($requirements->locations);
        list($requirements, $data) = $this->childRequirement($requirements, $role_array, $grade_array, $qualifications_array, $sites_array, $locations_array);
        return view('service.condition', ['red' => ucfirst($request->condition), 'requirements' => $requirements]);
    }

    public function conditionPost(Request $request) {
        $ids = explode(',', $request->ids);
        if ($request->ids == '') {
            Session::flash('error-condition', 'Condition Invalid');
            return redirect()->back();
        }
        $request->merge(['ids' => $ids]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('service') . 'conditions', ['form_params' => $request->all()]);
        $data = json_decode($response->getBody()->getContents());
        if (isset($data->error)) {
            Session::flash('error-service', $data->error);
            return redirect(url('service'));
        } else {
            Session::flash('success-service', $data->success);
            return redirect(url('service'));
        }
    }

    //####################################AJAX CALLS###################################

    public function getValues($value, $d_id) {
        $additionalParameters = $this->getValue($value, $d_id);
        return $additionalParameters;
    }

    private function operatorsWithType($type) {
        $data = [];
        foreach ($this->operators as $operator) {
            if ($type == $operator['type']) {
                $data[] = $operator;
            }
        }
        $data = Helpers::getDropDownData($data);
        return $data;
    }

    public function operatorTypes() {
        $data = [];
        foreach ($this->operators as $operator) {
            if ('integer' == $operator['type']) {
                $data[] = $operator;
            }
        }
        $data = Helpers::getDropDownData($data);
        return $data;
    }

    public function getOperatorbyParameter(Request $request) {
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'get-parameter/operator', ['form_params' => $request->all()]);
        $directorates = json_decode($response->getBody()->getContents());
        return response($directorates);
    }

    public function getServiceDropdown($d_id, $s_id, $c_id, $sb_id) {
        $services = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $d_id, 'directorate_id');
        $allservices = Helpers::getAllNodesByIdFromMainNode($specialities, 'service', $s_id, 'directorate_speciality_id');
        $services = Helpers::getDropDownData($allservices);
        if ($c_id != 'null' && $sb_id == 'null')
            $services = $this->filterService($c_id, null, $allservices);
        else if ($c_id != 'null' && $sb_id != 'null')
            $services = $this->filterService($c_id, $sb_id, $allservices);
        return response()->json($services);
    }

    public function getRequirementData(Request $request) {
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'get-requirement-data', ['form_params' => $request->all()]);
        $requirements = json_decode($response->getBody()->getContents());
        $role_array = $this->getGradeRoleArray($requirements->roles);
        $grade_array = $this->getGradeRoleArray($requirements->grades);
        $qualifications_array = $this->getGradeRoleArray($requirements->qualifications);
        $sites_array = $this->getGradeRoleArray($requirements->sites);
        $locations_array = $this->getGradeRoleArray($requirements->locations);
        list($requirements, $data) = $this->childRequirement($requirements, $role_array, $grade_array, $qualifications_array, $sites_array, $locations_array);
        if ($request->conditions) {
            $conditiondata = '';
            $condId = [];
            foreach ($data as $key => $requirement) {
                if (!empty($requirement->id)) {
//                    }
                    if ($requirement->condition_id == 0) {
                        $conditiondata .= '<tr><td>' . $requirement->value . ' , ' . $requirement->value_2 . '</td><td>' . $requirement->parameter . '</td><td>' . $requirement->condition . '</td><td><a  class= "delete-condtions"data="' . $requirement->id . '"> <i class="fa fa-trash"></i></a></td>';
                    } else {
                        $conditiondata .= '<td>' . $requirement->value . ' , ' . $requirement->value_2 . '</td><td>' . $requirement->parameter . '</td><td>' . $requirement->condition . '</td>';
                    }
                    $conditiondata .= '</tr>';
                }
            }
            return $conditiondata;
        }
        return $data;
    }

    private function childRequirement($requirements, $role_array, $grade_array, $qualifications_array, $sites_array, $locations_array) {
        $data = [];
        foreach ($requirements->requirement as $key => $requirement) {
            if (!empty($requirement->id)) {
                $data[$requirement->id] = $requirement;
            }
            if ($requirement->parameter == 'grade') {
                $requirement->value = array_search($requirement->value, $grade_array);
                if (!empty($requirement->value_2)) {
                    $requirement->value_2 = array_search($requirement->value_2, $grade_array);
                }
            }
            if ($requirement->parameter == 'role') {
                $requirement->value = array_search($requirement->value, $role_array);
                if (!empty($requirement->value_2)) {
                    $requirement->value_2 = array_search($requirement->value_2, $role_array);
                }
            }
            if ($requirement->parameter == 'qualification') {
                $requirement->value = array_search($requirement->value, $qualifications_array);
            }
            if ($requirement->parameter == 'site') {
                $requirement->value = array_search($requirement->value, $sites_array);
            }
            if ($requirement->parameter == 'location') {
                $requirement->value = array_search($requirement->value, $locations_array);
            }
//            foreach ($requirement->child_reqirement as $childKey => $requirementChild) {
//                if (!empty($requirementChild->id)) {
//                    $data[$requirementChild->id] = $requirementChild;
//                }
//                if ($requirementChild->parameter == 'grade') {
//                    $requirementChild->value = array_search($requirementChild->value, $grade_array);
//                    if (!empty($requirementChild->value_2)) {
//                        $requirementChild->value_2 = array_search($requirementChild->value_2, $grade_array);
//                    }
//                }
//                if ($requirementChild->parameter == 'role') {
//                    $requirementChild->value = array_search($requirementChild->value, $role_array);
//                    if (!empty($requirementChild->value_2)) {
//                        $requirementChild->value_2 = array_search($requirementChild->value_2, $role_array);
//                    }
//                }
//                if ($requirementChild->parameter == 'qualification') {
//                    $requirementChild->value = array_search($requirementChild->value, $qualifications_array);
//                }
//                if ($requirementChild->parameter == 'site') {
//                    $requirementChild->value = array_search($requirementChild->value, $sites_array);
//                }
//                if ($requirementChild->parameter == 'location') {
//                    $requirementChild->value = array_search($requirementChild->value, $locations_array);
//                }
//                
//            }
        }
        return [$requirements, $data];
    }

    protected function getGradeRoleArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[$node->name] = $node->id;
        }
        return $result;
    }

    public function saveRequirementCondition(Request $request) {
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'save-requirement-condition', ['form_params' => $request->all()]);
        $requirement = json_decode($response->getBody()->getContents());
        return [$requirement];
    }

    public function deleteRequirementCondition(Request $request) {
        $ids = explode(',', $request->ids);
        $request->merge(['ids' => $ids]);
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'delete-requirement-condition', ['form_params' => $request->all()]);
        $requirement = json_decode($response->getBody()->getContents());
        return [$requirement];
    }

    //############################PROTECTED FUNCCTION##############################

    protected function getService($record) {
        $data = [];
        if (isset($record['directorate']) && isset($record['speciality']) && isset($record['name'])) {
            $data['directorate'] = $record['directorate'];
            $data['speciality'] = $record['speciality'];
            $data['category'] = isset($record['category']) ? $record['category'] : "n/a";
            $data['subcategory'] = isset($record['subcategory']) ? $record['subcategory'] : "n/a";
            $data['name'] = $record['name'];
            $data['contactable'] = isset($record['contactable']) ? $record['contactable'] : "n/a";
            $data['min_standards'] = isset($record['min_standards']) ? $record['min_standards'] : "n/a";
        }
        return $data;
    }

    protected function getTemplate($record) {
        $data = NULL;
        if ((
                isset($record['start_date']) &&
                isset($record['end_date']) &&
                isset($record['cycle_length']) &&
                isset($record['starting_week'])
                )) {
            $start_date = new Carbon($record['start_date']);
            $end_date = ($record['end_date'] == '0000-00-00' ? '0000-00-00' : new Carbon($record['end_date']));
            $data['start_date'] = $start_date->format('Y-m-d');
            $data['end_date'] = (is_object($end_date) ? $end_date->format('Y-m-d') : $end_date);
            $data['cycle_length'] = $record['cycle_length'];
            $data['starting_week'] = $record['starting_week'];
        }
        return $data;
    }

    protected function getEvent($record) {
        $event = NULL;
        if (
                isset($record['week_no']) &&
                isset($record['start_day']) &&
                isset($record['start_time']) &&
                isset($record['duration']) &&
                isset($record['site'])
        ) {
            $start = new Carbon($record['start_time']);
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['start_day'];
            $event['start'] = $start->format('H:i');
            $event['duration'] = $record['duration'];
            $event['site'] = $record['site'];
            if (isset($record['locations']))
                $event['locations'] = $record['locations'];
        }
        return $event;
    }

    protected function getSpeciality($directorates) {
        $result [] = ['id' => -1, 'name' => 'Directorate Service'];
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        foreach ($specialities as $speciality) {
            array_push($result, $speciality);
        }
        $result = Helpers::getDropDownData($result);
        return $result;
    }

//    protected function getRequirementByColor($requirements, $color, $directorate_id, $operatorData, $directorateParameter) {
//        $operatorCheck = $operatorById = $operatorByName = [];
//        $result = $test = [];
//        foreach ($operatorData as $operator) {
//            $operatorById[$operator->id] = $operator->value;
//            $operatorByName[$operator->id] = $operator->name;
//        }
//        foreach ($requirements as $key => $requirement) {
//            if ($requirement->color == $color) {
//                $requirement->values = $this->getValues($requirement->parameter, $directorate_id);
//                if (!empty($directorateParameter)) {
//                    foreach ($directorateParameter as $operaterKey => $parameter) {
//                        $operatorArray = [];
//                        $operatorCheck[$operaterKey] = $parameter->name;
//                        if (in_array(ucfirst($requirement->parameter), $operatorCheck)) {
//                            if (ucfirst($requirement->parameter) === $parameter->name) {
//
//                                foreach ($parameter->operator as $operatorId) {
//                                    if ($operatorById[$operatorId]) {
//                                        $operatorArray[$operatorById[$operatorId]] = $operatorByName[$operatorId];
//                                        $requirements[$key]->operators = $operatorArray;
//                                    }
//                                }
//                            }
//                        } else {
//                            $requirements[$key]->operators = [];
//                        }
//                    }
//                } else {
//                    $requirements[$key]->operators = [];
//                }
//                $result[] = $requirement;
//            }
//        }
//        return $result;
//    }

    protected function filterService($cid, $sid = null, $services) {
        $result = [];
        foreach ($services as $service) {
            if ($sid != null) {
                if ($service->sub_category_id == $sid)
                    $result[$service->id] = $service->name;
            } else if ($service->category_id == $cid) {
                $result[$service->id] = $service->name;
            }
        }
        return $result;
    }

    protected function getColorsArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[strtolower($node->name)] = $node->position;
        }
        return $result;
    }

//    protected function getOperatorsArray($operators) {
//        $values = $names = [];
//        foreach ($operators as $operator) {
//            $names[$operator->name] = $operator->id;
//            $values[$operator->value] = $operator->id;
//        }
//        return [$names, $values];
//    }
//    protected function getParametersArray($parameters) {
//        $result = [];
//        foreach ($parameters as $parameter) {
//            $result['select'] = 'Select Parameter';
//            $result[lcfirst($parameter->name)] = $parameter->name;
//        }
//        return $result;
//    }

    protected function getFormatedData($data) {
        $result = [];
        $i = -1;
        $previous_service_name = NULL;
        foreach ($data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $result[$i] = $this->getService($data_node);
                $result [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEvent($data_node);
            if ($result [$i]['template'] != NULL && $event != NULL) {
                $result [$i]['template']['events'][] = $event;
            }
        }
        return $result;
    }

    protected function getMasterData($master_data) {
        $directorates = $specialities = $services = $count_array = $data = $operations = [];
        $categories = $subcategories = ['N/A'];
        $min_satandards = $contactable = ['yes', 'no'];
        $count = 1;
        foreach ($master_data as $node) {
            $directorates[] = $node->name;
            foreach ($node->speciality as $speciality) {
                $specialities[] = $speciality->name;
                foreach ($speciality->service as $service) {
                    $services[] = $service->name;
                }
            }
            foreach ($node->category as $category) {
                $categories[] = $category->name;
                foreach ($category->sub_category as $subcategory) {
                    $subcategories[] = $subcategory->name;
                }
            }
        }
        $count_array = [count($directorates), count($specialities), count($categories), count($subcategories), count($services), count($min_satandards), count($contactable)];
        $count = max($count_array);
        $data[] = ['directorate', 'speciality', 'category', 'subcategory', 'name', 'contactable', 'min_standards', 'operations'];
        $operations = ['create', 'update', 'delete', 'restore'];
        for ($i = 0; $i < $count; $i++) {
            $data[] = [
                (isset($directorates[$i]) ? $directorates[$i] : ''),
                (isset($specialities[$i]) ? $specialities[$i] : ''),
                (isset($categories[$i]) ? $categories[$i] : ''),
                (isset($subcategories[$i]) ? $subcategories[$i] : ''),
                (isset($services[$i]) ? $services[$i] : ''),
                (isset($min_satandards[$i]) ? $min_satandards[$i] : ''),
                (isset($contactable[$i]) ? $contactable[$i] : ''),
                (isset($operations[$i]) ? $operations[$i] : ''),
            ];
        }
        return $data;
    }

}
