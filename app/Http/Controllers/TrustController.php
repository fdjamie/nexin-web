<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Exception;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Auth;

class TrustController extends Controller {


    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

/*    public function index() {
        $trusts=array();
        $errors=array();
        $apiUrl=rtrim(Helpers::getAPIUrl('trust'),'/');
        $getTrusts=__get($apiUrl);
        if($getTrusts['status'])
        {
            $trusts=$getTrusts['apiResponseData'];
        }
        else
        {
            $errors=[(trans('messages.error'))];
        }
        return view('trust.viewAllTrusts', compact('trusts'))->withErrors($errors);
    }*/

    public function index() {

    $trusts=array();
    $errors=array();
        /*$apiUrl=Helpers::getAPIUrl('get-user-trusts-tree').auth()->user()->id;
    $getTrusts=__get($apiUrl);
      if($getTrusts['status'])
      {
          $trusts=$getTrusts['apiResponseData'];
      }
      else
      {
          $errors=[(trans('messages.error'))];
      }*/
        $trusts= session('userTrusts')['trusts'];

    return view('trust.viewAllTrusts', compact('trusts'))->withErrors($errors);
}



    public function create() {
        if (!auth()->user()->is_admin)
            abort(403);
        return view('trust.createTrust');
    }

    public function edit($id) {
        if (!auth()->user()->is_admin)
            abort(403);

        $trust=array();
        $errors=array();
        $apiUrl=Helpers::getAPIUrl('trust') . $id;
        $getTrust=__get($apiUrl);

        if($getTrust['status'])
        {
            $result=$getTrust['apiResponseData'];


            if(isset($result->error))
            {
                $errors=[(trans('messages.error'))];
                return Redirect::back()->with('error-trust',$result->error);
            }
            else
            {
                $trust=$result;
                return view('trust.editTrustAdmin', compact('trust'));
            }

        }
        else
        {

            return Redirect::back()->with('error-trust',(trans('messages.error')));
        }


    }

    public function store(Request $request) {
        if (!auth()->user()->is_admin)
            abort(403);
        $apiUrl=rtrim(Helpers::getAPIUrl('trust'), '/');
        $paramArr=$request->all();
        $storeTrust=__post($apiUrl,$paramArr);
        if($storeTrust['status'])
        {

            $result=$storeTrust['apiResponseData'];

            if (isset($result->error) && ($result->error)) {
                return Redirect::back()->with('error-trust',$result->error)->withInput();
            } else {
                return redirect('/trust/')->with('success-trust','Trust Added successfully.');
            }
        }
        else
        {
            return Redirect::back()->with('error-trust',[[(trans('messages.error'))]])->withInput();

        }

    }

    public function update(Request $request, $id) {
        if (!auth()->user()->is_admin)
            abort(403);

        $apiUrl=Helpers::getAPIUrl('trust') . $id;
        $paramArr=$request->all();
        $updateTrust=__put($apiUrl,$paramArr);
        if($updateTrust['status'])
        {
            $result=$updateTrust['apiResponseData'];

            if (isset($result->error) && !empty($result->error)) {
                return Redirect::back()->with('error-trust',$result->error)->withInput();
            } else {
                return redirect('/trust/')->with('success-trust','Trust Successfully Updated.');
            }
        }
        else
        {
            return Redirect::back()->with('error-trust',[[(trans('messages.error'))]])->withInput();

        }


    }

    public function destroy($id) {
        if (!auth()->user()->is_admin)
            abort(403);

        $apiUrl=Helpers::getAPIUrl('trust').$id;
        $deleteTrust=__delete($apiUrl);

        if($deleteTrust['status'])
        {
            $result=$deleteTrust['apiResponseData'];
            if (isset($result->error)) {
                return redirect('/trust/')->with('error-trust',$result->error);
            } else {
                return redirect('/trust/')->with('success-trust','Trust Successfully Deleted.');
            }
        }
        else
        {
            return redirect('/trust/')->with('error-trust',(trans('messages.error')));

        }

    }

    public function infoTrust($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . $id);
        $result = json_decode($response->getBody()->getContents());
        return view('trust.trustInfo');
    }

    public function export() {
        if (!auth()->user()->is_admin)
            abort(403);

        $trusts=array();
        $errors=array();
        $apiUrl=Helpers::getAPIUrl('trust');
        $getTrusts=__get($apiUrl);
        if($getTrusts['status'])
        {
            $trusts=$getTrusts['apiResponseData'];
            $data[] = ['name'];
            foreach ($trusts as $trust) {
                /* if (isset($trust) && count($trust))*/
                if (isset($trust) && !empty($trust))
                    array_push($data, ['name' => $trust->name]);
            }
            ExcelHelper::exportExcel('Trust_backup', $data);
            return redirect('/trust/')->with('success-trust','Trust data exported successfully.');
        }
        else
        {
            return redirect('/trust/')->with('error-trust',(trans('messages.error')));
        }



    }

    public function import(Request $request) {
        if (!auth()->user()->is_admin)
            abort(403);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust'));
            $trusts = json_decode($response->getBody()->getContents());
            $trust_names = Helpers::gettingArrayOfName($trusts);
            foreach ($data as $data_node) {
                if (!isset($data_node['name'])) {
                    $data_node['name'] = '';
                    $data_node['trust_name_error'] = 'Trust name is required';
                }
                if (in_array($data_node['name'], $trust_names))
                    $data_node['trust_error'] = 'This trust already exist in the system';
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('trust.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function upload(Request $request) {
        if (!auth()->user()->is_admin)
            abort(403);
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('trust') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-trust', $result->error);
            return redirect('/trust');
        } else {
            Session::flash('success-trust', $result->success);
            return redirect('/trust');
        }
    }

}
