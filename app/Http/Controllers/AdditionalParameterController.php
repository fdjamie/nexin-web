<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class AdditionalParameterController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {

        $this->module=config('module.additional-parameters');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        __authorize($this->module,'view',true);
        $responseData = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorates = json_decode($response->getBody()->getContents());
        if ($request->ajax()) {
            foreach ($directorates as $parameters) {
                if($request->directorate_id == $parameters->id) {
                    foreach ($parameters->additional_parameter as $key => $additional_parameter) {
                        $responseData[$additional_parameter->id] = $additional_parameter->parameter;
                    }
                }
            }
            return $responseData;
        }
        return view('additional-parameter.allParameter', compact('directorates'));
    }

    public function create() {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('additional-parameter.createParameter', compact('directorates'));
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('additional-parameter'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && !empty($result->error)) {
            Session::flash('error-parameter', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-parameter', 'Parameter added sucessfully.');
            return redirect('/additional-parameter');
        }
    }

    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('additional-parameter') . $id);
        $additional_parameter = json_decode($response->getBody()->getContents());
        $directorate_id = $additional_parameter->directorate_id;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('additional-parameter.editParameter', compact('directorates', 'directorate_id', 'additional_parameter'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        /*echo '<pre>';
        print_r($request->all());
        print_r(Helpers::getAPIUrl('additional-parameter') . $id);
        exit;*/
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('additional-parameter') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-parameter', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-parameter', $result->updated);
            return redirect('/additional-parameter');
        }
    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('additional-parameter') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-parameter', $result->error);
        return redirect('/additional-parameter');
    }

    public function exportAdditionalParameter() {
         $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'values', 'Directorate'];
        $ref_data[] = ['name', 'values', 'Directorate'];
        $ref_data[] = ['Dummy parameter name', 'Dummy Values', 'Dummy Directorate'];
        foreach ($directorates as $directorate) {
            if (isset($directorate) && count($directorate))
                foreach ($directorate->additional_parameter as $additionalParameters) {
                    array_push($data, [$additionalParameters->parameter, $additionalParameters->value, $directorate->name]);
                }
        }
        ExcelHelper::exportExcel('AdditionalParameters_backup', $data, $ref_data);
        Session::flash('sucess-parameter', 'Additional parameter data exported successfully');
        return redirect()->back();
    }

    public function importAdditionalparameter(Request $request) {
        __authorize($this->module,'add',true);
        $viewData = [];
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $importData = ExcelHelper::importExcel($request->file);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_name = Helpers::gettingArrayOfName($directorates);
        $parameter_name = Helpers::gettingArrayOfParameterName($directorates);
        foreach ($importData as $data) {
            if (empty($data['directorate'])) {
                $data['directorate'] = '';
                $data['directorate_required'] = 'directoreate  name requrired';
            }
            if (empty($data['name'])) {
                $data['name'] = '';
                $data['parameter_required'] = 'parameter name requrired';
            }
            if (empty($data['values'])) {
                $data['values'] = '';
                $data['values_required'] = 'values are requrired';
            }
            if (in_array($data['directorate'], $directorate_name) && in_array($data['name'], $parameter_name)) {
                $data['diractorate_err'] = ' directorate name and paremter already exits';
            } elseif (!in_array($data['directorate'], $directorate_name)) {
                $data['diractorate_not_exits'] = ' directorate name not exits in record';
            }
            $viewData [] = $data;
        }
        $fileName = Input::file('file')->getClientOriginalName();
        return view('additional-parameter.viewUploadedData', ['fileName' => $fileName, 'viewData' => $viewData]);
    }

    public function uploadAdditionalParameter(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->fileName), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('additional-parameter') . 'additionalparameter-upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
//        dd($result);
        Session::flash('success-parameter', $result->success);
        return redirect('/additional-parameter');
    }

}
