<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Helpers;
use Validator;
use Carbon\Carbon;
class SignupRequestController extends Controller
{

    private $trustId;

    public function __construct()
    {
         $this->trustId=session('activeTrust');


    }


    public function index(){


        $trusts=array();
        $apiUrl= $apiUrl=Helpers::getAPIUrl('getAllTrusts');
        /*$getTrusts=__get($apiUrl,'',false);*/
        $getTrusts=__getUnauthorize($apiUrl);
        if($getTrusts['status']) {
            $trusts = $getTrusts['apiResponseData'];
        }


            return view('signup',compact('trusts'));


    }

    public function storeUserRequest(Request $request)
    {

        $validator = $this->validateRequest($request->all());
        if (!$validator->fails()) {

            $paramArr[]=array();
            $paramArr['email']=$request->email;
            $paramArr['name']=$request->name;
            $paramArr['password']=$request->password;
            $paramArr['password_confirmation']=$request->password_confirmation;
            $paramArr['trust']=$request->trust;
            $apiUrl=Helpers::getAPIUrl('signupRequest');
            $storeRequest=__postUnauthorize($apiUrl,$paramArr);

            if($storeRequest['status'])
            {

                $result=$storeRequest['apiResponseData'];
                if($result->status)
                {


                    return redirect()->back()->with('success', ['Done']);
                }
                else
                {

                    return redirect()->back()->withInput()->withErrors($result->errors);
                }

            }
            else
            {
                return redirect()->back()->withInput()->withErrors(['errors'=>['error'=>['Unknown Error Occured! Try Again']]]);

            }


        }else {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

    }




    protected function validateRequest(array $data) {
        return Validator::make($data, [
            'trust' => 'required',
            'name' => 'required',
            'name' => 'required',
            'email' => 'required|email|max:255|unique:tb_user',
            'password' => 'required|min:6|confirmed',
        ]);
    }


    public function getTrustRequests(){



        if(auth()->user()->is_admin!=1)
        {
            abort(403);
        }

        $requests=array();
        $errors=array();
        $apiUrl=Helpers::getAPIUrl('get-all-requests') . $this->trustId;
        $roles=new HomeController;
        $roles=$roles->getTrustRoles();
        $getRequests=__get($apiUrl);

        if($getRequests['status'])
        {
            $result=$getRequests['apiResponseData'];

           if($result->status){

               $requests=$result->data;

           }
            else
            {
                $errors=['error'=>trans('messages.id')];
            }

        }


        return view('requests/list_all_requests',compact('requests','errors','roles'));




    }

    public function updateRequest(Request $request){


        $validator = $this->validateUpdateRequest($request->all());
        if (!$validator->fails()) {

            $paramArr[]=array();
            $paramArr['requestId']=$request->requestId;
            if($request->status=="verified")
            {
                $paramArr['roles']=$request->roles;
            }

            $paramArr['status']=$request->status;
            $paramArr['updatedBy']=auth()->user()->id;
            $paramArr['updatedAt']=Carbon::now()->toDateTimeString();
            $apiUrl=Helpers::getAPIUrl('updateRequestStatus');

            $updateRequest=__post($apiUrl,$paramArr);


            if($updateRequest['status'])
            {

                $result=$updateRequest['apiResponseData'];
                if($result->status)
                {


                    return redirect()->back()->with('success', "Status Updated Successfully");
                }
                else
                {

                  /*  return redirect()->back()->withInput()->withErrors($result->errors);*/
                    return redirect()->back()->with('error', "Validation Error:Something Is Missing In Request");
                }

            }
            else
            {
                return redirect()->back()->with('error', "Unknown Error Occured! Try Again");

            }


        }else {

            return redirect()->back()->with('error', "Validation Error:Something Is Missing In Request");
        }

    }



    protected function validateUpdateRequest(array $data) {
        if($data['status']=="verified")
        {
            return Validator::make($data, [
                'status'=>'required',
                'requestId'=>'required',
                'roles'=>'required',
            ]);
        }
        else
        {

            return Validator::make($data, [
                'status'=>'required',
                'requestId'=>'required',

            ]);
        }


    }



}
