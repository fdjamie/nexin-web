<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\CategoryTree;

class ShiftTypeController extends Controller {

    use CategoryTree;

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate = null;
        $rota_groups = [];
        if (count($directorates))
            $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        return view('shift-type.allShiftType', compact('directorates', 'directorate', 'rota_groups'));
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directoratesForTree = json_decode($response->getBody()->getContents());
        $category_id = $subcategory_id = $speciality_id = null;
        $categories = $sub_categories = $specialities = $rota_groups = $services = $combined_services = [];
        if (count($directorates)) {
            $directorate_id = $directorates[0]->id;
            $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
            if (count($specialities))
                $speciality_id = 0;
            $categories = Helpers::getAllNodesByIdFromMainNode($directoratesForTree, 'category', $directorate_id, 'directorate_id');
            if (count($categories)) {
                $category_id = $categories[0]->id;
                $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $category_id, 'category_id');
                if (count($subcategories)) {
                    $subcategory_id = $subcategories[0]->id;
                    foreach ($subcategories as $node) {
                        if (isset($node->specialities) && in_array($specialities[0]->id, $node->specialities)) {
                            $sub_categories[$node->id] = $node->name;
                        }
                    }
                }
            }
        }
        $combined_services = $this->getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_id, $category_id, $subcategory_id);
//        return [$combined_services];
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        $speciality_nodes[] = ['id' => 0, 'name' => 'Parent Speciality'];
        $speciality_nodes[] = ['id' => -1, 'name' => 'Genral Services'];
        foreach ($specialities as $speciality) {
            array_push($speciality_nodes, $speciality);
        }
        $tree = $this->getTree($categories);
        $specialities = Helpers::getDropDownData($speciality_nodes);
        $categories = Helpers::getDropDownData($categories);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        return view('shift-type.createShiftType', compact('specialities', 'services', 'directorates', 'rota_groups', 'categories', 'sub_categories', 'combined_services', 'tree'));
    }

    public function store(Request $request) {
        if (!isset($request->overrides_teaching)) {
            $request->merge(['overrides_teaching' => 0]);
        }
        if (!isset($request->on_site))
            $request->merge(['on_site' => 0]);
        if (isset($request->service_id) && $request->service_id != '0') {
            $service = explode('-', $request->service_id);
            $request->merge(['service_id' => $service[1], 'service_type' => $service[0]]);
        } else {
            $request->merge(['service_id' => 0, 'service_type' => 'none']);
        }
        /*echo '<pre>';
        print_r($request->all());
        print_r(Helpers::getAPIUrl('shift-type'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('shift-type'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());

        if (isset($result->error) && count($result->error)) {
            Session::flash('error-shifttype', $result->error);
            return Redirect::back()->withErrors($result->error)->withInput();
        } else {
            Session::flash('success-shifttype', $request->name . ', Shift Added sucessfully.');
            return redirect('/shift-type/' . $request->rota_template_id);
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('shift-type') . $id);
        $shifttype = json_decode($response->getBody()->getContents());
        if ($shifttype->service_type == 's') {
            $category_id = (isset($shifttype->service->category_id) ? $shifttype->service->category_id : null);
            $subcategory_id = (isset($shifttype->service->sub_category_id) ? $shifttype->service->sub_category_id : null);
        }
        if ($shifttype->service_type == 'd') {
            $category_id = (isset($shifttype->directorate_service->category_id) ? $shifttype->directorate_service->category_id : null);
            $subcategory_id = (isset($shifttype->directorate_service->sub_category_id) ? $shifttype->directorate_service->sub_category_id : null);
        }
        if ($shifttype->service_type == 'none') {
            $category_id = null;
            $subcategory_id = null;
        }
        $directorates = $specialities = [];
        $directorate_id = (isset($shifttype->directorate_id) ? $shifttype->directorate_id : null);
        $speciality_id = (isset($shifttype->directorate_speciality_id) ? $shifttype->directorate_speciality_id : null);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate_id, 'directorate_id');
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate_id, 'directorate_id');
        $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', ($category_id != null ? $category_id : $categories[0]->id), 'category_id');
        foreach ($subcategories as $node) {
            if (isset($node->specialities) && in_array($speciality_id, $node->specialities)) {
                $sub_categories[$node->id] = $node->name;
            }
        }
        $combined_services = $this->getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_id, $category_id, $subcategory_id);
        $directorates = Helpers::getDropDownData($directorates);
        $speciality_nodes[] = ['id' => 0, 'name' => 'Parent Speciality'];
        $speciality_nodes[] = ['id' => -1, 'name' => 'Directorate Services'];
        foreach ($specialities as $speciality) {
            array_push($speciality_nodes, $speciality);
        }
        $specialities = Helpers::getDropDownData($speciality_nodes);
        $categories = Helpers::getDropDownData($categories);
        $subcategories = Helpers::getDropDownData($subcategories);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        return view('shift-type.editShiftType', compact('directorates', 'specialities', 'combined_services', 'shifttype', 'rota_groups', 'categories', 'subcategories', 'category_id', 'subcategory_id'));
    }

    public function update(Request $request, $id) {
        if (!isset($request->on_site))
            $request->merge(['on_site' => 0, 'overrides_teaching' => (isset($request->overrides_teaching) ? $request->overrides_teaching : 0)]);
        if (isset($request->service_id)) {
            $service = explode('-', $request->service_id);
            $request->merge(['service_id' => $service[1], 'service_type' => $service[0]]);
        } else {
            $request->merge(['service_id' => 0, 'service_type' => 'none']);
        }
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('shift-type') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('success', $request->name . ', Shift Updated sucessfully.');
        return redirect('/shift-type/' . $request->rota_template_id);
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('shift-type') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error', 'Shift Deleted sucessfully.');
        return Redirect::back()->withErrors(['error' => $result]);
        return redirect('/shift-type');
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('shift-type') . auth()->user()->trust_id . '/export');
        $directorates = json_decode($response->getBody()->getContents());
        list($data, $ref_data, $master_data) = $this->getExcelData($directorates);
        ExcelHelper::exportExcel('Shifttype_backup', $data, $ref_data, $master_data);
        return redirect('/shift-type');
    }

    public function import(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $trust_id = ['trust_id' => auth()->user()->trust_id];
            $response = $this->api_client->request("POST", Helpers::getAPIUrl('shift-type') . 'import', ['form_params' => $trust_id]);
            $directorates = json_decode($response->getBody()->getContents());
            list($directorate_array, $speciality_array, $rotagroup_array, $shifttype_array, $directorate_service, $speciality_service, $rotatemplate_array) = $this->getRelationalData($directorates);
            $view_data = $this->validateUploadedData($data, $directorate_array, $speciality_array, $rotagroup_array, $shifttype_array, $directorate_service, $speciality_service, $rotatemplate_array);
            $file_name = Input::file('file')->getClientOriginalName();
            $upate_flag = (isset($request->update_flag) ? $request->update_flag : 0);
            return view('shift-type.viewUploadedData', compact('file_name', 'view_data', 'update_flag'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id, 'update_flag' => $request->update_flag]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('shift-type') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-shifttype', $result->error);
            return redirect('/shift-type');
        } else {
            Session::flash('success-shifttype', $result->success);
            return redirect('/shift-type');
        }
    }

    public function importReferenceData(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $reference_data = $this->getReferenceData(ExcelHelper::importExcelNew(Input::file('file'), 1));
            $master_data = $this->getMasterData(ExcelHelper::importExcelNew(Input::file('file'), 2));
            list($view_data, $master_error) = $this->validatedDataFromMasterData($reference_data, $master_data);
            $file_name = Input::file('file')->getClientOriginalName();
            $flag = true;
            return view('shift-type.viewUploadedData', compact('file_name', 'view_data', 'flag', 'master_error'));
        }
    }

    public function uploadReferenceData(Request $request) {
        $reference_data = Excel::selectSheetsByIndex(1)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $master_data = Excel::selectSheetsByIndex(2)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['master_data' => $master_data, 'reference_data' => $reference_data, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('shift-type') . 'reference-data/upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        $messages = [$result->error, $result->success];
        Session::flash('info-shift', $messages);
        return redirect('/shift-type');
    }

    //#################################### PROTECTED FUNCTION ##########################

    protected function getDirectorateServices($directorates, $directorate_id, $speciality_id, $category_id, $subcategory_id) {
        $services = [];

        foreach ($directorates as $directortae) {
            if ($directortae->id == $directorate_id) {
                foreach ($directortae->services as $service) {
                    $service_speciality = (isset($service->specialities) ? $service->specialities : []);
                    if (
                            (($speciality_id == -1 || $speciality_id == '-1') || in_array($speciality_id, $service_speciality)) &&
                            (
                            (($category_id == null || $category_id == 'null') && ($subcategory_id == null || $subcategory_id == 'null')) ||
                            (($service->category_id == $category_id) && ($subcategory_id == null || $subcategory_id == 'null')) ||
                            (($service->category_id == $category_id) && ($service->sub_category_id == $subcategory_id))
                            )
                    )
                        $services['d-' . $service->id] = $service->name;
                }
            }
        }
        return $services;
    }

    protected function getServices($specialities, $speciality_id, $category_id, $subcategory_id) {
        $result = [];
        foreach ($specialities as $speciality) {
            if ($speciality->id == $speciality_id && $speciality->id != 0) {
                foreach ($speciality->service as $service) {
                    if (
                            (($category_id == null || $category_id == 'null') && ($subcategory_id == null || $subcategory_id == 'null')) ||
                            (($service->category_id == $category_id ) && ($subcategory_id == null || $subcategory_id == 'null')) ||
                            (($service->category_id == $category_id ) && ($service->sub_category_id == $subcategory_id))
                    ) {
                        $result['s-' . $service->id] = $service->name;
                    }
                }
            }
        }
        return $result;
    }

    protected function getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_id, $category_id, $subcategory_id) {
        $combined_service = [];
        if ($speciality_id != -1 || $speciality_id != '-1') {
            $combined_service = array_merge($combined_service, $this->getServices($specialities, $speciality_id, $category_id, $subcategory_id));
        } else {
            $combined_service = array_merge($combined_service, $this->getDirectorateServices($directorates, $directorate_id, $speciality_id, $category_id, $subcategory_id));
        }
        return $combined_service;
    }

    protected function getExcelData($directorates) {
        $data[] = ['name', 'directorate', 'speciality', 'service', 'service_type', 'rota_group', 'start_time', 'finish_time', 'overrides_teaching', 'nroc'];
        $ref_data[] = ['id', 'name', 'directorate', 'speciality', 'service', 'service_type', 'rota_group', 'start_time', 'finish_time', 'overrides_teaching', 'nroc', 'operations'];
        $master_data[] = ['name', 'directorate', 'speciality', 'service', 'service_type', 'rota_group', 'overrides_teaching', 'nroc', 'operations'];
        list($shift_info, $ref, $master) = $this->shiftTypeInfo($directorates);
        $data = array_merge($data, $shift_info);
        $ref_data = array_merge($ref_data, $ref);
        $master_data = array_merge($master_data, $master);
        return [$data, $ref_data, $master_data];
    }

    protected function shiftTypeInfo($directorates) {
        $result = $ref_data = $directorates_array = $specialities = $shift_types = $rota_groups = $count = $master_data = $services = [];
        $nroc = $overrides = ['Yes', 'No'];
        $specialities = $services = ['N/A'];
        $operations = ['create', 'update', 'delete', 'restore'];
        $service_types = ['service', 'general service', 'none'];
        foreach ($directorates as $directorate) {
            $directorates_array[] = $directorate->name;
            foreach ($directorate->rota_group as $rota_group) {
                $rota_groups[] = $rota_group->name;
                foreach ($rota_group->shift_type as $shift_type) {
                    $shift_types[] = $shift_type->name;
                    $service_type = 'none';
                    $service_name = 'N/A';
                    if ($shift_type->service_type == 's') {
                        $service_type = 'service';
                        $service_name = (isset($shift_type->service->name) ? $shift_type->service->name : 'deleted');
                    } else if ($shift_type->service_type == 'd') {
                        $service_type = 'general service';
                        $service_name = (isset($shift_type->directorate_service->name) ? $shift_type->directorate_service->name : 'deleted');
                    }

                    $result [] = [$shift_type->name, $directorate->name, (isset($shift_type->directorate_speciality->name) ? $shift_type->directorate_speciality->name : 'N/A'), $service_name, $service_type, $rota_group->name, $shift_type->start_time, $shift_type->finish_time, ($shift_type->overrides_teaching == 1 ? 'Yes' : 'No'), ($shift_type->nroc == 1 ? 'Yes' : 'No')];
                    $ref_data [] = [$shift_type->id, $shift_type->name, $directorate->name, (isset($shift_type->directorate_speciality->name) ? $shift_type->directorate_speciality->name : 'N/A'), $service_name, $service_type, $rota_group->name, $shift_type->start_time, $shift_type->finish_time, ($shift_type->overrides_teaching == 1 ? 'Yes' : 'No'), ($shift_type->nroc == 1 ? 'Yes' : 'No'), (!is_null($shift_type->deleted_at) ? 'deleted' : '')];
                }
            }
            foreach ($directorate->speciality as $speciality) {
                $specialities[] = $speciality->name;
                foreach ($speciality->service as $service) {
                    $services[] = $service->name;
                }
            }
            foreach ($directorate->services as $service_node) {
                $services[] = $service_node->name;
            }
        }
        $count = [count($directorates_array), count($specialities), count($shift_types), count($rota_groups)];
        for ($i = 0; $i < max($count); $i++) {
            $master_data[] = [
                (isset($shift_types[$i]) ? $shift_types[$i] : ''),
                (isset($directorates_array[$i]) ? $directorates_array[$i] : ''),
                (isset($specialities[$i]) ? $specialities[$i] : ''),
                (isset($services[$i]) ? $services[$i] : ''),
                (isset($service_types[$i]) ? $service_types[$i] : ''),
                (isset($rota_groups[$i]) ? $rota_groups[$i] : ''),
                (isset($overrides[$i]) ? $overrides[$i] : ''),
                (isset($nroc[$i]) ? $nroc[$i] : ''),
                (isset($operations[$i]) ? $operations[$i] : ''),
            ];
        }
        return [$result, $ref_data, $master_data];
    }

    protected function getRelationalData($directorates) {
        $directorate_array = $speciality_array = $rotagroup_array = $shifttype_array = $directorate_service = $speciality_service = $rotatemplate_array = [];
        foreach ($directorates as $directorate) {
            $directorate_array[] = $directorate->name;
            if (isset($directorate->speciality) && count($directorate->speciality)) {
                foreach ($directorate->speciality as $speciality) {
                    $speciality_array[$directorate->name][$speciality->id] = $speciality->name;
                    if (isset($speciality->service) && count($speciality->service)) {
                        foreach ($speciality->service as $service) {
                            $speciality_service[$speciality->name][$service->id] = $service->name;
                        }
                    }
                }
            }
            if (isset($directorate->rota_group) && count($directorate->rota_group)) {
                foreach ($directorate->rota_group as $rotagroup) {
                    $rotagroup_array[$directorate->name][$rotagroup->id] = $rotagroup->name;
                    if (isset($rotagroup->rota_template) && count($rotagroup->rota_template)) {
                        foreach ($rotagroup->rota_template as $rotatemplate) {
                            $rotatemplate_array[$rotagroup->name][$rotatemplate->id] = $rotatemplate->name;
                        }
                    }
                }
            }
            if (isset($directorate->shift_type) && count($directorate->shift_type)) {
                foreach ($directorate->shift_type as $shifttype) {
                    $shifttype_array[$directorate->name][$shifttype->id] = $shifttype->name;
                }
            }
            if (isset($directorate->services) && count($directorate->services)) {
                foreach ($directorate->services as $service) {
                    $directorate_service[$directorate->name][$service->id] = $service->name;
                }
            }
        }
        return [$directorate_array, $speciality_array, $rotagroup_array, $shifttype_array, $directorate_service, $speciality_service, $rotatemplate_array];
    }

    protected function validateUploadedData($data, $directorate_array, $speciality_array, $rotagroup_array, $shifttype_array, $directorate_service, $speciality_service, $rotatemplate_array) {
        $view_data = [];
        foreach ($data as $data_node) {
            if (!in_array($data_node['directorate'], $directorate_array))
                $data_node['directorate_error'] = 'No such directorate exist in system.';
            if (isset($data_node['speciality']) && isset($speciality_array[$data_node['directorate']]) && !array_search($data_node['speciality'], $speciality_array[$data_node['directorate']]))
                $data_node['speciality_error'] = 'No such speciality exist in directorate.';
            if (isset($data_node['rota_group']) && isset($rotagroup_array[$data_node['directorate']]) && !array_search($data_node['rota_group'], $rotagroup_array[$data_node['directorate']]))
                $data_node['rotagroup_error'] = 'No such rota group exist in this directorate.';
            if (isset($data_node['rota_group']) && isset($data_node['rota_template']) && isset($rotatemplate_array[$data_node['rota_group']]) && !array_search($data_node['rota_template'], $rotatemplate_array[$data_node['rota_group']]))
                $data_node['rotatemplate_error'] = 'No such rota template exist in this rota group.';
            if (isset($shifttype_array[$data_node['directorate']]) && array_search($data_node['name'], $shifttype_array[$data_node['directorate']]))
                $data_node['shifttype_error'] = 'This shift type already exist in this directorate.';
            if ((strcasecmp($data_node['overrides_teaching'], 'Yes') != 0) && (strcasecmp($data_node['overrides_teaching'], 'No') != 0))
                $data_node['teaching_error'] = 'Invalid value for overrides teaching.';
            if ((strcasecmp($data_node['nroc'], 'Yes') != 0) && (strcasecmp($data_node['nroc'], 'NO') != 0))
                $data_node['nroc_error'] = 'Invalid value for nroc.';
            $view_data[] = $data_node;
        }
        return $view_data;
    }

    protected function getReferenceData($refernce_data) {
        $data = [];
        foreach ($refernce_data as $data_node) {
            if ((isset($data_node['operations']) &&
                    (
                    $data_node['operations'] == 'create' ||
                    $data_node['operations'] == 'update' ||
                    $data_node['operations'] == 'delete' ||
                    $data_node['operations'] == 'restore'))) {
                $data[] = $data_node;
            }
        }
        return $data;
    }

    protected function getMasterData($master_data) {
        $data = [];
        foreach ($master_data as $data_node) {
            if (isset($data_node['directorate']))
                $data['directorates'][] = strtolower($data_node['directorate']);
            if (isset($data_node['name']))
                $data['shift_types'][] = strtolower($data_node['name']);
            if (isset($data_node['speciality']))
                $data['specialities'][] = strtolower($data_node['speciality']);
            if (isset($data_node['service']))
                $data['services'][] = (isset($data_node['service']) ? strtolower($data_node['service']) : null);
            if (isset($data_node['service_type']))
                $data['service_types'][] = strtolower($data_node['service_type']);
            if (isset($data_node['rota_group']))
                $data['rota_groups'][] = strtolower($data_node['rota_group']);
            if (isset($data_node['overrides_teaching']))
                $data['overrides_teaching'][] = strtolower($data_node['overrides_teaching']);
            if (isset($data_node['nroc']))
                $data['nroc'][] = strtolower($data_node['nroc']);
            if (isset($data_node['operations']))
                $data['operations'][] = strtolower($data_node['operations']);
        }
        return $data;
    }

    protected function validatedDataFromMasterData($reference_data, $master_check) {
        $data = [];
        $master_error = false;
        foreach ($reference_data as $data_node) {
            if (isset($data_node['directorate']) && !in_array(strtolower($data_node['directorate']), $master_check['directorates'])) {
                $data_node['directorate_error'] = 'This directorate does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['name']) && !in_array(strtolower($data_node['name']), $master_check['shift_types'])) {
                $data_node['shifttype_error'] = 'This Shift type does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['speciality']) && !in_array(strtolower($data_node['speciality']), $master_check['specialities'])) {
                $data_node['speciality_error'] = 'This speciality does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['service']) && !in_array(strtolower($data_node['service']), $master_check['services'])) {
                $data_node['service_error'] = 'This Service does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['service_type']) && !in_array(strtolower($data_node['service_type']), $master_check['service_types'])) {
                $data_node['servicetype_error'] = 'This Service Type does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['rota_group']) && !in_array(strtolower($data_node['rota_group']), $master_check['rota_groups'])) {
                $data_node['rotagroup_error'] = 'This Rota Group does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['overrides_teaching']) && !in_array(strtolower($data_node['overrides_teaching']), $master_check['overrides_teaching'])) {
                $data_node['teaching_error'] = 'Overrides Teaching value does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['nroc']) && !in_array(strtolower($data_node['nroc']), $master_check['nroc'])) {
                $data_node['nroc_error'] = 'nroc value does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['operations']) && !in_array(strtolower($data_node['operations']), $master_check['operations'])) {
                $data_node['operations_error'] = 'Operations value does not exit in Master Data';
                $master_error = true;
            }
            $data[] = $data_node;
        }
        return [$data, $master_error];
    }

}
