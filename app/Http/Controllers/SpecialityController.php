<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class SpecialityController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.specialities');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
        $directorates = json_decode($response->getBody()->getContents());
        return view('speciality.allSpecialities', compact('directorates'));
    }

    public function create() {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('speciality.addSpeciality', compact('directorates'));
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('directoratespeciality'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-speciality', $result->error);
            return Redirect::back();
        }
        Session::flash('success-speciality', $request->name . ', speciality added successfully.');
        return redirect('/specialities');
    }

    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directoratespeciality') . $id);
        $speciality = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('speciality.editSpeciality', compact('speciality', 'directorates'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('directoratespeciality') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        If (isset($result->error) && count($result->error)) {
            Session::flash('error-speciality', $result->error);
            return redirect('/specialities');
        } else {
            Session::flash('success-speciality', $result->success);
            return redirect('/specialities');
        }
    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('directoratespeciality') . $id);
        $results = json_decode($response->getBody()->getContents());
        Session::flash('error-speciality', $results->error);
        return redirect('/specialities');
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
        $directorates = json_decode($response->getBody()->getContents());
        $specialties = Helpers::getAllNodesFromMainNode($directorates, 'speciality');
        $data[] = ['directorate', 'name', 'abbreviation'];
        $ref_data[] = ['directorate', 'name', 'abbreviation'];
        $ref_data[] = ['Dummy Directorate', 'Dummy Speciality', 'DS'];
        foreach ($specialties as $speciality) {
            if (isset($speciality) && count($speciality))
                array_push($data, [$speciality->directorate->name, $speciality->name, $speciality->abbreviation]);
        }
        ExcelHelper::exportExcel('Directorate_Speciality_backup', $data, $ref_data);
        Session::flash('success-speciality', 'Speciality data exported successfully.');
        return redirect('/specialities');
    }

    public function import(Request $request) {
        __authorize($this->module,'add',true);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $records = ExcelHelper::importExcel(Input::file('file'));
            $data = $records;
            $directorate_nodes = $speciality_nodes = $view_data = [];
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
            $directorates = json_decode($response->getBody()->getContents());
            $directorates_nodes = Helpers::gettingArrayOfName($directorates);
            $specialties = Helpers::getAllNodesFromMainNode($directorates, 'speciality');
            foreach ($specialties as $speciality) {
                $speciality_nodes[] = ['name' => $speciality->name, 'directorate' => $speciality->directorate->name];
            }
            $specialties = Helpers::gettingArrayOfName($specialties);
            foreach ($data as $data_node) {
                $check = false;
                if(!isset($data_node['directorate']) ) {
                    $data_node['directorate'] = null;
                }
                (in_array($data_node['directorate'], $directorates_nodes) ? $data_node['directorate_error'] = null : $data_node['directorate_error'] = 'This directorate does not exist in system.');
                foreach ($speciality_nodes as $speciality_node) {
                    if (isset($data_node['directorate']) && $data_node['directorate'] == $speciality_node['directorate'] && $data_node['name'] == $speciality_node['name'])
                        $check = true;
                }
                ($check ? $data_node['speciality_error'] = 'This Speciality already exist for this directorate' : $data_node['speciality_error'] = null);

                $view_data[] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('speciality.viewUploadedData', compact('view_data', 'file_name')); //[$view_data];
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    //$reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records,'trust_id'=>auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('directoratespeciality') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-speciality', $result->error);
            return redirect('/specialities');
        } else {
            Session::flash('success-speciality', $result->success);
            return redirect('/specialities');
        }
    }

}
