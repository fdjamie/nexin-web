<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class LocationController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $result = json_decode($response->getBody()->getContents());
        $sites = $result->site;
        return view('location.allLocations', compact('sites'));
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $result = json_decode($response->getBody()->getContents());
        $sites = $result->site;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $groups = $sub_groups = [];
        if (count($directorates)) {
            $groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'group', $directorates[0]->id, 'directorate_id');
            if (count($groups))
                $sub_groups = Helpers::getAllNodesByIdFromMainNode($groups, 'subgroup', $groups[0]->id, 'group_id');
            else
                $sub_groups = [];
        }
        $sites = Helpers::getDropDownData($sites);
        $directorates = Helpers::getDropDownData($directorates);
        $groups = Helpers::getDropDownData($groups);
        $sub_groups = Helpers::getDropDownData($sub_groups);
        return view('location.createLocation', compact('sites', 'directorates', 'groups', 'sub_groups'));
    }

    public function store(Request $request) {
        if (!isset($request->group_id))
            $request->merge(['group_id' => 0]);
        if (!isset($request->subgroup_id))
            $request->merge(['subgroup_id' => 0]);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('location'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-location', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-location', $request->name . ', location added successfully');
            return redirect('/locations');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('location') . $id);
        $location = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $result = json_decode($response->getBody()->getContents());
        $sites = $result->site;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = ($location->group != NULL) ? $location->group->directorate_id : 0;
        $groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'group', $directorate_id, 'directorate_id');
        if (count($groups))
            $sub_groups = Helpers::getAllNodesByIdFromMainNode($groups, 'subgroup', $location->group_id, 'group_id');
        else
            $sub_groups = [];
        $sites = Helpers::getDropDownData($sites);
        $directorates = Helpers::getDropDownData($directorates);
        $groups = Helpers::getDropDownData($groups);
        $sub_groups = Helpers::getDropDownData($sub_groups);
        return view('location.editLocation', compact('location', 'sites', 'directorates', 'groups', 'sub_groups'));
    }

    public function update(Request $request, $id) {
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('location') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (!isset($result->updated)) {
            Session::flash('error-location', $result);
            return redirect()->back();
        } else {
            Session::flash('success-location', $result->updated);
            return redirect('/locations');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('location') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-location', $result->error);
        return redirect('/locations');
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'service', 'limit', 'site'];
        $ref_data[] = ['name', 'service', 'limit', 'site'];
        $ref_data[] = ['Dummy Location', 'Yes/No', 'integer', 'Dummy Site'];
        foreach ($sites as $site) {
            if (isset($site->location) && count($site->location))
                foreach ($site->location as $location) {
                    array_push($data, [$location->name, (($location->service == 1) ? 'Yes' : 'No'), $location->limit, $site->name]);
                }
        }
        ExcelHelper::exportExcel('Location_backup', $data, $ref_data);
        Session::flash('success-location', 'Locations data exported successfully.');
        return redirect('/locations');
    }

    public function import(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
            $sites = json_decode($response->getBody()->getContents());
            $site_names = Helpers::gettingArrayOfName($sites);
            foreach ($data as $data_node) {
                if (!isset($data_node['name'])) {
                    $data_node['name'] = '';
                    $data_node['location_name_error'] = 'Loccation name is required';
                }
                if (!isset($data_node['service'])) {
                    $data_node['service'] = 'No';
                }
                if ($data_node['service'] != 'Yes' && $data_node['service'] != 'No') {
                    $data_node['service_error'] = 'Invalid service value, service vlaue should be either Yes or No';
                }
                if (!in_array($data_node['site'], $site_names))
                    $data_node['site_error'] = 'This site does not exist in the system or in this trust';
                foreach ($sites as $site) {
                    if ($data_node['site'] == $site->name)
                        $locations = Helpers::gettingArrayOfName($site->location);
                }
                if (in_array($data_node['name'], $locations))
                    $data_node['location_error'] = 'This location already exist for this site.';
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('location.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records]);
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('location') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-location', $result->error);
            return redirect('/locations');
        } else {
            Session::flash('success-location', $result->success);
            return redirect('/locations');
        }
    }

}
