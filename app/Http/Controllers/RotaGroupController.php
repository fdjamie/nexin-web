<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class RotaGroupController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.rota-groups');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {

        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate = null;
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        return view('rota-group.allRotaGroup', compact('rota_groups', 'directorates', 'directorate'));
    }

    public function create() {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('rota-group.createRotaGroup', compact('directorates'));
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        /*echo '<pre>';
        print_r($request->all());
        print_r(rtrim(Helpers::getAPIUrl('rota-group'), '/'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('rota-group'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-rota-group', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-rota-group', 'Rota Group Added successfully.');
            return redirect('/rota-group/');
        }
    }

    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id.'/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('rota-group') . $id);
        $rota_group = json_decode($response->getBody()->getContents());
        return view('rota-group.editRotaGroup', compact('rota_group', 'directorates'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        /*echo '<pre>';
        echo Helpers::getAPIUrl('rota-group')."\n";
        print_r($request->all());
        exit;*/
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('rota-group') . $id, ['form_params' => $request->all()]);
        // dd((string)$response->getBody());
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-rota-group', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-rota-group', 'Rota Group Updated successfully.');
            return redirect('/rota-group/');
        }
    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('rota-group') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-rota-group', $result);
        return Redirect::back();
    }

}
