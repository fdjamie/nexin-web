<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class RotationController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        $directorates = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/post');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        else
            $directorate = null;
        $date_range = (isset($request->date_range) ? $request->date_range : date('Y-m-d'));
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id');
        $date_range = new Carbon($date_range);
        $directorates = Helpers::getDropDownData($directorates);
        return view('rotation.allRotation', compact('specialities', 'directorates', 'directorate', 'date_range'));
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate = null;
        if (count($directorates))
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-staff') . 'directorate/' . $directorate);
        $directorate_staff = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id');
        $posts = (($specialities) ? Helpers::getAllNodesByIdFromMainNode($specialities, 'post', $specialities[0]->id, 'directorate_speciality_id') : []);
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate, 'directorate_id');
        $rota_group_id = (isset($posts[0]->rota_template->rota_group_id) ? $posts[0]->rota_template->rota_group_id : 0);
        $rota_templates = Helpers::getDropDownData(Helpers::getAllNodesByIdFromMainNode($rota_groups, 'rota_template', $rota_group_id, 'rota_group_id'));
        $position_max = (isset($posts[0]->rota_template->content->shift_type) ? count($posts[0]->rota_template->content->shift_type) : 1);
        $selected_template = (isset($posts[0]->rota_template_id) ? $posts[0]->rota_template_id : NULL);
        $current_position = (isset($posts[0]->assigned_week) ? $posts[0]->assigned_week : 1);
        $posts = Helpers::getDropDownData($posts);
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        $speciality = null;
        return view('rotation.createRotation', compact('posts', 'rota_templates', 'selected_template', 'directorates', 'directorate', 'specialities', 'speciality', 'current_position', 'position_max', 'directorate_staff', 'rota_group_id', 'rota_groups'));
    }

    public function store(Request $request) {
        $request->merge(['staff_member' => (isset($request->staff_member) ? $request->staff_member : 0)]);
        $request->merge(['template_position' => (isset($request->assigned_week) ? $request->assigned_week : null)]);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('rotation-detail'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-rotation', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-rotation', ' Rotation added successfuly.');
            return redirect('/rotations');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('rotation-detail') . $id);
        $rotation = json_decode($response->getBody()->getContents());
        $directorate = (isset($request->directorate) ? $request->directorate : $rotation->post->directorate_speciality->directorate_id);
        $speciality = $rotation->post->directorate_speciality_id;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-staff') . 'directorate/' . $directorate);
        $directorate_staff = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id');
        $posts = Helpers::getAllNodesByIdFromMainNode($specialities, 'post', $specialities[0]->id, 'directorate_speciality_id');
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate, 'directorate_id');
        $rota_group_id = (isset($rotation->post->rota_template->rota_group_id) ? $rotation->post->rota_template->rota_group_id : 0);
        $rota_templates = Helpers::getDropDownData(Helpers::getAllNodesByIdFromMainNode($rota_groups, 'rota_template', $rota_group_id, 'rota_group_id'));
        $position_max = (isset($rotation->post->rota_template->content->shift_type) ? count($rotation->post->rota_template->content->shift_type) : 1);
        $end_date = $rotation->end_date;
        $posts = Helpers::getDropDownData($posts);
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        if (isset($rotation->staff->id))
            $staff[$rotation->staff->id] = $rotation->staff->gmc . " [" . $rotation->staff->name . "]";
        else
            $staff = [];
        return view('rotation.editRotation', compact('posts', 'rota_templates', 'rotation', 'directorates', 'directorate', 'specialities', 'speciality', 'position_max', 'directorate_staff', 'id', 'staff', 'rota_groups', 'rota_group_id'));
    }

    public function update(Request $request, $id) {
        if (!isset($request->permanent))
            $request->merge(['permanent' => 0]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('rotation-detail') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-rotation', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-rotation', ' Rotation added successfuly.');
            return redirect('/rotations');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('rotation-detail') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-rotation', $result->error);
        return redirect('/rotations');
    }

}
