<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use App\Http\Helpers;
use Auth;
use Hash;
use App\LoginSecurity;
use Carbon\Carbon;
use Mail;
use App\Helpers\LoginAuthenticationHelper;
class EmailLoginSecurityController extends Controller
{

    private $trustId;
    private $maxNoEmailSent=10;
    private $codeExpireAfterSeconds=180;


    public function __construct()
    {
        $this->middleware('auth');
        $this->trustId=session('activeTrust');
    }

    public function enableEmailCodeVerification(Request $request)
    {

        $user = Auth::user();

        if (!empty($user->emailSecurity)) {
            $user->emailSecurity->enable = 1;
            $enable = $user->emailSecurity->save();
        } else {
            $enable = LoginSecurity::create([
                'user_id' => $user->id,
                'enable' => 1,
                'type' => 'email'
            ]);
        }
        if ($enable) {
            $session = array();
            $session['verified'] = true;
            $session['time'] = Carbon::now();
            session(['emailCodeVerification' => $session]);
            return redirect('account-security')->with('success', trans('messages.enabled'));
        } else {
            return redirect('account-security')->with('error', trans('messages.error'));
        }
    }


    public function disableEmailCodeVerification(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->with("error", trans('messages.passwordNotMatch'));
        }

        $user = Auth::user();
        $user->emailSecurity->enable = 0;
        $disabled = $user->emailSecurity->save();

        if ($disabled) {
            return redirect('account-security')->with('success', trans('messages.disabled'));
        } else {
            return redirect('account-security')->with('error', trans('messages.error'));
        }


    }

    public function emailCodeVerifcation()
    {
        $user = Auth::user();
        $userEmailVerification = $user->emailSecurity;
        if (empty($userEmailVerification) || !$userEmailVerification->enable) {
            return redirect()->back();
        } else {
            if (!session('emailCodeVerification')['codeSent']) {
                $this->sendCode();
            }
            return view('email-code-verification.code-verification');

        }

    }

    public function sendCode(Request $request = null)
    {


        $user = Auth::user();
          if($user->emailSecurity->code_sent== $this->maxNoEmailSent){

            return redirect('email-code-verification')->with('error',trans('messages.codeLimitExceeded'));
        }

        $code = mt_rand(1000, 9999);
        if (!$this->sendEmail($user, $code)) {
            return redirect('email-code-verification');
        }

        $user->emailSecurity->secret = $code;
        $user->emailSecurity->code_sent = $user->emailSecurity->code_sent+1;
        $user->emailSecurity->save();
        $session = array();
        $session['verified'] = false;
        $session['time'] = Carbon::now();
        $session['codeSent'] = true;
        session(['emailCodeVerification' => $session]);
        if ($request) {
            return redirect('email-code-verification')->with('success',trans('messages.codeSent'));
        }
        return;
    }

    private function sendEmail($user, $code)
    {
        /*$data = array('name' => $user->name, 'code' => $code);
        Mail::send('email-code-verification.verification-code-mail', $data, function ($message) use ($user) {
            $message->to($user->email, 'Nexin Login ')->subject
            ('Nexin Login Verification');
            $message->from('admin@nexin.com', 'Nexin');
        });
        return true;*/

        $apiUrl=Helpers::getAPIUrl('sendEmail');
        $params=array();
        $params['userId']=$user->id;
        $params['trustId']=$this->trustId;
        $params['code']=$code;
        $params['type']='verification-code';
        $sendVerification=__post($apiUrl,$params);
        return true;




    }

    public function verifyCode(Request $request)
    {

        if (!empty(session('emailCodeVerification')) && session('emailCodeVerification')['verified']) {
            return redirect()->back();
        }
        $this->validate($request, [
            'code' => 'required',

        ]);

        $code = implode('', $request->code);
        $user = Auth::user();
        if ($user->emailSecurity->secret == $code) {
            if (!$this->checkCodeExpiry($user)) {
                return redirect('email-code-verification')->with('error', trans('messages.codeExpired'));
            }
            $session = array();
            $session['verified'] = true;
            session(['emailCodeVerification' => $session]);
            $user->emailSecurity->secret = "";
            $user->emailSecurity->code_sent=0;
            $user->emailSecurity->save();
            LoginAuthenticationHelper::updateLoginVerificationInLoginDetailTbl('email');
            return redirect('/');
        } else {

            return redirect('email-code-verification')->with('error',trans('messages.codeIncorrect'))->withInput();

        }


    }





    private function checkCodeExpiry($user)
    {

        $codeSentDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $user->emailSecurity->updated_at);
        $currentDateTime = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        $diff = $codeSentDateTime->diffInSeconds($currentDateTime);
        if ($diff > $this->codeExpireAfterSeconds) {

            return false;

        } else {

            return true;

        }


    }


}
