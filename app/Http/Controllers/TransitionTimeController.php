<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class TransitionTimeController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.transition-times');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('transition-time') . 'directorate/' . auth()->user()->trust_id);
        $directorates = json_decode($response->getBody()->getContents());
        return view('transition-time.allTransitionTime', compact('directorates'));
    }

    public function create() {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('transition-time.createTransitionTime', compact('directorates'));
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('transition-time'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-time', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-time', 'Transition Time Added successfully.');
            return redirect('/transition-times/');
        }
    }

    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('transition-time') . $id);
        $time = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('transition-time.editTransitionTime', compact('time', 'directorates'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('transition-time') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-time', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-time', 'Transition time updated successfully.');
            return redirect('/transition-times/');
        }
    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('transition-time') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-time', $result->error);
        return redirect('/transition-times');
    }

}
