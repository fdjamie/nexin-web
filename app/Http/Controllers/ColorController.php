<?php

namespace App\Http\Controllers;

use File;
use Validator;
use App\Http\Helpers;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;

class ColorController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('color'));
        $colors = json_decode($response->getBody()->getContents());
        return view('color.allColor', compact('colors'));
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('color'));
        $colors = json_decode($response->getBody()->getContents());
        $color_array = $this->colorsDropdown($colors);
        return view('color.createColor', compact('color_array'));
    }

    public function store(Request $request) {
        if (!isset($request->position))
            $request->merge(['position' => 1]);
        else
            $request->merge(['position' => $request->position + $request->place]);
        /*echo '<pre>';
        print_r($request->all());
        print_r(rtrim(Helpers::getAPIUrl('color'), '/'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('color'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-color', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-color', $result->success);
            return redirect('/colors');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('color') . $id);
        $color = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('color'));
        $colors = json_decode($response->getBody()->getContents());
        $color_array = $this->colorsDropdown($colors);
        return view('color.editColor', compact('color', 'color_array'));
    }

    public function update(Request $request, $id) {
        $request->merge(['priority' => $request->position + $request->place]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('color') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-color', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-color', $result->success);
            return redirect('/colors');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('color') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-color', $result->error);
        return redirect('/colors');
    }

//############################# AJAX CALLS ########################
    public function getColorsCode() {
        $result = [];
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('color'),'/'));
        $colors = json_decode($response->getBody()->getContents());
        foreach($colors as $color){
           $result[$color->position] = $color->code;
        }
        return $result;
    }

//############################# PROTECTED FUNCTION ###################

    protected function colorsDropdown($colors) {
        $color_array = [];
        foreach ($colors as $color) {
            $color_array[$color->position] = $color->name;
        }
        return $color_array;
    }

}
