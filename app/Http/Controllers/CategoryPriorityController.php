<?php

namespace App\Http\Controllers;

use File;
use Validator;
use App\Http\Helpers;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use MaddHatter\LaravelFullcalendar\Event;

class CategoryPriorityController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('color'),'/'));
        $color_array = json_decode($response->getBody()->getContents());
        $royg = $colors = [];
        if (isset($color_array)) {
            foreach ($color_array as $color) {
                $royg [$color->position] = $color->letter;
                $colors[$color->position] = $color->code;
            }
        }

        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/category-priority');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);

        return view('category-priority.allPriority', compact('directorates', 'specialities', 'directorate_id', 'royg', 'colors', 'color_array'));
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/category-priority');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorates[0]->id, 'directorate_id');
//        return [$categories];
        $tree = app('App\Http\Controllers\ServiceController')->getTree($categories);
        $category_priority = Helpers::getAllNodesByIdFromMainNode($specialities, 'category_priority', $specialities[0]->id, 'directorate_speciality_id');
        $category_array = [];
        // foreach ($category_priority as $priority) {
        //     $category_array[$priority->priority] = $priority->category->name;
        // }
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        $categories = Helpers::getDropDownData($categories);
        $types = ['category' => 'Category', 'subcategory' => 'Subcategory', 'service' => 'Service', 'general_service' => 'General Service'];
        return view('category-priority.createPriority', compact('category_array', 'directorates', 'specialities', 'categories', 'tree', 'types'));
    }

    public function store(Request $request) {
        if (!isset($request->priority))
            $request->merge(['priority' => 1]);
        else
            $request->merge(['priority' => $request->priority + $request->place]);
        $type_id = $this->getTypeTypeid($request);
        $request->merge(['type_id' => $type_id]);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('category-priority'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-priority', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-priority', 'Priority Added Successfully');
            return redirect('/category-priority');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('category-priority') . $id);
        $priority = json_decode($response->getBody()->getContents());
        
        $value = $this->getValues($priority);
        $services = [];
        if ($priority->type == 'service' || $priority->type == 'general_service')
            $services = $this->getServices($priority, $value);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/category-priority');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $priority->speciality->directorate_id, 'directorate_id');
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $priority->speciality->directorate_id, 'directorate_id');
        $tree = app('App\Http\Controllers\ServiceController')->getTree($categories);
        $category_priority = Helpers::getAllNodesByIdFromMainNode($specialities, 'category_priority', $priority->directorate_speciality_id, 'directorate_speciality_id');
        $category_array = [];
        foreach ($category_priority as $priority_node) {
            $type = $priority_node->type;
//            $category_array[$priority_node->priority] = $priority_node->$type->name;
            $category_array[$priority_node->priority] = $type;
        }
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        $categories = Helpers::getDropDownData($categories);
        $services = Helpers::getDropDownData($services);
        $types = ['category' => 'Category', 'subcategory' => 'Subcategory', 'service' => 'Service', 'general_service' => 'General Service'];
        return view('category-priority.editPriority', compact('category_array', 'directorates', 'specialities', 'categories', 'priority', 'types', 'tree', 'value', 'services'));    }

    public function update(Request $request, $id) {
        $request->merge(['priority' => $request->priority + $request->place]);
        $type_id = $this->getTypeTypeid($request);
        $request->merge(['type_id' => $type_id]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('category-priority') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->success) && count($result->success)) {
            Session::flash('success-priority', $result->success);
            return redirect('/category-priority');
        } else {
            Session::flash('error-priority', $result);
            return Redirect::back();
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('category-priority') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-priority', $result->error);
        return redirect('/category-priority');
    }

    public function updateRequirementOrder(Request $request) {
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('category-priority') . 'update/requirement-order', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-priority', $result->error);
            return redirect('/category-priority');
        } else {
            Session::flash('success-priority', $result->success);
            return redirect('/category-priority');
        }
    }

    //############################## AJAX CALLS ######################

    public function getCategoryPriority($id) {
        $results = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directoratespeciality') . $id . '/category-priority');
        $priorities = json_decode($response->getBody()->getContents());
        foreach ($priorities as $priority) {
            $results[$priority->priority] = $priority->category->name;
        }
        return $results;
    }

    //############################## PROTECTED FUMCTION ######################

    protected function getTypeTypeid(Request $data) {
        $result = [];
        switch ($data->type) {
            case 'category':
                return $data->category_id;
                break;
            case 'subcategory':
                return $data->sub_category_id;
                break;
            case 'service':
                return $data->service_id;
                break;
            case 'general_service':
                return $data->service_id;
                break;
        }
    }

    protected function getValues($priority) {
        $result = [];
        switch ($priority->type) {
            case 'category':
                return [
                    'category_id' => $priority->type_id,
                    'subcategory_id' => null,
                    'service_id' => null,
                ];
                break;
            case 'subcategory':
                return [
                    'category_id' => $priority->subcategory->category_id,
                    'subcategory_id' => [$priority->subcategory->id => $priority->subcategory->name],
                    'service_id' => null,
                ];
                break;
            case 'service':
                return [
                    'category_id' => ((isset($priority->service->category_id) && $priority->service->category_id != 0) ? $priority->service->category_id : null),
                    'subcategory_id' => ((isset($priority->service->sub_category_id) && $priority->service->sub_category_id != 0) ? [$priority->service->sub_category->id => $priority->service->sub_category->name] : null),
                    'service_id' => $priority->type_id,
                ];
                break;
            case 'general_service':
                return [
                    'category_id' => ((isset($priority->service->category_id) && $priority->service->category_id != 0) ? $priority->service->category_id : null),
                    'subcategory_id' => ((isset($priority->service->sub_category_id) && $priority->service->sub_category_id != 0) ? [$priority->service->sub_category->id => $priority->service->sub_category->name] : null),
                    'service_id' => $priority->type_id,
                ];
                break;
        }
    }

    protected function getServices($priority, $value) {
        switch ($priority->type) {
            case 'service':
                return $priority->speciality->service;
                break;
            case 'general_service':
                return $priority->speciality->directorate->services;
                break;
        }
    }

}
