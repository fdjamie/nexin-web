<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/post');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        else
            $directorate = null;
        $specialities = [];
        $total_posts = 0;
        $assigned_posts = 0;
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id');
        foreach ($specialities as $speciality) {
            if ($speciality->directorate_id == $directorate) {
                foreach ($speciality->post as $post) {
                    if ($post->staff_id != 0) {
                        $assigned_posts ++;
                    }
                }
                $total_posts = $total_posts + count($speciality->post);
            }
        }
        $directorates = Helpers::getDropDownData($directorates);
        $vacant_posts = $total_posts - $assigned_posts;
        if (count($directorates))
            $directorte_name = $directorates[$directorate];
        else
            $directorte_name = null;
        return view('post.allPost', compact('directorates', 'specialities', 'directorate', 'total_posts', 'assigned_posts', 'vacant_posts', 'directorte_name'));
    }

    public function create(Request $request) {
        $directorates = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        else
            $directorate = null;
        $specialties = Helpers::getDropDownData(Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id'));
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate, 'directorate_id');
        $rota_group_id = (isset($rota_groups[0]->id) ? $rota_groups[0]->id : 0 );
        $rota_templates = Helpers::getAllNodesByIdFromMainNode($rota_groups, 'rota_template', $rota_group_id, 'rota_group_id');
        $week_cycle = ($rota_templates) ? count($rota_templates[0]->content->shift_type) : 1;

        $rota_groups = Helpers::getDropDownData($rota_groups);
        $rota_templates = Helpers::getDropDownData($rota_templates);
        $directorates = Helpers::getDropDownData($directorates);
        return view('post.createPost', compact('rota_templates', 'directorates', 'week_cycle', 'specialties', 'directorate', 'rota_groups'));
    }

    public function store(Request $request) {
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('post'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-post', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-post', $request->name . ', Post Added Successfully');
            return redirect('/post');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('post') . $id);
        $post = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate = (isset($request->directorate) ? $request->directorate : $post->directorate_speciality->directorate_id);
        $specialties = Helpers::getDropDownData(Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate, 'directorate_id'));
        $rota_groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'rota_group', $directorate, 'directorate_id');
        $rota_group_id = (isset($rota_groups[0]->id) ? $rota_groups[0]->id : 0 );
        $rota_templates = Helpers::getAllNodesByIdFromMainNode($rota_groups, 'rota_template', $rota_group_id, 'rota_group_id');
        $speciality = $post->directorate_speciality_id;
        $post_parameters = [];
        if (isset($post->additional_parameters)) {
            foreach ($post->additional_parameters as $key => $value) {
                list($post_parameters['parameter'][], $post_parameters['values'][]) = $this->getAdditionalParameter($directorate, $speciality, $key);
                $post_parameters['selected_parameter'][] = $key;
                $post_parameters['selected_value'] [] = $value;
            }
        }
//        return [$post_parameters];
        $week_cycle = ($rota_templates) ? count($rota_templates[0]->content->shift_type) : 1;
        list($parameters, $values) = $this->getAdditionalParameter($directorate, $speciality);
        $rota_templates = Helpers::getDropDownData($rota_templates);
        $directorates = Helpers::getDropDownData($directorates);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        return view('post.editPost', compact('rota_templates', 'directorates', 'rota_groups', 'post', 'directorate', 'specialties', 'week_cycle', 'staff', 'directorate_staff', 'parameters', 'values', 'post_parameters'));
    }

    public function update(Request $request, $id) {
        if (!isset($request->permanent))
            $request->merge(['permanent' => 0]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('post') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-post', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-post', $result->success);
            return redirect('/post');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('post') . $id);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/post');
    }

    public function roster($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('post') . $id);
        $post = json_decode($response->getBody()->getContents());
        $shifts = $post->rota_template->rota_group->shift_type;
        $shift = array();
        foreach ($shifts as $shift_data) {
            $shift[$shift_data->id] = $shift_data;
        }
        $template = $post->rota_template->content->shift_type;
        return view('post.postRoster', compact('post', 'template', 'shift'));
    }

    public function rotation($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('post') . $id);
        $post = json_decode($response->getBody()->getContents());
        return view('post.postRotaions', compact('post'));
    }

    public function addAdditionalParameters(Request $request, $id) {
        $parameters = [];
        $additional['parameter'] = $request->parameter;
        $additional['value'] = $request->value;
        for ($index = 0; $index < count($additional['parameter']); $index++) {
            if ($additional['parameter'][$index] != 'select' && count($additional['value'][$index]))
                $parameters['parameter'][$additional['parameter'][$index]] = $additional['value'][$index];
        }
        $parameters = (count($parameters) ? $parameters : null);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('post') . $id . '/additional-parameter', ['form_params' => $parameters]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->success)) {
            Session::flash('success-parameter', $result->success);
            return Redirect::back();
        } else if (isset($result->error)) {
            Session::flash('error-parameter', $result->error);
            return Redirect::back();
        }
    }

    public function getAdditionalParameter($d_id, $s_id, $p_id = null) {
        $parameters[] = ['id' => 'select', 'name' => 'Select Parameter'];
        $values = $value_array = [];
        $param_id = null;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorates = json_decode($response->getBody()->getContents());
        $additional_parameters = Helpers::getAllNodesByIdFromMainNode($directorates, 'additional_parameter', $d_id, 'directorate_id');
        if (count($additional_parameters))
            $param_id = (($p_id != null && $p_id != 'null' ) ? $p_id : $additional_parameters[0]->id);
        foreach ($additional_parameters as $parameter) {
            $parameters [] = ['id' => $parameter->parameter, 'name' => $parameter->parameter];
            if ($parameter->parameter == $param_id)
                $value_array = explode(',', $parameter->value);
        }
        foreach ($value_array as $value) {
            $values[] = ['id' => strtolower($value), 'name' => $value];
        }
        $parameters = Helpers::getDropDownData($parameters);
        $values = Helpers::getDropDownData($values);
        return [$parameters, $values];
    }

    public function exportPost() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/post');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['Directorate', 'speciality', 'name', 'Description', 'Rota Group', 'Rota Template', 'Start Date', 'End Date', 'Assigned Week'];
        $ref_data[] = ['Directorate', 'speciality', 'name', 'Description', 'Rota Group', 'Rota Template', 'start Date', 'End Date', 'Assigned Week'];
        $ref_data[] = ['Dummy Directorate', ' Dummy speciality', ' Dummy name', ' Dummy Description', 'Dummy Rota Group', ' Dummy Rota Template', '2017-12-2', '2017-12-25 /Permanent', '1'];
        dd($directorates);
        foreach ($directorates as $directorate) {
            if (isset($directorate) && count($directorate))
                foreach ($directorate->speciality as $speciality) {
                    foreach ($speciality->post as $posts) {
                        array_push($data, [$directorate->name, $speciality->name, $posts->name, $posts->description, $posts->rota_template->rota_group->name, $posts->rota_template->name, $posts->start_date, $posts->end_date == '1111-00-00' ? 'Permanent' : $posts->end_date, $posts->assigned_week]);
                    }
                }
        }
        ExcelHelper::exportExcel('Post_backup', $data, $ref_data);
        Session::flash('success-post', 'Posts Export Successfully');
        return redirect()->back();
    }

    public function importPost(Request $request) {
        $viewData = [];
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/post');
        $directorates = json_decode($response->getBody()->getContents());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $importData = ExcelHelper::importExcel($request->file);
        $directorate_name = Helpers::gettingArrayOfName($directorates);
        list($spesiality, $post, $rotaTemplate, $rotaGroup) = Helpers::gettingSpecialityAndRotaGroupName($directorates);
        foreach ($importData as $data) {
            if (empty($data['directorate'])) {
                $data['directorate'] = '';
                $data['errors'] = ['required' => 'directoreate name requrired'];
            }
            if (empty($data['name'])) {
                $data['name'] = '';
                $data['erros'] = ['required' => 'name requrired'];
            }
            if (empty($data['speciality'])) {
                $data['speciality'] = '';
                $data['errors'] = ['required' => 'speciality requrired'];
            }
            if (empty($data['rota_template'])) {
                $data['rota_template'] = '';
                $data['errors'] = ['required' => 'rota template requrired'];
            }
            if (empty($data['start_date'])) {
                $data['start_date'] = '';
                $data['errors'] = ['required' => 'start date requrired'];
            }
            if (empty($data['assigned_week'])) {
                $data['assigned_week'] = '';
                $data['errors'] = ['required' => 'assigned week requrired'];
            }
            if (!in_array($data['directorate'], $directorate_name)) {
                $data ['errors'] = $this->errors('directorate');
            } else {
                if (array_search($data['speciality'], $spesiality[$data['directorate']]) !== false) {
                    if (array_search($data['rota_group'], $rotaGroup[$data['directorate']]) !== false) {
                        if (array_search($data['rota_template'], $rotaTemplate[$data['directorate']][$data['speciality']]) !== false) {
                            if (array_search($data['assigned_week'], $rotaTemplate[$data['directorate']][$data['speciality']]) !== false) {
                                if (in_array($data['directorate'], $directorate_name) && array_search($data['rota_group'], $rotaGroup[$data['directorate']]) && array_search($data['speciality'], $spesiality[$data['directorate']]) && array_search($data['name'], $post[$data['directorate']][$data['speciality']]) && array_search($data['assigned_week'], $post[$data['directorate']][$data['speciality']]) && array_search($data['assigned_week'], $rotaTemplate[$data['directorate']][$data['speciality']]) && array_search($data['rota_template'], $rotaTemplate[$data['directorate']][$data['speciality']])
                                ) {
                                    $data['errors'] = ['data' => 'whole already exits'];
                                } else {
                                    $data['errors'] = ['new_data' => 'whole data already exits'];
                                }
                            } else {
                                $data['errors'] = ['assigned_week_not_exists' => 'Assigned week should not be exeed the rota tamplates total week'];
                            }
                        } else {
                            $data['errors'] = $this->errors('rota_templete');
                        }
                    } else {
                        $data['errors'] = $this->errors('rota_group');
                    }
                } else {
                    $data['errors'] = $this->errors('speciality');
                }
            }
            $viewData [] = $data;
        }
        $fileName = Input::file('file')->getClientOriginalName();
        return view('post.viewUploadedData', ['viewData' => $viewData, 'fileName' => $fileName]);
    }

    public function postUpload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->fileName), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('posts') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('success-post', $result->success);
        return redirect()->route('post-index');
    }

    private function errors($error = '') {
        $data = [];
        $data['assigned_week_not_exists'] = 'Assigned week should not be exeed the rota tamplates total week';
        $data['rota_templete_not_exists'] = 'This Rota template does not exist in this rota group';
        $data['rota_group_not_exists'] = 'This Rota group does not exist in the dirctorate';
        $data['speciality_not_exists'] = 'This Speciality does not exist in the directorate';
        if ($error == 'rota_templete') {
            $data['rota_templete_not_exists'] = 'Rota Template not exists in rota group';
            unset($data['rota_group_not_exists']);
            unset($data['speciality_not_exists']);
        }
        if ($error == 'directorate') {
            $data['diractorate_err'] = 'This directorate do not exist in the system';
        }
        if ($error == 'rota_group') {
            $data['rota_group_not_exists'] = 'This Rota group does not exist in the dirctorate';
            unset($data['speciality_not_exists']);
        }
        if ($error == 'speciality') {
            $data['speciality_not_exists'] = 'This Speciality does not exist in the directorate';
            unset($data['rota_templete_not_exists']);
            unset($data['rota_group_not_exists']);
        }
        return $data;
    }

}
