<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class GroupController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.groups');

        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        return view('group.allGroup', compact('directorates'));
    }

    public function create() {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('group.createGroup', compact('directorates'));
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        /*echo '<pre>';
        print_r($request->all());
        print_r(rtrim(Helpers::getAPIUrl('group'), '/'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('group'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-group', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-group', 'Group Added successfully.');
            return redirect('/groups/');
        }
    }

    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('group') . $id);
        $group = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('group.editGroup', compact('group', 'directorates'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('group') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-group', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-group', $result->updated);
            return redirect('/groups/');
        }
    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('group') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-group', $result->error);
        return redirect('/groups');
    }

    public function export() {
        __authorize($this->module,'view',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'directorate'];
        $ref_data[] = ['name', 'directorate'];
        $ref_data[] = ['Dummy Group', 'Directorate Name'];
        foreach ($directorates as $directorate) {
            if (isset($directorate->group) && count($directorate->group))
                foreach ($directorate->group as $group) {
                    array_push($data, [$group->name, $directorate->name]);
                }
        }
        ExcelHelper::exportExcel('Group_backup', $data, $ref_data);
        Session::flash('success-group', 'Groups data exported successfully.');
        return redirect('/groups');
    }

    public function import(Request $request) {
        __authorize($this->module,'add',true);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $directorate_array = $group_array = $view_data = [];
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
            $directorates = json_decode($response->getBody()->getContents());
            list($directorate_array, $group_array) = $this->getCheckData($directorates);
            $view_data = $this->validateUploadedData($data, $directorate_array, $group_array);
            $file_name = Input::file('file')->getClientOriginalName();
            return view('group.viewUploadedData', compact('file_name', 'view_data'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('group') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-group', $result->error);
            return redirect('/groups');
        } else {
            Session::flash('success-group', $result->success);
            return redirect('/groups');
        }
    }

//################################ PROTECRTED FUNCTIONS ##############################

    protected function getCheckData($directorates) {
        $directorate_array = $group_array = [];
        foreach ($directorates as $directorate) {
            $directorate_array [] = $directorate->name;
            if (isset($directorate->group) && count($directorate->group)) {
                foreach ($directorate->group as $group) {
                    $group_array[$directorate->name][$group->id] = $group->name;
                }
            }
        }
        return [$directorate_array, $group_array];
    }

    protected function validateUploadedData($data, $directorates, $groups) {
        $view_data = [];
        foreach ($data as $data_node) {
            if (!in_array($data_node['directorate'], $directorates))
                $data_node['directorate_error'] = 'No such directorate exist in system.';
            if (isset($groups[$data_node['directorate']]) && array_search($data_node['name'], $groups[$data_node['directorate']]))
                $data_node['group_error'] = 'This Group already  exist in this directorate.';
            $view_data[] = $data_node;
        }
        return $view_data;
    }

}
