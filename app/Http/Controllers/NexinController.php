<?php

namespace App\Http\Controllers;

//use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;

class NexinController extends Controller
{
	private $api_base_url, $api_name;
	
	public function __construct(){
		$this->api_base_url=config('app.API_URL');
	}
	public function get_api_url($api_name=''){
		if($api_name==='')
			return $this->api_base_url;	
		else
			return $this->api_base_url.config('apis.'.$api_name);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://napis.loc/v1/');
        echo $res->getStatusCode();
        // 200
        echo $res->getHeaderLine('content-type');
        // 'application/json; charset=utf8'
        echo $res->getBody();        
//        $client = new \GuzzleHttp\Client();
//        $headers = ['Content-Type' => 'application/json'];
//        $jsonData = json_encode($_POST);
//        print_r($this->get_api_url('test_api'));
//        try{
//            $res = $client->request('GET', $this->get_api_url('test_api'));
//        } catch (ServerException $ex) {
//            print_r($ex);
//        }
//            $res = $client->request('POST', $this->get_api_url('test_api'),[
//            'json' => [
//                    'company_name' => 'company_name',
//                    'last_date_apply' => '06/12/2015',
//                    'rid' => '89498',
//                ],
//            ]);
//        } catch (\GuzzleHttp\Exception\ClientException $e){
//            print_r($e);
//        }
        die;
//        print_r($client);
//        die;
//        $result = json_decode($res->getBody(TRUE));
//        return view('index.index', ['result' => $result['data']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
