<?php

namespace App\Http\Controllers;

use File;
use Validator;
use App\Http\Helpers;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;

class CommandController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        $date = new Carbon(session()->get('date_time'));
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('command') . $date->format('Y-m-d'));
        $results = json_decode($response->getBody()->getContents());
        return response()->json($results);
    }

    public function optimalAssignment() {
        $date = new Carbon(session()->get('date_time'));
        $assignments = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('optimal-assignment') . $date->format('Y-m-d'));
        $assignment_tasks = json_decode($response->getBody()->getContents());
        foreach ($assignment_tasks as $assignment_task) {
            foreach ($assignment_task->assignment_session as $assignment_session) {
                foreach ($assignment_session->assignment as $assignment) {
                    $assignments[$assignment->transition_time->name][] = ['service' => $assignment->service->name, 'staff' => $assignment->staff->name];
                }
            }
        }
        return view('optimal-assignment.optimal-assignmentindex',compact('assignments'));
    }

}
