<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers;
use App\Http\Requests;
use GuzzleHttp\Client;

class OperatorController extends Controller {

    private $api_token, $api_url, $api_client;
    private $operators = [
            ['name' => 'Equal to', 'id' => '=', 'type' => 'integer'],
            ['name' => 'Greater or Equal to', 'id' => '>=', 'type' => 'integer'],
            ['name' => 'Greater than', 'id' => '>', 'type' => 'integer'],
            ['name' => 'Less or Equal to', 'id' => '<=', 'type' => 'integer'],
            ['name' => 'Less than', 'id' => '<', 'type' => 'integer'],
            ['name' => 'Not equal to', 'id' => '!=', 'type' => 'integer'],
            ['name' => 'Between', 'id' => 'between', 'type' => 'integer'],
            ['name' => 'Any', 'id' => 'any', 'type' => 'string'],
            ['name' => 'Is', 'id' => 'is', 'type' => 'string'],
            ['name' => 'Is not', 'id' => 'is_no', 'type' => 'string'],
    ];

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());

        $operators = Helpers::getDropDownData($this->operators);
        $types = ['ineger' => 'Integer', 'string' => 'String'];
        $heading = _('All Operators');
        return view('operator.index', ['heading' => $heading, 'types' => $types, 'operators' => $operators]);
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $operators = Helpers::getDropDownData($this->operators);
        $directorates = Helpers::getDropDownData($directorates);
        $types = ['integer' => 'Integer', 'string' => 'String'];
        $heading = _('All Operators');
        return view('operator.create', ['heading' => $heading, 'types' => $types, 'operators' => $operators, 'directorates' => $directorates]);
    }

    public function store(Request $request) {
        $name ='' ;
        $operators = Helpers::getDropDownData($this->operators);
        foreach ($operators as $key => $value) {
            if($key == $request->value){
                $name = $value;
            }
        }
        if(!empty($name)){
        $request->merge(['name' => $name]);
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'operator/test', ['form_params' => $request->all()]);
        $parameters = json_decode($response->getBody()->getContents());
        return redirect(route('operator-index'));
        }
        return redirect()->back();
    }

    public function getParameter($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'requirement/get-parameter/test/' . $id);
        $parameters = json_decode($response->getBody()->getContents());
        $parameters = Helpers::getDropDownData($parameters);
        return response($parameters);
    }

}
