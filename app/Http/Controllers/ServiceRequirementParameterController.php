<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers;
use App\Http\Requests;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;

class ServiceRequirementParameterController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.parameters');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        __authorize($this->module,'view',true);
//        $directorate = 1;
//        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
//        $directorates = json_decode($response->getBody()->getContents());
//        $directorates = Helpers::getDropDownData($directorates);
//        if (count($request->directorate)) {
//            $directorate = $request->directorate;
//            $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'requirement/get-parameter/test/' . $directorate);
//            $parameters = json_decode($response->getBody()->getContents());
//        } else {
//            $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'requirement/get-parameter/test/' . $directorate);
//            $parameters = json_decode($response->getBody()->getContents());
//        }
//        $heading = _('All Parameter');
//        return view('parameter.index', ['parameters' => $parameters, 'heading' => $heading, 'directorates' => $directorates, 'directorate' => $directorate]);
//    public function index() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/parameter');
        $directorates = json_decode($response->getBody()->getContents());
//        dd($directorates);
        $heading = _('All Parameter');
        return view('parameter.index', ['directorates' => $directorates, 'heading' => $heading]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'get-operators');
        $operator = json_decode($response->getBody()->getContents());
        $parameters = ['role' => 'Role', 'grade' => 'Grade', 'qualification' => 'Qualifications', 'name' => 'Name', 'site' => 'Site', 'location' => 'Location', 'additional_parameter' => 'Additional_Parameter'];
        $heading = _('Add Parameter');
        if ($request->ajax()) {
            $return = [];
            if ($request->parameter !== 'role' && $request->parameter !== 'grade') {
                foreach ($operator as $key => $ss) {
                    if ($ss == 'Equal to') {
                        $return[$key] = $ss;
//                    
                    }
                }
//                return $request->parameter;
                return response($return);
            } else {
                foreach ($operator as $key => $ss) {
//                    if ($ss == 'Equal to') {
                    $return[$key] = $ss;
//                    
//                    }
                }
                return response($return);
            }
        }
        return view('parameter.create', ['directorates' => $directorates, 'heading' => $heading, 'parameters' => $parameters, 'operators' => $operator]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('service') . 'requirement/parameter/test', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-parameters', $result->error);
            return redirect()->back();
        } else {
            Session::flash('success-parameters', 'Parameter add successfully');
            return redirect(route('service.requirement.parameter.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        __authorize($this->module,'edit',true);
        $parameterDropdown = [];
        $response = $this->api_client->request("get", Helpers::getAPIUrl('service') . 'requirement/parameter/test/' . $id);
        $parameters = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("post", Helpers::getAPIUrl('service') . 'get-operators');
        $operators = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        $heading = _('Edit Parameter');
        foreach ($parameters as $key => $parameter) {
            $parameterDropdown[$parameter->name] = $parameter->name;
            foreach ($operators as $operatorKey => $operator) {
                if ($parameter->name != 'Role' && $parameter->name != 'Grade') {
                    if ($operator != 'Equal to') {
//                        dd($operatorKey);
                        unset($operators->$operatorKey);
                    }
                }
            }
        }

//        $ss;
//        foreach ($operators as $operateKey => $operator) {
//            foreach ($parameters as $key => $parameter) {
//                $ss = in_array($operateKey, $parameter->operator);
//            }
//        }
//        dd($ss);

        return view('parameter.edit', ['directorates' => $directorates, 'parameters' => $parameters,
            'parameterDropdown' => $parameterDropdown, 'heading' => $heading, 'id' => $id, 'operators' => $operators]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('service') . 'requirement/parameter/test/' . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-parameters', $result->error);
            return redirect()->back();
        } else {
            Session::flash('success-parameters', 'Parameter updated successfully');
            return redirect(route('service.requirement.parameter.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
//        return response($id);
        __authorize($this->module,'delete',true);
        $response = $this->api_client->request("delete", Helpers::getAPIUrl('service') . 'requirement/parameter/test/' . $id);
        $parameters = json_decode($response->getBody()->getContents());
        return response(['message' => 'deleted']);
    }

    private function type() {
        return [
            'integer' => 'Integer',
            'string' => 'String'
        ];
    }

}
