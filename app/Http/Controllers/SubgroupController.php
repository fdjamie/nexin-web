<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class SubgroupController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = null;
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        $groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'group', $directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        return view('subgroup.allSubgroup', compact('directorates', 'groups', 'directorate_id'));
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $groups = [];
        if (count($directorates))
            $groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'group', $directorates[0]->id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        $groups = Helpers::getDropDownData($groups);
        return view('subgroup.createSubgroup', compact('groups', 'directorates'));
    }

    public function store(Request $request) {
        /*echo '<pre>';
        print_r($request->all());
        print_r(rtrim(Helpers::getAPIUrl('subgroup'), '/'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('subgroup'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-group', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-group', 'Sub group added successfully.');
            return redirect('/subgroups');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('subgroup') . $id);
        $subgroup = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
//        $groups = $this->getGroupsFromDirectorate($directorates, $subgroup->group->directorate->id);
        $groups = Helpers::getAllNodesByIdFromMainNode($directorates, 'group', $subgroup->group->directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        $groups = Helpers::getDropDownData($groups);
        return view('subgroup.editSubgroup', compact('subgroup', 'groups', 'directorates'));
    }

    public function update(Request $request, $id) {
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('subgroup') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-group', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-group', 'Subgroup updated successfully.');
            return redirect('/subgroups/');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('subgroup') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-group', $result->error);
        return redirect('/subgroups');
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'group', 'directorate'];
        $ref_data[] = ['name', 'group', 'directorate'];
        $ref_data[] = ['Dummy Subgroup', 'Group Name', 'Directorate Name'];
        foreach ($directorates as $directorate) {
            if (isset($directorate->group) && count($directorate->group))
                foreach ($directorate->group as $group) {
                    if (isset($group->subgroup) && count($group->subgroup))
                        foreach ($group->subgroup as $subgroup) {
                            array_push($data, [$subgroup->name, $group->name, $directorate->name]);
                        }
                }
        }
        ExcelHelper::exportExcel('Subgroup_backup', $data, $ref_data);
        Session::flash('success-group', 'Groups data exported successfully.');
        return redirect('/subgroups');
    }

    public function import(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $directorate_array = $group_array = $view_data = [];
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
            $directorates = json_decode($response->getBody()->getContents());
            list($directorate_array, $group_array, $subgroup_array) = $this->getCheckData($directorates);
            $view_data = $this->validateUploadedData($data, $directorate_array, $group_array, $subgroup_array);
            $file_name = Input::file('file')->getClientOriginalName();
            return view('subgroup.viewUploadedData', compact('file_name', 'view_data'));
        }
    }
    
    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('subgroup') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-group', $result->error);
            return redirect('/subgroups');
        } else {
            Session::flash('success-group', $result->success);
            return redirect('/subgroups');
        }
    }

    //################################### PROTECTED FUNCTIONS ##########################

    protected function getCheckData($directorates) {
        $directorate_array = $group_array = $subgroup_array = [];
        foreach ($directorates as $directorate) {
            $directorate_array [] = $directorate->name;
            if (isset($directorate->group) && count($directorate->group)) {
                foreach ($directorate->group as $group) {
                    $group_array[$directorate->name][$group->id] = $group->name;
                    if (isset($group->subgroup) && count($group->subgroup)) {
                        foreach ($group->subgroup as $subgroup) {
                            $subgroup_array[$group->name][$subgroup->id] = $subgroup->name;
                        }
                    }
                }
            }
        }
        return [$directorate_array, $group_array, $subgroup_array];
    }
    
    protected function validateUploadedData($data, $directorates, $groups, $subgroups) {
        $view_data = [];
        foreach ($data as $data_node) {
            if (!in_array($data_node['directorate'], $directorates))
                $data_node['directorate_error'] = 'No such directorate exist in system.';
            if (isset($groups[$data_node['directorate']]) && !array_search($data_node['group'], $groups[$data_node['directorate']]))
                $data_node['group_error'] = 'This Group doest not exist in this directorate.';
            if (
                    (isset($groups[$data_node['directorate']]) && array_search($data_node['group'], $groups[$data_node['directorate']])) &&
                    (isset($subgroups[$data_node['group']]) && array_search($data_node['name'], $subgroups[$data_node['group']]))
               )
                $data_node['subgroup_error'] = 'This Subgroup already  exist in this directorate.';
            $view_data[] = $data_node;
        }
        return $view_data;
    }

}
