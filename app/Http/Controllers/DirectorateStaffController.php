<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class DirectorateStaffController extends Controller {

    private $api_token, $api_url, $api_client, $module;

    public function __construct() {
        $this->module = config('module.staff');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        __authorize($this->module, 'view', true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/staff');
        $directorates = json_decode($response->getBody()->getContents());
        return view('directorate.list_staff', compact('directorates'));
    }

    public function create() {
        __authorize($this->module, 'add', true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
        $directorates = json_decode($response->getBody()->getContents());
        $specialties = $grades = $roles = [];
        if (count($directorates))
            $specialties = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        $specialties = Helpers::getDropDownData($specialties);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $grades_roles = json_decode($response->getBody()->getContents());
        if (count($grades_roles)) {
            $grades = Helpers::getAllNodesByIdFromMainNode($grades_roles, 'grade', $grades_roles[0]->id, 'directorate_id');
            $grades = Helpers::getDropDownData($grades);
            $roles = Helpers::getAllNodesByIdFromMainNode($grades_roles, 'role', $grades_roles[0]->id, 'directorate_id');
            $roles = Helpers::getDropDownData($roles);
        }
        $directorates = Helpers::getDropDownData($grades_roles);
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('qualification'), '/'));
        $qualifications = json_decode($response->getBody()->getContents());
        $qualifications = Helpers::getDropDownData($qualifications);
        return view('directorate.add_staff', compact('directorates', 'specialties', 'grades', 'roles', 'qualifications'));
    }

    public function store(Request $request) {
        __authorize($this->module, 'add', true);
        if (isset($request->qualifications)) {
            $request->merge(['qualifications' => json_encode(['qualification_id' => $request->qualifications, 'public' => (isset($request->qualifications_public) ? $request->qualifications_public : 0)])]);
//            $request->merge(['qualifications' => ['qualification_id' => $request->qualifications, 'public' => (isset($request->qualifications_public) ? $request->qualifications_public : 0)]]);
        } else
            $request->merge(['qualifications' => null]);
        $request->merge(['individual_bleep' => $request->individual_bleep . '^' . (isset($request->individual_bleep_public) ? $request->individual_bleep_public : 0)]);
        $request->merge(['mobile' => $request->mobile . '^' . (isset($request->mobile_public) ? $request->mobile_public : 0)]);
        $postData = array();
        foreach ($request->all() as $key => $value) {
            if ($key == 'profile_pic') {
                array_push($postData, [
                    'name' => $key,
                    'contents' => fopen(Input::file('profile_pic')->getPathname(), 'r'),
                    'filename' => str_replace(' ', '', Input::file($key)->getClientOriginalName()),
                ]);
            } else {
                array_push($postData, ['name' => $key, 'contents' => $value]);
            }
        }
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('directorate-staff'), '/'), ['multipart' => $postData]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-staff', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-staff', 'Staff Members added successfully.');
            return redirect('/directorate-staff');
        }
    }

    public function edit($id) {
        __authorize($this->module, 'edit', true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-staff') . $id);
        $record = json_decode($response->getBody()->getContents());
//        return [$record];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
        $directorates = json_decode($response->getBody()->getContents());
        $specialties = $specialties = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $record->directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        $specialties = Helpers::getDropDownData($specialties);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $grades_roles = json_decode($response->getBody()->getContents());
        $grades = Helpers::getAllNodesByIdFromMainNode($grades_roles, 'grade', $record->directorate_id, 'directorate_id');
        $grades = Helpers::getDropDownData($grades);
        $roles = Helpers::getAllNodesByIdFromMainNode($grades_roles, 'role', $record->directorate_id, 'directorate_id');
        $roles = Helpers::getDropDownData($roles);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('qualification'));
        $qualifications = json_decode($response->getBody()->getContents());
        $qualifications = Helpers::getDropDownData($qualifications);
        return view('directorate.edit_staff', compact('directorates', 'record', 'specialties', 'roles', 'grades', 'qualifications'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module, 'edit', true);
        $request->merge(['individual_bleep' => $request->individual_bleep . '^' . (isset($request->individual_bleep_public) ? $request->individual_bleep_public : 0)]);
        $request->merge(['mobile' => $request->mobile . '^' . (isset($request->mobile_public) ? $request->mobile_public : 0)]);
        if (!isset($request->appointed_till) || $request->appointed_till == '')
            $request->merge(['appointed_till' => null]);
        if (!Input::hasFile('profile_pic')) {
            /* echo '<pre>';
              print_r(Helpers::getAPIUrl('directorate-staff') . $id);
              print_r($request->all());
              exit; */
            $response = $this->api_client->request("PUT", Helpers::getAPIUrl('directorate-staff') . $id, ['form_params' => $request->all()]);
            $record = json_decode($response->getBody()->getContents());
//            return [$record];
            if (isset($record->error)) {
                Session::flash('error-staff', $record->error);
                return redirect::back();
            } else {
                Session::flash('success-staff', 'Staff Updated Successfully.');
                return redirect('/directorate-staff');
            }
        } else {
            $postData = array();
            foreach ($request->all()as $key => $value) {
                if (Input::hasFile($key)) {
                    array_push($postData, [
                        'name' => $key,
                        'contents' => fopen(Input::file('profile_pic')->getPathname(), 'r'),
                        'filename' => str_replace(' ', '', Input::file($key)->getClientOriginalName()),
                    ]);
                } else {
                    array_push($postData, ['name' => $key, 'contents' => $value]);
                }
            }
            $response = $this->api_client->request("PUT", Helpers::getAPIUrl('directorate-staff') . $id, ['multipart' => $postData]);
            $record = json_decode($response->getBody()->getContents());
        }
        if (isset($record->error)) {
            Session::flash('error-staff', $record->error);
            return redirect('/directorate-staff');
        } else {
            Session::flash('success-staff', 'Staff Updated Successfully.');
            return redirect('/directorate-staff');
        }
    }

    public function destroy($id) {
        __authorize($this->module, 'delete', true);
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('directorate-staff') . $id);
        $record = json_decode($response->getBody()->getContents());
        Session::flash('error-staff', $record->error);
        return redirect('/directorate-staff');
    }

    public function searchStaff(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('') . 'v1/search/staff/' . $request->get('q'));
        $records = json_decode($response->getBody()->getContents());
        return response()->json(['total_count' => count($records), 'items' => $records]);
    }

    public function searchAppointedStaff(Request $request, $id) {
        /* echo(Helpers::getAPIUrl('') . 'v1/search/directorate/' . $id . '/staff/' . $request->get('q'));
          exit; */
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('') . 'v1/search/directorate/' . $id . '/staff/' . $request->get('q'));
        $records = json_decode($response->getBody()->getContents());
        $staffs = [];
        foreach ($records as $record) {
            if (isset($record) && count($record)) {
                $staffs[] = $record;
            }
        }
        return response()->json(['total_count' => count($staffs), 'items' => $staffs]);
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-staff') . 'export/' . auth()->user()->trust_id);
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('qualification'));
        $qualifications = json_decode($response->getBody()->getContents());
        $qualifation_array = $this->getQualificationsArray($qualifications);
        $data[] = ['name', 'gmc', 'grade', 'role', 'qualifications', 'qualifications_public', 'individual_bleep', 'individual_bleep_public', 'email', 'mobile', 'mobile_public', 'directorate', 'speciality', 'appointed_from', 'appointed_till'];
        $ref_data[] = ['name', 'gmc', 'grade', 'role', 'qualifications', 'qualifications_public', 'individual_bleep', 'individual_bleep_public', 'email', 'mobile', 'mobile_public', 'directorate', 'speciality', 'appointed_from', 'appointed_till'];
        $ref_data[] = ['John Doe', '7439001', 'Dummy Grade', 'Dummy Role', 'PACES', 'Yes/No', '7430', 'Yes/No', 'john@doctors.org.uk', '7111000002', 'Yes/No', 'Dummy Directorate', 'Dummy Speciality', '2017-06-14', '2017-12-31 /(Leave blank)'];
        foreach ($directorates as $directorate) {
            foreach ($directorate->staff as $staff) {
                $bleep = explode('^', $staff->individual_bleep);
                $mobile = explode('^', $staff->mobile);
                $staff_qualification = [];
                if (isset($staff) && count($staff))
                    if (isset($staff->qualifications->qualification_id)) {
                        foreach ($staff->qualifications->qualification_id as $qualification) {
                            $staff_qualification[] = array_search($qualification, $qualifation_array);
                        }
                        $qualification_public = ($staff->qualifications->public == 0 ? 'No' : 'Yes');
                    }
                array_push($data, [$staff->name, $staff->gmc, $staff->grade->name, $staff->role->name, implode(',', $staff_qualification), (isset($qualification_public) ? $qualification_public : ''), $bleep[0], ($bleep[1] == 0 ? 'No' : 'Yes'), $staff->email, $mobile[0], ($mobile[1] == 0 ? 'No' : 'Yes'), $directorate->name, $staff->directorate_speciality->name, $staff->appointed_from, $staff->appointed_till]);
            }
        }
        ExcelHelper::exportExcel('Staff_backup', $data, $ref_data);
        return redirect('/directorate-staff');
    }

    public function import(Request $request) {
        __authorize($this->module, 'add', true);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = Excel::selectSheetsByIndex(0)->load(Input::file('file'), function($reader) {
                        // $reader->ignoreEmpty();
                        $records = $reader->all();
                        return $records->toArray();
                    })->toArray();
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
            $directorates = json_decode($response->getBody()->getContents());
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('qualification'));
            $qualifications = json_decode($response->getBody()->getContents());
            $qualifications = Helpers::gettingArrayOfName($qualifications);
            $directorate_names = Helpers::gettingArrayOfName($directorates);
            foreach ($data as $data_node) {
                $staff_gmcs = [];
                $grade_role = '';
                foreach ($directorates as $directorate) {
                    if ($directorate->name == $data_node['directorate']) {
                        $speciality_names = Helpers::gettingArrayOfName($directorate->speciality);
                        $role_names = Helpers::gettingArrayOfName($directorate->role);
                        $grade_names = Helpers::gettingArrayOfName($directorate->grade);
                        $staff_qualification = (isset($data_node['qualifications']) ? explode(',', $data_node['qualifications']) : '');
                        $i = 1;
                        foreach ($staff_qualification as $qualification_node) {
                            if (isset($data_node['qualifications']) && !in_array($qualification_node, $qualifications))
                                $data_node['qualification_error'][] = 'Qualification ' . $i . ' does not exist in system';
                            $i++;
                        }
                        foreach ($directorate->staff as $staff) {
                            $staff_gmcs[] = $staff->gmc;
                        }
                        foreach ($directorate->grade as $grade) {
                            if ($data_node['grade'] == $grade->name && $data_node['directorate'] == $directorate->name)
                                $grade_role = $grade->role->name;
                        }
                    }
                }
                if (!isset($data_node['name'])) {
                    $data_node['name'] = '';
                    $data_node['name_error'] = 'Staff member name is required';
                }
                if (!in_array($data_node['directorate'], $directorate_names))
                    $data_node['directorate_error'] = 'This directorate does not exist in system.';
                if (!in_array($data_node['grade'], $grade_names))
                    $data_node['grade_error'] = 'This grade does not exist in this directorate or in system.';
                if (!in_array($data_node['role'], $role_names))
                    $data_node['role_error'] = 'This role does not exist in this directorateor or in system';
                if (!in_array($data_node['speciality'], $speciality_names))
                    $data_node['speciality_error'] = 'This speciality does not exist in this directorateor or in system';
                if (in_array($data_node['gmc'], $staff_gmcs))
                    $data_node['gmc_error'] = 'This staff member already exist for this directorate.';
                if ($grade_role != '' && $data_node['role'] != $grade_role)
                    $data_node['grade_role_error'] = 'This role is not associated with this grade.';
                if ($data_node['qualifications_public'] != '' && (strcasecmp($data_node['qualifications_public'], 'Yes') != 0 && strcasecmp($data_node['qualifications_public'], 'No') != 0))
                    $data_node['qualifications_public_error'] = 'Qualifications public has invalid value.';
                if ($data_node['mobile_public'] != '' && (strcasecmp($data_node['mobile_public'], 'Yes') != 0 && strcasecmp($data_node['mobile_public'], 'No') != 0))
                    $data_node['mobile_public_error'] = 'Mobile public has invalid value.';
                if ($data_node['individual_bleep_public'] != '' && (strcasecmp($data_node['individual_bleep_public'], 'Yes') != 0 && strcasecmp($data_node['individual_bleep_public'], 'No') != 0))
                    $data_node['individual_bleep_public_error'] = 'Individual bleep public has invalid value.';

                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            $update_flag = (isset($request->update_flag) ? $request->update_flag : 0);
            return view('directorate.uploadedStaff', compact('view_data', 'file_name', 'update_flag'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        if (!isset($request->update_flag))
            $request->merge(['update_flag' => 0]);
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('directorate-staff') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-staff', $result->error);
            return redirect('/directorate-staff');
        } else {
            Session::flash('success-staff', $result->success);
            return redirect('/directorate-staff');
        }
    }

    //############################### PROTECTED FUNCTION #############################

    protected function getQualificationsArray($qualifications) {
        $result = [];
        foreach ($qualifications as $qualification) {
            $result [$qualification->name] = $qualification->id;
        }
        return $result;
    }

}
