<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class SubCategoryController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = null;
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        return view('sub-category.allSubCategory', compact('categories', 'directorate_id', 'directorates'));
    }

    public function indexChild($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('sub-category') . $id . '/child');
        $result = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('sub-category') . $id . '/parents');
        $parents = json_decode($response->getBody()->getContents());
        $parents = array_reverse($parents);
        return view('sub-category.allChildSubCategory', ['subcategories' => $result, 'parents' => $parents]);
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        $categories = [];
        if (count($directorates))
            $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorates[0]->id, 'directorate_id');
        $categories = Helpers::getDropDownData($categories);
        $directorates = Helpers::getDropDownData($directorates);
        return view('sub-category.createSubCategory', compact('categories', 'directorates', 'specialities'));
    }

    public function store(Request $request) {
        if (!isset($request->parent_id))
            $request->merge(['parent_id' => 0]);
        else {
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('sub-category') . $request->parent_id);
            $subcategory = json_decode($response->getBody()->getContents());
            $request->merge(['category_id' => $subcategory->category_id]);
        }
        /*echo '<pre>';
        print_r($request->all());
        print_r(rtrim(Helpers::getAPIUrl('sub-category'), '/'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('sub-category'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-category', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-category', 'Sub category Added successfully.');
            return redirect('/sub-category/');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('sub-category') . $id);
        $sub_category = json_decode($response->getBody()->getContents());
        $subcategory_specialties = [];
        if (isset($sub_category->specialities))
            $subcategory_specialties = $sub_category->specialities;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $sub_category->category->directorate_id, 'directorate_id');
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $sub_category->category->directorate_id, 'directorate_id');
        $categories = Helpers::getDropDownData($categories);
        $directorates = Helpers::getDropDownData($directorates);
        return view('sub-category.editSubCategory', compact('sub_category', 'categories', 'directorates', 'specialities', 'subcategory_specialties'));
    }

    public function update(Request $request, $id) {
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        if (!isset($request->parent_id))
            $request->merge(['parent_id' => 0]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('sub-category') . $id, ['form_params' => $request->all()]);
        $category = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-category', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-category', 'Category Updated successfully.');
            if ($request->parent_id > 0)
                return redirect('/sub-category/' . $request->parent_id . '/sub-category');
            else
                return redirect('/sub-category/');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('sub-category') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-category', $result->error);
        return redirect('/sub-category');
    }

//############################## AJAX CALLS ####################
    public function getChildSubCategories($id, $s_id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('sub-category') . $id . '/children');
        $subcategories = json_decode($response->getBody()->getContents());
        $result = [];
        foreach ($subcategories->child_recursive as $subcategory) {
            $result[$subcategory->id] = $subcategory->name;
        }
        if (count($result))
            $result[0] = 'select subcategory';
        return response($result);
    }

    public function searchSubcategory(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('category') . $request->category_id . '/sub-category/' . $request->q, ['connect_timeout' => 5]);
        $subcategories = json_decode($response->getBody()->getContents());
//        return [$subcategories];
        return response()->json(['total_count' => count($subcategories), 'items' => $subcategories]);
    }

}
