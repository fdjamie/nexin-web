<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Helpers;
use Validator;
use Carbon\Carbon;
use Redirect;
class EmailTemplatesController extends Controller
{

    private $trustId;

    public function __construct()
    {
        if(auth()->user()->is_admin!=1)
        {
            abort(403);
        }
        $this->trustId=session('activeTrust');


    }


    public function index(){


        $templates=array();
        $apiUrl=$apiUrl=Helpers::getAPIUrl('emailTemplates').$this->trustId;

        $getEmailTemplates=__get($apiUrl);

        if($getEmailTemplates['status']) {
            $result = $getEmailTemplates['apiResponseData'];
            if($result->status)
            {
                $templates=$result->data;
            }
        }


        return view('email-templates.templates',compact('templates'));


    }

    public function getTrustTemplate(Request $request)
    {

        $templatHtml=array();
       /* $apiUrl=Helpers::getAPIUrl('trustTemplateHtml').$request->id."/".$this->trustId;*/
        $apiUrl=Helpers::getAPIUrl('trustTemplateHtml').$request->id;
        $getTemplate=__get($apiUrl);
        return $getTemplate;

    }

      function getEmailTypeTemplate(Request $request)
      {

          $apiUrl=Helpers::getAPIUrl('emailTypeTemplate').$request->id."/".$this->trustId;
          $getEmailTemplates=__get($apiUrl);
          return $getEmailTemplates;

      }

    function updateTemplate(Request $request)
    {

        $apiUrl=Helpers::getAPIUrl('updateTemplate');
        $params=array();
        $params['emailTypeId']=$request->emailTypeId;
        $params['templateId']=$request->templateId;
        $params['trustId']=$this->trustId;
        $updateTemplate=__post($apiUrl,$params);
        return $updateTemplate;

    }

    function createNewTemplate()
    {

        $templates=array();
        $apiUrl=$apiUrl=Helpers::getAPIUrl('emailTemplates').$this->trustId;

        $getEmailTemplates=__get($apiUrl);

        if($getEmailTemplates['status']) {
            $result = $getEmailTemplates['apiResponseData'];
            if($result->status)
            {
                $templates=$result->data;
            }
        }


        return view('email-templates.add_template',compact('templates'));


    }

    function storeNewTemplate(Request $request)
    {
       $this->validate($request, ['template' => 'required','templateType'=>'required','from'=>'email']);
        $apiUrl = Helpers::getAPIUrl('createNewTemplate');
        $request->request->add(['trustId' => $this->trustId]);
        $paramArr = array();
        $paramArr=$request->all();
        $storeTemp= __post($apiUrl, $paramArr);
        if ($storeTemp['status']) {
            $result = $storeTemp['apiResponseData'];
            if (!$result->status) {
                return Redirect::back()->withErrors($result->errors)->withInput();
            } else {

                return redirect('email-templates')->with('success', $result->msg);
            }
        } else {
            return Redirect::back()->withErrors(['error' => trans('messages.error')])->withInput();

        }

    }

}
