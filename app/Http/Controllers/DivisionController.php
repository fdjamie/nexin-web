<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class DivisionController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        return view('division.allDivisions', compact('trusts'));
    }


    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        $trust = array();
        array_push($trust, ['id' => $trusts->id, 'name' => $trusts->name]);
        $trust = Helpers::getDropDownData($trust);
        return view('division.createDivision', compact('trust'));
    }

    public function store(Request $request) {
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('division'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && !empty($result->error)) {
            Session::flash('error-division', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-division', $request->name . ', Division Added Successfully.');
            return redirect('/divisions');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('division') . $id);
        $division = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        $trust = array();
        array_push($trust, ['id' => $trusts->id, 'name' => $trusts->name]);
        $trust = Helpers::getDropDownData($trust);
        return view('division.editDivision', compact('division', 'trust'));
    }

    public function update(Request $request, $id) {
        /*dd($request->session()->all());
        echo '<pre>';
        echo Helpers::getAPIUrl('division')."\n";
        print_r($request->all());
        exit;*/
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('division') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        // print_r($request->all());
        return redirect('/divisions');
    }

  
    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('division') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-division', $result->error);
        return redirect('/divisions');
    }
    
    public function deleted() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('division') . 'deleted/' . auth()->user()->trust_id);
        $divisions = json_decode($response->getBody()->getContents());
        return view('division.deletedDivisions', compact('divisions'));
    }

    public function restore() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('division') . 'restore/' . auth()->user()->trust_id);
        $divisions = json_decode($response->getBody()->getContents());
        if (isset($divisions->error)) {
            Session::flash('error-division', $divisions->error);
            return redirect('/divisions');
        } else {
            Session::flash('success-division', $divisions->success);
            return redirect('/divisions');
        }
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/division');
        $trust = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'trust'];
        $ref_data[] = ['name', 'trust'];
        $ref_data[] = ['Dummy Division', 'Dummy Trust'];
        foreach ($trust->division as $division) {
            if (isset($division) && !empty($division))
                array_push($data, ['name' => $division->name, 'trust' => $trust->name]);
        }
        ExcelHelper::exportExcel('Division_backup', $data, $ref_data);
        Session::flash('success-division', 'Divisions data exported successfully.');
        return redirect('/divisions');
    }

    public function import(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/division');
            $trust = json_decode($response->getBody()->getContents());
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust'));
            $trusts = json_decode($response->getBody()->getContents());
            $trust_names = Helpers::gettingArrayOfName($trusts);
            $divisions = Helpers::gettingArrayOfName($trust->division);
            foreach ($data as $data_node) {
                if (!isset($data_node['name'])) {
                    $data_node['name'] = '';
                    $data_node['division_name_error'] = 'Division name is required';
                }
                if (!in_array($data_node['trust'], $trust_names))
                    $data_node['trusts_error'] = 'This trust does not exist in the system';
                if ($data_node['trust'] != $trust->name && in_array($data_node['trust'], $trust_names))
                    $data_node['trust_error'] = 'You are not authorized for this trust';
                if ($data_node['trust'] == $trust->name && in_array($data_node['name'], $divisions))
                    $data_node['division_error'] = 'This division already exist for this trust';
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('division.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records]);
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('division') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-division', $result->error);
            return redirect('/divisions');
        } else {
            Session::flash('success-division', $result->success);
            return redirect('/divisions');
        }
    }

}
