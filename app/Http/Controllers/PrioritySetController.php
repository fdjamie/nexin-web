<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class PrioritySetController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        //
    }

    public function create() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        return view('priority-set.createPrioritySet', compact('directorates', 'specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('priority-set'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-priority', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-priority', $result->success);
            return redirect('/category-priority');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('priority-set') . $id);
        $priority_set = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directoratespeciality') . $priority_set->directorate_speciality_id . '/category-priority');
        $category_priority = json_decode($response->getBody()->getContents());
        $dropdown [0] = 'All Services';
        foreach ($category_priority as $priority) {
            if ($priority->type == 'category')
                $name = $priority->category[0]->name;
            elseif ($priority->type == 'subcategory') {
                $name = $priority->subcategory[0]->name;
            } elseif ($priority->type == 'service')
                $name = $priority->service[0]->name;
            else
                $name = $priority->directorate_service[0]->name;
            $dropdown[$priority->id] = $name . ' (' . $priority->type . ')';
        }
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('color'), '/'));
        $color_array = json_decode($response->getBody()->getContents());
        $royg = $colors = [];
        if (isset($color_array)) {
            foreach ($color_array as $color) {
                $royg [$color->position] = $color->letter;
                $colors[$color->position] = $color->code;
            }
        }
//        return [$priority_set];
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $priority_set->speciality->directorate_id, 'directorate_id');
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        return view('priority-set.editPrioritySet', compact('directorates', 'specialities', 'priority_set', 'royg', 'dropdown', 'colors'));
    }

    public function update(Request $request, $id) {
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('priority-set') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-priority', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-priority', $result->updated);
            return redirect('/category-priority');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('priority-set') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-priority', $result->error);
        return redirect('/category-priority');
    }

}
