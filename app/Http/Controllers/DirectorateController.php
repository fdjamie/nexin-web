<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\ElasticSearchHandler;

class DirectorateController extends Controller {

    private $api_token, $api_url, $api_client,$module;

    public function __construct() {
        $this->module=config('module.directorates');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {

        __authorize($this->module,'view',true);
         $directorates=array();
         $errors=array();
        $apiUrl=Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates';
      /*  $apiUrl=Helpers::getAPIUrl('trust') . session('activeTrust') . '/directorates';*/
        $getDirectorates=__get($apiUrl);
        if($getDirectorates['status'])
        {
            $directorates=$getDirectorates['apiResponseData'];
        }
        else
        {
            $errors=[(trans('messages.error'))];
        }
        return view('directorate.allDirectorate', compact('directorates'))->withErrors($errors);


    }



 /*   public function index() {

        $directorates=array();
        $errors=array();
        $apiUrl=Helpers::getAPIUrl('trust') . auth()->user()->id . '/directorates';
        $getDirectorates=__get($apiUrl);

        if($getDirectorates['status'])
        {
            $directorates=$getDirectorates['apiResponseData'];
        }
        else
        {
            $errors=[(trans('messages.error'))];
        }
        return view('directorate.allDirectorate', compact('directorates'))->withErrors($errors);


    }*/



    public function create() {

        __authorize($this->module,'add',true);

        $trusts=array();
        $divisions=array();
        $errors=array();
        $apiUrl=Helpers::getAPIUrl('trust') . auth()->user()->trust_id;
        /*$apiUrl=Helpers::getAPIUrl('trust') . session('activeTrust');*/
        $getTrusts=__get($apiUrl);

        if($getTrusts['status'])
        {
            $trusts=$getTrusts['apiResponseData'];
            $divisions = Helpers::getDropDownData($trusts->division);
        }
        else
        {
            $errors=[(trans('messages.error'))];
            Session::flash('error-directorate', [$errors]);

        }
        return view('directorate.createDirectorate', compact('trust', 'divisions', 'specialities'));



    }

    public function store(Request $request) {

        __authorize($this->module,'add',true);


      /*  $request->merge(['trust_id' => session('activeTrust')]);*/
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $apiUrl=rtrim(Helpers::getAPIUrl('directorate'), '/');
        $paramArr=$request->all();

       /* echo '<pre>';

        /*echo '<pre>';

        echo Helpers::getAPIUrl('directorate')."\n";
        print_r($paramArr);exit;*/
        $storeDirectorate=__post($apiUrl,$paramArr);
        if($storeDirectorate['status'])
        {

            $result=$storeDirectorate['apiResponseData'];

            if (isset($result->error) && ($result->error)) {
                return Redirect::back()->with('error-directorate',$result->error)->withInput();
            } else {
                return redirect('/directorates')->with('success-directorate', $request->name . ', directorate added sucessfully.');
            }
        }
        else
        {
            return Redirect::back()->with('error-directorate',[[(trans('messages.error'))]])->withInput();

        }


    }

    public function edit($id) {


        __authorize($this->module,'edit',true);

        $directorate=array();
        $trust=array();
        $divisions=array();
        $arrors=array();
        $directorateApiurl=Helpers::getAPIUrl('directorate') . $id;
        $getDirectorate=__get($directorateApiurl);
       $trustApiurl=Helpers::getAPIUrl('trust') . auth()->user()->trust_id;
      /*  $trustApiurl=Helpers::getAPIUrl('trust') . session('activeTrust');*/
        $getTrust=__get($trustApiurl);
        if($getDirectorate['status'] && $getTrust['status']) {

            $directorate = $getDirectorate['apiResponseData'];
            $trust = $getTrust['apiResponseData'];
            $divisions = Helpers::getDropDownData($trust->division);
            return view('directorate.editDirectorate', compact('directorate', 'trust', 'divisions'));
        }
        else
        {

            return redirect()->back()->with('error-directorate',(trans('messages.error')));
        }

    }

    public function update(Request $request, $id) {

        __authorize($this->module,'edit',true);
       $request->merge(['trust_id' => auth()->user()->trust_id]);
      /* $request->merge(['trust_id' => session('activeTrust')]);*/
        if (!isset($request->division_id))
            $request->merge(['division_id' => 0]);
        if (!isset($request->service))
            $request->merge(['service' => 0]);
        $apiUrl=Helpers::getAPIUrl('directorate') . $id;
        $paramArr=$request->all();
        /*echo '<pre>';
        print_r($apiUrl);
        print_r($paramArr);
        exit;*/
        $updateDirectory=__put($apiUrl,$paramArr);
        if($updateDirectory['status'])
        {
            $result=$updateDirectory['apiResponseData'];
            if (isset($result->error) && !empty($result->error)) {
                Session::flash('error-directorate', $result->error);
                return Redirect::back();
            } else {
                Session::flash('success-directorate', $result->updated);
                return redirect('/directorates');
            }
        }
        else
        {
            return Redirect::back()->with('error-directorate',[[(trans('messages.error'))]])->withInput();

        }


    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $apiUrl=Helpers::getAPIUrl('directorate') . $id;
        $deleteDirectorate=__delete($apiUrl);
        if($deleteDirectorate['status'])
        {
            $result=$deleteDirectorate['apiResponseData'];
            if (isset($result->error)) {
                Session::flash('error-directorate', $result->error);
                return Redirect::back();
            } else {
                Session::flash('success-directorate', "Directorate Deleted Successfully.");
                return redirect('/directorates');
            }
        }
        else
        {
            Session::flash('error-directorate', (trans('messages.error')));
            return Redirect::back();

        }



    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
/*        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . session('activeTrust') . '/directorates');*/
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'service', 'division', 'trust'];
        $ref_data[] = ['name', 'service', 'division', 'trust'];
        $ref_data[] = ['Dummy Directorate', 'Yes / No', 'Dummy Division', 'Dummy Trust'];
        foreach ($directorates as $directorate) {
            if (isset($directorate) && !empty($directorate))
                array_push($data, [$directorate->name, ($directorate->service == 1 ? 'Yes' : 'No'), (isset($directorate->division) ? $directorate->division->name : 'N/A'), $directorate->trust->name]);
        }
        ExcelHelper::exportExcel('Directorate_backup', $data, $ref_data);
        Session::flash('success-directorate', 'Directorates data exported successfully.');
        return redirect('/directorates');
    }

    public function import(Request $request) {
        __authorize($this->module,'edit',true);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = ExcelHelper::importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . session('activeTrust') . '/division/directorate');
/*            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/division/directorate');*/
            $trust = json_decode($response->getBody()->getContents());
            $division_names = Helpers::gettingArrayOfName($trust->division);
/*            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');*/
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . session('activeTrust') . '/directorates');
            $directorates = json_decode($response->getBody()->getContents());
            $ind_directorate_name = Helpers::gettingArrayOfName($directorates);
            foreach ($data as $data_node) {
                $directorate_names = [];
                foreach ($trust->division as $division) {
                    if ($data_node['division'] == $division->name && $data_node['trust'] == $trust->name)
                        $directorate_names = Helpers::gettingArrayOfName($division->directorate);
                }
                if ($data_node['trust'] != $trust->name)
                    $data_node['trust_error'] = 'This trust does not exitt in system or unaturized trust.';
                if ($data_node['division'] != 'N/A' && !in_array($data_node['division'], $division_names))
                    $data_node['division_error'] = 'This division does not exist in system or in this trust.';
                if (in_array($data_node['name'], $directorate_names))
                    $data_node['directorate_error'] = 'This directorate already exist in this division.';
                if (in_array($data_node['name'], $ind_directorate_name) && $data_node['division'] == 'N/A')
                    $data_node['independent_directorate_error'] = 'This directorate already exist in the system.';
                if ($data_node['service'] != 'Yes' && $data_node['service'] != 'No')
                    $data_node['service_error'] = 'Invalid value for service.';
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('directorate.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
/*        $request->merge(['data' => $records,'trust_id'=>auth()->user()->trust_id]);*/
        $request->merge(['data' => $records,'trust_id'=>session('activeTrust')]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('directorate'). 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-directorate', $result->error);
            return redirect('/directorates');
        } else {
            Session::flash('success-directorate', $result->success);
            return redirect('/directorates');
        }
    }

}
