<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class SpecialityPrioritySetController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function store(Request $request) {
        $data = [];
        for ($i = 0; $i < count($request->level); $i++) {
            $data[]= ['id'=>(isset($request->id[$i])?$request->id[$i]:0),'priority_set_id'=>$request->priority_set_id,'level'=>$request->level[$i],'from'=>$request->from[$i],'priority_id'=>$request->priority_id[$i],'to'=>$request->to[$i]];
        }
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('speciality-priority-set') . 'store-update', ['form_params' => $data]);
        $result = json_decode($response->getBody()->getContents());
        if(isset($result->success)){
            Session::flash('success-priority', $result->success);
            return Redirect::back();
        }else{
            Session::flash('error-priority', 'Could not add priority set.');
            return Redirect::back();
        }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('speciality-priority-set') . $id);
        $result = json_decode($response->getBody()->getContents());
    }

}
