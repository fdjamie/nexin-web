<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Helpers;
use Illuminate\Support\Facades\Redirect;

class UserRolesController extends Controller
{


    private $trustId;

    public function __construct()
    {
        $this->trustId = session('activeTrust');
    }

    public function index()
    {
        $trustRoles = array();
        $errors = array();

        $getTrustRoles=$this->getTrustRoles();
        if ($getTrustRoles['status']) {
            $result = $getTrustRoles['apiResponseData'];
            if (!$result->status) {

                $errors = $result->msg;

            } else {
                $trustRoles = $result->data;

            }
        } else {
            $errors = [trans('messages.error')];

        }

        return view('user-roles.user_roles', compact('trustRoles', 'errors'));


    }

    private function getTrustRoles()
    {
        $apiUrl = Helpers::getAPIUrl('get-trust-roles') . "/" . $this->trustId;
        return $getTrustRoles = __get($apiUrl);
    }

    public function create()
    {


        return view('user-roles.add_user_roles');
    }


    public function store(Request $request)
    {

        $this->validate($request, ['name' => 'required']);

        $apiUrl = Helpers::getAPIUrl('store-user-role');
        $paramArr = array();
        $paramArr['name'] = $request->name;
        $paramArr['trust_id'] = $this->trustId;
        $storeTrustRole = __post($apiUrl, $paramArr);
        if ($storeTrustRole['status']) {
            $result = $storeTrustRole['apiResponseData'];
            if (!$result->status) {
                return Redirect::back()->withErrors($result->msg)->withInput();
            } else {

                return redirect('/user-roles')->with('success', 'Role ' . $result->msg);
            }
        } else {
            return Redirect::back()->withErrors([trans('messages.error')])->withInput();

        }

    }


    public function rolePermissions($id){

        $modules=array();
        $trustRoles = array();
        $permissions=array();
        $errors = array();
        $trustRoleId=$id;
        $apiUrl = Helpers::getAPIUrl('get-module-permissions');
        $apiUrl=$apiUrl.'/'.$id;
        $get = __get($apiUrl);

        if ($get['status']) {
            $result = $get['apiResponseData'];
            if (!$result->status) {

                $errors = $result->msg;

            } else {


                $modules=@$result->data->modules;
                $permissions=@$result->data->permissions;
                $rolePermissions=@$result->data->trustRolePermissions;
                $getTrustRoles=$this->getTrustRoles();
                if ($getTrustRoles['status']) {
                    $result = $getTrustRoles['apiResponseData'];
                    if ($result->status) {
                        $trustRoles = $result->data;
                    }
                }

            }
        } else {
            $errors = [trans('messages.error')];

        }

        return  view('user-roles.permissions', compact('modules','permissions','trustRoleId' ,'rolePermissions','errors','trustRoles'));




    }

    public function storeRolePermissions(Request $request){


        $this->validate($request, ['trustRoleId' => 'required']);
        $apiUrl = Helpers::getAPIUrl('store-role-permission');
        $paramArr = array();
        $paramArr=$request->all();
        $storePerm = __post($apiUrl, $paramArr);
         if ($storePerm['status']) {
            $result = $storePerm['apiResponseData'];
            if (!$result->status) {
                return Redirect::back()->with('error',$result->msg)->withInput();
            } else {

                return Redirect::back()->with('success', 'Permissions Saved Successfully');
            }
        } else {
            return Redirect::back()->with('error',[trans('messages.error')])->withInput();

        }

    }


}
