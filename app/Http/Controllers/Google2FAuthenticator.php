<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;
use Hash;
use App\LoginSecurity;

class Google2FAuthenticator extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function show2faForm(Request $request){

        $user = Auth::user();
        $google2fa_url = "";
        if($user->google2Fauth){

            $google2fa = app('pragmarx.google2fa');
            $google2fa->setAllowInsecureCallToGoogleApis(true);
            $google2fa_url = $google2fa->getQRCodeGoogleUrl(
                'Nexin',
                $user->email,
                $user->google2Fauth->secret
            );
        }
        $data = array(
            'user' => $user,
            'google2fa_url' => $google2fa_url
        );
        return view('google-2f.google2fAuth')->with('data', $data);
    }


    public function generate2faSecret(Request $request){
        $user = Auth::user();
        // Initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');
        // Add the secret key to the registration data
        LoginSecurity::create([
            'user_id' => $user->id,
            'enable' => 0,
            'secret' => $google2fa->generateSecretKey(),
            'type'=>'authApp'
        ]);

        return redirect('/google-2f-authenticate')->with('success',trans('messages.keyGenerated'));
    }

    public function enable2fa(Request $request){

        $user =   Auth::user();

        $google2fa = app('pragmarx.google2fa');
        $secret = $request->input('verify-code');

        $valid = $google2fa->verifyKey($user->google2Fauth->secret, $secret,'1');

        if($valid){
            $user->google2Fauth->enable = 1;
            $user->google2Fauth->save();

            $session=array();
            $session['auth_passed']=true;
            $session['auth_time']=Carbon::now();
            session(['google2fa'=>$session]);
           /* return redirect('google-2f-authenticate')->with('success',"2FA is Enabled Successfully.");*/
            return redirect('account-security')->with('success',trans('messages.enabled'));
        }else{
            /*return redirect('google-2f-authenticate')->with('error',"Invalid Verification Code, Please try again.");*/
            return redirect('google-2f-authenticate')->with('error',trans('messages.codeIncorrect'));
        }
    }


    public function disable2fa(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error",trans('messages.passwordNotMatch'));
        }


        $user =   Auth::user();
       /* $user->google2Fauth->delete();*/
        $user->google2Fauth->enable =0;
         $user->google2Fauth->save();
        return redirect('account-security')->with('success',trans('messages.disabled'));
    }

}
