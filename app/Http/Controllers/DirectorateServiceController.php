<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\GetParametersArray;
use Carbon\Carbon;
use App\Http\Traits\ServiceRequirmentColor;

class DirectorateServiceController extends Controller {

    use GetParametersArray,
        ServiceRequirmentColor;

    private $api_token, $api_url, $api_client,$module;


    public function __construct() {
        $this->module=config('module.general-services');
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        __authorize($this->module,'view',true);
        $directorates=array();
        $apiUrl=Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/service';
        $getDirectorateServc=__get($apiUrl);
        if($getDirectorateServc['status']) {
            $directorates=$getDirectorateServc['apiResponseData'];
        }
        else {

            Session::flash('error-service', trans('messages.error'));
        }
        return view('directorate-service.allServices', compact('directorates'));


    }

    public function create() {
        __authorize($this->module,'add',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $categories = $sub_categories = $specialities = [];
        if (count($directorates)) {
            $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorates[0]->id, 'directorate_id');
            $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        }
        if (count($categories))
            $sub_categories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $categories[0]->id, 'category_id');
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/service');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        $categories = Helpers::getDropDownData($categories);
        $sub_categories = Helpers::getDropDownData($sub_categories);
        return view('directorate-service.createService', compact('directorates', 'sub_categories', 'categories', 'specialities'));
    }

    public function store(Request $request) {
        __authorize($this->module,'add',true);
        $apiUrl=rtrim(Helpers::getAPIUrl('directorate-service'), '/');
        $paramArr=$request->all();
        /*echo '<pre>';
        echo "$apiUrl\n";
        print_r($paramArr);
        exit;*/
        $store=__post($apiUrl,$paramArr);
        if($store['status'])
        {
            $result=$store['apiResponseData'];

            if (isset($result->error) && !empty($result->error)) {
                Session::flash('error-service', $result->error);
                return Redirect::back()->withInput();
            } else {
                Session::flash('success-service', $request->name . ', directorate Service added sucessfully.');
                return redirect('/directorate-services');
            }
        }
        else
        {
            return Redirect::back()->with('error-service',[[(trans('messages.error'))]])->withInput();

        }



    }

    public function edit($id) {
        __authorize($this->module,'edit',true);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-service') . $id);
        $service = json_decode($response->getBody()->getContents());
        $service_specialties = [];
        if (isset($service->specialities))
            $service_specialties = $service->specialities;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $service->directorate_id, 'directorate_id');
        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $service->directorate_id, 'directorate_id');
        $category_id = isset($categories[0]->id) ? $categories[0]->id : 0;
        $sub_categories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', ($service->category_id != 0 ? $service->category_id : $category_id), 'category_id');
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/service');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        $categories = Helpers::getDropDownData($categories);
        $sub_categories = Helpers::getDropDownData($sub_categories);
        return view('directorate-service.editService', compact('directorates', 'sub_categories', 'categories', 'service', 'specialities', 'service_specialties'));
    }

    public function update(Request $request, $id) {
        __authorize($this->module,'edit',true);
        if (!isset($request->specialities))
            $request->merge(['specialities' => null]);
        if (!isset($request->contactable))
            $request->merge(['contactable' => 0]);
        if (!isset($request->min_standards))
            $request->merge(['min_standards' => 0]);
        $apiUrl=Helpers::getAPIUrl('directorate-service') . $id;
        $paramArr=$request->all();
        $updateDirectory=__put($apiUrl,$paramArr);
        if($updateDirectory['status'])
        {
            $result=$updateDirectory['apiResponseData'];
            if (isset($result->error) && count($result->error)) {
                Session::flash('error-service', $result->error);
                return Redirect::back();
            } else {
                Session::flash('success-service', $result->updated);
                return redirect('/directorate-services');
            }
        }
        else
        {
            return Redirect::back()->with('error-service',[[(trans('messages.error'))]])->withInput();

        }




    }

    public function destroy($id) {
        __authorize($this->module,'delete',true);
        $apiUrl=Helpers::getAPIUrl('directorate-service') . $id;
        $deleteDirectorateService=__delete($apiUrl);
        if($deleteDirectorateService['status']) {
            $result=$deleteDirectorateService['apiResponseData'];
            if (isset($result->error)) {
                Session::flash('error-service', $result->error);
                return Redirect::back();
            } else {
                Session::flash('success-service','directorate Service Deleted sucessfully.');
                return redirect('/directorates');
            }
        }
        else {
            return Redirect::back()->with('error-service',trans('messages.error'));

        }
    }

    public function import(Request $request) {
        __authorize($this->module,'add',true);
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $data = ExcelHelper::importExcelNew(Input::file('file'), 0);
        /*echo '<pre>';
        print_r($data);
        exit;*/
        if ($data[0] == null) {
            return Redirect::back()->withErrors('No data found in uploaded file.');
        }
        $view_data = $template = $events = $formated_data = [];
        $formated_data = $this->getFormatedData($data);
        $view_data = $this->getViewData($formated_data);
        $file_name = Input::file('file')->getClientOriginalName();
        return view('directorate-service.viewUploadedData', compact('view_data', 'file_name'));
    }

    public function export() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('deleted') . 'directorate-service/' . auth()->user()->trust_id);
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesFromMainNode($directorates, 'speciality');
        $speciality_array = $this->getSearchArray($directorates);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $sites = $trust->site;
        list($data, $ref_data, $directorate_array, $categories, $subcategories, $services, $specialities, $master_data) = $this->getExportData($directorates, $speciality_array, $sites);
        ExcelHelper::exportExcel('Directorate_services_backup', $data, $ref_data, $master_data);
        Session::flash('success-service', 'Directorates services data exported successfully.');
        return redirect('/directorate-services');
    }

    public function upload(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    //$reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $data = $this->getFormatedData($records);
        $request->merge(['trust_id' => auth()->user()->trust_id, 'data' => $data]);
        /*echo json_encode($request->all());
        exit;*/
        /*echo '<pre>';
        print_r($request->all());
        print_r(Helpers::getAPIUrl('directorate-service') . 'upload');
        exit;*/
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('directorate-service') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-service', $result->error);
            return redirect('/directorate-services');
        } else {
            Session::flash('success-service', $result->success);
            return redirect('/directorate-services');
        }
    }

    public function importReferenceData(Request $request) {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $reference_data = $this->getReferenceData(ExcelHelper::importExcelNew(Input::file('file'), 1));
        $master_data = $this->getMasterData(ExcelHelper::importExcelNew(Input::file('file'), 2));
        list($view_data, $master_error) = $this->validatedDataFromMasterData($reference_data, $master_data);
        $file_name = Input::file('file')->getClientOriginalName();
        $flag = true;
        return view('directorate-service.viewUploadedData', compact('view_data', 'file_name', 'master_data', 'flag'));
    }

    public function uploadReferenceData(Request $request) {
        $reference_data = Excel::selectSheetsByIndex(1)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $master_data = Excel::selectSheetsByIndex(2)->load(Helpers::getFile('uploads', $request->file_name), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['master_data' => $master_data, 'reference_data' => $reference_data, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('directorate-service') . 'reference-data/upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        $messages = [$result->error, $result->success];
        Session::flash('info-service', $messages);
        return redirect('/directorate-services');
    }

    public function viewSerivceTemplate($id) {

        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-service') . $id);
        $service = json_decode($response->getBody()->getContents());

        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $locations = app('App\Http\Controllers\HomeController')->getTrustLocations($trust->site);
        $color = '#6BA5C1';
        $events = [];

        if (!is_null($service->template) && count(($service->template))) {
            // dd($service->template);
            $events = app('App\Http\Controllers\HomeController')->getServiceEvents($service, $locations, $color);
            Session::forget('service-template-create');
        } else {
            Session::flash('service-template-create', 'service template not found, add service template first.');
        }
        $start_date = (isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? $service->template->start_date : date('Y-m-d');
        $end_date = (isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? $service->template->end_date : date('Y-m-d');
        $cycle_length = (isset($service->template->cycle_length) ) ? $service->template->cycle_length : 1;
        $starting_week = (isset($service->template->starting_week) ) ? $service->template->starting_week : 1;

        $calendar = app('App\Http\Controllers\HomeController')->fullCalanderJs($events, 'view');
        return view('directorate-service.viewServiceTemplate', compact('service', 'cycle_length', 'start_date', 'end_date', 'starting_week', 'calendar'));
    }

    public function createSerivceTemplate($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-service') . $id);
        $service = json_decode($response->getBody()->getContents());
        if (!is_null($service->template) && count($service->template)) {
            return redirect('/directorate-service/template/edit/' . $id);
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $sites = Helpers::getDropDownData($trust->site);
        return view('directorate-service.addServiceTemplate', compact('service', 'sites'));
    }

    public function storeSerivceTemplate(Request $request) {
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('directorate-service/template'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return ([$result]);
    }

    public function editSerivceTemplate($id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-service') . $id);
        $service = json_decode($response->getBody()->getContents());
        if (is_null($service->template) || !count($service->template)) {
            return redirect('/directorate-service/template/add/' . $id);
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $sites = Helpers::getDropDownData($trust->site);
        $start_date = (isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? $service->template->start_date : date('Y-m-d');
        $end_date = (isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? $service->template->end_date : date('Y-m-d');
        $cycle_length = (isset($service->template->cycle_length) ) ? $service->template->cycle_length : 1;
        $starting_week = (isset($service->template->starting_week) ) ? $service->template->starting_week : 1;
        return view('directorate-service.editServiceTemplate', compact('sites', 'service', 'cycle_length', 'starting_week', 'start_date', 'end_date'));
    }

    public function updateSerivceTemplate(Request $request) {
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('directorate-service/template') . $request->service_id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return ([$result]);
    }

    public function destroyServiceTemplate($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('directorate-service/template') . $id);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->deleted)) {
            Session::flash('error-service', $result->deleted);
            return redirect('/directorate-services');
        } else {
            Session::flash('error-service', $result->not_found);
            return redirect('/directorate-services');
        }
    }

    public function getSerivceTemplate(Request $request) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-service') . $request->get('serviceId'));
        $result = json_decode($response->getBody()->getContents());
        return response()->json($result);
    }

    public function requirement($id) {
        $parameters = $operators = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('service') . 'get-operators-value');
        $operator = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-service') . $id);
        $service = json_decode($response->getBody()->getContents());
        $directorate_id = $service->directorate_id;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'requirement/get-parameter/test/' . $directorate_id);
        $directorate = json_decode($response->getBody()->getContents());
        $operators = $this->getOperator($operator);
        $parameters = $this->getParametersArray($directorate->parameters);
        $values = Helpers::getAllNodesByIdFromMainNode($directorates, 'role', $directorate_id, 'directorate_id');
        $values = Helpers::getDropDownData($values);
        $status = ['present' => 'Present', 'mostly_present' => 'Mostly Present', 'contactable' => 'Contactable'];
        $red_requirements = $orange_requirements = $yellow_requirements = $green_requirements = $blue_requirements = [];
        if (isset($service->requirement) && count($service->requirement)) {
            $red_requirements = $this->getRequirementByColor($service->requirement, 'red', $directorate_id, $operator, $directorate->parameters);
            $orange_requirements = $this->getRequirementByColor($service->requirement, 'orange', $directorate_id, $operator, $directorate->parameters);
            $yellow_requirements = $this->getRequirementByColor($service->requirement, 'yellow', $directorate_id, $operator, $directorate->parameters);
            $green_requirements = $this->getRequirementByColor($service->requirement, 'green', $directorate_id, $operator, $directorate->parameters);
            $blue_requirements = $this->getRequirementByColor($service->requirement, 'blue', $directorate_id, $operator, $directorate->parameters);
        }
        return view('directorate-service.serviceRequirment', compact('parameters', 'operators', 'values', 'status', 'id', 'service', 'red_requirements', 'orange_requirements', 'yellow_requirements', 'green_requirements', 'blue_requirements', 'directorate_id'));
    }

    public function storeRequirement(Request $request) {
        $requiremetns['red'] = $request->red;
        $requiremetns['orange'] = $request->orange;
        $requiremetns['yellow'] = $request->yellow;
        $requiremetns['green'] = $request->green;
        $requiremetns['blue'] = $request->blue;
        $store = [];
        foreach ($requiremetns as $key => $requirement) {
            for ($index = 0; $index < count($requirement['number']); $index++) {
                if ($requirement['parameter'][$index] != 'select')
                    $store[] = [
                        'service_id' => $request->service_id,
                        'color' => $key,
                        'parameter' => $requirement['parameter'][$index],
                        'operator' => $requirement['operator'][$index],
                        'value' => $requirement['value'][$index],
                        'value_2' => (isset($requirement['value_2'][$index])) ? $requirement['value_2'][$index] : '',
                        'priority' => (isset($requirement['priority'][$index]) ? $requirement['priority'][$index] : 0),
                        'status' => $requirement['status'][$index],
                        'number' => $requirement['number'][$index],
                        'id' => (isset($requirement['id'][$index]) ? $requirement['id'][$index] : 0)
                    ];
            }
        }
        /*echo '<pre>';
        print_r($store);
        print_r(rtrim(Helpers::getAPIUrl('directorate-service'), '/') . '/requirement');
        exit;*/
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('directorate-service'), '/') . '/requirement', ['form_params' => $store]);
        $result = json_decode($response->getBody()->getContents());
        // dd($result);
        Session::flash('success-requirement', $result->success);
        return Redirect::back();
    }

    public function destroyRequirement($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('directorate-service') . 'requirement/' . $id);
        $result = json_decode($response->getBody()->getContents());
        return [$result];
    }

//####################################AJAX CALLS###################################

    public function getValues($value, $d_id) {
        $additionalParameters = $this->getValue($value, $d_id);
        return $additionalParameters;
    }

    public function getServiceDropdown($d_id, $c_id, $sb_id) {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/service');
        $directorates = json_decode($response->getBody()->getContents());
        $allservices = Helpers::getAllNodesByIdFromMainNode($directorates, 'services', $d_id, 'directorate_id');
        $services = Helpers::getDropDownData($allservices);
        if ($c_id != 'null' && $sb_id == 'null')
            $services = $this->filterService($c_id, null, $allservices);
        else if ($c_id != 'null' && $sb_id != 'null')
            $services = $this->filterService($c_id, $sb_id, $allservices);
        return response()->json($services);
    }

//########################## PROTECTED FUNCTIONS ############################

    protected function getService($record) {
        $data = [];
        $data['directorate'] = isset($record['directorate']) ? $record['directorate'] : "n/a";
        $data['category'] = isset($record['category']) ? $record['category'] : "n/a";
        $data['subcategory'] = isset($record['subcategory']) ? $record['subcategory'] : "n/a";
        $data['name'] = isset($record['name']) ? $record['name'] : "n/a";
        $data['contactable'] = isset($record['contactable']) ? $record['contactable'] : "n/a";
        $data['min_standards'] = isset($record['min_standards']) ? $record['min_standards'] : "n/a";
        $data['specialities'] = (isset($record['specialities']) ? $record['specialities'] : null);
        return $data;
    }

    protected function getTemplate($record) {

        $data = NULL;
        if ((
                isset($record['start_date']) &&
                isset($record['start_date']) &&
                isset($record['cycle_length']) &&
                isset($record['starting_week'])
                )) {
            $start_date = new Carbon($record['start_date']);
            $end_date = ($record['end_date'] == '0000-00-00' ? '0000-00-00' : new Carbon($record['end_date']));
            $data['start_date'] = $start_date->format('Y-m-d');
            $data['end_date'] = (is_object($end_date) ? $end_date->format('Y-m-d') : $end_date);
            $data['cycle_length'] = $record['cycle_length'];
            $data['starting_week'] = $record['starting_week'];
        };
        return $data;
    }

    protected function getEvent($record) {
        $event = [];
        if (
                isset($record['week_no']) &&
                isset($record['day']) &&
                isset($record['start']) &&
                isset($record['duration']) &&
                isset($record['site']) &&
                isset($record['locations'])
        ) {
            $start = new Carbon($record['start']);
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['day'];
            $event['start'] = $start->format('H:i');
            $event['duration'] = $record['duration'];
            $event['site'] = $record['site'];
            if (isset($record['locations']))
                $event['locations'] = $record['locations'];
        }
        return $event;
    }

    protected function getCheckData() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $categories = Helpers::getAllNodesFromMainNode($directorates, 'category');
        $subcategories = Helpers::getAllNodesFromMainNode($categories, 'sub_category');
        $directorate_array = $this->getMainnodeArray($directorates);
        $directorate_service = $this->getSubnodesArray($directorates, 'services');
        $speciality_array = $this->getSubnodesArray($directorates, 'speciality');
        $category_array = $this->getSubnodesArray($directorates, 'category');
        $category_service = $this->getSubnodesArray($categories, 'directorate_service');
        $subcategory_array = $this->getSubnodesArray($categories, 'sub_category');
        $subcategory_servie = $this->getSubnodesArray($subcategories, 'directorate_service');

        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $site_array = $this->getMainnodeArray($sites);
        $location_array = $this->getSubnodesArray($sites, 'location');
        return [$directorate_array, $directorate_service, $speciality_array, $site_array, $location_array, $category_array, $category_service, $subcategory_array, $subcategory_servie];
    }

    protected function getSubnodesArray($nodes, $subnode) {
        $result = $subresult = [];
        foreach ($nodes as $node) {
            if (isset($node->$subnode) && count($node->$subnode)) {
                foreach ($node->$subnode as $child) {
                    $result[$node->name][$child->name] = $child->name;
                }
            }
        }
        return $result;
    }

    protected function getMainnodeArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[$node->name] = $node->name;
        }
        return $result;
    }

    protected function getFormatedData($data) {
        $formated_data = [];
        $previous_service_name = NULL;
        $i = -1;
        /*echo '<pre>';
        print_r($data);
        exit;*/
        foreach ($data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $formated_data[$i] = $this->getService($data_node);
                $formated_data [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEvent($data_node);
            if ($formated_data [$i]['template'] != NULL && $event != NULL) {
                $formated_data [$i]['template']['events'][] = $event;
            }
        }
        return $formated_data;
    }

    protected function getViewData($data) {
        $view_data = [];
        list($directorate_array, $directorate_service, $speciality_array, $site_array, $location_array, $category_array, $category_service, $subcategory_array, $subcategory_servie) = $this->getCheckData();
        $i = 0;
        foreach ($data as $data_node) {
            $i++;
            if (!array_search($data_node['directorate'], $directorate_array))
                $data_node['directorate_error'] = "This directorate do not exist in the system or in this trust.";
            if (isset($data_node['specialities']) && $data_node['specialities'] != '') {
                $speciality_names = explode(',', $data_node['specialities']);
                foreach ($speciality_names as $speciality_node) {
                    if (!array_search($speciality_node, $speciality_array[$data_node['directorate']]))
                        $data_node['speciality_error'][] = $data_node['name'] . ' , "' . $speciality_node . '" speciality do not exist in system or in this Directorate.';
                }
            }
            if ($data_node['category'] != 'N/A') {
                if (isset($category_array[$data_node['directorate']]) && (!array_search($data_node['category'], $category_array[$data_node['directorate']])))
                    $data_node['category_error'] = "This category do not belong to this directorate or do not exist in the system.";
                if ($data_node['subcategory'] != 'N/A') {
                    if (isset($subcategory_array[$data_node['category']]) && (!array_search($data_node['subcategory'], $subcategory_array[$data_node['category']])))
                        $data_node['subcategory_error'] = "This subcategory do not belong to this category or do not exist in the system.";
                    if (isset($subcategory_servie[$data_node['subcategory']]) && (array_search($data_node['name'], $subcategory_servie[$data_node['subcategory']])))
                        $data_node['service_error'] = "This service already exist in this subcategory.";
                }else {
                    if (isset($category_service[$data_node['category']]) && (array_search($data_node['name'], $category_service[$data_node['category']])))
                        $data_node['service_error'] = "This service already exist in this category.";
                }
            }else {
                if (isset($directorate_service[$data_node['directorate']]) && (array_search($data_node['name'], $directorate_service[$data_node['directorate']])))
                    $data_node['service_error'] = "This service already exist in this directorate.";
            }

            if ($data_node['contactable'] != 'Yes' && $data_node['contactable'] != 'No')
                $data_node['contactable_error'] = "Invalid value for contactable.";
            if ($data_node['min_standards'] != 'Yes' && $data_node['min_standards'] != 'No')
                $data_node['standards_error'] = "Invalid value for min standards.";
            if (isset($data_node['template']['cycle_length']) && $data_node['template']['cycle_length'] <= 0)
                $data_node['cycle_length_error'] = "Invalid value for cycle length, Cycle length value should be greater then 0";
            if (isset($data_node['template']['starting_week']) && ($data_node['template']['starting_week'] <= 0 || $data_node['template']['starting_week'] > $data_node['template']['cycle_length']))
                $data_node['starting_week_error'] = "Invalid value for Starting week, Starting Week value should be greater then 0 and less or equal to cycle length";
            if (isset($data_node['template']['events']) && count($data_node['template']['events']) > 1) {
                $j = 1;
                foreach ($data_node['template']['events'] as $event) {
                    if ($event['week_no'] <= 0 || $event['week_no'] > $data_node['template']['cycle_length'])
                        $data_node['events_error']['week_no'][] = $data_node['name'] . ', event ' . $j . ' week no has invalid value.';
                    if ($event['day'] < 0 || $event['day'] > 6)
                        $data_node['events_error']['day'][] = $data_node['name'] . ', event ' . $j . ' day has invalid value, day value be between 0 to 6.';
                    if (!is_numeric($event['duration']))
                        $data_node['events_error']['duration'][] = $data_node['name'] . ', event ' . $j . ' duration has invalid value, duration should be a numeric value.';
                    if (!array_search($event['site'], $site_array))
                        $data_node['events_error']['site'][] = $data_node['name'] . ', event ' . $j . ' "' . $event['site'] . '"  do not exist in the system or in this trust.';

                    $event_locations = explode(',', $event['locations']);
                    foreach ($event_locations as $location_node) {
                        if ($location_node != '') {
                            if (isset($data_node['site']) && isset($location_array[$data_node['site']]) && !array_search($location_node, $location_array[$data_node['site']]))
                                $data_node['events_error']['location'][] = $data_node['name'] . ', event ' . $j . ' "' . $location_node . '" do not exist in the system or in this site.';
                        }
                    }
                    $j++;
                }
            }
            $view_data [] = $data_node;
        }

        return $view_data;
    }

    protected function filterService($cid, $sid = null, $services) {
        $result = [];
        foreach ($services as $service) {
            if ($sid != null) {
                if ($service->sub_category_id == $sid)
                    $result[$service->id] = $service->name;
            } else if ($service->category_id == $cid) {
                $result[$service->id] = $service->name;
            }
        }
        return $result;
    }

    protected function getSearchArray($parentnodes) {
        $result = [];
        foreach ($parentnodes as $nodes) {
            foreach ($nodes->speciality as $node) {
                $result [$nodes->id][$node->name] = $node->id;
            }
        }

        return $result;
    }

    protected function getExportData($directorates, $speciality_array, $sites) {
        $data[] = ['directorate', 'category', 'subcategory', 'name', 'contactable', 'min_standards', 'specialities', 'start_date', 'end_date', 'cycle_length', 'starting_week', 'week_no', 'day', 'start', 'duration', 'end_time', 'end_day', 'site', 'locations'];
        $ref_data[] = ['id', 'directorate', 'category', 'subcategory', 'name', 'contactable', 'min_standards', 'specialities', 'operations'];
        $master_data[] = ['directorate', 'category', 'subcategory', 'name', 'specialities', 'contactable', 'min_standards', 'operations'];
        $min_standard = $contactable = ['yes', 'no'];
        $operations = ['create', 'update', 'delete', 'restore'];
        $directorate_array = $categories = $subcategories = $services = $specialities = $count = [];
        foreach ($directorates as $directorate) {
            $directorate_array[] = $directorate->name;
            $site_name = null;
            if (isset($directorate->services) && count($directorate->services)) {
                foreach ($directorate->services as $service) {
                    $services[] = $service->name;
                    $specialties_name = [];
                    if (isset($service->specialities) && count($service->specialities)) {
                        foreach ($service->specialities as $speciality_node) {
                            if (array_search($speciality_node, $speciality_array[$directorate->id]))
                                $specialties_name[] = array_search($speciality_node, $speciality_array[$directorate->id]);
                        }
                    }
                    $ref_data[] = [$service->id, $directorate->name, ($service->category_id != 0 ? $service->category->name : 'N/A'), ($service->sub_category_id != 0 && !is_null($service->sub_category) ? $service->sub_category->name : 'N/A'), $service->name, ($service->contactable ? 'Yes' : 'No'), ($service->min_standards ? 'Yes' : 'No'), implode(',', $specialties_name), (!is_null($service->deleted_at) ? 'deleted' : '')];
                    if (isset($service->template->events) && count($service->template->events)) {
                        foreach ($service->template->events as $event) {
                            $time = ($event->duration / 60) + $event->start;
                            $end = date('H:i', mktime(0, $time * 60));
                            $end_day = ($time > 24 ? $event->day + 1 : $event->day);
                            $location = '';
                            $location_array = [];
                            foreach ($sites as $site) {
                                if ($event->site_id == $site->id) {
                                    $site_name = $site->name;
                                    $location_array = Helpers::getDropDownData($site->location);
                                    if (isset($event->location_id)) {
                                        foreach ($event->location_id as $location_id) {
                                            $location = $location . $location_array[$location_id] . (($location_id != end($event->location_id)) ? ',' : '');
                                        }
                                    }
                                }
                            }
                            array_push(
                                $data,
                                [
                                    $directorate->name,
                                    (($service->category_id == 0) ? 'N/A' : $service->category->name),
                                    (($service->sub_category_id == 0 || is_null($service->sub_category)) ? 'N/A' : $service->sub_category->name),
                                    $service->name, ($service->contactable == 1 ? 'Yes' : 'No'),
                                    ($service->min_standards == 1 ? 'Yes' : 'No'),
                                    implode(',', $specialties_name),
                                    (isset($service->template->start_date) ? $service->template->start_date : 'N/A'),
                                    (isset($service->template->end_date) ? $service->template->end_date : 'N/A'),
                                    (isset($service->template->cycle_length) ? $service->template->cycle_length : 'N/A'),
                                    (isset($service->template->starting_week) ? $service->template->starting_week : 'N/A'),
                                    $event->week_no,
                                    $event->day,
                                    $event->start,
                                    $event->duration,
                                    $end, $end_day,
                                    $site_name,
                                    $location
                                ]
                            );
                        }
                    } else {
                        array_push($data, [$directorate->name, (($service->category_id == 0) ? 'N/A' : $service->category->name), (($service->sub_category_id == 0 || is_null($service->sub_category)) ? 'N/A' : $service->sub_category->name), $service->name, ($service->contactable == 1 ? 'Yes' : 'No'), ($service->min_standards == 1 ? 'Yes' : 'No'), implode(',', $specialties_name), '', '', '', '', '', '', '', '', '', '', '', '']);
                    }
                }
                foreach ($directorate->category as $category) {
                    $categories[] = $category->name;
                    foreach ($category->sub_category as $subcategory) {
                        $subcategories[] = $subcategory->name;
                    }
                }
                foreach ($directorate->speciality as $speciality) {
                    $specialities[] = $speciality->name;
                }
            }
        }
        $categories[] = $subcategories[] = 'N/A';
        $count = [count($directorate_array), count($categories), count($subcategories), count($services), count($specialities)];
        for ($i = 0; $i < max($count); $i++) {
            $master_data[] = [
                (isset($directorate_array[$i]) ? $directorate_array[$i] : ''),
                (isset($categories[$i]) ? $categories[$i] : ''),
                (isset($subcategories[$i]) ? $subcategories[$i] : ''),
                (isset($services[$i]) ? $services[$i] : ''),
                (isset($specialities[$i]) ? $specialities[$i] : ''),
                (isset($contactable[$i]) ? $contactable[$i] : ''),
                (isset($min_standard[$i]) ? $min_standard[$i] : ''),
                (isset($operations[$i]) ? $operations[$i] : ''),
            ];
        }
        return [$data, $ref_data, $directorate_array, $categories, $subcategories, $services, $specialities, $master_data];
    }

    protected function getReferenceData($refernce_data) {
        $data = [];
        foreach ($refernce_data as $data_node) {
            if ((isset($data_node['operations']) &&
                    (
                    $data_node['operations'] == 'create' ||
                    $data_node['operations'] == 'update' ||
                    $data_node['operations'] == 'delete' ||
                    $data_node['operations'] == 'restore'))) {
                $data[] = $data_node;
            }
        }
        return $data;
    }

    protected function getMasterData($master_data) {
        $data = [];
        foreach ($master_data as $data_node) {
            if (isset($data_node['directorate']))
                $data['directorates'][] = $data_node['directorate'];
            if (isset($data_node['category']))
                $data['categories'][] = $data_node['category'];
            if (isset($data_node['subcategory']))
                $data['subcategories'][] = $data_node['subcategory'];
            if (isset($data_node['name']))
                $data['services'][] = (isset($data_node['name']) ? $data_node['name'] : null);
            if (isset($data_node['specialities']))
                $data['specialities'][] = $data_node['specialities'];
            if (isset($data_node['min_standards']))
                $data['min_standards'][] = $data_node['min_standards'];
            if (isset($data_node['contactable']))
                $data['contactable'][] = $data_node['contactable'];
            if (isset($data_node['operations']))
                $data['operations'][] = $data_node['operations'];
        }
        return $data;
    }

    protected function validatedDataFromMasterData($reference_data, $master_check) {
        $data = [];
        $master_error = false;
        foreach ($reference_data as $data_node) {
            $specialities = [];
            if (isset($data_node['specialities'])) {
                $specialities = explode(',', $data_node['specialities']);
                foreach ($specialities as $speciality) {
                    if (in_array($speciality, $master_check['specialities']))
                        $data_node['check_speciality'][] = $speciality;
                }
            }
            if (isset($data_node['directorate']) && !in_array($data_node['directorate'], $master_check['directorates'])) {
                $data_node['directorate_error'] = 'This directorate does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['category']) && !in_array($data_node['category'], $master_check['categories'])) {
                $data_node['category_error'] = 'This category does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['subcategory']) && !in_array($data_node['subcategory'], $master_check['subcategories'])) {
                $data_node['subcategory_error'] = 'This subcategory does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['contactable']) && !in_array(strtolower($data_node['contactable']), $master_check['contactable'])) {
                $data_node['contactable_error'] = 'Contactable value does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['min_standards']) && !in_array(strtolower($data_node['min_standards']), $master_check['min_standards'])) {
                $data_node['standards_error'] = 'Min standards value does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['operations']) && !in_array(strtolower($data_node['operations']), $master_check['operations'])) {
                $data_node['operations_error'] = 'Operations value does not exit in Master Data';
                $master_error = true;
            }
            if (isset($data_node['name']) && !in_array($data_node['name'], $master_check['services'])) {
                $data_node['service_error'] = 'this service does not exit in Master Data';
                $master_error = true;
            }
            $data[] = $data_node;
        }
        return [$data, $master_error];
    }

}
