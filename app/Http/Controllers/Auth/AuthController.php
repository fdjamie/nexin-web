<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use Auth;
use App\Helpers\LoginAuthenticationHelper;
use App\Http\Helpers;
use Session;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * It will save Login user ip browser and device from which user login and will check if user for the given details authenticated or not
     * if don't want to save login detail and authenticate user then just make it false.
     *
     */
    private $saveLoginDetailForAnthentication=true;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:tb_user',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'api_token' => str_random(60)
        ]);
    }

    public function showRegistrationForm() {
        abort(403);
    }

    public function login(\Illuminate\Http\Request $request) {
        $client = new \GuzzleHttp\Client();
        try{

            $response = $client->request("POST", config('app.API_URL') . rtrim(config('apis.login'), '/'), ['form_params' => $request->all()]);

            $result = json_decode($response->getBody()->getContents());
        }catch (\Exception $e)
        {

            Log::warning('Login : '.$e);
            return Redirect::back()->withErrors(['apiError'=>['Error Occur.Try Again!']])->withInput(Input::all());
        }

        if ($result->error == 'yes') {
            return Redirect::back()->withErrors($result->messages)->withInput(Input::all());
        } else {

            if (is_null(session()->get('date_time'))) {
                session()->put('date_time', new Carbon());
                session()->put('date', new Carbon());
            }

            Auth::loginUsingId($result->user_id);
            /* *ADDED BY WILLIAM FOR ATHENTICATION IP BROWSER   */
            if($this->saveLoginDetailForAnthentication){
                LoginAuthenticationHelper::saveLoginDetail();
            }




            $throttles = $this->isUsingThrottlesLoginsTrait();
            return $this->handleUserWasAuthenticated($request, $throttles);
        }
    }

    public function logout()
    {

       if(!empty(session('emailCodeVerification'))){
            session()->forget('emailCodeVerification');
        }

        if(!empty(session('smsCodeVerification'))){
            session()->forget('smsCodeVerification');
        }
        if(!empty(session('loginDetailId'))){
            session()->forget('loginDetailId');
        }
        session()->flush();
        (new Authenticator(request()))->logout();
        Auth::logout();



        return redirect('/login');


    }



}
