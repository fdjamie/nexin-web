<?php

namespace App\Http\Controllers;

use File;
use Validator;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use MaddHatter\LaravelFullcalendar\Event;
use App\Http\Traits\CategoryTree;
use Auth;


class HomeController extends Controller
{

    use CategoryTree;

    private $api_token, $api_url, $api_client, $trustId;

//  ############################################################################   PROTECTED FUNCTIONS       ############################################################################

    protected function getAPIUrl($api_name)
    {
        if ($api_name === '')
            return config('app.API_URL');
        else
            return config('app.API_URL') . config('apis.' . $api_name);
    }

    public static function getDirectoratesFromDivisions($divisions)
    {
        $directorates = array();
        foreach ($divisions as $division) {
            if (isset($division->directorate) && count($division->directorate)) {
                foreach ($division->directorate as $directorate) {
                    array_push($directorates, $directorate);
                }
            }
        }
        return $directorates;
    }

    public function getRotaTemplateFromDirectorate($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('directorate') . $id);
        $rota = json_decode($response->getBody()->getContents());
        return response()->json($rota->rota_template);
    }

    public function getRotaTemplateWeekCycle($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('rota-template') . $id);
        $rota = json_decode($response->getBody()->getContents());
        return response()->json(count($rota->content->shift_type));
    }

    public function getTrustLocations($sites)
    {
        $trust_locations = array();
        foreach ($sites as $site) {
            foreach ($site->location as $location) {
                $trust_locations[$location->id] = $location->name;
            }
        }
        return $trust_locations;
    }

    protected function getDirectorateSpecialityDropDownArray($directorate_speciality)
    {
        $specialities = array();
        $posts = array();
        foreach ($directorate_speciality as $speciality) {
            if (isset($speciality) && count($speciality)) {
                $specialities[$speciality->id] = $speciality->name;
                if (isset($speciality->post) && count($speciality->post)) {
                    foreach ($speciality->post as $post) {
                        $posts[$post->id] = $post->name;
                    }
                }
            }
        }
        return [$specialities, $posts];
    }

    protected function getSpecialtiesFromDirectorates($directorates)
    {
        $specialties = [];
        foreach ($directorates as $directorate_node) {
            foreach ($directorate_node->speciality as $speciality) {
                if (!is_null($speciality)) {
                    array_push($specialties, $speciality);
                }
            }
        }
        return $specialties;
    }

    protected function getSpecialityFromDirectorate($directorates, $directorate_id)
    {
        $specialties = [];
        foreach ($directorates as $directorate) {
            foreach ($directorate->speciality as $speciality) {
                if (!is_null($speciality) && $speciality->directorate_id == $directorate_id) {
                    array_push($specialties, $speciality);
                }
            }
        }
        return $specialties;
    }

    protected function getPostsFromSpecialities($specialites, $speciality_id)
    {
        $posts = [];
        foreach ($specialites as $speciality) {
            foreach ($speciality->post as $post) {
                if (isset($post) && $post->directorate_speciality_id == $speciality_id)
                    array_push($posts, $post);
            }
        }
        return $posts;
    }

    protected function getEventWeekDiff($starting_week, $event_week, $cycle_length)
    {
        $diff_in_weeks = 0;
        if ($event_week != $starting_week) {
            $diff_in_weeks = $event_week - $starting_week;
            if ($diff_in_weeks < 0) {
                $diff_in_weeks += $cycle_length;
            }
        }
        return $diff_in_weeks;
    }

    protected function getEventLocationsSrting($location_array, $locations)
    {
        $names = array();
        if (count($location_array)) {
            foreach ($location_array as $index) {
                $names[] = $locations[$index];
            }
            return '(' . implode(', ', $names) . ')';
        }
        return '(no location found)';
    }

    public function getServiceEvents($service, $locations, $color)
    {
        $start_date = ((isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? Carbon::createFromFormat('Y-m-d H:i:s', $service->template->start_date . ' 00:00:00') : Carbon::createFromFormat('Y-m-d H:i:s', $service->created_at));
        $end_date = ((isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? Carbon::createFromFormat('Y-m-d H:i:s', $service->template->end_date . ' 23:59:59') : $this->getCarbonYmdFormat(new Carbon('last day of December next year')) . '23:59:59');
        $start_of_week = $start_date->copy()->startOfWeek();
        $events = [];
        while ($start_of_week <= $end_date) {
            if(isset($service->template->events))
            foreach ($service->template->events as $event) {
                $event_date = $start_of_week->copy()->addWeek($this->getEventWeekDiff($service->template->starting_week, $event->week_no, $service->template->cycle_length));
                if ($event->day == 0) {
                    $event_date->endOfWeek();
                } elseif ($event->day > 1) {
                    $event_date->addDays($event->day - 1);
                }
                list($hour, $minute) = explode(":", $event->start);
                $start_time = $event_date->copy();
                $start_time->hour = $hour;
                $start_time->minute = $minute;
                $end_time = $start_time->copy()->addMinutes($event->duration);
                if ($start_time < $start_date || $start_time > $end_date)
                    continue;
                $lnames = '';
                if (isset($event->location_id))
                    $lnames = $this->getEventLocationsSrting($event->location_id, $locations);
                $events[] = Calendar::event(
                    $service->name . ', ' . $lnames, false, //full day event?
                    $start_time->format('Y-m-d H:i'), $end_time->format('Y-m-d H:i'), $event->id, [
                        'color' => $color
                    ]
                );
//                $events[] = Calendar::event([$data]);
            }
            $start_of_week->addWeek($service->template->cycle_length);
        }
        return $events;
    }

    protected function getServiceEventsByLocation($location_id, $services, $locations)
    {
        $end = new Carbon('last day of next year');
        mt_srand(234);
        $events = [];
        foreach ($services as $service) {
            $start = ($service->template->start_date == '0000-00-00' ? $service->created_at : $service->template->start_date . ' 00:00:00');
            $color = $this->random_color();
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start);
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', ((isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? $service->template->end_date . ' 23:59:59' : $end));
            $start_of_week = $start_date->copy()->startOfWeek();
            while ($start_of_week <= $end_date) {
                foreach ($service->template->events as $event) {
                    if (array_intersect($location_id, $event->location_id)) {
                        $event_date = $start_of_week->copy()->addWeek($this->getEventWeekDiff($service->template->starting_week, $event->week_no, $service->template->cycle_length));
                        if ($event->day == 0) {
                            $event_date->endOfWeek();
                        } elseif ($event->day > 1) {
                            $event_date->addDays($event->day - 1);
                        }
                        list($hour, $minute) = explode(":", $event->start);
                        $start_time = $event_date->copy();
                        $start_time->hour = $hour;
                        $start_time->minute = $minute;
                        $end_time = $start_time->copy()->addMinutes($event->duration);
//                return [$start_time->format('Y-m-d H:i'), $end_time->format('Y-m-d H:i')];
                        if ($start_time < $start_date || $start_time > $end_date)
                            continue;

                        $lnames = $this->getEventLocationsSrting($event->location_id, $locations);
                        $events[] = Calendar::event(
                            $service->name . ', ' . $lnames, false, //full day event?
                            $start_time->format('Y-m-d H:i'), $end_time->format('Y-m-d H:i'), $event->id, [
                                'color' => $color
                            ]
                        );
                    }
                }
                $start_of_week->addWeek($service->template->cycle_length);
            }
        }
        return $events;
    }

    protected function getLocationServices($location_id, $data)
    {
        $location_services = [];
        // dd($data);
        foreach ($data as $speciality) {
            if(!empty($speciality->service))
            foreach ($speciality->service as $service) {
                if (!is_null($service->template) && count($service->template)) {
                    foreach ($service->template->events as $event) {
                        if (isset($event->location_id) && array_intersect($location_id, $event->location_id)) {
                            $location_services[] = $service;
                            break;
                        }
                    }
                }
            }
        }
//        return $all_locations[0];
        return $location_services;
    }

    protected function random_color_part()
    {
        return str_pad(dechex(mt_rand(1, 255)), 2, '0', STR_PAD_LEFT);
    }

    protected function random_color()
    {
        return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function fullCalanderJs($events, $type)
    {
        $options = [
            'header' => [
                'left' => 'prev,next',
                'center' => 'title',
//                'center' => '',
                'right' => 'agendaWeek,2Weeks,3Weeks,4Weeks,5Weeks'
            ],
            'views' => [
                '2Weeks' => [
                    'type' => 'agenda',
                    'duration' => ['weeks' => 2],
                    'buttonText' => '2 weeks',
//                            'titleFormat' => '[Week 1 Week 2]',
                ],
                '3Weeks' => [
                    'type' => 'agenda',
                    'duration' => ['weeks' => 3],
                    'buttonText' => '3 weeks',
                    //                          'titleFormat' => '[Week 1 Week 2 Week 3]',
                ],
                '4Weeks' => [
                    'type' => 'agenda',
                    'duration' => ['weeks' => 4],
                    'buttonText' => '4 weeks',
//                            'titleFormat' => '[Week 1 Week 2 Week 3 Week 4]',
                ],
                '5Weeks' => [
                    'type' => 'agenda',
                    'duration' => ['weeks' => 5],
                    'buttonText' => '5 weeks',
//                            'titleFormat' => '[Week 1 Week 2 Week 3 Week 4]',
                ],
            ],
            'firstDay' => 1,
            'defaultView' => (!is_null(session()->get('view')) ? session()->get('view') : 'agendaWeek'),
            'allDaySlot' => false,
            'slotDuration' => '01:00',
            'columnFormat' => ' D ddd ',
            'contentHeight' => 'auto',
            'timeFormat' => 'HH:mm',
            'axisFormat' => 'HH:mm',
        ];
        $calendar = Calendar::setOptions($options);

//        $calendar->setCallbacks([
//            'viewRender' => "function(view, element) {
//                            var id = $('#calendar-service').children('.fc').attr('id');
//                            $('#'+id).fullCalendar( 'gotoDate','".session()->get('date')."' );}"
//        ]);

        if ($type == 'view') {
            $calendar->setCallbacks([
                'viewRender' => "function(view, element) {
                            var id = $('#calendar-service').find('.fc').attr('id');
                            $('#'+id).fullCalendar( 'gotoDate','" . session()->get('date') . "' );}"
            ]);
        }

        if ($type != 'view') {
            $calendar->setCallbacks([//set fullcalendar callback options (will not be JSON encoded)
                'eventClick' => "function(event, element) {
                    console.log(event);
                    $('#edit').modal({show: 'true'});
                    $('#edit_location_id').val(event.location_id);
                    $('#edit_site_id').val(event.site_id);
                    $('#edit_start_time').val(moment(event.start._d).add(-1, 'hours').format('HH:mm'));
                    $('#edit_end_time').val(moment(event.end._d).add(-1, 'hours').format('HH:mm'));
                    $('#event_start_date_edit').val(moment(event.start._d).format('YYYY-MM-DD'));
                    $('#event_end_date_edit').val(moment(event.end._d).format('YYYY-MM-DD'));
                    eventSource = event;
                    getLocationsEdit($('#edit_site_id').val(), eventSource);}",
                'dayClick' => "function(date) {
                    $('#add').modal({show: 'true'});
                    $('#day').val(moment(date._d).format('dddd'));
                    $('#start_time').val(moment(date._d).add(-1, 'hours').format('HH:mm'));
                    $('#end_time').val(moment(date._d).add(-1, 'hours').format('HH:mm'));
                    $('#event_start_date').val(moment(date._d).format('YYYY-MM-DD'));}",
                'eventResizeStop' => "function (event) {
                    $('#calendar-service').fullCalendar('updateEvent', event);}",
            ]);
        }
        $calendar->addEvents($events);
        return $calendar;
    }

    protected function getServicesFromSpecialties($specialties)
    {
        $services = [];
        foreach ($specialties as $speciality) {
            foreach ($speciality->service as $service) {
                if (!is_null($service))
                    array_push($services, $service);
            }
        }
        return $services;
    }

    protected function getServicesFromSpeciality($speciality)
    {
        $services = [];
        foreach ($speciality->service as $service) {
            if (!is_null($service))
                array_push($services, $service);
        }
        return $services;
    }

    protected function gettingArrayOfName($nodes)
    {
        $names = [];
        foreach ($nodes as $node) {
            $names [] = $node->name;
        }
        return $names;
    }

    protected function getCarbonYmdFormat($date)
    {
        $date = new Carbon($date);
        $date = $date->format('Y-m-d');
        return $date;
    }

    protected function getSpecialtiesForShiftDetail($directorates, $directorate_id)
    {
        $specialities [] = ['id' => 0, 'name' => 'Parent Speciality'];
        foreach ($directorates as $directorate) {
            foreach ($directorate->speciality as $speciality) {
                if (!is_null($speciality) && $speciality->directorate_id == $directorate_id)
                    $specialities [] = $speciality;
            }
        }

        return $specialities;
    }

    protected function getServicesForShiftDetail($specialties, $flag, $category, $speciality_id)
    {
        $services = [];
        if ($flag != 0) {
            foreach ($specialties as $speciality) {
                if ($specialties[0] != $speciality) {
                    foreach ($speciality->service as $service) {
                        if (!is_null($service) && $service->category_id == $category && $service->directorate_speciality_id == $speciality_id)
                            $services [] = $service;
                    }
                }
            }
        }
        return $services;
    }

    protected function getDirectorateServices($directorates, $directorate_id, $speciality_id, $category_id, $subcategory_id)
    {
        $services = [];
        foreach ($directorates as $directortae) {
            if ($directortae->id == $directorate_id) {
                foreach ($directortae->services as $service) {
                    $service_speciality = (isset($service->specialities) ? $service->specialities : []);
                    if (
                        (($speciality_id == -1 || $speciality_id == '-1') || in_array($speciality_id, $service_speciality)) &&
                        (
                            (($category_id == null || $category_id == 'null') && ($subcategory_id == null || $subcategory_id == 'null')) ||
                            (($service->category_id == $category_id) && ($subcategory_id == null || $subcategory_id == 'null')) ||
                            (($service->category_id == $category_id) && ($service->sub_category_id == $subcategory_id))
                        )
                    )
                        $services['d-' . $service->id] = $service->name;
                }
            }
        }
        return $services;
    }

    protected function getServices($specialities, $speciality_id, $category_id, $subcategory_id)
    {
        $result = [];
        foreach ($specialities as $speciality) {
            if ($speciality->id == $speciality_id && $speciality->id != 0) {
                foreach ($speciality->service as $service) {
                    if (
                        (($category_id == null || $category_id == 'null') && ($subcategory_id == null || $subcategory_id == 'null')) ||
                        (($service->category_id == $category_id) && ($subcategory_id == null || $subcategory_id == 'null')) ||
                        (($service->category_id == $category_id) && ($service->sub_category_id == $subcategory_id))
                    ) {
                        $result['s-' . $service->id] = $service->name;
                    }
                }
            }
        }
        return $result;
    }

    protected function getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_id, $category_id, $subcategory_id)
    {
        $combined_service = [];
        if ($speciality_id != -1 || $speciality_id != '-1') {
            $combined_service = array_merge($combined_service, $this->getServices($specialities, $speciality_id, $category_id, $subcategory_id));
        } else {
            $combined_service = array_merge($combined_service, $this->getDirectorateServices($directorates, $directorate_id, $speciality_id, $category_id, $subcategory_id));
        }

        return $combined_service;
    }

    protected function getSubCategoriesFromCategory($categories, $category_id)
    {
        $sub_categories = [];
        foreach ($categories as $category) {
            foreach ($category->sub_category as $sub_category) {
                if ($sub_category->category_id == $category_id)
                    array_push($sub_categories, $sub_category);
            }
        }
        return $sub_categories;
    }

    protected function getRotaGroupFromDirectorate($directorates, $directorate)
    {
        $rota_groups = [];
        foreach ($directorates as $directorate_node) {
            foreach ($directorate_node->rota_group as $rota_group) {
                if ($rota_group->directorate_id == $directorate)
                    array_push($rota_groups, $rota_group);
            }
        }
        return $rota_groups;
    }

    protected function getShiftTypesFromRotaGroup($rota_group)
    {
        $shift_types = [];
        foreach ($rota_group->shift_type as $shift_type) {
            if (isset($shift_type))
                array_push($shift_types, $shift_type);
        }
        return $shift_types;
    }

    protected function getRotaTemplatesFromRotaGroup($rota_groups, $rota_group_id)
    {
        $rota_templates = [];
        foreach ($rota_groups as $rota_group) {
            foreach ($rota_group->rota_template as $rota_template) {
                if (isset($rota_template) && $rota_template->rota_group_id)
                    array_push($rota_templates, $rota_template);
            }
        }
        return $rota_templates;
    }

    protected function getRolesFromDirectorate($directorates, $directorate_id)
    {
        $roles = [];
        foreach ($directorates as $directorate) {
            foreach ($directorate->role as $role) {
                if ($role->directorate_id == $directorate_id)
                    array_push($roles, $role);
            }
        }
        return $roles;
    }

    protected function getGradesFromDirectorates($directorates, $directorate_id)
    {
        $grades = [];
        foreach ($directorates as $directorate) {
            foreach ($directorate->grade as $grade) {
                if ($grade->directorate_id == $directorate_id)
                    array_push($grades, $grade);
            }
        }
        return $grades;
    }

    protected function getGradesFromSpecialties($specialties, $speciality_id)
    {
        $grades = [];
        foreach ($specialties as $specialty) {
            foreach ($specialty->additional_grade as $grade) {
                if ($grade->directorate_speciality_id == $speciality_id)
                    array_push($grades, $grade);
            }
        }
        return $grades;
    }

    protected function getGradesPositionsFromSpecialties($specialties, $speciality_id)
    {
        $grades = [];
        foreach ($specialties as $specialty) {
            foreach ($specialty->additional_grade as $grade) {
                if ($grade->directorate_speciality_id == $speciality_id)
                    $grades[] = ['id' => $grade->position, 'name' => $grade->name];
            }
        }
        return $grades;
    }

    protected function exportExcel($file_name, $data, $ref_data)
    {
        $success = Excel::create($file_name . '(' . date('Y-M-d') . ')', function ($excel) use ($data, $ref_data) {
            $excel->sheet('Data', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
            $excel->sheet('Refrence Data', function ($sheet) use ($ref_data) {
                $sheet->fromArray($ref_data, null, 'A1', false, false);
            });
        })->download('xlsx');
        if ($success)
            return true;
        else
            return false;
    }

    protected function importExcel($file)
    {
        $records = Excel::selectSheetsByIndex(0)->load($file, function ($reader) {
            // $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        return $records;
    }

    public static function importFileValidate(array $data)
    {
        $message = [
            'file.required' => 'Please select excel/csv file to upload.',
            'file.mimes' => 'Selected file must be of xls,xlsx or csv type.',
        ];
        return Validator::make($data, [
            'file' => 'required|mimes:xls,xlsx,csv',
        ], $message);
    }

    protected function getSubgroupsFromGroups($groups, $group_id)
    {
        $sub_groups = [];
        foreach ($groups as $group) {
            foreach ($group->subgroup as $subgroup) {
                if ($subgroup->group_id == $group_id)
                    $sub_groups[] = $subgroup;
            }
        }
        return $sub_groups;
    }

    protected function getGroupsFromDirectorate($directorates, $directorate_id)
    {
        $groups = [];
        foreach ($directorates as $directorate) {
            foreach ($directorate->group as $group) {
                if ($group->directorate_id == $directorate_id)
                    $groups[] = $group;
            }
        }
        return $groups;
    }

    protected function getCategoryFromDirectorate($directorates, $directorate_id)
    {
        $categories = [];
        foreach ($directorates as $directorate) {
            foreach ($directorate->category as $category) {
                if ($category->directorate_id == $directorate_id)
                    $categories [] = $category;
            }
        }
        return $categories;
    }

    protected function getService($record)
    {
        $data = [];
        if (isset($record['directorate']) && isset($record['speciality']) && isset($record['name'])) {
            $data['directorate'] = $record['directorate'];
            $data['speciality'] = $record['speciality'];
            $data['category'] = isset($record['category']) ? $record['category'] : "n/a";
            $data['subcategory'] = isset($record['subcategory']) ? $record['subcategory'] : "n/a";
            $data['name'] = $record['name'];
            $data['contactable'] = isset($record['contactable']) ? $record['contactable'] : "n/a";
            $data['min_standards'] = isset($record['min_standards']) ? $record['min_standards'] : "n/a";
        }
        return $data;
    }

    protected function getTemplate($record)
    {
        $data = NULL;
        if ((
            isset($record['start_date']) &&
            isset($record['start_date']) &&
            isset($record['cycle_length']) &&
            isset($record['starting_week'])
        )) {
            $data['start_date'] = $record['start_date'];
            $data['end_date'] = $record['end_date'];
            $data['cycle_length'] = $record['cycle_length'];
            $data['starting_week'] = $record['starting_week'];
        };
        return $data;
    }

    protected function getEvent($record)
    {
        $event = NULL;
        if (
            isset($record['week_no']) &&
            isset($record['day']) &&
            isset($record['start']) &&
            isset($record['duration']) &&
            isset($record['site'])
        ) {
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['day'];
            $event['start'] = $record['start'];
            $event['duration'] = $record['duration'];
            $event['site'] = $record['site'];
            if (isset($record['locations']))
                $event['locations'] = $record['locations'];
        }
        return $event;
    }

    protected function getCategorySubcategoryForShiftDetails($categories, $shift_detail)
    {
        if ($shift_detail->service_type == 's') {
            $category = (isset($shift_detail->service->category_id) ? $shift_detail->service->category_id : 0);
            $sub_categories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', ($category != 0 ? $category : $categories[0]->id), 'category_id');
            $sub_category = (isset($shift_detail->service->sub_category_id) ? $shift_detail->service->sub_category_id : 0);
        }
        if ($shift_detail->service_type == 'd') {
            $category = (isset($shift_detail->directorate_service->category_id) ? $shift_detail->directorate_service->category_id : 0);
            $sub_categories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', ($category != 0 ? $category : $categories[0]->id), 'category_id');
            $sub_category = (isset($shift_detail->directorate_service->sub_category_id) ? $shift_detail->directorate_service->sub_category_id : 0);
        }
        if ($shift_detail->service_type == 'none') {
            $category = null;
            $sub_category = null;
            $sub_categories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $categories[0]->id, 'category_id');
        }
        return ['category' => $category, 'subcategory' => $sub_category, 'subcategories' => $sub_categories];
    }

    public function __construct()
    {

        $this->trustId = session('activeTrust');
        /* $this->middleware('auth');*/
        $this->middleware(['auth']);
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);


    }

    /**
     * @return \Illuminate\Http\Response
     * https://andrew.cool/blog/64/How-to-use-API-tokens-for-authentication-in-Laravel-5-2
     */

    public function index()
    {
        $trust = array();
        $accountStatus = array();
        if (session('userTrusts')['status']) {
            $response = $this->api_client->request("GET", $this->getAPIUrl('get-tree') . auth()->user()->trust_id);
            /* $response = $this->api_client->request("GET", $this->getAPIUrl('get-tree') . session('activeTrust'));*/
            $trust = json_decode($response->getBody()->getContents());
        } else if (!(session('userTrusts')['status']) && auth()->user()->signup) {
            $apiUrl = $apiUrl = Helpers::getAPIUrl('getUserAccountStatus') . auth()->user()->id;
            $getAccountStatus = __get($apiUrl);

            if ($getAccountStatus['status']) {
                $accountStatus = $getAccountStatus['apiResponseData'];
                if ($accountStatus->status) {
                    $accountStatus = $accountStatus->data;
                }
            }


        }

        if (Auth::user()->is_admin == 2)
            return view('admin.systemAdminHome');

        return view('admin.mainTreeView', compact('trust', 'accountStatus'));
    }

    /*   public function index() {


          $response = $this->api_client->request("GET", $this->getAPIUrl('get-user-trusts-tree') . auth()->user()->id.'/tree');
          $trusts = json_decode($response->getBody()->getContents());
          return view('admin.mainTreeView', compact('trusts'));
       }*/



    public function switchTrust($trustId)
    {
        session()->put('activeTrust', base64_decode($trustId));
        session()->put('trustChanged', true);

        return redirect('/');
    }


    public function nexinOld()
    {
        return view('home');
    }

//  ############################################################################    SPECIALITY RELATED       ############################################################################

    public function getDirectorateSpecialityList($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('directorate') . $id);
        $result = json_decode($response->getBody()->getContents());
        $directorateSpeciality = array();
        foreach ($result->speciality as $speciality) {
            if ($speciality != NULL)
                $directorateSpeciality[$speciality->id] = $speciality->name;
        }
        return response()->json($directorateSpeciality);
    }

    public function specialityServicesWithCategoryFitler($id, $cat_id, $sub_cat_id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('directoratespeciality') . $id);
        $result = json_decode($response->getBody()->getContents());
        $services = [];
        foreach ($result->service as $service) {
            if ($service->category_id == $cat_id && $service->sub_category_id == $sub_cat_id)
                $services[$service->id] = $service->name;
        }
        return response()->json($services);
    }

//    public function getDirectorateSpeciality(Request $request) {
//        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
//        $trust = json_decode($response->getBody()->getContents());
//        $response = $this->api_client->request("GET", $this->getAPIUrl('directorate') . $request->directorate);
//        $result = json_decode($response->getBody()->getContents());
//        $specialties = array();
//        $directorates = array();
//        $directorate = $request->directorate;
//        foreach ($result->speciality as $speciality) {
//            if ($speciality != NULL)
//                array_push($specialties, ['id' => $speciality->id, 'name' => $speciality->name, 'directorate' => $result->name, 'abbreviation' => $speciality->abbreviation]);
//        }
//        foreach ($trust->division as $division) {
//            if (isset($division->directorate) && count($division->directorate)) {
//                foreach ($division->directorate as $directorate_node) {
//                    array_push($directorates, ['id' => $directorate_node->id, 'name' => $directorate_node->name]);
//                }
//            }
//        }
//        $directorates = Helpers::getDropDownData($directorates);
//        return view('speciality.trustSpecialities', compact('specialties', 'directorate', 'directorates'));
//    }
//  ############################################################################        STAFF RELATED       ############################################################################
    public function showAllStaff()
    {
        if (!auth()->user()->is_admin)
            abort(403);
        $response = $this->api_client->request("GET", $this->getAPIUrl('staff'));
        $staffs = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
        $grades = json_decode($response->getBody()->getContents());
        $grades = Helpers::getDropDownData($grades);
        $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
        $roles = json_decode($response->getBody()->getContents());
        $roles = Helpers::getDropDownData($roles);
        return view('staff.allStaff', compact('staffs', 'grades', 'roles'));
    }

    public function createStaff()
    {
        if (!auth()->user()->is_admin)
            abort(403);
        $response = $this->api_client->request("GET", $this->getAPIUrl('speciality'));
        $specialities = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getDropDownData($specialities);
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
        $grades = json_decode($response->getBody()->getContents());
        $grades = Helpers::getDropDownData($grades);
        $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
        $roles = json_decode($response->getBody()->getContents());
        $roles = Helpers::getDropDownData($roles);
        return view('staff.createStaff', compact('specialities', 'grades', 'roles'));
    }

    public function storeStaff(Request $request)
    {
        if (!auth()->user()->is_admin)
            abort(403);
        if (!Input::hasFile('file')) {
            if ($request->qualifications_public) {
                $qualifications = $request->qualifications . '^' . $request->qualifications_public;
                $request->merge(['qualifications' => $request->qualifications . '^' . $request->qualifications_public]);
            } else {
                $request->merge(['qualifications' => $request->qualifications . '^0']);
            }
            if ($request->individual_bleep_public) {
                $request->merge(['individual_bleep' => $request->individual_bleep . '^' . $request->individual_bleep_public]);
            } else {
                $request->merge(['individual_bleep' => $request->individual_bleep . '^0']);
            }
            if ($request->mobile_public) {
                $request->merge(['mobile' => $request->mobile . '^' . $request->mobile_public]);
            } else {
                $request->merge(['mobile' => $request->mobile . '^0']);
            }
            $postData = array();
//            return [$request->profile_pic];
            foreach ($request->all() as $key => $value) {
                if (Input::hasFile('profile_pic')) {
                    array_push($postData, [
                        'name' => $key,
                        'contents' => fopen(Input::file('profile_pic')->getPathname(), 'r'),
                        'filename' => str_replace(' ', '', Input::file($key)->getClientOriginalName()),
                    ]);
                } else {
                    array_push($postData, ['name' => $key, 'contents' => $value]);
                }
            }
            $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('staff'), '/'), ['multipart' => $postData]);
            $result = json_decode($response->getBody()->getContents());
            if (isset($result->error) && count($result->error)) {
                Session::flash('error-staff', $result->error);
                return Redirect::back();
            } else {
                Session::flash('success-staff', 'Staff Members added successfully.');
                return redirect('/staff');
            }
        } else {
            $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
            if ($upload) {
                $records = Excel::selectSheetsByIndex(0)->load(Input::file('file'), function ($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
                $data = $records; //$request->data;

                $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
                $grades = json_decode($response->getBody()->getContents());
                $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
                $roles = json_decode($response->getBody()->getContents());
                $response = $this->api_client->request("GET", $this->getAPIUrl('staff'));
                $staffs = json_decode($response->getBody()->getContents());
                $gmc_all = $role_names = $grade_names = array();
                foreach ($grades as $grade) {
                    $grade_names[] = $grade->name;
                }
                foreach ($roles as $role) {
                    $role_names[] = $role->name;
                }
                foreach ($staffs as $staff) {
                    $gmc_all[] = $staff->gmc;
                }

                $file_name = Input::file('file')->getClientOriginalName();
                $replace = $request->replace;
                $view_data = array();
                foreach ($data as $data_node) {
                    if (in_array($data_node['grade'], $grade_names) == false) {
                        $data_node['error_grade'] = $data_node['grade'] . ' not found in system.';
                    }
                    if (in_array($data_node['role'], $role_names) == false) {
                        $data_node['error_role'] = $data_node['role'] . ' not found in system.';
                    }
                    if (in_array($data_node['gmc'], $gmc_all) == true) {
                        $data_node['error_gmc'] = $data_node['gmc'] . ' duplicate value, already exists';
                    }
                    array_push($view_data, $data_node);
                }
                return view('staff.uploadedStaff', compact('view_data', 'file_name', 'replace'));
            }
        }
    }

    public function uploadStaffData(Request $request)
    {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function ($reader) {
            $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        $request->merge(['data' => $records]);
        $response = $this->api_client->request("POST", $this->getAPIUrl('staff') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-staff', $result->error);
            return redirect('/staff');
        } else {
            Session::flash('success-staff', 'Staff Members data uploaded successfully.');
            return redirect('/staff');
        }
    }

    public function editStaff($id)
    {
        if (!auth()->user()->is_admin)
            abort(403);
        $response = $this->api_client->request("GET", $this->getAPIUrl('staff') . $id);
        $staff = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('speciality'));
        $specialities = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getDropDownData($specialities);
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
        $grades = json_decode($response->getBody()->getContents());
        $grades = Helpers::getDropDownData($grades);
        $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
        $roles = json_decode($response->getBody()->getContents());
        $roles = Helpers::getDropDownData($roles);
        $staff->profile_pic = Helpers::getProfileImage($staff->profile_pic);
        return view('staff.editStaff', compact('staff', 'specialities', 'grades', 'roles'));
    }

    public function updateStaff(Request $request, $id)
    {
        if (!auth()->user()->is_admin)
            abort(403);
        if ($request->qualifications_public) {
            $qualifications = $request->qualifications . '^' . $request->qualifications_public;
            $request->merge(['qualifications' => $request->qualifications . '^' . $request->qualifications_public]);
        } else {
            $request->merge(['qualifications' => $request->qualifications . '^0']);
        }
        if ($request->individual_bleep_public) {
            $request->merge(['individual_bleep' => $request->individual_bleep . '^' . $request->individual_bleep_public]);
        } else {
            $request->merge(['individual_bleep' => $request->individual_bleep . '^0']);
        }
        if ($request->mobile_public) {
            $request->merge(['mobile' => $request->mobile . '^' . $request->mobile_public]);
        } else {
            $request->merge(['mobile' => $request->mobile . '^0']);
        }
        if (!Input::hasFile('profile_pic')) {
            $response = $this->api_client->request("PUT", $this->getAPIUrl('staff') . $id, ['form_params' => $request->all()]);
        } else {
            $postData = array();
            foreach ($request->all() as $key => $value) {
                if (Input::hasFile($key)) {
                    array_push($postData, [
                        'name' => $key,
                        'contents' => fopen(Input::file('profile_pic')->getPathname(), 'r'),
                        'filename' => str_replace(' ', '', Input::file($key)->getClientOriginalName()),
                    ]);
                } else {
                    array_push($postData, ['name' => $key, 'contents' => $value]);
                }
            }
            $response = $this->api_client->request("PUT", $this->getAPIUrl('staff') . $id, ['multipart' => $postData]);
        }
        $result = json_decode($response->getBody()->getContents());
        return redirect('/staff');
//        return ($result);
    }

    public function destroystaff($id)
    {
        if (!auth()->user()->is_admin)
            abort(403);
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('staff') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-staff', $result->error);
        return redirect('/staff');
    }

    public function exportStaff()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('staff'));
        $staffs = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'gmc', 'speciality', 'grade', 'role', 'qualifications', 'individual_bleep', 'email', 'mobile'];
        foreach ($staffs as $staff) {
            if (isset($staff->speciality) && count($staff->speciality))
                $speciality = $staff->speciality->name;
            else
                $speciality = '';
            $qualifications = explode('^', $staff->qualifications);
            $individual_bleep = explode('^', $staff->individual_bleep);
            $mobile = explode('^', $staff->mobile);
            array_push($data, [$staff->name, $staff->gmc, $speciality, $staff->grade->name, $staff->role->name, $qualifications[0], $individual_bleep[0], $staff->email, $mobile[0]]);
        }
        Excel::create('Staff_backup(' . date('Y-M-d') . ')', function ($excel) use ($data) {

            $excel->sheet('Sheetname', function ($sheet) use ($data) {

                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xlsx');
        Session::flash('success-staff', 'Staff Members data exported successfully.');
        return redirect('/staff');
    }

//  ############################################################################        GRADES RELATED       ############################################################################
    public function showAllGrade(Request $request)
    {
        /* if (!auth()->user()->is_admin)
             abort(403);*/
        __authorize(config('module.grades'), 'view', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality-grade');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate_id = null;
        $specialities = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        $directorates = Helpers::getDropDownData($directorates);
        return view('grade.allGrades', compact('specialities', 'directorates', 'directorate_id'));
    }

    public function createGrade()
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.grades'), 'add', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $grades = $roles = [];
        if (count($directorates)) {
            $roles = $this->getRolesFromDirectorate($directorates, $directorates[0]->id);
            $grades = $this->getGradesFromDirectorates($directorates, $directorates[0]->id);
        }

        $grades_position = [];
        foreach ($grades as $grade) {
            $grades_position [] = ['id' => $grade->position, 'name' => $grade->name];
        }
        $directorates = Helpers::getDropDownData($directorates);
        $grades_position = Helpers::getDropDownData($grades_position);
        $grades = Helpers::getDropDownData($grades);
        $roles = Helpers::getDropDownData($roles);
        return view('grade.createGrade', compact('directorates', 'roles', 'grades', 'grades_position'));
    }

    public function storeGrade(Request $request)
    {
        __authorize(config('module.grades'), 'add', true);
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        if (!isset($request->position))
            $request->merge(['position' => 1]);
        else
            $request->merge(['position' => $request->position + $request->place]);
        /*echo '<pre>';
        print_r(rtrim($this->getAPIUrl('grade'), '/'));
        print_r($request->all());
        exit;*/
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('grade'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-grade', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-grade', $request->name . ', Grade Added Successfully');
            return redirect('/grades');
        }
    }

    public function destroyGrade($id)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.grades'), 'delete', true);
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('grade') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-grade', $result->error);
        return redirect('/grades');
    }

    public function editGrade($id)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.grades'), 'edit', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade') . $id);
        $grade = json_decode($response->getBody()->getContents());
        $roles = $this->getRolesFromDirectorate($directorates, $grade->directorate_id);
        $grades = $this->getGradesFromDirectorates($directorates, $grade->directorate_id);
        foreach ($grades as $grade_node) {
            $grades_position [] = ['id' => $grade_node->position, 'name' => $grade_node->name];
        }
        $roles = Helpers::getDropDownData($roles);
        $grades = Helpers::getDropDownData($grades);
        $grades_position = Helpers::getDropDownData($grades_position);
        $directorates = Helpers::getDropDownData($directorates);
        return view('grade.editGrade', compact('grade', 'directorates', 'roles', 'grades_position'));
    }

    public function updateGrade(Request $request, $id)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.grades'), 'edit', true);
        $request->merge(['position' => $request->position + $request->place]);
        $response = $this->api_client->request("PUT", $this->getAPIUrl('grade') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->success) && count($result->success)) {
            Session::flash('success-grade', $result->success);
            return redirect('/grades');
        } else {
            Session::flash('error-grade', $result);
            return Redirect::back();
        }
    }

    public function getGradePositions($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $grades = $this->getGradesFromDirectorates($directorates, $id);
        $grades_position = [];
        foreach ($grades as $grade) {
            $grades_position [] = ['id' => $grade->position, 'name' => $grade->name];
        }
        $grades_position = Helpers::getDropDownData($grades_position);
        return response()->json($grades_position);
    }

    public function exportGrade()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['position', 'name', 'role', 'directorate'];
        $ref_data[] = ['position', 'name', 'role', 'directorate'];
        $ref_data[] = ['integer', 'Dummy Grade', 'Dummy Role', 'Dummy Directorate'];
        foreach ($directorates as $directorate_node) {
            foreach ($directorate_node->grade as $grade) {
                if (isset($grade) && count($grade))
                    array_push($data, [$grade->position, $grade->name, $grade->role->name, $directorate_node->name]);
            }
        }
        $this->exportExcel('Grade_backup', $data, $ref_data);
        Session::flash('success-grade', 'Grades data exported successfully.');
        return redirect('/grades');
    }

    public function importGrade(Request $request)
    {
        $roles_info = $grades_info = [];
        $validator = $this->importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = $this->importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
            $directorates = json_decode($response->getBody()->getContents());
            $directorate_names = $this->gettingArrayOfName($directorates);
            foreach ($directorates as $directorate) {
                foreach ($directorate->grade as $grade) {
                    $grades_info [] = ['name' => $grade->name, 'directorate' => $directorate->name, 'role' => $grade->role->name];
                }
                foreach ($directorate->role as $role) {
                    if (count($role))
                        $roles_info [] = ['name' => $role->name, 'directorate' => $directorate->name];
                }
            }
            foreach ($data as $data_node) {
                $roles = [];
                foreach ($roles_info as $role_info) {
                    if ($role_info['directorate'] == $data_node['directorate'])
                        $roles [] = $role_info['name'];
                }
                if (!in_array($data_node['directorate'], $directorate_names))
                    $data_node['directorate_error'] = 'No directorate with this name exist in system';
                if (!in_array($data_node['role'], $roles))
                    $data_node['role_error'] = 'Invalid role, this role does not exist in system or in this directorate';
                foreach ($grades_info as $grade_info) {
                    if ($grade_info['name'] == $data_node['name'] && $data_node['directorate'] == $grade_info['directorate']) {
                        $data_node['grade_error'] = 'This grade already exist for this directorate';
                    }
                    if ($grade_info['name'] == $data_node['role'] && $data_node['role'] == $grade_info['role'] && $data_node['directorate'] == $grade_info['directorate']) {
                        $data_node['role_grade_error'] = 'This grade already exist with same role in this directorate.';
                    }
                    if (isset($data_node['role_grade_error']) || isset($data_node['grade_error']))
                        break;
                }
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('grade.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function uploadGrade(Request $request)
    {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function ($reader) {
            $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        $request->merge(['data' => $records]);
        $response = $this->api_client->request("POST", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorategrade/upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-speciality', $result->error);
            return redirect('/grades');
        } else {
            Session::flash('success-speciality', $result->success);
            return redirect('/grades');
        }
    }

//  ############################################################################        ROLES RELATED       ############################################################################

    public function showAllRole()
    {
        /*  if (!auth()->user()->is_admin)
              abort(403);*/
        __authorize(config('module.roles'), 'view', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        return view('role.allRoles', compact('directorates'));
    }

    public function createRole()
    {
        /* if (!auth()->user()->is_admin)
             abort(403);*/
        __authorize(config('module.roles'), 'add', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $roles = [];
        if (count($directorates))
            $roles = $this->getRolesFromDirectorate($directorates, $directorates[0]->id);
        $roles_position = [];
        foreach ($roles as $role) {
            $roles_position[] = ['id' => $role->position, 'name' => $role->name];
        }
        $directorates = Helpers::getDropDownData($directorates);
        $roles_position = Helpers::getDropDownData($roles_position);
        return view('role.createRole', compact('directorates', 'roles_position'));
    }

    public function storeRole(Request $request)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.roles'), 'add', true);
        if (!isset($request->position))
            $request->merge(['position' => 1]);
        else
            $request->merge(['position' => $request->position + $request->place]);
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('role'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-role', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-role', $request->name . ', Role added sucessfully.');
            return redirect('/roles');
        }
    }

    public function destroyRole($id)
    {
        __authorize(config('module.roles'), 'delete', true);
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('role') . $id);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/roles');
    }

    public function editRole($id)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.roles'), 'edit', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('role') . $id);
        $role = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $roles = $this->getRolesFromDirectorate($directorates, $role->directorate_id);
        $roles_position = [];
        foreach ($roles as $role_node) {
            $roles_position[] = ['id' => $role_node->position, 'name' => $role_node->name];
        }
        $directorates = Helpers::getDropDownData($directorates);
        $roles_position = Helpers::getDropDownData($roles_position);
        return view('role.editRole', compact('role', 'directorates', 'roles_position'));
    }

    public function updateRole(Request $request, $id)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.roles'), 'edit', true);
        $request->merge(['position' => $request->position + $request->place]);
        /*echo '<pre>';
        echo $this->getAPIUrl('role') . $id;
        print_r($request->all());
        exit;*/
        $response = $this->api_client->request("PUT", $this->getAPIUrl('role') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/roles');
    }

    public function exportRole()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['position', 'name', 'directorate'];
        $ref_data[] = ['position', 'name', 'directorate'];
        $ref_data[] = ['integer', 'Dummy Role', 'Dummy Directorate'];
        foreach ($directorates as $directorate_node) {
            foreach ($directorate_node->role as $grade) {
                if (isset($grade) && count($grade))
                    array_push($data, [$grade->position, $grade->name, $directorate_node->name]);
            }
        }
        $this->exportExcel('Role_backup', $data, $ref_data);
        Session::flash('success-role', 'Grades data exported successfully.');
        return redirect('/roles');
    }

    public function importRole(Request $request)
    {
        __authorize(config('module.roles'), 'add', true);
        $validator = $this->importFileValidate($request->all());
        if ($validator->fails())
            return Redierect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = $this->importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
            $directorates = json_decode($response->getBody()->getContents());
            $directorate_names = $this->gettingArrayOfName($directorates);
            $roles = [];
            foreach ($directorates as $directorate) {
                foreach ($directorate->role as $role) {
                    if (count($role))
                        $roles [] = ['name' => $role->name, 'directorate' => $directorate->name];
                }
            }
            foreach ($data as $data_node) {
                if (!in_array($data_node['directorate'], $directorate_names))
                    $data_node['directorate_error'] = 'No directorate with this name exist in system';
                foreach ($roles as $role) {
                    if ($data_node['name'] == $role['name'] && $data_node['directorate'] == $role['directorate']) {
                        $data_node['role_error'] = 'This role already exist with in this directorate.';
                        break;
                    }
                }
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('role.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function uploadRole(Request $request)
    {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function ($reader) {
            $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        $request->merge(['data' => $records]);
        $response = $this->api_client->request("POST", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directoraterole/upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-role', $result->error);
            return redirect('/roles');
        } else {
            Session::flash('success-role', $result->success);
            return redirect('/roles');
        }
    }

    public function getRoles($id)
    {
        $roles = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $roles = $this->getRolesFromDirectorate($directorates, $id);
        foreach ($roles as $role) {
            if ($role->directorate_id == $id)
                $roles [] = $role;
        }
        $roles = Helpers::getDropDownData($roles);
        return response()->json($roles);
    }

    public function getRolesPositions($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $roles = $this->getRolesFromDirectorate($directorates, $id);
        $roles_position = [];
        foreach ($roles as $role) {
            $roles_position [] = ['id' => $role->position, 'name' => $role->name];
        }
        $roles_position = Helpers::getDropDownData($roles_position);
        return response()->json($roles_position);
    }

//  ############################################################################        ADMIN RELATED       ############################################################################
    public function adminView()
    {
        return view('admin.home');
    }

    public function ajaxTest(Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('get-tree'));
        return $response->getBody()->getContents();
    }

    public function adminIndex()
    {
        return view('admin.mainTreeView');
    }

//  ############################################################################        TRUSTS IN ADMIN RELATED       ############################################################################
    public function adminshowAllTrusts()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust'));
        $trusts = json_decode($response->getBody()->getContents());
        return view('trust.viewAllTrusts', compact('trusts'));
    }

//  ############################################################################        SHIFT RELATED       ############################################################################
    public function showAllShift()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        $shifts = $trusts->shift;
        return view('shift.allShifts', compact('shifts'));
    }

    public function createShift()
    {
        return view('shift.createShift');
    }

    public function storeShift(Request $request)
    {
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('shift'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/shift');
    }

    public function editShift($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift') . $id);
        $shift = json_decode($response->getBody()->getContents());
        return view('shift.editShifts', compact('shift'));
    }

    public function updateShift(Request $request, $id)
    {
        $response = $this->api_client->request("PUT", $this->getAPIUrl('shift') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/shift');
    }

    public function destroyShift($id)
    {
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('shift') . $id);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/shift');
    }

//  ############################################################################        ROTATION TMEPLATE RELATED       ############################################################################

    public function showAllTemplateRotation()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $rotations = $trust->template;
        $total_weeks = 1;
        $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift'));
        $shifts = json_decode($response->getBody()->getContents());
        $shifts = Helpers::getDropDownData($shifts);
        return view('template.allTempalteRotations', compact('rotations', 'total_weeks', 'weekdays', 'shifts'));
    }

    public function createTemplateRotation()
    {
        $total_weeks = 1;
        $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift'));
        $shifts = json_decode($response->getBody()->getContents());
        $shifts = Helpers::getDropDownData($shifts);
        return view('template.createTemplateRotation', compact('total_weeks', 'weekdays', 'shifts'));
    }

    public function storeTemplateRotation(Request $request)
    {
        $request->merge(['trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('rotation'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/rotation-template');
    }

    public function editTemplateRotation($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('rotation') . $id);
        $rotation = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift'));
        $shifts = json_decode($response->getBody()->getContents());
        $shifts = Helpers::getDropDownData($shifts);
        $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        return view('template.editTemplateRotation', compact('rotation', 'weekdays', 'shifts'));
    }

    public function updateTemplateRotation(Request $request, $id)
    {
        $response = $this->api_client->request("PUT", $this->getAPIUrl('rotation') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/rotation');
    }

    public function destroyTemplateRotation($id)
    {
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('rotation') . $id);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/rotation');
    }

//  ############################################################################        ROTATIONS RELATED       ############################################################################
    public function showAllRotation(Request $request)
    {
        $directorates = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/post');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        else
            $directorate = null;
        $date_range = (isset($request->date_range) ? $request->date_range : date('Y-m-d'));
        $specialities = $this->getSpecialityFromDirectorate($directorates, $directorate);
        $date_range = new Carbon($date_range);
        $directorates = Helpers::getDropDownData($directorates);
        return view('rotation.allRotation', compact('specialities', 'directorates', 'directorate', 'date_range'));
    }

    public function createRotation(Request $request)
    {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate = null;
        if (count($directorates))
            $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate-staff') . 'directorate/' . $directorate);
        $directorate_staff = json_decode($response->getBody()->getContents());
        $specialities = $this->getSpecialityFromDirectorate($directorates, $directorate);
        $posts = (($specialities) ? $this->getPostsFromSpecialities($specialities, $specialities[0]->id) : []);
        $rota_groups = $this->getRotaGroupFromDirectorate($directorates, $directorate);
        $rota_group_id = (isset($posts[0]->rota_template->rota_group_id) ? $posts[0]->rota_template->rota_group_id : 0);
        $rota_templates = Helpers::getDropDownData($this->getRotaTemplatesFromRotaGroup($rota_groups, $rota_group_id));
        $position_max = (isset($posts[0]->rota_template->content->shift_type) ? count($posts[0]->rota_template->content->shift_type) : 1);
        $selected_template = (isset($posts[0]->rota_template_id) ? $posts[0]->rota_template_id : NULL);
        $current_position = (isset($posts[0]->assigned_week) ? $posts[0]->assigned_week : 1);
        $posts = Helpers::getDropDownData($posts);
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        $speciality = null;
        return view('rotation.createRotation', compact('posts', 'rota_templates', 'selected_template', 'directorates', 'directorate', 'specialities', 'speciality', 'current_position', 'position_max', 'directorate_staff', 'rota_group_id', 'rota_groups'));
    }

    public function getRotationsRotas($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('post') . $id);
        $post_data = json_decode($response->getBody()->getContents());
//        return [$post_data];
        if (count($post_data)) {
            $start_date = $post_data->start_date;
            $end_date = $post_data->end_date;
            $assigned_week = $post_data->assigned_week;
            $template_name = $post_data->rota_template->name;
            $total_weeks = count($post_data->rota_template->content->shift_type);
            $template_id = $post_data->rota_template_id;
            $d_id = $post_data->rota_template->directorate_id;
            $response = $this->api_client->request("GET", $this->getAPIUrl('directorate') . $d_id);
            $directorate_data = json_decode($response->getBody()->getContents());
            $rota_template = Helpers::getDropDownData($directorate_data->rota_template);
            return response()->json(['start_date' => $start_date, 'end_date' => $end_date, 'assigned_week' => $assigned_week, 'directorate_id' => $d_id, 'rota_templates' => $rota_template, 'template_name' => $template_name, 'template_id' => $template_id, 'total_weeks' => $total_weeks]);
        } else {
            return response()->json(['empty' => 'No data found']);
        }
    }

    public function getRotationsPosts($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('directoratespeciality') . $id);
        $posts = json_decode($response->getBody()->getContents());
        $posts = Helpers::getDropDownData($posts->post);
        return response()->json($posts);
    }

    public function destroyRotation($id)
    {
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('rotation-detail') . $id);
        $result = json_decode($response->getBody()->getContents());
        return redirect('/rotations');
    }

    public function detailRotation($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('rotation-detail') . $id);
        $rotation = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('rotation') . $rotation->template_id);
        $templateRotations = json_decode($response->getBody()->getContents());
        $templateRotations = $templateRotations->contents->shift;
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        $shifts = $trusts->shift;
        return view('rotation.rotationDetail', compact('rotation', 'templateRotations', 'shifts', 'id'));
    }

//  ############################################################################        POST RELATED       ############################################################################
//  ############################################################################        USERS RELATED       ############################################################################

    public function showAllUsers()
    {
        if (!auth()->user()->is_admin)
            abort(403);
        $response = $this->api_client->request("GET", $this->getAPIUrl('user'));
        $users = json_decode($response->getBody()->getContents());
        return view('users.list_all', compact('users'));
    }

    public function showTrustUsers()
    {
        /* if (!auth()->user()->is_admin)
             abort(403);*/

        __authorize(config('module.user'), 'view', true);
        $users = array();
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust-user') . $this->trustId);


        $users = json_decode($response->getBody()->getContents());


        /*
         $users=@$users[0]->users;*/
        return view('users.list_all', compact('users'));
    }


    public function destroyUser($id)
    {
        __authorize(config('module.user'), 'delete', true);
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('user') . $id);
        $result = json_decode($response->getBody()->getContents());

//        return ($result);
        return redirect('/user');
    }

    public function createUsers()
    {
        /*  if (!auth()->user()->is_admin)
              abort(403);*/
        __authorize(config('module.user'), 'add', true);
        $roles = array();
        $getTrustRoles = $this->getTrustRoles();

        if ($getTrustRoles) {
            $roles = $getTrustRoles;
        }
        /*   $roles = Helpers::getDropDownData($roles);*/
        return view('users.new', compact('roles'));
    }

    public function getTrustRoles()
    {

        $apiUrl = Helpers::getAPIUrl('get-trust-roles') . "/" . $this->trustId;

        $getTrustRoles = __get($apiUrl);

        if ($getTrustRoles['status']) {
            $result = $getTrustRoles['apiResponseData'];
            if ($result->status) {
                $roles = $result->data;

                return $roles;

            }
        }
        return false;
    }

    public function createTrustUsers()
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.user'), 'add', true);
        /* $response = $this->api_client->request("GET", $this->getAPIUrl('trust'));*/
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust'));
        $trusts = json_decode($response->getBody()->getContents());
        $trusts = Helpers::getDropDownData($trusts);
        return view('users.new', compact('trusts'));
    }


    /*   public function storeUser(Request $request) {
           if (!auth()->user()->is_admin)
               abort(403);


           $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('register'), '/'), ['form_params' => $request->all()]);
           $result = json_decode($response->getBody()->getContents());
           if (isset($result->error) && $result->error == 'yes')
               return Redirect::back()->withErrors($result->messages);
           else
               return redirect('/user')->withErrors(['msg' => 'user created.']);
       }*/

    public function storeUser(Request $request)
    {
        /*if (!auth()->user()->is_admin)
            abort(403);*/
        __authorize(config('module.user'), 'add', true);
        $apiUrl = rtrim($this->getAPIUrl('create-trust-user'), '/');
        $request->request->add(['trust_id' => $this->trustId]);
        $paramArr = $request->all();
        $storeUser = __post($apiUrl, $paramArr);

        if ($storeUser['status']) {

            $result = $storeUser['apiResponseData'];
            if (isset($result->error) && ($result->error)) {
                return Redirect::back()->withErrors($result->messages)->withInput();
            } else {
                return redirect('/user')->withErrors(['msg' => 'user created.']);
            }
        } else {

            return redirect('/user/new')->withErrors(['msg' => trans('messages.error')])->withInput();


        }


    }


    /* public function editUsers($id) {
       /*  if (!auth()->user()->is_admin)
             abort(403);
         __authorize(config('module.user'),'edit',true);
         $response = $this->api_client->request("GET", $this->getAPIUrl('trust'));
         $trusts = json_decode($response->getBody()->getContents());
         $trusts = Helpers::getDropDownData($trusts);
         $response = $this->api_client->request("GET", $this->getAPIUrl('user') . $id);
         $user = json_decode($response->getBody()->getContents());
         return view('users.edit', compact('trusts', 'user'));
     }*/

    public function editUsers($id)
    {
        __authorize(config('module.user'), 'edit', true);

        $apiUrl = $this->getAPIUrl('user') . $id;
        $userTrustId = $id;
        $getUser = __get($apiUrl);
        $user = array();
        $roles = array();
        if ($getUser['status']) {
            $result = $getUser['apiResponseData'];
            if (isset($result->status) && !$result->status) {
                return Redirect::back()->withErrors($result->msg)->withInput();
            } else {
                $user = $result->data;
                $getTrustRoles = $this->getTrustRoles();
                if ($getTrustRoles) {
                    $roles = $getTrustRoles;
                }

                return view('users.edit', compact('user', 'roles', 'userTrustId'));
            }
        } else {

            return Redirect::back()->withErrors("messages.error")->withInput();

        }


    }

    /*    public function updateUser(Request $request, $id) {
        if (!auth()->user()->is_admin)
            abort(403);
        $response = $this->api_client->request("PUT", $this->getAPIUrl('user') . $id, ['form_params' => $request->all()]);

        $result = json_decode($response->getBody()->getContents());
        return redirect('/user');
    }*/

    public function updateUser(Request $request, $id)
    {
        __authorize(config('module.user'), 'edit', true);
        $apiUrl = $this->getAPIUrl('user') . $id;
        $paramArr = $request->all();
        $updateUser = __put($apiUrl, $paramArr);
        if ($updateUser['status']) {

            $result = $updateUser['apiResponseData'];
            if (isset($result->error) && ($result->error)) {
                return Redirect::back()->withErrors($result->messages)->withInput();
            } else {
                return redirect('/user')->withErrors(['msg' => 'Updated Successfully']);
            }
        } else {

            return redirect('/user/new')->withErrors(['msg' => trans('messages.error')]);


        }


    }

//  ############################################################################        POST DETAILS RELATED       ############################################################################

    public function createPostDetails($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('post') . $id);
        $post = json_decode($response->getBody()->getContents());
        $rota_template = $post->directorate_speciality->directorate->rota_template;
        $directorate = $post->directorate_speciality->directorate->name;
        $directorate_id = $post->directorate_speciality->directorate->id;
        return view('post.assignPost', compact('staffs', 'id', 'rota_template', 'directorate', 'post', 'directorate_id'));
    }

    public function storePostDetails(Request $request, $id)
    {
        $request->merge(['post_id' => $id]);
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('post-details'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && $result->error == 'yes')
            return Redirect::back()->withErrors($result->messages);
        else
            return redirect('/post')->withErrors(['msg' => 'user created.']);
    }

    public function editPostDetails($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('post-details') . $id);
        $postdetail = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('post') . $postdetail->post_id);
        $post = json_decode($response->getBody()->getContents());
//        return [$postdetail];
        $rota_template = $post->directorate_speciality->directorate->rota_template;
        $directorate = $post->directorate_speciality->directorate->name;
        $directorate_id = $post->directorate_speciality->directorate->id;
        $speciality = $postdetail->post->directorate_speciality->name;
        $staff[$postdetail->staff->id] = $postdetail->staff->gmc . " [" . $postdetail->staff->name . "]";
        return view('post.editPostDetail', compact('postdetail', 'rota_template', 'directorate', 'id', 'post', 'speciality', 'directorate_id', 'staff'));
    }

    public function updatePostDetails(Request $request, $id)
    {
//        return [$request->all()];
        $response = $this->api_client->request("PUT", $this->getAPIUrl('post-details') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
//        return [$result];
        return redirect('/post');
    }

//  ############################################################################        PROFILE RELATED       ############################################################################
    public function profileDetails($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('staff') . $id);
        $staff = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('speciality') . $staff->speciality_id);
        $speciality = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade') . $staff->grade_id);
        $grade = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('role') . $staff->role_id);
        $role = json_decode($response->getBody()->getContents());
//        return [compact('staff', 'speciality', 'grade', 'role')];
        return view('profile', compact('staff', 'speciality', 'grade', 'role'));
    }

    public function allProfileDetails()
    {

        $response = $this->api_client->request("GET", $this->getAPIUrl('staff'));
        $staffs = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
        $grades = json_decode($response->getBody()->getContents());
        $grades = Helpers::getDropDownData($grades);
        $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
        $roles = json_decode($response->getBody()->getContents());
        $roles = Helpers::getDropDownData($roles);
        return view('allProfile', compact('staffs', 'grades', 'roles'));
    }

//  ############################################################################    SERVICE RELATED       ############################################################################


    public function getsiteLocations($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trusts = json_decode($response->getBody()->getContents());
        $sites = $trusts->site;
        $locations = array();
        foreach ($sites as $site) {
            if ($site->location) {
                foreach ($site->location as $location) {
                    if ($location->site_id == $id)
                        array_push($locations, ['id' => $location->id, 'name' => $location->name]);
                }
            }
        }
        return ($locations);
    }

    public function getServiceTemplate(Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('service') . $request->get('serviceId'));
        $result = json_decode($response->getBody()->getContents());
        return response()->json($result);
    }

    public function deleteSerivceTemplate($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('service/template') .'delete/'. $id);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->deleted)) {
            Session::flash('service-teamplate-deleted', $result->deleted);
            return redirect('/service');
        }
        return redirect('/service');
    }

    public function viewSerivceTemplate($id)
    {
//        dd( $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $locations = $this->getTrustLocations($trust->site);
        $response = $this->api_client->request("GET", $this->getAPIUrl('service') . $id);
        $service = json_decode($response->getBody()->getContents());
        $color = '#6BA5C1';
        $events = [];
        if (!is_null($service->template) && count(($service->template))) {
            $events = $this->getServiceEvents($service, $locations, $color);
            Session::forget('service-template-create');
        } else {
            Session::flash('service-template-create', 'service template not found, add service template first.');
        }
        $start_date = (isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? $service->template->start_date : date('Y-m-d');
        $end_date = (isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? $service->template->end_date : date('Y-m-d');
        $cycle_length = (isset($service->template->cycle_length)) ? $service->template->cycle_length : 1;
        $starting_week = (isset($service->template->starting_week)) ? $service->template->starting_week : 1;

        $calendar = $this->fullCalanderJs($events, 'view');
        return view('service.viewServiceTemplate', compact('service', 'cycle_length', 'start_date', 'end_date', 'starting_week', 'calendar'));
    }

    public function viewSpecialitySerivceTemplate($id, Request $request)
    {
//        return [$request->all()];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $locations = $this->getTrustLocations($trust->site);
        (isset($request->services) ? $selected_services = $request->services : $selected_services = []);
        $response = $this->api_client->request("GET", $this->getAPIUrl('directoratespeciality') . $id);
        $speciality = json_decode($response->getBody()->getContents());
        $services = $speciality->service;
        $events = [];
        mt_srand(254);
        foreach ($speciality->service as $service) {
            $color = $this->random_color();
            if (!is_null($service->template) && count($service->template) && in_array($service->id, $selected_services)) {
                $service_events = $this->getServiceEvents($service, $locations, $color);
                foreach ($service_events as $service_event) {
                    $events[] = $service_event;
                }
            }
        }
        $type = 'view';
        $calendar = $this->fullCalanderJs($events, $type);
        return view('service.specialityServiceTemplate', compact('service', 'speciality', 'duration', 'calendar', 'services', 'id', 'selected_services'));
    }

    public function viewLocationServiceTemplate($id)
    {
        $events = array();
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/service');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesFromMainNode($directorates, 'speciality');
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $locations = Helpers::getAllNodesFromMainNode($sites, 'location');
        $locations = Helpers::getDropDownData($locations);
        $location_id [] = $id;
        $services = $this->getLocationServices($location_id, $specialities);
        $events = $this->getServiceEventsByLocation($location_id, $services, $locations);
        $response = $this->api_client->request("GET", $this->getAPIUrl('location') . $id);
        $location = json_decode($response->getBody()->getContents());

        $type = 'view';
        $calendar = $this->fullCalanderJs($events, $type);
        return view('service.locationServiceTemplate', compact('location', 'calendar'));
    }

    public function viewSiteSerivceTemplate($id, Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('site') . $id);
        $site = json_decode($response->getBody()->getContents());
        $locations = array();
        foreach ($site->location as $location) {
            $locations[$location->id] = $location->name;
        }
        $selected_locations = (isset($request->locations) ? $request->locations : []);
        $services = $this->getLocationServices($selected_locations, $trust->division);
        $events = $this->getServiceEventsByLocation($selected_locations, $services, $locations);
        $type = 'view';
        $calendar = $this->fullCalanderJs($events, $type);
        return view('service.siteServiceTemplate', compact('site', 'duration', 'calendar', 'locations', 'id', 'selected_locations'));
    }

    public function importService(Request $request)
    {
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $data = $this->importExcel(Input::file('file'));
        if ($data[0] == null) {
            return Redirect::back()->withErrors('No data found in uploaded file.');
        }
        $view_data = $template = $events = $formated_data = [];
        $previous_service_name = NULL;
        $i = -1;
        foreach ($data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $formated_data[$i] = $this->getService($data_node);
                $formated_data [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEvent($data_node);
            if ($formated_data [$i]['template'] != NULL && $event != NULL) {
                $formated_data [$i]['template']['events'][] = $event;
            }
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_names = Helpers::gettingArrayOfName($directorates);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $site_names = Helpers::gettingArrayOfName($sites);
        $i = 0;
        foreach ($formated_data as $data_node) {
            $i++;
            $category_names = $categories = $subcategory_names = $service_names = $location_names = [];
            if (!in_array($data_node['directorate'], $directorate_names))
                $data_node['directorate_error'] = "This directorate do not exist in the system or in this trust.";
            foreach ($directorates as $directorate) {
                if ($directorate->name == $data_node['directorate']) {
                    $specialties = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate->id, 'directorate_id');
                    $speciality_names = Helpers::gettingArrayOfName($specialties);
                    if ($data_node['category'] != 'N/A') {
                        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate->id, 'directorate_id');
                        $category_names = Helpers::gettingArrayOfName($categories);
                        if ($data_node['subcategory'] != 'N/A') {
                            foreach ($categories as $category_node) {
                                if ($category_node->name == $data_node['category']) {
                                    $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $category_node->id, 'category_id');
                                    $subcategory_names = Helpers::gettingArrayOfName($subcategories);
                                    foreach ($subcategories as $subcategory) {
                                        if ($subcategory->name == $data_node['subcategory']) {
                                            $services = Helpers::getAllNodesByIdFromMainNode($subcategories, 'service', $subcategory->id, 'sub_category_id');
                                            $service_names = Helpers::gettingArrayOfName($services);
                                            if (in_array($data_node['name'], $service_names))
                                                $data_node['service_error'] = 'This service already exist in this subcategory.';
                                        }
                                    }
                                }
                            }
                        } else {
                            foreach ($categories as $category_node) {
                                if ($category_node->name == $data_node['category']) {
                                    $services = Helpers::getAllNodesByIdFromMainNode($categories, 'service', $category_node->id, 'category_id');
                                    $service_names = Helpers::gettingArrayOfName($services);
                                    if (in_array($data_node['name'], $service_names))
                                        $data_node['service_error'] = 'This service already exist in this category.';
                                }
                            }
                        }
                    } else {
                        foreach ($specialties as $speciality) {
                            if ($data_node['speciality'] == $speciality->name) {
                                $services = Helpers::getAllNodesByIdFromMainNode($specialties, 'service', $speciality->id, 'directorate_speciality_id');
                                $service_names = Helpers::gettingArrayOfName($services);
                                if (in_array($data_node['name'], $service_names))
                                    $data_node['service_error'] = 'This service already exist in this speciality.';
                            }
                        }
                    }
                }
            }
            if (!in_array($data_node['directorate'], $directorate_names))
                $data_node['directorate_error'] = "Tis directorate do not exist in the system or in this trust.";
            if (!in_array($data_node['speciality'], $speciality_names))
                $data_node['speciality_error'] = "Tis speciality do not exist in the system or in this directorate.";
            if ($data_node['category'] != 'N/A' && !in_array($data_node['category'], $category_names))
                $data_node['category_error'] = "Tis category do not exist in the system or in this directorate.";
            if ($data_node['subcategory'] != 'N/A' && !in_array($data_node['subcategory'], $subcategory_names))
                $data_node['subcategory_error'] = "Tis subcategory do not exist in the system or in this category.";
            if ($data_node['contactable'] != 'Yes' && $data_node['contactable'] != 'No')
                $data_node['contactable_error'] = "Invalid value for contactable.";
            if ($data_node['min_standards'] != 'Yes' && $data_node['min_standards'] != 'No')
                $data_node['standards_error'] = "Invalid value for min standards.";
            if (isset($data_node['template']['cycle_length']) && $data_node['template']['cycle_length'] <= 0)
                $data_node['cycle_length_error'] = "Invalid value for cycle length, Cycle length value should be greater then 0";
            if (isset($data_node['template']['starting_week']) && ($data_node['template']['starting_week'] <= 0 || $data_node['template']['starting_week'] > $data_node['template']['cycle_length']))
                $data_node['starting_week_error'] = "Invalid value for Starting week, Starting Week value should be greater then 0 and less or equal to cycle length";
            if (isset($data_node['template']['events']) && count($data_node['template']['events']) > 1) {
                $j = 1;
                foreach ($data_node['template']['events'] as $event) {
                    foreach ($sites as $site) {
                        if ($site->name == $event['site']) {
                            $location_names = Helpers::gettingArrayOfName($site->location);
                        }
                    }
                    if ($event['week_no'] <= 0 || $event['week_no'] > $data_node['template']['cycle_length'])
                        $data_node['events_error']['week_no'][] = $data_node['name'] . ', event ' . $j . ' week no has invalid value.';
                    if ($event['day'] < 0 || $event['day'] > 6)
                        $data_node['events_error']['day'][] = $data_node['name'] . ', event ' . $j . ' day has invalid value, day value be between 0 to 6.';
                    if (!is_numeric($event['duration']))
                        $data_node['events_error']['duration'][] = $data_node['name'] . ', event ' . $j . ' duration has invalid value, duration should be a numeric value.';
                    if (!in_array($event['site'], $site_names))
                        $data_node['events_error']['site'][] = $data_node['name'] . ', event ' . $j . ' "' . $event['site'] . '"  do not exist in the system or in this trust.';
                    if (isset($event['locations'])) {
                        $event_locations = explode(',', $event['locations']);
                        foreach ($event_locations as $location_node) {
                            if ($location_node != '') {
                                if (!in_array($location_node, $location_names))
                                    $data_node['events_error']['location'][] = $data_node['name'] . ', event ' . $j . ' "' . $location_node . '" do not exist in the system or in this site.';
                            }
                        }
                    }
                    $j++;
                }
            }
            $view_data [] = $data_node;
        }
        $file_name = Input::file('file')->getClientOriginalName();
        return view('service.viewUploadedData', compact('view_data', 'file_name'));
    }

//  ############################################################################    SHIFT TYPE RELATED       ############################################################################

    public function showDirectorateShiftTypes(Request $request)
    {
        $directorate = $request->directorate;
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $directorates = $this->getDirectoratesFromDivisions($trust->division);
        $shifttypes = array();
        foreach ($directorates as $directorate_node) {
            if (isset($directorate_node->shift_type) && !empty($directorate_node->shift_type)) {
                foreach ($directorate_node->shift_type as $shifttype) {
                    if ($directorate == $shifttype->directorate_id) {
                        array_push($shifttypes, $shifttype);
                    }
                }
            }
        }
        $directorates = Helpers::getDropDownData($directorates);
        return view('shift-type.allShiftType', compact('directorates', 'directorate', 'shifttypes'));
    }

    public function getDirectorateRotaGroups($id)
    {
        $rota_groups = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $rota_groups = $this->getRotaGroupFromDirectorate($directorates, $id);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        return response()->json($rota_groups);
    }

    public function getRotaGroupShiftTypes($did, $gid)
    {
        $rota_groups = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $rota_groups = $this->getRotaGroupFromDirectorate($directorates, $did);
        $shift_types = [];
        if (count($rota_groups)) {
            foreach ($rota_groups as $rota_group) {
                if ($rota_group->id == $gid) {
                    foreach ($rota_group->shift_type as $shift_type) {
                        if (isset($shift_type))
                            $shift_types[$shift_type->id] = $shift_type->name;
                    }
                }
            }
        }
        return response()->json($shift_types);
    }

//  ############################################################################   ROTA TEMPLATE RELATED       ############################################################################
    public function indexRotaTemplate(Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate = [];
        $rota_groups = [];
        $rota_groups = $this->getRotaGroupFromDirectorate($directorates, $directorate);
//        return [$rota_groups];
        $directorates = Helpers::getDropDownData($directorates);
        return view('rota-template.allRotaTemplate', compact('rota_groups', 'directorates', 'directorate'));
    }

    public function createRotaTemplate()
    {
        $total_weeks = 1;
        $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $rota_groups = $shift_types = [];
        if (count($directorates))
            $rota_groups = $this->getRotaGroupFromDirectorate($directorates, $directorates[0]->id);
        if (count($rota_groups))
            $shift_types = $this->getShiftTypesFromRotaGroup($rota_groups[0]);
        $rota_groups = Helpers::getDropDownData($rota_groups);
        $directorates = Helpers::getDropDownData($directorates);
        $shift_types = Helpers::getDropDownData($shift_types);
        $directorate = null;
        $speciality = null;
        return view('rota-template.createRotaTemplate', compact('total_weeks', 'weekdays', 'directorates', 'specialties', 'directorate', 'speciality', 'rota_groups', 'shift_types'));
    }


    public function storeRotaTemplate(Request $request) {
        /*echo '<pre>';
        print_r($request->all());
        print_r(rtrim($this->getAPIUrl('rota-template'), '/'));
        exit;*/

        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('rota-template'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        $directorate = $request->directorate;
        $speciality = $request->directorate_speciality_id;
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-rotatemplate', $result->error);
            return Redirect::back()->withErrors($result->error);
        } else {
            Session::flash('success', $request->name . ', Rota Template Added sucessfully.');
            return $this->indexRotaTemplate($request);
        }
    }

    public function editRotaTemplate($id)
    {
        $total_weeks = 1;
        $response = $this->api_client->request("GET", $this->getAPIUrl('rota-template') . $id);
        $rotaTemplate = json_decode($response->getBody()->getContents());
        $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $rota_groups = $this->getRotaGroupFromDirectorate($directorates, $rotaTemplate->directorate_id);
        foreach ($rota_groups as $rota_group) {
            if ($rota_group->id == $rotaTemplate->rota_group_id) {
                $shift_types = $this->getShiftTypesFromRotaGroup($rota_group);
                break;
            }
        }
        $rota_groups = Helpers::getDropDownData($rota_groups);
        $directorates = Helpers::getDropDownData($directorates);
        $shift_types = Helpers::getDropDownData($shift_types);
        return view('rota-template.editRotaTemplate', compact('total_weeks', 'weekdays', 'directorates', 'shift_types', 'specialties', 'rotaTemplate', 'rota_groups'));
    }

    public function updateRotaTemplate(Request $request, $id)
    {
        $response = $this->api_client->request("PUT", $this->getAPIUrl('rota-template') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('success', $request->name . ', Rota Template Updated sucessfully.');
        return redirect('/rota-template');
    }

    public function viewRotaTemplate($id)
    {
        $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $response = $this->api_client->request("GET", $this->getAPIUrl('rota-template') . $id);
        $rotaTemplate = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", rtrim($this->getAPIUrl('shift-type'), '/'));
        $shiftTypes = json_decode($response->getBody()->getContents());
        $shiftTypesData = array();
        $templateShiftTypes = array();
        foreach ($rotaTemplate->content->shift_type as $templateshiftype) {
            foreach ($templateshiftype as $shifttype) {
                array_push($templateShiftTypes, $shifttype);
            }
        }
        foreach ($templateShiftTypes as $templateShift) {
            foreach ($shiftTypes as $shiftnode) {
                if ($shiftnode->id == $templateShift) {
                    array_push($shiftTypesData, $shiftnode);
                } elseif ($templateShift == '') {
                    array_push($shiftTypesData, ['name' => 'Off', 'start_time' => '', 'finish_time' => '']);
                    break;
                }
            }
        }
        $weekcycle = count($rotaTemplate->content->shift_type);
        $templatename = $rotaTemplate->name;
        return view('rota-template.viewRotaTemplate', compact('shiftTypesData', 'weekdays', 'templatename'));
    }

    public function destroyRotaTemplate($id)
    {
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('rota-template') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error', 'Rota Template Deleted sucessfully.');
        return redirect('/rota-template');
    }

    public function getShiftsTypeData($did, $sid)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift-type'));
        $shiftTypes = json_decode($response->getBody()->getContents());
        $specialties = array();
        $shifttypes = array();
        foreach ($shiftTypes as $shiftType) {
            if ($shiftType->directorate_speciality_id == $sid && $shiftType->directorate_id == $did) {
                array_push($shifttypes, $shiftType);
            }
        }
        return (['shifttypes' => $shifttypes]);
    }

    public function exportRotaTemplate()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['Directorate', 'Rota Group Name', 'Name', 'Description', 'week cycle', 'Start Date', 'End Date'];
        $ref_data[] = ['Directorate', 'Rota Group Name', 'Name', 'Description', 'week cycle', 'Start Date', 'End Date'];
        $ref_data[] = ['Dummy Directorate name', 'Dummy Rota Group Name', 'Dummy Rota Template name', 'Dummy Rota Tamplate Description', 'Dummy week cycle', '2017-12-22', '2017-12-22'];
        foreach ($directorates as $directorate) {
            if (isset($directorate) && count($directorate))
                foreach ($directorate->rota_group as $rotaGroup) {
//                if(empty($rotaGroup->rota_template)){
//                        array_push($data, [$directorate->name, $rotaGroup->name]);
//                }else{
                    foreach ($rotaGroup->rota_template as $rotaTemplate) {
                        array_push($data, [$directorate->name, $rotaGroup->name, $rotaTemplate->name,
                            $rotaTemplate->description,
                            ($rotaTemplate->content->shift_type == '') ? '' : count($rotaTemplate->content->shift_type),
                            $rotaTemplate->start_date, ($rotaTemplate->end_date == '1111-00-00') ? 'Permanent' : $rotaTemplate->end_date]);
                    }
//                    }
                }
        }
//        dd($data);
        ExcelHelper::exportExcel('Rota-Template_backup', $data, $ref_data);
        Session::flash('success', 'Directorates data exported successfully.');
        return redirect()->back();
    }

    public function importRotaTemplate(Request $request)
    {
        $viewData = [];
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $directorateName = Helpers::gettingArrayOfName($directorates);
//        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $importData = ExcelHelper::importExcel($request->file);
        foreach ($importData as $data) {
            if (in_array($data['directorate'], $directorateName)) {
                $data['directorat_err'] = 'Directorate Already exist';
                foreach ($data as $roteGroups) {

                }
            }
            $viewData = $data;
        }
        dd($viewData);
    }

//  ############################################################################   DIRECTORATE-STAFF FUNCTIONS       ############################################################################

    public function indexDirectorateSpecialityStaff(Request $request)
    {
//        return[$request->all()];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
        $grades = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
        $roles = json_decode($response->getBody()->getContents());
        $selected_grades = (isset($request->selected_grades) ? $request->selected_grades : []);
        $selected_roles = (isset($request->selected_roles) ? $request->selected_roles : []);
        $directorates = $this->getDirectoratesFromDivisions($trust->division);
        $start_date = (isset($request->start_date) ? $this->getCarbonYmdFormat($request->start_date) : $this->getCarbonYmdFormat(date('Y-m-d')));
        $end_date = (isset($request->end_date) ? $this->getCarbonYmdFormat($request->end_date) : $this->getCarbonYmdFormat(date('Y-m-d', strtotime('+1 month'))));
        $directorate = (isset($request->directorate) ? $request->directorate : $directorates[0]->id);
        $specialities = $this->getSpecialityFromDirectorate($directorates, $directorate);
        $directorates = Helpers::getDropDownData($directorates);
//        return [$specialities,$grades,$roles];
        return view('directorate.allStaffSpeciality', compact('directorates', 'specialities', 'directorate', 'start_date', 'end_date', 'grades', 'roles', 'selected_grades', 'selected_roles'));
    }

//  ############################################################################   WORKING LINKS FUNCTION       ############################################################################
    public function getWorkingLinks()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust'));
        $trusts = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
        $trust_data = json_decode($response->getBody()->getContents());
        /* $response = $this->api_client->request("GET", $this->getAPIUrl('user'));
         $users = json_decode($response->getBody()->getContents());*/
        $users = array();
        $response = $this->api_client->request("GET", $this->getAPIUrl('grade'));
        $grades = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('role'));
        $roles = json_decode($response->getBody()->getContents());
        $divisions = $trust_data->division;
        $directorates = $this->getDirectoratesFromDivisions($trust_data->division);
        $staffs = $services = $posts = $rotations = $sites = $locations = $rota_templates = $shift_types = [];
        $specialties = $this->getSpecialtiesFromDirectorates($directorates);
        foreach ($directorates as $directorate) {
            foreach ($directorate->staff as $staff) {
                if (!is_null($staff))
                    array_push($staffs, $staff);
            }
            foreach ($directorate->rota_template as $rota_template) {
                if (!is_null($rota_template))
                    array_push($rota_templates, $rota_template);
            }
        }
        if (count($specialties))
            $specialties = $specialties[0];
        foreach ($specialties as $speciality) {
//            if (count($speciality->service)) {
//                foreach ($speciality->service as $service) {
//                    if (!is_null($service))
//                        array_push($services, $service);
//                }
//            }
//            foreach ($speciality->post as $post) {
//                if (!is_null($post))
//                    array_push($posts, $post);
//            }
        }
        foreach ($trust_data->site as $site) {
            if (!is_null($site))
                array_push($sites, $site);
        }
        foreach ($sites as $site) {
            foreach ($site->location as $location) {
                if (!is_null($location))
                    array_push($locations, $location);
            }
        }
        foreach ($rota_templates as $rota_template) {
//            foreach ($rota_template->shift_type as $shift_type) {
//                if (!is_null($shift_type))
//                    array_push($shift_types, $shift_type);
//            }
        }
//        return[$roles];
        $site_url = env('APP_URL');
        return view('links', compact('site_url', 'trusts', 'divisions', 'directorates', 'staffs', 'specialties', 'services', 'posts', 'sites', 'locations', 'rota_templates', 'shift_types', 'users', 'grades', 'roles'));
    }

//  ############################################################################        CATEGORY RELATED       ############################################################################
    public function showAllCategory()
    {

        __authorize(config('module.category'), 'view', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        return view('category.allCategory', compact('directorates'));
    }

    public function createCategory()
    {
        __authorize(config('module.category'), 'add', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('category.createCategory', compact('directorates'));
    }


    public function storeCategory(Request $request) {
        __authorize(config('module.category'),'add',true);
        /*echo '<pre>';
        print_r(rtrim($this->getAPIUrl('category'), '/'));
        print_r($request->all());
        exit;*/

        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('category'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-category', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-category', 'Category Added successfully.');
            return redirect('/category/');
//            return [$result];
        }
    }

    public function editCategory($id)
    {
        __authorize(config('module.category'), 'edit', true);
        $response = $this->api_client->request("GET", $this->getAPIUrl('category') . $id);
        $category = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $directorates = Helpers::getDropDownData($directorates);
        return view('category.editCategory', compact('category', 'directorates'));
    }

    public function updateCategory(Request $request, $id)
    {
        __authorize(config('module.category'), 'edit', true);
        $response = $this->api_client->request("PUT", $this->getAPIUrl('category') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-category', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-category', 'Category Updated successfully.');
            return redirect('/category/');
        }
    }

    public function destroyCategory($id)
    {
        __authorize(config('module.category'), 'delete', true);
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('category') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-category', $result->error);
        return redirect('/category');
    }

    public function getCategories($id)
    {
        $categories = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $categories = $this->getCategoryFromDirectorate($directorates, $id);
        $categories = Helpers::getDropDownData($categories);
        return response()->json($categories);
    }

    public function exportCategory()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['Directorate', 'name'];
        $ref_data[] = ['Directorate', 'name'];
        $ref_data[] = ['Dummy Directorate name', 'Dummy Category name'];
        foreach ($directorates as $directorate) {
            if (isset($directorate) && count($directorate))
                foreach ((($directorate->category) != [] ? $directorate->category : array()) as $category) {
                    array_push($data, [$directorate->name, $category->name]);
                }
        }
        ExcelHelper::exportExcel('Category_backup', $data, $ref_data);
        Session::flash('success-category', 'Directorates data exported successfully.');
        return redirect()->back();
    }

    public function importCategory(Request $request)
    {
        $checked_data = [];
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $importData = ExcelHelper::importExcel($request->file);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_name = Helpers::gettingArrayOfName($directorates);
        $category_name = Helpers::gettingArrtayOfCategoryName($directorates);
        foreach ($importData as $data) {
            if (empty($data['directorate'])) {
                $data['directorate'] = '';
                $data['directorate_required'] = 'directoreate or category name requrired';
            }
            if (empty($data['name'])) {
                $data['name'] = '';
                $data['directorate_required'] = 'directoreate or category name requrired';
            }
            if (in_array($data['directorate'], $directorate_name) && in_array($data['name'], $category_name)) {
                $data['diractorate_err'] = ' directorate name and category name already exits';
            } elseif (!in_array($data['directorate'], $directorate_name)) {
                $data['diractorate_not_exits'] = ' directorate name not exits in record';
            }
            $viewData [] = $data;
        }
        $fileName = Input::file('file')->getClientOriginalName();
        return view('category.viewUploadedData', ['viewData' => $viewData, 'fileName' => $fileName]);
    }

    public function importCategoryUpload(Request $request)
    {
//        return [$request->all()];
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->fileName), function ($reader) {
            $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('category') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->success)) {
            Session::flash('success-category', $result->success);
            return redirect('/category');
        } else {
            Session::flash('error-category', $result->error);
            return redirect('/category');
        }
    }

//  ############################################################################        SERVICE SUB CATEGORY RELATED       ############################################################################
    public function showAllSubCategory()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('sub-category'));
        $sub_categories = json_decode($response->getBody()->getContents());
        return view('category.allCategory', compact('sub_categories'));
    }

    public function indexSubCategory(Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = null;
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        $categories = $this->getCategoryFromDirectorate($directorates, $directorate_id);
        $directorates = Helpers::getDropDownData($directorates);
        return view('sub-category.allSubCategory', compact('categories', 'directorate_id', 'directorates'));
    }

    public function getSubCategories($d_id, $c_id)
    {
        $sub_categories = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $sub_categories = [];
        $categories = $this->getCategoryFromDirectorate($directorates, $d_id);
        if (count($categories))
            $sub_categories = $this->getSubCategoriesFromCategory($categories, $c_id);
        $sub_categories = Helpers::getDropDownData($sub_categories);
        return response()->json($sub_categories);
    }

    public function getSpecialitySubCategories($d_id, $c_id, $s_id)
    {
        $sub_categories = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directorates = json_decode($response->getBody()->getContents());
        $sub_categories = [];
        $categories = $this->getCategoryFromDirectorate($directorates, $d_id);
        if (count($categories))
            $subcategories = $this->getSubCategoriesFromCategory($categories, $c_id);
        foreach ($subcategories as $node) {
            if (isset($node->specialities) && in_array($s_id, $node->specialities))
                $sub_categories[$node->id] = $node->name;
        }
        return response()->json($sub_categories);
    }

//  ############################################################################        SHIFT SUB CATEGORY RELATED       ############################################################################
    public function showAllShiftDetail($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift-type') . $id);
        $shift_type = json_decode($response->getBody()->getContents());
        if (count($shift_type->shift_detail))
            return redirect('/shift/' . $id . '/shift-detail/edit');
        else
            return redirect('/shift/' . $id . '/shift-detail/new');
    }

    public function createShiftDetail($id)
    {
        // Fix error in view in case of no sub_categories
        $sub_categories = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift-type') . $id);
        $shift_type = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directoratesForTree = json_decode($response->getBody()->getContents());
        if (count($shift_type->shift_detail))
            return redirect('/shift/' . $id . '/shift-detail/edit');
        else {
            $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
            $directorates = json_decode($response->getBody()->getContents());
            $directorate_id = $speciality_id = $category_id = $subcategory_id = null;
            $speciality_nodes = $categories = [];
            if (count($directorates)) {
                $directorate_id = $directorates[0]->id;
                $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
                $speciality_nodes [] = ['id' => 0, 'name' => 'Parent speciality'];
                $speciality_nodes [] = ['id' => -1, 'name' => 'Directorate Services'];
                foreach ($specialities as $speciality) {
                    array_push($speciality_nodes, $speciality);
                }
                if (count($specialities))
                    $speciality_id = 0;
                $categories = Helpers::getAllNodesByIdFromMainNode($directoratesForTree, 'category', $directorate_id, 'directorate_id');
                if (count($categories))
                    $category_id = $categories[0]->id;
                $subcategories = Helpers::getAllNodesByIdFromMainNode($categories, 'sub_category', $categories[0]->id, 'category_id');
                if (count($subcategories)) {
                    $subcategory_id = $subcategories[0]->id;
                    foreach ($subcategories as $node) {
                        if (isset($node->specialities) && in_array($specialities[0]->id, $node->specialities)) {
                            $sub_categories[$node->id] = $node->name;
                        }
                    }
                }
            }
            $combined_services = $this->getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_id, $category_id, $subcategory_id);
            $tree = $this->getTree($categories);
            $specialities = Helpers::getDropDownData($speciality_nodes);
            $categories = Helpers::getDropDownData($categories);
            return view('shift-detail.createShiftDetail', compact('categories', 'shift_type', 'specialities', 'combined_services', 'categories', 'sub_categories', 'tree'));
        }
    }

    public function storeShiftDetail(Request $request)
    {
        $data = [];
        for ($i = 0; $i <= 2; $i++) {
            $service = [];
            if (isset($request->service_id[$i])) {
                $service = explode('-', $request->service_id[$i]);
                $service_id = $service[1];
                $service_type = $service[0];
            } else {
                $service_id = 0;
                $service_type = 'none';
            }
            if (isset($request->directorate_speciality_id[$i])) {
                $data[] = [
                    'directorate_speciality_id' => $request->directorate_speciality_id[$i],
                    'shift_id' => $request->shift_id,
                    'service_id' => $service_id,
                    'service_type' => $service_type,
                    'category_id' => $request->category_id[$i],
                    'start_time' => $request->start_time[$i],
                    'end_time' => $request->end_time[$i],
                    'overrides_teaching' => (isset($request->overrides_teaching[$i]) ? $request->overrides_teaching[$i] : 0),
                    'nroc' => (isset($request->nroc[$i]) ? $request->nroc[$i] : 0),
                    'on_site' => (isset($request->on_site[$i]) ? 1 : 0)
                ];
            }
        }
        /*echo '<pre>';
        print_r(json_encode($data));
        print_r(rtrim($this->getAPIUrl('shift-detail'), '/'));
        exit;*/
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('shift-detail'), '/'), ['form_params' => $data]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-shift-detail', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-shift-type', 'shift split Time Added successfully.');
            return redirect('/shift-type');
        }
    }

    public function editShiftDetail($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('shift-type') . $id);
        $shift_type = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/category/subcategory');
        $directoratesForTree = json_decode($response->getBody()->getContents());
        $shift_detail = $shift_type->shift_detail;
        if (!count($shift_detail))
            return redirect('/shift/' . $id . '/shift-detail/new');
        else {
            $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
            $directorates = json_decode($response->getBody()->getContents());
            $speciality_nodes[] = ['id' => 0, 'name' => 'Parent Speciality'];
            $speciality_nodes [] = ['id' => -1, 'name' => 'Directorate Services'];
            $directorate_id = (isset($shift_detail[0]->speciality->directorate_id) ? $shift_detail[0]->speciality->directorate_id : $directorates[0]->id);
            $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
            foreach ($specialities as $speciality) {
                array_push($speciality_nodes, $speciality);
            }
            $categories = Helpers::getAllNodesByIdFromMainNode($directoratesForTree, 'category', $directorate_id, 'directorate_id');
            $speciality_one = (isset($shift_detail[0]->directorate_speciality_id) ? $shift_detail[0]->directorate_speciality_id : $specialities[1]->id);
            $shiftdetail_one = $this->getCategorySubcategoryForShiftDetails($categories, $shift_detail[0]);
            $category_one = $shiftdetail_one['category'];
            $sub_categories_one = $shiftdetail_one['subcategories'];
            $sub_category_one = $shiftdetail_one['subcategory'];
            $combined_services_one = $this->getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_one, $category_one, $sub_category_one);

            $speciality_two = (isset($shift_detail[1]->directorate_speciality_id) ? $shift_detail[1]->directorate_speciality_id : $specialities[0]->id);
            if (isset($shift_detail[1])) {
                $shiftdetail_two = $this->getCategorySubcategoryForShiftDetails($categories, $shift_detail[1]);
                $category_two = $shiftdetail_two['category'];
                $sub_categories_two = $shiftdetail_two['subcategories'];
                $sub_category_two = $shiftdetail_two['subcategory'];
            } else {
                $category_two = null;
                $sub_categories_two = [];
                $sub_category_two = null;
            }
            $combined_services_two = $this->getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_two, $category_two, $sub_category_two);

            $speciality_three = (isset($shift_detail[2]->directorate_speciality_id) ? $shift_detail[2]->directorate_speciality_id : $specialities[0]->id);
            if (isset($shift_detail[2])) {
                $shiftdetail_three = $this->getCategorySubcategoryForShiftDetails($categories, $shift_detail[2]);
                $category_three = $shiftdetail_three['category'];
                $sub_categories_three = $shiftdetail_three['subcategories'];
                $sub_category_three = $shiftdetail_three['subcategory'];
            } else {
                $category_three = null;
                $sub_categories_three = [];
                $sub_category_three = null;
            }
            $combined_services_three = $this->getServicesForShiftType_Detial($directorates, $directorate_id, $specialities, $speciality_three, $category_three, $sub_category_three);
            $tree = $this->getTree($categories);
            $specialities = Helpers::getDropDownData($speciality_nodes);
            $categories = Helpers::getDropDownData($categories);
            $sub_categories_one = Helpers::getDropDownData($sub_categories_one);
            $sub_categories_two = Helpers::getDropDownData($sub_categories_two);
            $sub_categories_three = Helpers::getDropDownData($sub_categories_three);
            return view('shift-detail.editShiftDetail', compact('speciality_one', 'speciality_two', 'speciality_three', 'shift_detail', 'specialities', 'combined_services_one', 'combined_services_two', 'combined_services_three', 'categories', 'category', 'shift_type', 'category_one', 'category_two', 'category_three', 'speciality_three', 'speciality_two', 'speciality_one', 'sub_category_one', 'sub_categories_one', 'sub_category_two', 'sub_categories_two', 'sub_category_three', 'sub_categories_three', 'tree'));
        }
    }

    public function updateShiftDetail(Request $request)
    {
        $data = [];
        for ($i = 0; $i <= 2; $i++) {
            $service = [];
            if (isset($request->service_id[$i])) {
                $service = explode('-', $request->service_id[$i]);
                $service_id = $service[1];
                $service_type = $service[0];
            } else {
                $service_id = 0;
                $service_type = 'none';
            }
            if (isset($request->directorate_speciality_id[$i]) || isset($request->id[$i])) {
                $data[] = [
                    'id' => (isset($request->id[$i]) ? $request->id[$i] : null),
                    'directorate_speciality_id' => (isset($request->directorate_speciality_id[$i]) ? $request->directorate_speciality_id[$i] : null),
                    'shift_id' => (isset($request->shift_id) ? $request->shift_id : null),
                    'service_id' => $service_id,
                    'service_type' => $service_type,
                    'category_id' => (isset($request->category_id[$i]) ? $request->category_id[$i] : null),
                    'start_time' => (isset($request->start_time[$i]) ? $request->start_time[$i] : null),
                    'end_time' => (isset($request->end_time[$i]) ? $request->end_time[$i] : null),
                    'overrides_teaching' => (isset($request->overrides_teaching[$i]) ? $request->overrides_teaching[$i] : 0),
                    'nroc' => (isset($request->nroc[$i]) ? $request->nroc[$i] : 0),
                    'on_site' => (isset($request->on_site[$i]) ? 1 : 0)
                ];
            }
        }
        $response = $this->api_client->request("PUT", $this->getAPIUrl('shift-detail') . $request->shift_id, ['form_params' => $data]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-shift-detail', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-shift-type', 'Shift Time Updated successfully.');
            return redirect('/shift-type');
        }
    }

    public function destroyShiftDetail($id)
    {
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('shift-detail') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-shift-detail', $result->error);
        return Redirect::back();
    }

//  ############################################################################        ROTA GROUP CATEGORY RELATED       ############################################################################
//    public function createRotaGroup() {
//        
//    }
//
//    public function storeRotaGroup(Request $request) {
//        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('rota-group'), '/'), ['form_params' => $request->all()]);
//        $result = json_decode($response->getBody()->getContents());
//        if (isset($result->error) && count($result->error)) {
//            Session::flash('error-rota-group', $result->error);
//            return Redirect::back();
//        } else {
//            Session::flash('success-rota-group', 'Rota Group Added successfully.');
//            return redirect('/rota-group/');
//        }
//    }
//
//    public function editRotaGroup($id) {
//        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id);
//        $trusts = json_decode($response->getBody()->getContents());
//        $directorates = $this->getDirectoratesFromDivisions($trusts->division);
//        $directorates = Helpers::getDropDownData($directorates);
//        $response = $this->api_client->request("GET", $this->getAPIUrl('rota-group') . $id);
//        $rota_group = json_decode($response->getBody()->getContents());
//        return view('rota-group.editRotaGroup', compact('rota_group', 'directorates'));
//    }
//
//    public function updateRotaGroup(Request $request, $id) {
//        $response = $this->api_client->request("PUT", $this->getAPIUrl('rota-group') . $id, ['form_params' => $request->all()]);
//        $result = json_decode($response->getBody()->getContents());
//        if (isset($result->error) && count($result->error)) {
//            Session::flash('error-rota-group', $result->error);
//            return Redirect::back();
//        } else {
//            Session::flash('success-rota-group', 'Rota Group Updated successfully.');
//            return redirect('/rota-group/');
//        }
//    }
//
//    public function destroyRotaGroup($id) {
//        $response = $this->api_client->request("DELETE", $this->getAPIUrl('rota-group') . $id);
//        $result = json_decode($response->getBody()->getContents());
//        Session::flash('error-rota-group', $result);
//        return Redirect::back();
//    }

    public function getRotaGroupRotaTemplates($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('rota-group') . $id);
        $rota_groups = json_decode($response->getBody()->getContents());
        return response()->json($rota_groups->rota_template);
    }

    public function exportRotaGroup()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['name', 'description', 'directorate'];
        $ref_data[] = ['name', 'description', 'directorate'];
        $ref_data[] = ['Dummy Rota Group', 'Description for Rota Group', 'Dummy Directorate'];
        foreach ($directorates as $directorate) {
            foreach ($directorate->rota_group as $rota_group) {
                if (isset($rota_group) && count($rota_group))
                    array_push($data, [$rota_group->name, $rota_group->description, $directorate->name]);
            }
        }
        $this->exportExcel('Rota_Group_backup', $data, $ref_data);
        Session::flash('success-rota-group', 'Rota Groups data exported successfully.');
        return redirect('/rota-group');
    }

    public function importRotaGroup(Request $request)
    {
        $validator = $this->importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = $this->importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
            $directorates = json_decode($response->getBody()->getContents());
            $directorate_names = $this->gettingArrayOfName($directorates);
            foreach ($data as $data_node) {
                $rota_groups_names = [];
                foreach ($directorates as $directorate) {
                    if ($directorate->name == $data_node['directorate'])
                        $rota_groups_names = $this->gettingArrayOfName($directorate->rota_group);
                }
                if (in_array($data_node['name'], $rota_groups_names))
                    $data_node['rota_group_error'] = 'This rota group already exist for this directorate.';
                if (!in_array($data_node['directorate'], $directorate_names))
                    $data_node['rota_group_error'] = 'This directorate does not exist in system.';
                if (!isset($data_node['description']))
                    $data_node['description'] = '';
                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('rota-group.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function uploadRotaGroup(Request $request)
    {
//	return [auth()->user()->api_token];
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function ($reader) {
            $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        $request->merge(['data' => $records]);
        $response = $this->api_client->request("POST", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group/upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-directorate', $result->error);
            return redirect('/rota-group');
        } else {
            Session::flash('success-directorate', $result->success);
            return redirect('/rota-group');
        }
    }

//  ############################################################################        ADDITIONAL GRADE RELATED       ############################################################################
    public function showAllAdditionalGrade(Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate_id = null;
        $specialties = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        $directorate_roles = $directorate_grades = '';
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $all_directorates = json_decode($response->getBody()->getContents());
        $grades = $this->getGradesFromDirectorates($all_directorates, $directorate_id);
        $roles = $this->getRolesFromDirectorate($all_directorates, $directorate_id);
        foreach ($grades as $grade) {
            if (isset($grade) && $grade->directorate_id == $directorate_id)
                $directorate_grades = $directorate_grades . $grade->name . ',';
        }
        foreach ($roles as $role) {
            if (isset($role) && $role->directorate_id == $directorate_id)
                $directorate_roles = $directorate_roles . $role->name . ',';
        }
        $directorates = Helpers::getDropDownData($directorates);
        return view('additional-grade.staffOverview', compact('specialties', 'directorates', 'directorate_grades', 'directorate_roles', 'directorate_id'));
    }

    public function showAllAdditionalGradeTest(Request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
//        return [$directorates];
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate_id = null;
        $specialties = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        $directorate_roles = $directorate_grades = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $all_directorates = json_decode($response->getBody()->getContents());
//        return [$all_directorates];
        $grades = $this->getGradesFromDirectorates($all_directorates, $directorate_id);
        $roles = $this->getRolesFromDirectorate($all_directorates, $directorate_id);
        mt_srand(250);
        foreach ($roles as $role) {
            if (isset($role) && $role->directorate_id == $directorate_id) {
                $role_text[] = $role->name;
                $color = $this->random_color();
                $directorate_roles[] = ['name' => $role->name, 'color' => $color, 'id' => $role->id];
            }
        }
        if (!empty($directorate_roles))
            $role_width = floor(100 / count($directorate_roles)) - 1;
        $role_coutns = [];
        foreach ($grades as $grade) {
            foreach ($directorate_roles as $role) {
                if ($role['id'] == $grade->role->id) {
                    $color = $role['color'];
                }
            }
            if (isset($grade) && $grade->directorate_id == $directorate_id) {
                $grades_text[] = $grade->name;
                $directorate_grades[] = ['name' => $grade->name, 'color' => $color, 'role' => $grade->role->id];
            }
        }
        if (!empty($directorate_grades))
            $grade_width = floor(100 / count($directorate_grades)) - 1;
        $directorates = Helpers::getDropDownData($directorates);
        return view('additional-grade.staffOverview', compact('specialties', 'directorates', 'directorate_grades', 'directorate_roles', 'directorate_id', 'grade_width', 'grades_text', 'role_width', 'role_text'));
    }

    public function indexAdditionalGrade(request $request)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate_id = null;
        $specialities = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        $directorates = Helpers::getDropDownData($directorates);
        return view('additional-grade.gradesView', compact('specialities', 'directorates', 'directorate_id'));
    }

    public function createAdditionalGrade(Request $request)
    {
//        return [$request->all()];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        if (count($directorates))
            $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        else
            $directorate_id = null;
        $grades = $this->getGradesFromDirectorates($directorates, $directorate_id);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
        $specialties = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        if (count($specialties))
            $additional_grades = $this->getGradesPositionsFromSpecialties($specialties, $specialties[0]->id);
        else
            $additional_grades = [];
        $grades = Helpers::getDropDownData($grades);
        $specialties = Helpers::getDropDownData($specialties);
        $directorates = Helpers::getDropDownData($directorates);
        $additional_grades = Helpers::getDropDownData($additional_grades);
        return view('additional-grade.createGrade', compact('grades', 'specialties', 'directorate_id', 'directorates', 'additional_grades'));
    }

    public function storeAdditionalGrade(Request $request)
    {
        if (!isset($request->position))
            $request->merge(['position' => 1]);
        else
            $request->merge(['position' => $request->position + $request->place]);
        $response = $this->api_client->request("POST", rtrim($this->getAPIUrl('additional-grade'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());

        if (isset($result->error) && count($result->error)) {
            Session::flash('error-grade', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-grade', $result->success);
            return redirect('/additional-grade/');
        }
    }

    public function editAdditionalGrade(Request $request, $id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('additional-grade') . $id);
        $grade = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = $grade->directorate_speciality->directorate_id;
        $grades = $this->getGradesFromDirectorates($directorates, $directorate_id);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
        $specialties = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        $additional_grades = $this->getGradesPositionsFromSpecialties($specialties, $specialties[0]->id);
        $specialties = $this->getSpecialityFromDirectorate($directorates, $directorate_id);
        $grades = Helpers::getDropDownData($grades);
        $specialties = Helpers::getDropDownData($specialties);
        $directorates = Helpers::getDropDownData($directorates);
        $additional_grades = Helpers::getDropDownData($additional_grades);
        return view('additional-grade.editGrade', compact('grades', 'specialties', 'directorate_id', 'directorates', 'grade', 'additional_grades'));
    }

    public function updateAdditionalGrade(Request $request, $id)
    {
        $request->merge(['position' => $request->position + $request->place]);
        $response = $this->api_client->request("PUT", $this->getAPIUrl('additional-grade') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-grade', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-grade', 'Grade Updated successfully.');
            return redirect('/additional-grade/');
        }
    }

    public function destroyAdditionalGrade($id)
    {
        $response = $this->api_client->request("DELETE", $this->getAPIUrl('additional-grade') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-grade', $result->error);
        return Redirect::back();
    }

    public function exportAdditionalGrade()
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
        $data[] = ['position', 'name', 'grade', 'speciality', 'directorate'];
        $ref_data[] = ['position', 'name', 'grade', 'speciality', 'directorate'];
        $ref_data[] = ['integer', 'Dummy Additional Grade', 'Dummy Grade', 'Dummy Speciality', 'Dummy Directorate'];
        foreach ($directorates as $directorate) {
            $directorate->name = isset($directorate->name) ? $directorate->name:null;
            foreach ($directorate->speciality as $specialtiy) {
                $specialtiy->name = isset($specialtiy->name)? $specialtiy->name:null;
                foreach ($specialtiy->additional_grade as $grade) {
                    if (isset($grade) && count($grade))
                        $grade->position = isset($grade->position) ? $grade->position : null;
                        $grade->name = isset($grade->name) ? $grade->name : null;
                        $gradeName = (isset($grade->grade) && isset($grade->grade->name)) ? $grade->grade->name : null;
                        array_push($data, [$grade->position, $grade->name, $gradeName, $specialtiy->name, $directorate->name]);
                }
            }
        }

        $this->exportExcel('Additional_Grade_backup', $data, $ref_data);
        Session::flash('success-grade', 'Grades data exported successfully.');
        return redirect('/additional-grade');
    }

    public function getAdditionalGradePositions($id, $d_id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
        $directorates = json_decode($response->getBody()->getContents());
        $specialties = $this->getSpecialityFromDirectorate($directorates, $d_id);
        $additional_grades = $this->getGradesPositionsFromSpecialties($specialties, $id);
        $additional_grades = Helpers::getDropDownData($additional_grades);
        return response()->json($additional_grades);
    }

    public function importAdditionalGrade(Request $request)
    {
        $validator = $this->importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        if ($upload) {
            $data = $this->importExcel(Input::file('file'));
            $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/specilality/additionalgrade');
            $directorates = json_decode($response->getBody()->getContents());
            $directorate_names = $this->gettingArrayOfName($directorates);
            /*echo '<pre>';
            print_r($data);
            exit;*/
            foreach ($data as $data_node) {
                $speciality_names = $additional_grades = $grades_names = [];
                foreach ($directorates as $directorate) {
                    if ($directorate->name == $data_node['directorate']) {
                        $grades_names = $this->gettingArrayOfName($directorate->grade);
                        $speciality_names = $this->gettingArrayOfName($directorate->speciality);
                        foreach ($directorate->speciality as $speciality) {
                            if ($data_node['speciality'] == $speciality->name) {
                                $additional_grades = $this->gettingArrayOfName($speciality->additional_grade);
                            }
                        }
                    }
                }
                if (!in_array($data_node['directorate'], $directorate_names))
                    $data_node['directorate_error'] = 'This directorate does not exist in the system.';
                if (!in_array($data_node['grade'], $grades_names))
                    $data_node['grade_error'] = 'This grade does not exist in the system or in this directorate. ';
                if (!in_array($data_node['speciality'], $speciality_names))
                    $data_node['speciality_error'] = 'This speciality does not exist in the system or in this directorate.';
                if (in_array($data_node['name'], $additional_grades))
                    $data_node['additional_grade_error'] = 'This Additional grade already exist for this speciality.';

                $view_data [] = $data_node;
            }
            $file_name = Input::file('file')->getClientOriginalName();
            return view('additional-grade.viewUploadedData', compact('view_data', 'file_name'));
        }
    }

    public function uploadAdditionalGrade(Request $request)
    {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->file_name), function ($reader) {
            $reader->ignoreEmpty();
            $records = $reader->all();
            return $records->toArray();
        })->toArray();
        $request->merge(['data' => $records]);
        $response = $this->api_client->request("POST", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/speciality/additional-grade/upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-speciality', $result->error);
            return redirect('/additional-grade');
        } else {
            Session::flash('success-speciality', $result->success);
            return redirect('/additional-grade');
        }
    }

//#################################### SERVICES POSTS VIEW ######################
    protected function getSitesLocations($sites)
    {
        $site_array = $location_array = [];
        foreach ($sites as $site) {
            $site_array[$site->name] = $site->id;
            if (isset($site->location) && count($site->location))
                foreach ($site->location as $location) {
                    $location_array[$site->name][$location->name] = $location->id;
                }
        }
        return [$site_array, $location_array];
    }

    protected function getFormatedService($service, $sites_names)
    {
        $results = [
            'name' => $service->name,
            'category' => (isset($service->category) ? $service->category->name : '-'),
            'contactable' => $service->contactable,
            'min_standards' => $service->min_standards,
            'sites' => implode(',', array_unique($sites_names))
        ];
        return $results;
    }

    protected function getServiceWithSites($nodes, $service_node, $id, $match_id, $sites)
    {
        $results = [];
        $services = Helpers::getAllNodesByIdFromMainNode($nodes, $service_node, $id, $match_id);
        foreach ($services as $service) {
            $sites_names = [];
            if (isset($service->template->events) && count($service->template->events)) {
                foreach ($service->template->events as $event) {
                    if (isset($event->site_id))
                        $sites_names[] = array_search($event->site_id, $sites);
                }
            }
            $results [] = $this->getFormatedService($service, $sites_names);
        }
        return $results;
    }

    protected function getSpecialitiesFormated($directorates, $directorate_id)
    {
        $results = [];
        foreach ($directorates as $directorate) {
            if ($directorate->id == $directorate_id) {
                if (isset($directorate->speciality) && count($directorate->speciality)) {
                    foreach ($directorate->speciality as $speciality) {
                        $results[] = $speciality->id;
                    }
                }
            }
        }
        return $results;
    }

    public function servicePostView(Request $request)
    {
        $speciality_id = null;
        $post_array = [];
        $form_data = ['trust_id' => auth()->user()->trust_id];
        $response = $this->api_client->request("POST", $this->getAPIUrl('service') . 'post/view', ['form_params' => $form_data]);
        $directorates = json_decode($response->getBody()->getContents());
        $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        $speciality_array = $this->getSpecialitiesFormated($directorates, $directorate_id);
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
        if (count($specialities))
            $speciality_id = ((isset($request->speciality_id) && in_array($request->speciality_id, $speciality_array)) ? $request->speciality_id : $specialities[0]->id);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        list($site_array, $location_array) = $this->getSitesLocations($sites);
        $directorate_services = $this->getServiceWithSites($directorates, 'services', $directorate_id, 'directorate_id', $site_array);
        $speciality_services = $this->getServiceWithSites($specialities, 'service', $speciality_id, 'directorate_speciality_id', $site_array);
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/additionalparameter');
        $directorate_nodes = json_decode($response->getBody()->getContents());
        $parameters = Helpers::getAllNodesByIdFromMainNode($directorate_nodes, 'additional_parameter', $directorate_id, 'directorate_id');
        foreach ($parameters as $parameter) {
            $parameter_array[$parameter->parameter] = $parameter->id;
        }
        $posts = Helpers::getAllNodesByIdFromMainNode($specialities, 'post', $speciality_id, 'directorate_speciality_id');
        foreach ($posts as $post) {
            if (isset($post->additional_parameters) && !is_null($post->additional_parameters)) {
                foreach ($post->additional_parameters as $key => $node) {
                    $paramater_name = array_search($key, $parameter_array);
                    $post->parameter[] = $paramater_name . ' - (' . implode(',', array_map('ucfirst', $node)) . ')';
                }
            }
            $post_array[] = $post;
        }
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        return view('service.servicePostView', compact('directorates', 'specialities', 'directorate_id', 'speciality_id', 'post_array', 'directorate_services', 'speciality_services'));
    }

//    public function servicePostData() {
//        $form_data = ['trust_id'=>auth()->user()->trust_id];
//        $response = $this->api_client->request("POST", $this->getAPIUrl('service'). 'post/view', ['form_params' => $form_data]);
//        $directorates = json_decode($response->getBody()->getContents());
//        $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
//        $categories = Helpers::getAllNodesByIdFromMainNode($directorates, 'category', $directorate_id, 'directorate_id');
//        $directorate_services = Helpers::getAllNodesFromMainNode($categories, 'directorate_service');
//        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
//        $speciality_id = (isset($request->speciality_id) ? $request->speciality_id : $specialities[0]->id);
//        $speciality_services = Helpers::getAllNodesByIdFromMainNode($specialities, 'service', $speciality_id, 'directorate_speciality_id');
//        $posts = Helpers::getAllNodesByIdFromMainNode($specialities, 'post', $speciality_id, 'directorate_speciality_id');
//        $directorates = Helpers::getDropDownData($directorates);
//        $specialities = Helpers::getDropDownData($specialities);
//        return response()->json($categories);
//    }
//  ########################################################       Groups RELATED       ############################################################################

    public function getGrade($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/grade-role');
        $grades_roles = json_decode($response->getBody()->getContents());
        $grades = $this->getGradesFromDirectorates($grades_roles, $id);
        $grades = Helpers::getDropDownData($grades);
        return response()->json($grades);
    }

//  ############################################################################   Sub Group  RELATED   ############################################################################

    public function getGroups($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $gorups = $this->getGroupsFromDirectorate($directorates, $id);
        $gorups = Helpers::getDropDownData($gorups);
        return response()->json($gorups);
    }

    public function getSubgroups($g_id, $d_id)
    {
        $sub_groups = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/groups/subgroup');
        $directorates = json_decode($response->getBody()->getContents());
        $gorup_nodes = $this->getGroupsFromDirectorate($directorates, $d_id);
        $subgorup_nodes = $this->getSubgroupsFromGroups($gorup_nodes, $g_id);
        foreach ($subgorup_nodes as $sub_group) {
            $sub_groups[$sub_group->id] = $sub_group->name;
        }
        return response()->json($sub_groups);
    }

//  ############################################################################   Header Date  RELATED   ############################################################################
    public function setDateInSession(Request $request)
    {
        session()->forget('date');
        session()->forget('date_time');
        session()->forget('view');
        $date = $this->getCarbonYmdFormat($request->date);
        session()->put('date', $date);
        session()->put('date_time', $request->date);
        session()->put('view', 'agendaWeek');
        return response()->json(session()->get('date_time'));
    }

    public function setCalenderDateInSession(Request $request)
    {
        session()->forget('date');
        session()->forget('view');
        $date = $this->getCarbonYmdFormat($request->date);
        session()->put('date', $date);
        session()->put('view', $request->view);
    }

//################################# Ajax Calls #################################
    public function getShiftTypeService($d_id, $s_id, $c_id, $sb_id)
    {
        $combine_services = [];
        $response = $this->api_client->request("GET", $this->getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/all-data');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $d_id, 'directorate_id');
        $combine_services = $this->getServicesForShiftType_Detial($directorates, $d_id, $specialities, $s_id, $c_id, $sb_id);
        return response()->json($combine_services);
    }

    public function getCategoryTree($id)
    {
        $response = $this->api_client->request("GET", $this->getAPIUrl('directorate') . $id . '/category');
        $categories = json_decode($response->getBody()->getContents());
        $tree = app('App\Http\Controllers\ServiceController')->getTree($categories);
        return $tree;
    }

    public function getUserEmails()
    {


        $emails = array();
        $apiUrl = Helpers::getAPIUrl('getUserEmails');
        $apiUrl = $apiUrl . '/' . auth()->user()->id;
        $get = __get($apiUrl);

        if ($get['status']) {
            $result = $get['apiResponseData'];
            if($result->status) {
                $emails=$result->data;
            }

            return view('emails',compact('emails'));


        }

    }

    public function uploadEmailTemplateImage()
    {
        $file = Input::file('image');
        $destinationPath = 'upload/';
        $filename = time().'.'.$file->getClientOriginalExtension();;
        Input::file('image')->move($destinationPath, $filename);
        $path=asset('upload/'.$filename);
        return $path;

    }
}
