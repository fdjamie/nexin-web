<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use MaddHatter\LaravelFullcalendar\Event;
use Yajra\Datatables\Facades\Datatables;

class ServiceOverviewController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index(Request $request) {
        $transition_times = $transition_times_services = $transition_times_staff = [];
        $date = new Carbon(session()->get('date_time'));
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'overview/' . auth()->user()->trust_id . '/' . $date->format('Y-m-d'));
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate') . auth()->user()->trust_id . '/transition-time');
        $directorates_times = json_decode($response->getBody()->getContents());
        $directorate_id = (isset($request->directorate_id) ? $request->directorate_id : $directorates[0]->id);
        $transition_times = [];
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
        $directorates_dropdown = Helpers::getDropDownData($directorates);
        $specialities_dropdown = Helpers::getDropDownData($specialities);
        $first_speciality_id = null;

        $testing_services = [];
        if (count($specialities))
            $first_speciality_id = $specialities[0]->id;
        $speciality_id = (isset($request->speciality_id) ? $request->speciality_id : $first_speciality_id);
        $speciality_array = $this->getSpecialityArray($directorates);
        $speciality_id = ((isset($speciality_array[$directorate_id]) && array_search($speciality_id, $speciality_array[$directorate_id])) ? $speciality_id : $first_speciality_id);
        foreach ($directorates_times as $directorate) {
            foreach ($directorate->transition_time as $time) {
                $transition_times[$directorate->id][] = ['id' => $time->id, 'name' => $time->name, 'start_time' => $time->start_time, 'end_time' => $time->end_time];
            }
        }
        if (empty($transition_times)) {
            Session::flash('error-service-overview', 'Please add transition times to view service overview.');
            return view('service.serviceOverview', compact('staffs', 'checks', 'count', 'service_info', 'directorate_id', 'speciality_id', 'specialities_dropdown', 'directorates_dropdown', 'requirements_array', 'staff_array', 'priority_set', 'calendar', 'events', 'transition_times_services', 'transition_times_staff', 'transition_times'));
        }
        $day = $date->dayOfWeek;
        $calendar = $events = [];
        $staff_timeline = $service_timeline = [];
        $service_info = $requirements = $events = $resources = $priority_set = $requirement_set = [];
        if (!is_null($speciality_id)) {
            $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate') . $directorate_id . '/speciality/' . $speciality_id . '/date/' . strtotime($date->format('Y-m-d')) . '/over-view');
            $data = json_decode($response->getBody()->getContents());
            if (!isset($data->speciality[0]->priority_set) || empty($data->speciality[0]->priority_set[0]->priorities)) {
                Session::flash('error-service-overview', 'Please create priority set for viewing service overview.');
                return view('service.serviceOverview', compact('staffs', 'checks', 'count', 'service_info', 'directorate_id', 'speciality_id', 'specialities_dropdown', 'directorates_dropdown', 'requirements_array', 'staff_array', 'priority_set', 'calendar', 'events', 'transition_times_services', 'transition_times_staff', 'transition_times'));
            }
            $priority_set = (isset($data->speciality[0]->priority_set[0]->priorities) ? $this->getPrioritySet($data->speciality[0]->priority_set[0]->priorities) : []);
            $allservices = $this->getAllService($data, $date->weekOfYear, $day);
            $requirements = $this->getServicesEventByDate($allservices, $day, $date);
            $request->merge(['directorate_id' => $directorate_id, 'speciality_id' => $speciality_id, 'date' => $date]);
            $colors = app('App\Http\Controllers\ColorController')->getColorsCode();
            list($service_resources, $service_events) = $this->getServicesTimeline($request, true);
            $transition_times_services = $this->getTransitionEvents($date, $service_events, $transition_times[$directorate_id], 'service');
            list($staff_resources, $staff_events) = $this->getStaffTimeline($request, true);
            list($transition_times_staff, $services_staff) = $this->getTransitionEvents($date, $staff_events, $transition_times[$directorate_id], 'staff', $transition_times_services);
            $response = $this->api_client->request("POSt", Helpers::getAPIUrl('directorate-staff') . 'availability', ['form_params' => $transition_times_staff]);
            $staff_availability = json_decode($response->getBody()->getContents());
            $response = $this->api_client->request("POSt", Helpers::getAPIUrl('service') . 'staff/available', ['form_params' => $services_staff]);
            $services_staff = json_decode($response->getBody()->getContents());
            $response = $this->api_client->request("POSt", Helpers::getAPIUrl('service') . 'running', ['form_params' => $transition_times_services]);
            $service_running = json_decode($response->getBody()->getContents());
            $all_events = array_merge($service_events, $staff_events);
            foreach ($all_events as $event) {
                if ($event['start'] != '00:00')
                    $events [$event['start']][$event['type'] . '-' . (isset($event['service_id']) ? $event['service_id'] : $event['staff_id'])] = $event['end'];
                if ($event['end'] != '23:59')
                    $events [$event['end']][$event['type'] . '-' . (isset($event['service_id']) ? $event['service_id'] : $event['staff_id'])] = 'End time';
            }
            ksort($events);
            $qualification_array = $this->gettingQualifications();
            list($directorates, $directorate_id, $speciality_array, $specialities, $speciality_id, $additional_parameter, $services, $grades, $grade_array, $roles, $role_array, $sitesArray) = $this->getDirectorateData($speciality_id, $directorate_id);
            $staff_set = $this->getStaffSet($specialities, $speciality_id);
            $staffs = (isset($staff_set) && count($staff_set) ? $staff_set[$speciality_id] : []);
            if (isset($requirements_data[0]->checks[0]))
                $count = array_fill(0, count($requirements_data[0]->checks), 0);
        }
        $requirements_array = $this->getRequirementArray($requirements, $directorate_id, $directorates);
        return view('service.serviceOverview', compact('staffs', 'checks', 'count', 'service_info', 'directorate_id', 'speciality_id', 'specialities_dropdown', 'directorates_dropdown', 'requirements_array', 'staff_array', 'priority_set', 'calendar', 'events', 'transition_times_services', 'transition_times_staff', 'transition_times'));
    }

    public function getTransitionEvents($date, $events, $times, $type, $services = null) {
        $data = $services_staff = $times_array = [];
        foreach ($events as $event) {
            $starting = $event['start'];
            $ending = $event['end'];
            foreach ($times as $time) {
                if ($time['start_time'] > $time['end_time']) {
                    $transition_time_start = new Carbon($date->format('Y-m-d') . ' 00:00');
                    $transition_time_end = new Carbon($date->format('Y-m-d') . ' ' . $time['end_time']);
                    $starting_greater_starttime = ($transition_time_start <= $service_starting && $transition_time_end > $service_starting ? true : false);
                    $starting_less_starttime = ($service_starting < $transition_time_start ? true : false);
                    $ending_less_endtime = ($service_ending > $transition_time_start && $service_ending <= $transition_time_end ? true : false);
                    $ending_greater_endtime = ($service_ending > $transition_time_end ? true : false);

                    $transition_time_start1 = new Carbon($date->format('Y-m-d') . ' ' . $time['start_time']);
                    $transition_time_end1 = new Carbon($date->format('Y-m-d') . ' 23:59');
                    $starting_greater_starttime1 = ($transition_time_start1 <= $service_starting && $transition_time_end1 > $service_starting ? true : false);
                    $starting_less_starttime1 = ($service_starting < $transition_time_start1 ? true : false);
                    $ending_less_endtime1 = ($service_ending > $transition_time_start1 && $service_ending <= $transition_time_end1 ? true : false);
                    $ending_greater_endtime1 = ($service_ending > $transition_time_end1 ? true : false);
                    if (($starting_greater_starttime) || ($ending_less_endtime) || ($starting_less_starttime && $ending_less_endtime) || ($starting_less_starttime && $ending_greater_endtime)) {
                        if (!isset($transition_times_services[$time['name']]['service']) || !in_array($event['id'], array_column($transition_times_services[$time['name']]['service'], 'id')))
                            $data[$time['id']][] = ['id' => $event['id'], 'name' => $event['title'], 'date' => $date->format('Y-m-d'), 'day' => $date->dayOfWeek, 'week' => $date->weekOfYear, 'start' => $starting, 'stop' => $ending];
                    } else if (
                            ($starting_greater_starttime1) || ($ending_less_endtime1) || ($starting_less_starttime1 && $ending_less_endtime1) || ($starting_less_starttime1 && $ending_greater_endtime1)
                    ) {
                        if (!isset($transition_times_services[$time['name']]['service']) || !in_array($event['id'], array_column($transition_times_services[$time['name']]['service'], 'id')))
                            $data[$time['id']][] = ['id' => $event['id'], 'name' => $event['title'], 'date' => $date->format('Y-m-d'), 'day' => $date->dayOfWeek, 'week' => $date->weekOfYear, 'start' => $starting, 'stop' => $ending];
                    }
                } else {
                    $transition_time_end = new Carbon($date->format('Y-m-d') . ' ' . $time['end_time']);
                    $transition_time_start = new Carbon($date->format('Y-m-d') . ' ' . $time['start_time']);
                    $service_starting = new Carbon($date->format('Y-m-d') . ' ' . $starting);
                    $service_ending = new Carbon($date->format('Y-m-d') . ' ' . $ending);
                    $starting_greater_starttime = ($transition_time_start <= $service_starting && $transition_time_end > $service_starting ? true : false);
                    $starting_less_starttime = ($service_starting < $transition_time_start ? true : false);
                    $ending_less_endtime = ($service_ending > $transition_time_start && $service_ending <= $transition_time_end ? true : false);
                    $ending_greater_endtime = ($service_ending > $transition_time_end ? true : false);
                    if (
                            ($starting_greater_starttime) || ($ending_less_endtime) || ($starting_less_starttime && $ending_less_endtime) || ($starting_less_starttime && $ending_greater_endtime) || ($starting_greater_starttime && $ending_less_endtime)
                    ) {
                        if (!isset($transition_times_services[$time['name']]['service']) || !in_array($event['id'], array_column($transition_times_services[$time['name']]['service'], 'id')))
                            $data[$time['id']][] = ['id' => $event['id'], 'name' => $event['title'], 'date' => $date->format('Y-m-d'), 'day' => $date->dayOfWeek, 'week' => $date->weekOfYear, 'start' => $starting, 'stop' => $ending];
                    }
                }
            }
        }
        if ($type == 'staff') {
//            return [1,$services];
            foreach ($times as $time) {
                $times_array[$time['id']] = ['start_time' => $time['start_time'], 'end_time' => $time['end_time']];
            }
            foreach ($services as $time_id => $values) {
                foreach ($values as $service) {
                    if (isset($data[$time_id])) {
                        foreach ($data[$time_id] as $staff) {
                            if (
                                    (
                                    (($staff['start'] >= $times_array[$time_id]['start_time']) && ($staff['stop'] <= $times_array[$time_id]['end_time'])) || (($service['start'] != '00:00' && $service['start'] >= $staff['start']) && $service['stop'] <= $staff['stop']) || (($service['start'] != '00:00' && $service['start'] >= $staff['start']) && $service['stop'] >= $staff['stop'])
                                    )
                            ) {
                                if (isset($services_staff[$service['id']])) {
                                    if (!in_array($staff['id'], array_column($services_staff[$service['id']], 'directorate_staff_id')))
                                        $services_staff[$service['id']][] = ['directorate_staff_id' => $staff['id'], 'transition_time_id' => $time_id, 'date' => $date->format('Y-m-d'), 'day' => $date->dayOfWeek, 'week' => $date->weekOfYear, 'start' => $starting, 'stop' => $ending];
                                }else {
                                    $services_staff[$service['id']][] = ['directorate_staff_id' => $staff['id'], 'transition_time_id' => $time_id, 'date' => $date->format('Y-m-d'), 'day' => $date->dayOfWeek, 'week' => $date->weekOfYear, 'start' => $starting, 'stop' => $ending];
                                }
                            }
                        }
                    }
                }
            }
            return [$data, $services_staff];
        }
        return $data;
    }

    public function testing(Request $request) {
        $qualification_array = $this->gettingQualifications();
        list($directorates, $directorate_id, $speciality_array, $specialities, $speciality_id, $additional_parameter, $services, $grades, $grade_array, $roles, $role_array, $sitesArray) = $this->getDirectorateData();
        $requirement_set = $this->getRequirementSet($services, $qualification_array, $role_array, $grade_array, $sitesArray);
        $requirements = (isset($requirement_set[$speciality_id]) && count($requirement_set[$speciality_id]) ? $requirement_set[$speciality_id] : []);
        $staff_set = $this->getStaffSet($specialities, $speciality_id);
        $staffs = (isset($staff_set) && count($staff_set) ? $staff_set[$speciality_id] : []);
        $requirements_data = $this->checkRequirements($requirements, $staffs, $qualification_array, $grades, $roles, $additional_parameter, $sitesArray);
        if (isset($requirements_data[0]->checks[0]))
            $count = array_fill(0, count($requirements_data[0]->checks), 0);
        $directorates = Helpers::getDropDownData($directorates);
        $specialities = Helpers::getDropDownData($specialities);
        return view('service.requirementView', compact('staffs', 'checks', 'requirements_data', 'directorate_id', 'speciality_id', 'count', 'directorates', 'specialities'));
    }

    public function assignService(Request $request) {
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('assign') . 'service', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-overview', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-overview', $result->success);
            return Redirect::back();
        }
    }

    public function updateAssignService(Request $request) {
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('assign') . 'staff/update', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error)) {
            Session::flash('error-overview', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-overview', $result->success);
            return Redirect::back();
        }
    }

    public function requirementData() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorates');
        $directorates = json_decode($response->getBody()->getContents());
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorates[0]->id, 'directorate_id');
        $directorate_id = $directorates[0]->id;
        $speciality_id = $specialities[0]->id;
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate') . $directorate_id . '/speciality/' . $speciality_id . '/category/service-requirement');
        $directorate = json_decode($response->getBody()->getContents());
        $speciality_data = $directorate->speciality;
        $category_priority = $speciality_data->category_priority;
        return [$category_priority];
    }

//######################## SERVICE OVERVIEW DATA PROTECTED FUNCITON #############################
    protected function gettingQualifications() {
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('qualification'), '/'));
        $qualifications = json_decode($response->getBody()->getContents());
        $result = [];
        foreach ($qualifications as $qualification) {
            $result[$qualification->name] = $qualification->id;
        }
        return $result;
    }

    protected function getSpecialityArray($directorates) {
        $result = [];
        foreach ($directorates as $directorate) {
            foreach ($directorate->speciality as $speciality) {
                $result[$directorate->id][$speciality->id] = $speciality->id;
            }
        }
        return $result;
    }

    protected function getDirectorateData($specialityId = null, $directorateId = null) {
        $date = new Carbon(session()->get('date_time'));
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'overview/' . auth()->user()->trust_id . '/' . $date->format('Y-m-d'));
        $directorates = json_decode($response->getBody()->getContents());
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/site');
        $sites = json_decode($response->getBody()->getContents());
        $sitesArray = $this->getSiteArray($sites);
        $directorate_id = (isset($directorateId) ? $directorateId : $directorates[0]->id);
        $speciality_array = $this->getSpecialityArray($directorates);
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $directorate_id, 'directorate_id');
        $speciality_id = (isset($specialityId) ? $specialityId : $specialities[0]->id);
        $speciality_id = (array_search($speciality_id, $speciality_array[$directorate_id]) ? $speciality_id : $specialities[0]->id);
        $additional_parameter = Helpers::getAllNodesByIdFromMainNode($directorates, 'additional_parameter', $directorate_id, 'directorate_id');
        $services = Helpers::getAllNodesFromMainNode($specialities, 'service');
        $grades = Helpers::getAllNodesByIdFromMainNode($directorates, 'grade', $directorate_id, 'directorate_id');
        $grade_array = $this->getGradeRoleArray($grades);
        $roles = Helpers::getAllNodesByIdFromMainNode($directorates, 'role', $directorate_id, 'directorate_id');
        $role_array = $this->getGradeRoleArray($roles);
        return [$directorates, $directorate_id, $speciality_array, $specialities, $speciality_id, $additional_parameter, $services, $grades, $grade_array, $roles, $role_array, $sitesArray];
    }

    protected function getGradeRoleArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[$node->name] = $node->id;
        }
        return $result;
    }

    protected function getSiteArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[$node->name] = $node->id;
        }
        return $result;
    }

    protected function getGradeRolePositionArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[$node->position] = $node->id;
        }
        return $result;
    }

    //###################### TIMELINES PROTECTED FUNCTIONS ##########################

    protected function getweekNo($service_date, $date, $cycle_length, $starting_week) {
        $service_start = new Carbon($service_date);
        $service_week = $service_start->weekOfYear;
        if ($date->year > $service_start->year) {
            $year_difference = $date->year - $service_start->year;
            $selected_week = ($year_difference * 52) + $date->weekOfYear;
        } else {
            $selected_week = $date->weekOfYear;
        }
        $difference = $selected_week - $service_week;
        if ($starting_week != null) {
            $no = ($difference % $cycle_length) + 1;
            $no = ($no + $starting_week) - 1;
            if ($no > $cycle_length) {
                $week_no = $no % $cycle_length;
            } else {
                $week_no = $no;
            }
        } else {
            $week_no = ($difference % $cycle_length) + 1;
        }
        return $week_no;
    }

    protected function getServicesEventByDate($allservices, $day, $date) {
        $result = $requirements = $resources = $events = [];
        foreach ($allservices as $key => $services) {
            foreach ($services as $service) {
                if (isset($service->template->events)) {
                    $service_start = ($service->template->start_date == '0000-00-00' ? $service->created_at : $service->template->start_date);
                    $week_no = $this->getweekNo($service_start, $date, $service->template->cycle_length, $service->template->starting_week);
                    $start_date = ((isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? new Carbon($service->template->start_date) : new Carbon($service->created_at));
                    $end_date = ((isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? new Carbon($service->template->end_date) : $this->getCarbonYmdFormat(new Carbon('last day of December next year')));
                    if ($date >= $start_date && $date <= $end_date) {
                        foreach ($service->template->events as $event) {
                            $days = round(($event->duration / 60) / 24);
                            $end_day = ($days > 1 ? $days + $event->day : $event->day);
                            if (($end_day == $day && $event->week_no == $week_no) ||
                                    (($event->day < $day && $end_day > $day) && $event->week_no == $week_no)) {
                                if (isset($service->requirement)) {
                                    foreach ($service->requirement as $requirement) {
                                        if (isset($service->from) && $requirement->priority == $service->from) {
                                            $requirement->name = $service->name;
                                            $requirements[$key][$service->id][] = $requirement;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $requirements;
    }

    protected function getShiftTypeTimeArray() {
        $result = [];
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('trust') . auth()->user()->trust_id . '/directorate/rota-group');
        $directorates = json_decode($response->getBody()->getContents());
        $rotagroups = Helpers::getAllNodesFromMainNode($directorates, 'rota_group');
        $shifttypes = Helpers::getAllNodesFromMainNode($rotagroups, 'shift_type');
        foreach ($shifttypes as $shifttype) {
            $start_day = $end_day = 0;
            if ($shifttype->finish_time <= $shifttype->start_time) {
                $finish_time = strtotime('+1 day ' . $shifttype->finish_time);
                $end_day++;
            } else
                $finish_time = strtotime($shifttype->finish_time);

            $result[$shifttype->id] = ['start' => $shifttype->start_time, 'end' => $shifttype->finish_time, 'duration' => ($finish_time - strtotime($shifttype->start_time)) / 3600, 'start_day' => $start_day, 'end_day' => $end_day];
        }
        ksort($result);
        return $result;
    }

    protected function getTimeFromShiftArray($id, $shifttype_array) {
        foreach ($shifttype_array as $shift) {
            $time = array_search($id, $shift);
        }
        return $time;
    }

    protected function getEventStartEnd($event, $date, $week_no) {
        $starting = $ending = '';
        $start = new Carbon($event->start);
        $end = ($event->duration / 60);
        $time = ($event->duration / 60) + $start->copy()->hour;
        $end_day = (ceil($time / 24) - 1) + $event->day;
        if ($end_day > $event->day && $event->day < $date->dayOfWeek && $end_day == $date->dayOfWeek && $event->week_no == $week_no) {
            $starting = '00:00';
            $ending = $start->copy()->addhours($end)->format('H:i');
        } else if ($event->day == $date->dayOfWeek && $end_day > $event->day && $event->week_no == $week_no) {
            $starting = $start->format('H:i');
            $ending = '23:59';
        } else if ($event->day < $date->dayOfWeek && $end_day > $date->dayOfWeek && $event->week_no == $week_no) {
            $starting = '00:00';
            $ending = '23:59';
        } else if ($event->week_no == $week_no) {
            $starting = $start->format('H:i');
            $ending = $start->copy()->addhours($end)->format('H:i');
        }
        return [$starting, $ending, $end_day];
    }

    protected function getStaffEventStartEnd($event_array, $week_day, $current, $pre) {
        $events = [];
        list($pre_event, $event, $start_day, $end_day, $pre_start_day, $pre_end_day) = $this->getStaffTimelineTiming($event_array, $week_day, $current, $pre);
        if ($start_day < $end_day && $start_day == $week_day && !empty($event)) {
            $starting = $event['start'];
            $ending = '23:59';
            $events [] = ['start' => $starting, 'end' => $ending];
        }
        if ($pre_end_day == $start_day && $end_day == $week_day && !empty($pre_event)) {
            $starting = '00:00';
            $ending = $pre_event['end'];
            $events [] = ['start' => $starting, 'end' => $ending];
        }
        if ($start_day == $end_day && $start_day == $week_day && !empty($event)) {
            $starting = $event['start'];
            $ending = $event['end'];
            $events [] = ['start' => $starting, 'end' => $ending];
        }
        return $events;
    }

    protected function getStaffTimelineTiming($event_array, $week_day, $current, $pre) {
        $pre_event = $event = $start_day = $end_day = $pre_start_day = $pre_end_day = null;
        $pre_event = ($pre != '' ? $event_array[$pre] : []);
        $event = ($current != '' ? $event_array[$current] : []);
        $start_day = (isset($event['start_day']) ? $event['start_day'] + $week_day : '');
        $end_day = (isset($event['end_day']) ? $event['end_day'] + $week_day : '');
        $pre_start_day = (isset($pre_event['start_day']) ? ($pre_event['start_day'] + $week_day) - 1 : '');
        $pre_end_day = (isset($pre_event['end_day']) ? ($pre_event['end_day'] + $week_day) - 1 : '');
        return [$pre_event, $event, $start_day, $end_day, $pre_start_day, $pre_end_day];
    }

    //################### REQUIREMENT TESTING PROTECTED FUNCTIONS ######################

    protected function checkRequirements($requirements, $staffs, $qualifications, $grades, $roles, $additional_parameter, $sitesArray) {
        $result = [];
        foreach ($requirements as $service_name => $requirement_set) {
            foreach ($requirement_set as $requirement) {
                $requirement->name = (isset($requirement->name) ? $requirement->name : $service_name);
                $requirement->checks = $this->compareRequirement($requirement, $staffs, $qualifications, $grades, $roles, $additional_parameter, $sitesArray);
                $result [$service_name][$requirement->service_id][] = $requirement;
            }
        }
        return $result;
    }

    protected function compareRequirement($requirement, $staffs, $qualifications, $grades, $roles, $additional_parameter, $sitesArray) {
        $result = [];
        $grade_position = $this->getGradeRolePositionArray($grades);
        $role_position = $this->getGradeRolePositionArray($roles);
        $additionalParameters = [];
        foreach ($additional_parameter as $additionalParameterValue) {
            $additionalParameters[$additionalParameterValue->value] = lcfirst($additionalParameterValue->parameter);
        }
        switch ($requirement->operator) {
            case '==':
                foreach ($staffs as $staff) {
//                dd($staff);
                    $staff_parameter = [];
                    if (isset($staff->additional_parameters->parameter) && count($staff->additional_parameters->parameter)) {
                        foreach ($staff->additional_parameters->parameter as $key => $parameter) {
                            $staff_parameter[lcfirst($key)] = $parameter;
                        }
                    }
                    switch ($requirement->parameter) {
                        case 'role':
                            if ($staff->role_id == $requirement->value)
                                $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                            else
                                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
                            break;
                        case 'grade':
                            if ($staff->grade_id == $requirement->value)
                                $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                            else
                                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
                            break;
                        case 'qualification':
                            if (isset($staff->qualifications->qualification_id) && in_array($requirement->value, $staff->qualifications->qualification_id))
                                $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                            else
                                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
                            break;
                        case 'name':
                            if ($requirement->value == $staff->name)
                                $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                            else
                                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
                            break;
                        case 'site':
                            if (in_array($requirement->value, $sitesArray) == $requirement->value)
                                $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                            else
                                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
                            break;
                        default:
                            if (isset($staff_parameter[$requirement->parameter]) && in_array($requirement->value, $staff_parameter[$requirement->parameter]))
                                $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                            else
                                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
                            break;
                    }
                }
                return $result;
                break;

            case '>':
                $names = [];
                $names = $this->checkGreaterValue($requirement, $staffs, $grade_position, $role_position);
                return $names;
                break;

            case '<':
                $names = [];
                $names = $this->checkLessValue($requirement, $staffs, $grade_position, $role_position);
                return $names;
                break;

            case '>=':
                $names = [];
                $names = $this->checkGreaterEqualValue($requirement, $staffs, $grade_position, $role_position);
                return $names;
                break;

            case '<=':
                $names = [];
                $names = $this->checkLessEqualValue($requirement, $staffs, $grade_position, $role_position);
                return $names;
                break;

            case '!=':
                $names = [];
                $names = $this->checkNotEqualValue($requirement, $staffs, $grade_position, $role_position);
                return $names;
                break;

            case 'between':
                $names = [];
                $names = $this->checkBetweenValue($requirement, $staffs, $grade_position, $role_position);
                return $names;
                break;

            case 'any':
                $names = [];
                $names = array_fill(0, count($staffs), 0);
                return $names;
                break;

            case 'is':
                $names = [];
                $names = array_fill(0, count($staffs), 0);
                return $names;
                break;

            case 'is_not':
                $names = [];
                $names = array_fill(0, count($staffs), 0);
                return $names;
                break;

            default:
                break;
        }
    }

    protected function checkGreaterValue($requirement, $staffs, $grade_position, $role_position) {
        $result = $names = [];
        foreach ($staffs as $staff) {
            if ($requirement->parameter == 'role') {
                $value = array_search($requirement->value, $role_position);
                if ($staff->role->position > $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'grade') {
                $value = array_search($requirement->value, $grade_position);
                if ($staff->grade->position > $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'qualification' || $requirement->parameter == 'name' || $requirement->parameter == 'sub-team')
                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => $staff->end_time];
        }
        return $result;
    }

    protected function checkLessValue($requirement, $staffs, $grade_position, $role_position) {
        $names = $result = [];
        foreach ($staffs as $staff) {
            if ($requirement->parameter == 'role') {
                $value = array_search($requirement->value, $role_position);
                if ($staff->role->position < $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'grade') {
                $value = array_search($requirement->value, $grade_position);
                if ($staff->grade->position < $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'qualification' || $requirement->parameter == 'name' || $requirement->parameter == 'sub-team')
                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => $staff->end_time];
        }
        return $result;
    }

    protected function checkGreaterEqualValue($requirement, $staffs, $grade_position, $role_position) {
        $result = $names = [];
        foreach ($staffs as $staff) {
            if ($requirement->parameter == 'role') {
                $value = array_search($requirement->value, $role_position);
                if ($staff->role->position >= $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'grade') {
                $value = array_search($requirement->value, $grade_position);
                if ($staff->grade->position >= $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'qualification' || $requirement->parameter == 'name' || $requirement->parameter == 'sub-team')
                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => $staff->end_time];
        }
        return $result;
    }

    protected function checkLessEqualValue($requirement, $staffs, $grade_position, $role_position) {
        $result = $names = [];
        foreach ($staffs as $staff) {
            if ($requirement->parameter == 'role') {
                $value = array_search($requirement->value, $role_position);
                if ($staff->role->position <= $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'grade') {
                $value = array_search($requirement->value, $grade_position);
                if ($staff->grade->position <= $value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'qualification' || $requirement->parameter == 'name' || $requirement->parameter == 'sub-team')
                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => $staff->end_time];
        }
        return $result;
    }

    protected function checkNotEqualValue($requirement, $staffs, $grade_position, $role_position) {
        $result = $names = [];
        foreach ($staffs as $staff) {
            if ($requirement->parameter == 'role') {
                if ($staff->role_id != $requirement->value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'grade') {
                if ($staff->grade_id != $requirement->value)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'qualification' || $requirement->parameter == 'name' || $requirement->parameter == 'sub-team')
                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => $staff->end_time];
        }
        return $result;
    }

    protected function checkBetweenValue($requirement, $staffs, $grade_position, $role_position) {
        $result = $names = [];
        foreach ($staffs as $staff) {
            if ($requirement->parameter == 'role') {
                $value = array_search($requirement->value, $role_position);
                $value2 = array_search($requirement->value_2, $role_position);
                if ($staff->role->position >= $value && $staff->role_id <= $value2)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'grade') {
                $value = array_search($requirement->value, $grade_position);
                $value2 = array_search($requirement->value_2, $grade_position);
                if ($staff->grade->position >= $value && $staff->grade->position <= $value2)
                    $result[] = ['value' => ((isset($staff->assign) && $staff->assign->service_id == $requirement->service_id) ? 's' : 1), 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => (isset($staff->assign->end) ? $staff->assign->end : $staff->end_time), 'start_time' => (isset($staff->assign->start) ? $staff->assign->start : $staff->start_time), 'assign_id' => (isset($staff->assign->id) ? $staff->assign->id : 0), 'change' => (isset($staff->assign->change) ? $staff->assign->change : 0)];
                else
                    $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id];
            }
            if ($requirement->parameter == 'qualification' || $requirement->parameter == 'name' || $requirement->parameter == 'sub-team')
                $result[] = ['value' => 0, 'staff' => $staff->id, 'service' => $requirement->service_id, 'end_time' => $staff->end_time];
        }
        return $result;
    }

    protected function getAllServiceWithName($data, $selected_week, $day) {
        $services = [];
        foreach ($data->speciality as $speciality) {
            return $speciality->priority_set;
            foreach ($speciality->priority_set as $priority_set) {
                if (isset($priority_set->priority->category) && count($priority_set->priority->category)) {
                    foreach ($priority_set->priority->category as $category) {
                        foreach ($category->service as $service) {
                            $service->from = $priority_set->from;
                            $service->to = $priority_set->to;
                            $services [$service->name][] = $service;
                        }
                    }
                } else if (isset($priority_set->priority->subcategory) && count($priority_set->priority->subcategory)) {
                    foreach ($priority_set->priority->subcategory as $category) {
                        foreach ($category->service as $service) {
                            $service->from = $priority_set->from;
                            $service->to = $priority_set->to;
                            $services ['level-' . $priority_set->level][] = $service;
                        }
                    }
                } else if (isset($priority_set->priority->service) && count($priority_set->priority->service)) {
                    $priority_set->priority->service[0]->from = $priority_set->from;
                    $priority_set->priority->service[0]->to = $priority_set->to;
                    $services [$service->name][] = $priority_set->priority->service[0];
                } else if (isset($priority_set->priority->general_service) && count($priority_set->priority->general_service)) {
                    $priority_set->priority->general_service[0]->from = $priority_set->from;
                    $priority_set->priority->general_service[0]->to = $priority_set->to;
                    $services [$service->name][] = $priority_set->priority->general_service[0];
                } else if (isset($priority_set->priority->services) && count($priority_set->priority->services)) {
                    foreach ($priority_set->priority->services as $service) {
                        $service->from = $priority_set->from;
                        $service->to = $priority_set->to;
                        $services[$service->name][] = $service;
                    }
                }
            }
        }
        return $services;
    }

    protected function getAllService($data, $selected_week, $day) {
        $services = [];
        foreach ($data->speciality as $speciality) {
            foreach ($speciality->priority_set as $priority_set) {
                foreach ($priority_set->priorities as $priority) {
                    if (isset($priority->category_priority->category) && count($priority->category_priority->category)) {
                        foreach ($priority->category_priority->category as $category) {
                            foreach ($category->service as $service) {
                                $service->from = $priority->from;
                                $service->to = $priority->to;
                                $services ['level-' . $priority->level][] = $service;
                            }
                        }
                    } else if (isset($priority->category_priority->subcategory) && count($priority->category_priority->subcategory)) {
                        foreach ($priority->category_priority->subcategory as $category) {
                            foreach ($category->service as $service) {
                                $service->from = $priority->from;
                                $service->to = $priority->to;
                                $services ['level-' . $priority->level][] = $service;
                            }
                        }
                    } else if (isset($priority->category_priority->service) && count($priority->category_priority->service)) {
//                        dd('here');
                        $priority->category_priority->service[0]->from = $priority->from;
                        $priority->category_priority->service[0]->to = $priority->to;
                        $services ['level-' . $priority->level][] = $priority->category_priority->service[0];
                    } else if (isset($priority->category_priority->general_service) && count($priority->category_priority->general_service)) {
                        $priority->category_priority->general_service[0]->from = $priority->from;
                        $priority->category_priority->general_service[0]->to = $priority->to;
                        $services ['level-' . $priority->level][] = $priority->category_priority->general_service[0];
                    } else if (isset($priority->category_priority->services) && count($priority->category_priority->services)) {
                        foreach ($priority->category_priority->services as $service) {
                            $service->from = $priority->from;
                            $service->to = $priority->to;
                            $services['level-' . $priority->level][] = $service;
                        }
                    }
                }
            }
        }
        return $services;
    }

    protected function getRequirementArray($requirements, $directorate_id, $directorates) {
        $requirments_array = [];
        $grades = Helpers::getAllNodesByIdFromMainNode($directorates, 'grade', $directorate_id, 'directorate_id');
        $grade_array = $this->getGradeRoleArray($grades);
        $roles = Helpers::getAllNodesByIdFromMainNode($directorates, 'role', $directorate_id, 'directorate_id');
//        dd($grade_array);
        $role_array = $this->getGradeRoleArray($roles);
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('qualification'), '/'));
        $qualifications = json_decode($response->getBody()->getContents());
        $qualification_array = $this->gettingQualifications($qualifications);
        foreach ($requirements as $key => $level) {
            foreach ($level as $service_id) {
                foreach ($service_id as $requirement) {
                    $value2 = "";
                    if ($requirement->parameter == 'role') {
                        $value = array_search($requirement->value, $role_array);
                        if (!empty($requirement->value_2)) {
                            $value2 = array_search($requirement->value_2, $role_array);
                        } else {
                            $value2 = "";
                        }
                    } else if ($requirement->parameter == 'grade') {
                        $value = array_search($requirement->value, $grade_array);
                        if (!empty($requirement->value_2)) {
                            $value2 = array_search($requirement->value_2, $grade_array);
                        } else {
                            $value2 = "";
                        }
                    } else if ($requirement->parameter == 'qualification') {
                        $value = array_search($requirement->value, $qualification_array);
                        if (!empty($requirement->value_2)) {
                            $value2 = array_search($requirement->value_2, $qualification_array);
                        } else {
                            $value2 = "";
                        }
                    } else
                        $value = $requirement->value;
                    $requirement->value_name = $value;
                    $requirement->value2_name = $value2;
                    $requirments_array[$key][] = $requirement;
                }
            }
        }
        return $requirments_array;
    }

    protected function getPrioritySet($priority_set) {
        $result = $color_array = [];
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('color'), '/'));
        $colors = json_decode($response->getBody()->getContents());
        foreach ($colors as $color) {
            $color_array[$color->position] = $color->code;
        }
        foreach ($priority_set as $priority) {
            $name = (isset($priority->category_priority->type) ? $this->getPriorityName($priority->category_priority) : 'All Services');
            $result[] = ['level' => $priority->level, 'from' => $color_array[$priority->from], 'priority' => $name, 'to' => $color_array[$priority->to]];
        }
        return $result;
    }

    protected function getPriorityName($priority) {
        $name = 'All services';
        if ($priority->type == 'category')
            $name = $priority->category[0]->name . '( ' . $priority->type . ' )';
        else if ($priority->type == 'subcategory')
            $name = $priority->subcategory[0]->name . '( ' . $priority->type . ' )';
        else if ($priority->type == 'service')
            $name = $priority->service[0]->name . '( ' . $priority->type . ' )';
        else if ($priority->type == 'general_service')
            $name = $priority->general_service[0]->name . '( ' . $priority->type . ' )';
        return $name;
    }

    protected function random_color_part() {
        return str_pad(dechex(mt_rand(1, 255)), 2, '0', STR_PAD_LEFT);
    }

    protected function random_color() {
        return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    protected function getCarbonYmdFormat($date) {
        $date = new Carbon($date);
        $date = $date->format('Y-m-d');
        return $date;
    }

    protected function getRequirementTable($requirements_level) {
        $result = [];
        foreach ($requirements_level as $key => $level) {
            foreach ($level as $requirement) {
                $result['data'][] = [$key, $requirement->name, $requirement->parameter, $requirement->operator, $requirement->value_name, $requirement->status, $requirement->number];
            }
        }
        return $result;
    }

    //############################### AJAX Calls ################################

    protected function getServicesTimeline(Request $request, $flag = false) {
        $date = new Carbon($request->date);
        $directorate_id = ((isset($request->directorate_id) && $request->directorate_id != '')?$request->directorate_id:0);
        $speciality_id = ((isset($request->speciality_id) && $request->speciality_id != '')?$request->speciality_id:0);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate') . $directorate_id . '/speciality/' . $speciality_id . '/date/' . strtotime($date->format('Y-m-d')) . '/over-view');
        $data = json_decode($response->getBody()->getContents());
        $allservices = $this->getAllService($data, $date->weekOfYear, $date->dayOfWeek);
        $resources = $events = [];
        $time = strtotime($date->format('H:i'));
        mt_srand(389);
        foreach ($allservices as $key => $services) {
            foreach ($services as $service) {
                if (isset($service->template->events)) {
                    $service_start = ($service->template->start_date == '0000-00-00' ? $service->created_at : $service->template->start_date);
                    $week_no = $this->getweekNo($service_start, $date, $service->template->cycle_length, $service->template->starting_week);
                    $color = $this->random_color();
                    $start_date = ((isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? new Carbon($service->template->start_date) : new Carbon($service->created_at));
                    $end_date = ((isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? new Carbon($service->template->end_date) : $this->getCarbonYmdFormat(new Carbon('last day of December next year')));
                    if ($date >= $start_date && $date <= $end_date) {
                        $event2 = [];
                        foreach ($service->template->events as $event) {
                            list($starting, $ending, $end_day) = $this->getEventStartEnd($event, $date, $week_no);

                            if (($end_day == $date->dayOfWeek && $event->week_no == $week_no) ||
                                    ($event->day == $date->dayOfWeek && $event->week_no == $week_no) ||
                                    ($event->day < $date->dayOfWeek && $event->week_no == $week_no && $end_day > $date->dayOfWeek)) {
                                if (!in_array('service-' . $service->id, array_column($resources, 'id'))) {
                                    $resources[] = ['id' => 'service-' . $service->id, 'title' => $service->name, 'eventColor' => $color, 'type' => 'service'];
                                }
//                                $id = $event->id . '-' . $service->id;
                                $id = $service->id;
                                if (!in_array($id, array_column($events, 'id'))) {
                                    $events[] = ['id' => $id, 'resourceId' => 'service-' . $service->id, 'start' => $starting, 'end' => $ending, 'title' => $service->name, 'day' => $event->day, 'end_day' => $end_day, 'selected_day' => $date->dayOfWeek, 'event_week' => $event->week_no, 'week_no' => $week_no, 'type' => 'service', 'service_id' => $service->id];
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!$flag)
            return response()->json(['resources' => $resources, 'events' => $events]);
        else
            return [$resources, $events];
    }

    protected function getStaffTimeline(Request $request, $flag = false) {
        $date = new Carbon($request->date);
        $directorate_id = ((isset($request->directorate_id) && $request->directorate_id != '')?$request->directorate_id:0);
        $speciality_id = ((isset($request->speciality_id) && $request->speciality_id != '')?$request->speciality_id:0);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate') . $directorate_id . '/speciality/' . $speciality_id . '/post');
        $data = json_decode($response->getBody()->getContents());
        $shifttype_array = $this->getShiftTypeTimeArray();
        $resources = $events = [];
        if ($date->dayOfWeek == 0)
            $week_day = 6;
        else
            $week_day = $date->dayOfWeek - 1;
        mt_srand(159);
        foreach ($data->speciality as $speciality) {
            foreach ($speciality->post as $post) {
                $color = $this->random_color();
                $template_date = (!is_null($post->start_date) ? new Carbon($post->start_date) : new Carbon($post->created_at));
                $template_week = ($this->getweekNo($template_date, $date, count($post->rota_template->content->shift_type), $post->assigned_week)) - 1;
                if (isset($post->rota_template->content->shift_type)) {
                    $template = $post->rota_template->content->shift_type;
                    $shift_id = $template[$template_week][$week_day];
                    $pre_day_index = (array_search($shift_id, $template[$template_week]) - 1 == -1 ? 6 : array_search($shift_id, $template[$template_week]) - 1);
                    $pre_shift_id = $template[$template_week][$pre_day_index];
                    if ($shift_id != '') {
//                        $id = 'staff-' . $post->staff->id;
                        $id = (isset($post->staff->id) ? $post->staff->id : NULL);
                        if (!is_null($id)) {
                            $all_events = $this->getStaffEventStartEnd($shifttype_array, $week_day, $shift_id, $pre_shift_id);
                            $resources[] = ['id' => 'staff-' . $id, 'title' => $post->staff->name, 'eventColor' => $color, 'type' => 'staff'];
                            foreach ($all_events as $event) {
                                $events[] = ['id' => $id, 'resourceId' => 'staff-' . $post->staff->id, 'start' => $event['start'], 'end' => $event['end'], 'title' => $post->staff->name, 'template_week' => $template_week, 'type' => 'staff', 'staff_id' => $post->staff->id];
                            }
                        }
                    }
                }
            }
        }
        if (!$flag)
            return response()->json(['resources' => $resources, 'events' => $events]);
        else
            return [$resources, $events];
    }

    public function getRequiremetByDateTime(Request $request) {
//        return [$request->all()];
        $requirements = $requirement_array = $test = $available_staff = $services = [];
        $colors = app('App\Http\Controllers\ColorController')->getColorsCode();
        $date = new Carbon($request->date);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('directorate') . $request->directorate_id . '/speciality/' . $request->speciality_id . '/date/' . strtotime($date->format('Y-m-d')) . '/over-view');
        $data = json_decode($response->getBody()->getContents());
        $services = [];
        $day = $date->dayOfWeek;
        $time = $date->format('H:i');
        if ($request->staff == 'staff') {
            $allservices = $this->getAllServiceWithName($data, $date->weekOfYear, $day);
        } else {
            $allservices = $this->getAllService($data, $date->weekOfYear, $day);
        }
        $result = $requirements = $resources = $events = $storing_service = [];
        foreach ($allservices as $key => $services) {
            foreach ($services as $service) {
                if (isset($service->template->events)) {
                    $service_start = ($service->template->start_date == '0000-00-00' ? $service->created_at : $service->template->start_date);
                    $week_no = $this->getweekNo($service_start, $date, $service->template->cycle_length, $service->template->starting_week);
                    $start_date = ((isset($service->template->start_date) && $service->template->start_date != '0000-00-00') ? new Carbon($service->template->start_date) : new Carbon($service->created_at));
                    $end_date = ((isset($service->template->end_date) && $service->template->end_date != '0000-00-00') ? new Carbon($service->template->end_date) : $this->getCarbonYmdFormat(new Carbon('last day of December next year')));
                    if ($date >= $start_date && $date <= $end_date) {
                        foreach ($service->template->events as $event) {
                            list($starting, $ending, $end_day) = $this->getEventStartEnd($event, $date, $week_no);
                            if ((($event->day == $day && $event->week_no == $week_no) && ($time >= $starting && $time < $ending)) ||
                                    (($end_day == $day && $event->week_no == $week_no) && ($time >= $starting && $time < $ending)) ||
                                    (($event->day < $day && $event->week_no == $week_no && $end_day > $day) && ($time >= $starting && $time < $ending))) {
                                $storing_service [$key][] = $service;
                                if (isset($service->requirement)) {
                                    foreach ($service->requirement as $requirement) {
                                        if (isset($service->from) && $requirement->priority == $service->from) {
                                            $requirement->name = $service->name;
                                            $requirement->color = $colors[$requirement->priority];
                                            $requirement->starting = (isset($starting) ? $starting : '');
                                            $requirement->ending = (isset($ending) ? $ending : '');
                                            $requirement->duration = $event->duration;
                                            $requirement->day = $event->day;
                                            $requirement->end_day = $end_day;
                                            $requirements[$key][$service->id][] = $requirement;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('service') . 'overview/' . auth()->user()->trust_id . '/' . $date->format('Y-m-d'));
        $directorates = json_decode($response->getBody()->getContents());
//        return [$directorates];
        list($directorates, $directorate_id, $speciality_array, $specialities, $speciality_id, $additional_parameter, $services, $grades, $grade_array, $roles, $role_array, $sitesArray) = $this->getDirectorateData($request->speciality_id, $request->directorate_id);
        $specialities = Helpers::getAllNodesByIdFromMainNode($directorates, 'speciality', $request->directorate_id, 'directorate_id');
        $posts = Helpers::getAllNodesByIdFromMainNode($specialities, 'post', $request->speciality_id, 'directorate_speciality_id');
//        return [$posts];
        $available_staff = $this->getStaffArrayByTime($posts, $date, $time);
        $qualification_array = $this->gettingQualifications();
        $requirement_array = $this->getRequirementArray($requirements, $request->directorate_id, $directorates);
        $requirements_data = $this->checkRequirements($requirement_array, $available_staff, $qualification_array, $grades, $roles, $additional_parameter, $sitesArray);
        $data_store = [];
        foreach ($storing_service as $key => $level) {
            $data_store[] = [
                'event_time' => $time,
                'event_date' => $date->format('Y-m-d'),
                'level' => $key,
//                'service'=>json_encode($level),
//                'staff'=>json_encode($available_staff),
                'service' => $level,
                'staff' => $available_staff,
            ];
        }
//        return [$data_store];
        return json_encode(['staff' => $available_staff, 'requirement' => $requirements_data]);
    }

    //######################### Testing Protected Function ######################

    protected function getRequirementSet($services, $qualification_array, $role_array, $grade_array, $sitesArray) {
        $requirement_set = [];
        foreach ($services as $service) {
            if (isset($service->requirement) && count($service->requirement)) {
                foreach ($service->requirement as $requirement) {
                    $value2 = null;
                    if ($requirement->parameter == 'role') {
                        $value = array_search($requirement->value, $role_array);
                        $value2 = (isset($requirement->value_2) ? array_search($requirement->value_2, $role_array) : null);
                    } else if ($requirement->parameter == 'grade') {
                        $value = array_search($requirement->value, $grade_array);
                        $value2 = (isset($requirement->value_2) ? array_search($requirement->value_2, $grade_array) : null);
                    } else if ($requirement->parameter == 'qualification') {
                        $value = array_search($requirement->value, $qualification_array);
                    } else if ($requirement->parameter == 'site') {
                        $value = array_search($requirement->value, $sitesArray);
                    } else
                        $value = $requirement->value;
                    $requirement->value_name = $value;
                    $requirement->value2_name = $value2;
                    $requirement_set[$service->directorate_speciality_id][$service->name][] = $requirement;
                }
            }
        }
        return $requirement_set;
    }

    protected function getStaffSet($specialities, $speciality_id) {
        $result = [];
        foreach ($specialities as $speciality) {
            if (isset($speciality->post) && count($speciality->post)) {
                foreach ($speciality->post as $key => $post) {
                    if (isset($post->staff) && count($post->staff) && $speciality->id == $speciality_id) {
                        $post->staff->additional_parameters = (!empty($post->additional_parameters) ? $post->additional_parameters : '');
                        $result[$speciality->id][] = $post->staff;
                    }
                }
            }
        }
        return $result;
    }

    protected function getStaffFlagByTime($event_array, $week_day, $current, $pre, $time) {
        $flag = $end = $start = false;
        list($pre_event, $event, $start_day, $end_day, $pre_start_day, $pre_end_day) = $this->getStaffTimelineTiming($event_array, $week_day, $current, $pre);
        if (!empty($event) && $start_day < $end_day && $start_day == $week_day && ($time >= $event['start'])) {
            $flag = true;
            $end = $event['end'];
            $start = $event['start'];
        }
        if (($start_day == $end_day && $start_day == $week_day && ($time >= $event['start'] && $time < $event['end'] )) || ($start_day < $end_day && $start_day == $week_day && $time > $event['start'])) {
            $flag = true;
            $end = $event['end'];
            $start = $event['start'];
        }
        if (!empty($pre_event) && $pre_end_day == $start_day && $pre_end_day == $week_day && ($time < $pre_event['end'])) {
            $flag = true;
            $end = $pre_event['end'];
            $start = $pre_event['start'];
        }

        return [$flag, $end, $start];
    }

    protected function getStaffArrayByTime($posts, $date, $time) {
        $available_staff = [];
        if ($date->dayOfWeek == 0)
            $week_day = 6;
        else
            $week_day = $date->dayOfWeek - 1;
        $shifttype_array = $this->getShiftTypeTimeArray();
        foreach ($posts as $post) {
            $template_date = ($post->start_date != '0000-00-00' ? new Carbon($post->start_date) : new Carbon($post->created_at));
            $template_week = ($this->getweekNo($template_date, $date, count($post->rota_template->content->shift_type), $post->assigned_week)) - 1;
            if (isset($post->rota_template->content->shift_type)) {
                $template = $post->rota_template->content->shift_type;
                $shift_id = $template[$template_week][$week_day];
                $pre_day_index = (array_search($shift_id, $template[$template_week]) - 1 == -1 ? 6 : array_search($shift_id, $template[$template_week]) - 1);
                $pre_shift_id = $template[$template_week][$pre_day_index];
                if ($shift_id != '') {
                    list($flag, $end, $start) = $this->getStaffFlagByTime($shifttype_array, $week_day, $shift_id, $pre_shift_id, $time);
                    if ($flag) {
                        $post->staff->additional_parameters = (!empty($post->additional_parameters) ? $post->additional_parameters : '');
                        $post->staff->end_time = $end;
                        $post->staff->start_time = $start;
                        $available_staff [] = $post->staff;
                    }
                }
            }
        }
        return $available_staff;
    }

}
