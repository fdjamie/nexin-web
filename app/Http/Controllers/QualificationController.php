<?php

namespace App\Http\Controllers;

use File;
use App\Http\Helpers;
use App\Http\ExcelHelper;
use App\Http\Requests;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class QualificationController extends Controller {

    private $api_token, $api_url, $api_client;

    public function __construct() {
        $this->middleware('auth');
        $this->api_url = config('app.API_URL');
        if (auth()->user())
            $this->api_token = auth()->user()->api_token;
        else
            $this->api_token = '';
        $this->api_client = new Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function index() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('qualification'));
        $qualifications = json_decode($response->getBody()->getContents());
        return view('qualification.allQualification', compact('qualifications'));
    }

    public function create() {
        return view('qualification.createQualification');
    }

    public function store(Request $request) {
        $response = $this->api_client->request("POST", rtrim(Helpers::getAPIUrl('qualification'), '/'), ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && count($result->error)) {
            Session::flash('error-qualification', $result->error);
            return Redirect::back();
        } else {
            Session::flash('success-qualification', $request->name . ', Qualification added successfully');
            return redirect('/qualifications');
        }
    }

    public function edit($id) {
        $response = $this->api_client->request("GET", rtrim(Helpers::getAPIUrl('qualification') . $id));
        $qualification = json_decode($response->getBody()->getContents());
        return view('qualification.editQualification', compact('qualification'));
    }

    public function update(Request $request, $id) {
        $response = $this->api_client->request("PUT", Helpers::getAPIUrl('qualification') . $id, ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        if (isset($result->error) && $result->error != '') {
            Session::flash('error-qualification', $result->error);
            return redirect('/qualifications');
        } else {
            Session::flash('success-qualification', $result->updated);
            return redirect('/qualifications');
        }
    }

    public function destroy($id) {
        $response = $this->api_client->request("DELETE", Helpers::getAPIUrl('qualification') . $id);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('error-qualification', $result->error);
        return redirect('/qualifications');
    }

    public function exportQualification() {
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('qualification'));
        $qualifications = json_decode($response->getBody()->getContents());
        $data[] = ['name'];
        $ref_data[] = ['name'];
        $ref_data[] = ['Dummy Qualification name'];
        foreach ($qualifications as $qualification) {
            if (isset($qualification) && count($qualifications))
                array_push($data, [$qualification->name]);
        }
        ExcelHelper::exportExcel('Qualification_backup', $data, $ref_data);
        Session::flash('success-qualification', 'Qualificatoin data exported successfully.');
        return redirect()->back();
    }

    public function importQualification(Request $request) {
        $checked_data = [];
        $qualificationName = [];
        $validator = HomeController::importFileValidate($request->all());
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->messages());
        $upload = Storage::disk('uploads')->put(Input::file('file')->getClientOriginalName(), File::get(Input::file('file')));
        $importData = ExcelHelper::importExcel($request->file);
        $response = $this->api_client->request("GET", Helpers::getAPIUrl('qualification'));
        $qualifications = json_decode($response->getBody()->getContents());
        foreach ($qualifications as $qualification) {
            $qualificationName[] = $qualification->name;
        }
        foreach ($importData as $data) {
            if (in_array($data['name'], $qualificationName)) {
                $data['name_err'] = 'Qualification already exist';
            }
            $viewData [] = $data;
        }
                $fileName = Input::file('file')->getClientOriginalName();
        return view('qualification.viewUploadedData',['viewData'=>$viewData, 'fileName'=>$fileName]);
    }
    public function uploadQualification(Request $request) {
        $records = Excel::selectSheetsByIndex(0)->load(Helpers::getFile('uploads', $request->fileName), function($reader) {
                    $reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        $request->merge(['data' => $records, 'trust_id' => auth()->user()->trust_id]);
        $response = $this->api_client->request("POST", Helpers::getAPIUrl('qualification') . 'upload', ['form_params' => $request->all()]);
        $result = json_decode($response->getBody()->getContents());
        Session::flash('success-qualification', $result->success);
        return redirect()->route('qualificaiotn-index');
    }

}
