<?php

namespace App\Http\Controllers;

use App\LoginSecurity;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Session;
use App\Helpers\LoginAuthenticationHelper;

class SmsLoginSecurityController extends Controller
{
    private $maxNoSmsSent=5;
    private $codeExpireAfterSeconds=120;
     
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function enableSmsCodeVerification(Request $request)
    {

        $user = Auth::user();
        if(empty($user->phone))
        {
            return redirect('account-security')->with('error', trans('messages.phoneMissing'));
        }



        if (!empty($user->smsSecurity)) {
            $user->smsSecurity->enable = 1;
            $enable = $user->smsSecurity->save();
        } else {
            $enable = LoginSecurity::create([
                'user_id' => $user->id,
                'enable' => 1,
                'type' => 'sms'
            ]);
        }
        if ($enable) {
            $session = array();
            $session['verified'] = true;
            $session['time'] = Carbon::now();
            session(['smsCodeVerification' => $session]);
            return redirect('account-security')->with('success', trans('messages.enabled'));
        } else {
            return redirect('account-security')->with('error', trans('messages.error'));
        }
    }


    public function disableSmsCodeVerification(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", trans('messages.passwordNotMatch'));
        }

        $user = Auth::user();
        $user->smsSecurity->enable = 0;
        $disabled = $user->smsSecurity->save();

        if ($disabled) {
            return redirect('account-security')->with('success', trans('messages.disabled'));
        } else {
            return redirect('account-security')->with('error', trans('messages.error'));
        }


    }

    public function smsCodeVerifcation()
    {
        $user = Auth::user();
        if(empty($user->phone))
        {
            Session::flash('error', trans('messages.phoneNotFound'));
            return view('sms-code-verification.code-verification');
        }
        $userSmsVerification = $user->smsSecurity;
        if (empty($userSmsVerification) || !$userSmsVerification->enable) {
            return redirect()->back();
        } else {
            if (!session('smsCodeVerification')['codeSent']) {
                $this->sendCode();
            }
            return view('sms-code-verification.code-verification');

        }

    }

    public function sendCode(Request $request = null)
    {


        $user = Auth::user();
        if ($user->smsSecurity->code_sent == $this->maxNoSmsSent) {

            return redirect('sms-code-verification')->with('error',trans('messages.codeLimitExceeded'));
        }

        $code = mt_rand(1000, 9999);

        $sendSms=$this->sendSms($user, $code);

        if(!$sendSms['status'])
        {
            Session::flash('error', $sendSms['msg'].". kindly  Contact Admin ");
            return view('sms-code-verification.code-verification');
        }
        $user->smsSecurity->secret = $code;
        $user->smsSecurity->code_sent = $user->smsSecurity->code_sent + 1;
        $user->smsSecurity->save();
        $session = array();
        $session['verified'] = false;
        $session['time'] = Carbon::now();
        $session['codeSent'] = true;
        session(['smsCodeVerification' => $session]);
        if ($request) {
            return redirect('sms-code-verification')->with('success', trans('messages.codeSent'));
        }
        return;
    }

    private function sendSms($user, $code)
    {


        $username = config('smsApi.userName');
        $password =config('smsApi.password');

        $destination = $user->phone; //Multiple numbers can be entered, separated by a comma
        $source    = 'Nexin';
        $text = 'Your Nexin Login Verification Code is '. $code;
        $ref = 'abc123';
        $content =  'username='.rawurlencode($username).
            '&password='.rawurlencode($password).
            '&to='.rawurlencode($destination).
            '&from='.rawurlencode($source).
            '&message='.rawurlencode($text).
            '&ref='.rawurlencode($ref);


        $smsbroadcast_response = $this->sendSMSApi($content);
        $response_lines = explode("\n", $smsbroadcast_response);
        $res=[];
        foreach( $response_lines as $data_line){

            $message_data = "";
            $message_data = explode(':',$data_line);
            if($message_data[0] == "OK"){
               $res['msg']= "The message to ".$message_data[1]." was successful, with reference ".$message_data[2]."\n";
               $res['status']=true;
            }elseif( $message_data[0] == "BAD" ){
                $res['status']=false;
                $res['msg']= "The message to ".$message_data[1]." was NOT successful. Reason: ".$message_data[2]."\n";
            }elseif( $message_data[0] == "ERROR" ){
                $res['status']=false;
                $res['msg']= "There was an error with this request. Reason: ".$message_data[1]."\n";
            }
        }


        return $res;

    }


    function sendSMSApi($content) {
        $ch = curl_init(config('smsApi.apiUrl'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close ($ch);
        return $output;
    }

    public function verifyCode(Request $request)
    {

        if (!empty(session('smsCodeVerification')) && session('smsCodeVerification')['verified']) {
            return redirect()->back();
        }
        $this->validate($request, [
            'code' => 'required',

        ]);

        $code = implode('', $request->code);
        $user = Auth::user();
        if ($user->smsSecurity->secret == $code) {
            if (!$this->checkCodeExpiry($user)) {
                return redirect('sms-code-verification')->with('error', trans('messages.codeExpired'));
            }
            $session = array();
            $session['verified'] = true;
            session(['smsCodeVerification' => $session]);
            $user->smsSecurity->secret = "";
            $user->smsSecurity->code_sent = 0;
            $user->smsSecurity->save();
            LoginAuthenticationHelper::updateLoginVerificationInLoginDetailTbl('sms');
            return redirect('/');
        } else {

            return redirect('email-code-verification')->with('error', trans('messages.codeIncorrect'));

        }


    }


    private function checkCodeExpiry($user)
    {

        $codeSentDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $user->smsSecurity->updated_at);
        $currentDateTime = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        $diff = $codeSentDateTime->diffInSeconds($currentDateTime);

        if ($diff > $this->codeExpireAfterSeconds) {

            return false;

        } else {

            return true;

        }


    }


}
