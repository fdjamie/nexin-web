<?php

namespace App\Http\Middleware;
use App\Helpers\LoginAuthenticationHelper;
use Closure;
use Auth;

class SmsCodeSecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        $userSmsVerification=$user->smsSecurity;
        $checkIfUserAlreadyAuthenticatedThrougSMS = LoginAuthenticationHelper::checkIfLoginVerified('sms');  // checkIfLoginVerified is Helpers => loginauthenticationhelper


        /*     if(empty($userEmailVerification) || !$userEmailVerification->enable || (!empty(session('emailCodeVerification')) && session('emailCodeVerification')['verified']))*/
        if(empty($userSmsVerification) || !$userSmsVerification->enable || $checkIfUserAlreadyAuthenticatedThrougSMS || (!empty(session('smsCodeVerification')) && session('smsCodeVerification')['verified']))
        {
            return $next($request);
        }
        else {

            return redirect('sms-code-verification');

        }
    }
}
