<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\Helpers\LoginAuthenticationHelper;

class EmailCodeSecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        $userEmailVerification = $user->emailSecurity;
        $checkIfUserAlreadyAuthenticatedThrougEmail = LoginAuthenticationHelper::checkIfLoginVerified('email');  // checkIfLoginVerified is Helpers => loginauthenticationhelper

        /*     if(empty($userEmailVerification) || !$userEmailVerification->enable || (!empty(session('emailCodeVerification')) && session('emailCodeVerification')['verified']))*/

        if (empty($userEmailVerification) || !$userEmailVerification->enable || $checkIfUserAlreadyAuthenticatedThrougEmail || (!empty(session('emailCodeVerification')) && session('emailCodeVerification')['verified'])) {
            return $next($request);
        } else {

            return redirect('email-code-verification');

        }

    }
}
