<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Google2fSupport\Google2FAAuthenticator;
use App\Helpers\LoginAuthenticationHelper;
use Closure;

class Google2FAMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checkIfUserAlreadyAuthenticatedThrougGoogle2f = LoginAuthenticationHelper::checkIfLoginVerified('googleApp');  // checkIfLoginVerified is Helpers => loginauthenticationhelper

        $authenticator = app(Google2FAAuthenticator::class)->boot($request);
        if ($authenticator->isAuthenticated() || $checkIfUserAlreadyAuthenticatedThrougGoogle2f) {
            return $next($request);
        }
        return $authenticator->makeRequestOneTimePasswordResponse();
    }
}
