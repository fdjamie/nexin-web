<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $this->loggedinUserTrusts();
        return $next($request);
    }

    public function loggedinUserTrusts()
    {


             $apiUrl=Helpers::getAPIUrl('get-user-trusts-tree').auth()->user()->id;
             $getTrusts=__get($apiUrl);

               if($getTrusts['status']) {
                $trusts=$getTrusts['apiResponseData'];
                if(isset($trusts->status) && !$trusts->status) {
                    $userTrusts['status']=false;
                    $userTrusts['trusts']="";
                    session()->put('userTrusts', $userTrusts);
                }
                else {

                    $userTrusts['status']=true;
                    $userTrusts['trusts']=$trusts;
                    session()->put('userTrusts', $userTrusts);
                    if(session('activeTrust') && session('trustChanged')) {

                       $arr=array_column($userTrusts['trusts'], 'id');

                       /*if(in_array(session('activeTrust'),$arr))
                       {*/
                           session()->put('activeTrust', session('activeTrust'));
                      /* }
                       else{
                           session()->put('activeTrust', $trusts[0]->id);
                       }*/


                    }
                    else{
                        session()->put('activeTrust', $trusts[0]->id);
                    }
                    $this->getRolesAndPermission();
                }
            }
            //todo    Auth::user()->setAttribute('trust_id',session('activeTrust'));
            /* this should be change in future.. why i did it??? Because trust and user relation is now many to many. it was many
            * to one that the previous developer did so that's why there was a field in user table trust_id
            * So now as relation is many to many so no need of that extra field in user table. but why i am assinging trust id here to auth array now???
            * this is becuase Auth::user()->trust_id used every where in the project so to change that to active trust id i think it
            *  will take much more time but in future if time will change this As now i have only this solution in my mind one solution was to write this in main controller constructor
            * but again i have to call parent controller constructor in every controller too so that'w i am doing like ........
            *
           */
        Auth::user()->setAttribute('trust_id',session('activeTrust'));
        }


        public function getRolesAndPermission(){


        $trusts=session('userTrusts')['trusts'];
        $activeTrust= session('activeTrust');
        foreach ($trusts as $t)
            {
                if($t->id==$activeTrust)
                {
                    $userTrustId=$t->user_trust[0]->id;
                    break;
                }
            }
            $apiUrl=Helpers::getAPIUrl('get-user-roles-permissions').'/'.$userTrustId;
            $getPermissions=__get($apiUrl);

            if($getPermissions['status']) {
                $getRolesPermissions=$getPermissions['apiResponseData'];
                if(isset($getRolesPermissions->status) && !$getRolesPermissions->status) {
                    return false;

                }
                else {

                    session()->put('userRolesPermissions', $getRolesPermissions->data);
                    return true;

                }
            }




        }



}
