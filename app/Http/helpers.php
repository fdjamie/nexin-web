<?php

namespace App\Http;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Auth;

class Helpers {

    private static function file_exists($url) {
        $headers = get_headers($url);
        if (intval(substr($headers[0], 9, 3)) < 400) {
            return TRUE;
        }
        return FALSE;
    }

    public static function getFile($disk, $filename) {
        return config('filesystems.disks.' . $disk . '.root') . $filename;
    }

    public static function getDropDownData($data) {
        return array_column(json_decode(json_encode($data), TRUE), 'name', 'id');
    }

    public static function booleanDropDown() {
        return ['0' => 'Disable', '1' => 'Enable'];
    }

    public static function getPostsOfTrusts($directorates) {
        $posts = array();
        $postDropDown = array();
        foreach ($directorates as $directorate) {
            foreach ($directorate->speciality as $directoratespeciality) {
                if ($directoratespeciality->post) {
                    $posts[] = $directoratespeciality->post;
                }
            }
        }
        foreach ($posts as $post) {
            foreach ($post as $singlePost) {
                $postDropDown[] = $singlePost;
            }
        }
        return $postDropDown;
    }

    public static function getProfileImage($filename) {
        $url = config('app.API_URL') . 'assets/images/staff_profile/' . $filename;
        if (Helpers::file_exists($url)) {
            $image = Image::make($url)->resize(120, 120);
            Storage::disk('staff_profile')->put($filename, $image->stream());
            if (Storage::disk('staff_profile')->exists($filename)) {
                return config('filesystems.disks.staff_profile.url') . $filename;
            }
        }
        return config('filesystems.disks.staff_profile.url') . 'default.png';
    }

    public static function getAPIUrl($api_name) {
        if ($api_name === '')
            return config('app.API_URL');
        else
            return config('app.API_URL') . config('apis.' . $api_name);
    }

    public static function gettingArrayOfName($nodes) {
        $names = [];
        foreach ($nodes as $node) {
            $names [] = $node->name;
        }
        return $names;
    }

    public static function gettingSpecialityAndRotaGroupName($data) {
        $specialities = $post = $rotaTemplate = $rotaGroup = [];
        foreach ($data as $node) {
            foreach ($node->speciality as $speciality) {
                $specialities[$node->name][$speciality->name] = $speciality->name;
                foreach ($speciality->post as $posts) {
                    $post[$node->name][$speciality->name][$posts->name] = $posts->name;
                    $post[$node->name][$speciality->name][$posts->start_date] = $posts->start_date;
                    $post[$node->name][$speciality->name][$posts->end_date] = $posts->end_date;
                    $post[$node->name][$speciality->name][$posts->assigned_week] = $posts->assigned_week;
                    $rotaTemplate[$node->name][$speciality->name][$posts->rota_template->name] = $posts->rota_template->name;
                    $rotaTemplate[$node->name][$speciality->name][count($posts->rota_template->content->shift_type)] = count($posts->rota_template->content->shift_type);
                }
            }
            foreach ($node->rota_group as $rotaGroups) {
                $rotaGroup[$node->name][$rotaGroups->name] = $rotaGroups->name;
            }
        }
        return [$specialities, $post, $rotaTemplate, $rotaGroup];
    }

    public static function gettingArrtayOfCategoryName($data) {
        $names = [];
        foreach ($data as $node) {
            foreach ($node->category as $category) {
                $names [] = $category->name;
            }
        }
        return $names;
    }

    public static function gettingArrayOfParameterName($data) {
        $names = [];
        foreach ($data as $node) {
            foreach ($node->additional_parameter as $parameter) {
                $names [] = $parameter->parameter;
            }
        }
        return $names;
    }

    public static function getAllNodesFromMainNode($nodes, $sub_node_name) {
        $result = [];
        foreach ($nodes as $node) {
            foreach ($node->$sub_node_name as $sub_node) {
                if (isset($sub_node) && !is_null($sub_node)) {
                    array_push($result, $sub_node);
                }
            }
        }
        return $result;
    }

    public static function getAllNodesByIdFromMainNode($nodes, $sub_node_name, $id, $match_id) {
        $result = [];
        foreach ($nodes as $node) {
            foreach ($node->$sub_node_name as $sub_node) {
                if ($sub_node->$match_id == $id)
                    $result[] = $sub_node;
            }
        }
        return $result;
    }





}
