<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */




Route::auth();
Route::get('logout', '\App\Http\Controllers\Auth\AuthController@logout');


Route::get('/signup-request', 'SignupRequestController@index');
Route::post('/signup-request', 'SignupRequestController@storeUserRequest');




Route::group(['middleware' => ['auth','2fa','emailCodeVerification','smsCodeVerification']], function () {
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);                                            //  to show Main Dashboard
Route::get('/home', 'HomeController@index');                                        //  to show Main Dashboard
Route::get('/index', 'HomeController@nexinOld');
//Route::get('/speciality/', 'HomeController@showAllSpeciality');                     //  to show Dashboard
//Route::get('/speciality/new', 'HomeController@createSpeciality');                   //  to show form for new record
//Route::post('/speciality', 'HomeController@storeSpeciality');                       //  to add record in database using api
//Route::get('/speciality/edit/{id}', 'HomeController@editSpeciality');               //  to show existing record in form
//Route::put('/speciality/{id}', 'HomeController@updateSpeciality');                  //  to edit|update  record in database using api
//Route::get('/speciality/delete/{id}', 'HomeController@destroySpeciality');          //  to delete existing record from database using api
//
Route::get('/switch-trust/{id}', 'HomeController@switchTrust')->name('switch-trust');

Route::get('/trust/', 'TrustController@index');
Route::get('/trust/new', 'TrustController@create');
Route::post('/trust', 'TrustController@store');
Route::get('/trust/edit/{id}', 'TrustController@edit');
Route::put('/trust/{id}', 'TrustController@update');
Route::get('/trust/delete/{id}', 'TrustController@destroy');
Route::get('/trust/info/{id}', 'TrustController@infoTrust');
Route::get('/trust/export', 'TrustController@export');
Route::post('/trust/import', 'TrustController@import');
Route::post('/trust/upload', 'TrustController@upload');

Route::get('/divisions/', 'DivisionController@index');
Route::get('/division/new', 'DivisionController@create');
Route::get('/division/deleted', 'DivisionController@deleted');
Route::get('//division/restore', 'DivisionController@restore');
Route::post('/division', 'DivisionController@store');
Route::get('/division/edit/{id}', 'DivisionController@edit');
Route::put('/division/{id}', 'DivisionController@update');
Route::get('/division/delete/{id}', 'DivisionController@destroy');
Route::get('/division/export', 'DivisionController@export');
Route::post('/division/import', 'DivisionController@import');
Route::post('/division/upload', 'DivisionController@upload');

Route::get('/directorates/', 'DirectorateController@index');
Route::get('/directorate/new', 'DirectorateController@create');
Route::post('/directorate', 'DirectorateController@store');
Route::get('/directorate/edit/{id}', 'DirectorateController@edit');
Route::put('/directorate/{id}', 'DirectorateController@update');
Route::get('/directorate/delete/{id}', 'DirectorateController@destroy');
Route::get('/directorate/export', 'DirectorateController@export');
Route::post('/directorate/import', 'DirectorateController@import');
Route::post('/directorate/upload', 'DirectorateController@upload');

Route::get('/specialities', 'SpecialityController@index');
Route::get('/speciality/new', 'SpecialityController@create');
Route::post('/speciality', 'SpecialityController@store');
Route::get('/speciality/edit/{id}', 'SpecialityController@edit');
Route::put('/speciality/{id}', 'SpecialityController@update');
Route::get('/speciality/delete/{id}', 'SpecialityController@destroy');
Route::get('/directoratespecialityservices/{id}/{cat_id}/{sub_cat_id}', 'HomeController@specialityServicesWithCategoryFitler');
Route::get('/getdirectoratespeciality/{id}', 'HomeController@getDirectorateSpecialityList');
Route::get('/speciality/export', 'SpecialityController@export');
Route::post('/speciality/import', 'SpecialityController@import');
Route::post('/speciality/upload', 'SpecialityController@upload');

Route::get('/directorate-staff', 'DirectorateStaffController@index');
Route::post('/directorate-staff', 'DirectorateStaffController@index');
Route::get('/directorate-speciality-staff', 'HomeController@indexDirectorateSpecialityStaff');
Route::post('/directorate-speciality-staff', 'HomeController@indexDirectorateSpecialityStaff');
Route::put('/directorate-staff/{id}', 'DirectorateStaffController@update');
Route::get('/directorate-staff/new', 'DirectorateStaffController@create');
Route::post('/directorate-staff/new', 'DirectorateStaffController@store');
Route::get('/directorate-staff/edit/{id}', 'DirectorateStaffController@edit');
Route::get('/directorate-staff/delete/{id}', 'DirectorateStaffController@destroy');
Route::get('/directorate-staff/search', 'DirectorateStaffController@searchStaff');
Route::get('/appointed-directorate-staff/{id}', 'DirectorateStaffController@searchAppointedStaff');
Route::get('/directorate-staff/export', 'DirectorateStaffController@export');
Route::post('/directorate-staff/import', 'DirectorateStaffController@import');
Route::post('/directorate-staff/upload', 'DirectorateStaffController@upload');

Route::get('/sites/', 'SiteController@index');
Route::get('/site/new', 'SiteController@create');
Route::post('/site', 'SiteController@store');
Route::get('/site/edit/{id}', 'SiteController@edit');
Route::put('/site/{id}', 'SiteController@update');
Route::get('/site/delete/{id}', 'SiteController@destroy');
Route::get('/site/export', 'SiteController@export');
Route::post('/site/import', 'SiteController@import');
Route::post('/site/upload', 'SiteController@upload');

Route::get('/locations/', 'LocationController@index');
Route::get('/location/new', 'LocationController@create');
Route::post('/location', 'LocationController@store');
Route::get('/location/edit/{id}', 'LocationController@edit');
Route::put('/location/{id}', 'LocationController@update');
Route::get('/location/delete/{id}', 'LocationController@destroy');
Route::get('/location/export', 'LocationController@export');
Route::post('/location/import', 'LocationController@import');
Route::post('/location/upload', 'LocationController@upload');

Route::get('/staff/', 'HomeController@showAllStaff');
Route::get('/staff/new', 'HomeController@createStaff');
Route::get('/staff/export', 'HomeController@exportStaff');
Route::post('/staff', 'HomeController@storeStaff');
Route::post('/uploadstaffdata', 'HomeController@uploadStaffData');
Route::get('/staff/edit/{id}', 'HomeController@editStaff');
Route::put('/staff/{id}', 'HomeController@updateStaff');
Route::get('/staff/delete/{id}', 'HomeController@destroyStaff');

Route::get('/grades/', 'HomeController@showAllGrade');
Route::put('/grades/', 'HomeController@showAllGrade');
Route::get('/grade/new', 'HomeController@createGrade');
Route::post('/grade', 'HomeController@storeGrade');
Route::get('/grade/edit/{id}', 'HomeController@editGrade');
Route::put('/grade/{id}', 'HomeController@updateGrade');
Route::get('/grade/delete/{id}', 'HomeController@destroyGrade');
Route::get('/getgrade/{id}', 'HomeController@getGrade');
Route::get('/getGradesPosition/{id}', 'HomeController@getGradePositions');
Route::get('/grade/export', 'HomeController@exportGrade');
Route::post('/grade/import', 'HomeController@importGrade');
Route::post('/grade/upload', 'HomeController@uploadGrade');

Route::get('/roles/', 'HomeController@showAllRole');
Route::get('/role/new', 'HomeController@createRole');
Route::post('/role', 'HomeController@storeRole');
Route::get('/role/edit/{id}', 'HomeController@editRole');
Route::put('/role/{id}', 'HomeController@updateRole');
Route::get('/role/delete/{id}', 'HomeController@destroyRole');
Route::get('/getRoles/{id}', 'HomeController@getRoles');
Route::get('/getRolesPositions/{id}', 'HomeController@getRolesPositions');
Route::get('/role/export', 'HomeController@exportRole');
Route::post('/role/import', 'HomeController@importRole');
Route::post('/role/upload', 'HomeController@uploadRole');

Route::get('/admin', 'HomeController@adminView');
Route::post('/test', 'HomeController@ajaxTest');
Route::get('/jstree', 'HomeController@jsonJsTreeData');
Route::get('/test', 'HomeController@ajaxTest');

Route::get('/shift/', 'HomeController@showAllShift');
Route::get('/shift/new', 'HomeController@createShift');
Route::post('/shift', 'HomeController@storeShift');
Route::get('/shift/edit/{id}', 'HomeController@editShift');
Route::put('/shift/{id}', 'HomeController@updateShift');
Route::get('/shift/delete/{id}', 'HomeController@destroyShift');

Route::get('/rotation-template/', 'HomeController@showAllTemplateRotation');
Route::get('/rotation-template/new', 'HomeController@createTemplateRotation');
Route::post('/rotation-template', 'HomeController@storeTemplateRotation');
Route::get('/rotation-template/edit/{id}', 'HomeController@editTemplateRotation');
Route::put('/rotation-template/{id}', 'HomeController@updateTemplateRotation');
Route::get('/rotation-template/delete/{id}', 'HomeController@destroyTemplateRotation');

Route::get('/rotations/', ['as' => 'rotations', 'uses' => 'RotationController@index']);
Route::put('/rotations/', 'RotationController@index');
Route::get('/rotations/new', ['as' => 'rotation/new', 'uses' => 'RotationController@create']);
Route::get('/getRotationsRotas/{id}', 'HomeController@getRotationsRotas');
Route::get('/getRotationsPosts/{id}', 'HomeController@getRotationsPosts');
Route::post('/rotations', 'RotationController@store');
Route::put('/rotations/{id}', 'RotationController@update');
Route::get('/rotations/edit/{id}', ['as' => 'rotation/edit', 'uses' => 'RotationController@edit']);
Route::get('/rotations/delete/{id}', 'RotationController@destroy');
Route::get('/rotations/detail/{id}', ['as' => 'rotation/detail', 'uses' => 'HomeController@detailRotation']);

Route::get('/post', 'PostController@index')->name('post-index');
Route::post('/post', 'PostController@index');
Route::get('/post/new', 'PostController@create');
Route::post('/post/new', 'PostController@create');
Route::post('/post/add', 'PostController@store');
Route::get('/post/edit/{id}', 'PostController@edit');
Route::post('/post/edit/{id}', 'PostController@edit');
Route::put('/post/{id}', 'PostController@update');
Route::get('/post/delete/{id}', 'PostController@destroy');
Route::get('/post-roster/{id}', 'PostController@roster');
Route::get('/post-rotation/{id}', 'PostController@rotation');
Route::post('/post/{id}/additional-parameter', 'PostController@addAdditionalParameters');
Route::get('post-export' , 'PostController@exportPost')->name('post-export');
Route::post('post-import' , 'PostController@importPost')->name('post-import');
Route::post('post-upload' , 'PostController@postUpload')->name('post-upload');

Route::get('/post-details/{id}', 'HomeController@createPostDetails');
Route::post('/post-details/{id}', 'HomeController@storePostDetails');
Route::get('/post-details/edit/{id}', 'HomeController@editPostDetails');
Route::put('/post-details/{id}', 'HomeController@updatePostDetails');

/*Route::get('/user', 'HomeController@showAllUsers');*/
Route::get('/user', 'HomeController@showTrustUsers')->name('user');
Route::get('/user/new', 'HomeController@createUsers');
Route::get('/user/edit/{id}', 'HomeController@editUsers');
Route::get('/user/delete/{id}', 'HomeController@destroyUser');
Route::put('/user/{id}', 'HomeController@updateUser');
/*Route::post('/user', 'HomeController@storeUser');*/
Route::post('/user', 'HomeController@storeUser');


Route::get('/profile/{id}', 'HomeController@profileDetails');
Route::get('/profile', 'HomeController@allProfileDetails');

Route::get('/service', 'ServiceController@index');
Route::put('/service', 'ServiceController@index');
Route::get('/service/new', 'ServiceController@create');
Route::post('/service', 'ServiceController@store');
Route::get('/service/edit/{id}', 'ServiceController@edit');
Route::put('/service/{id}', 'ServiceController@update');
Route::get('/service/delete/{id}', 'ServiceController@destroy');
Route::get('/service/template/add/{id}', 'ServiceController@createServiceTemplate');
Route::get('/service/template/{id}', 'HomeController@viewSerivceTemplate');
Route::get('/service/template/edit/{id}', 'ServiceController@editServiceTemplate');
Route::get('/service/template/delete/{id}', 'HomeController@deleteSerivceTemplate');
Route::get('/speciality/{id}/services', 'HomeController@viewSpecialitySerivceTemplate');
Route::post('/speciality/{id}/services', 'HomeController@viewSpecialitySerivceTemplate');
Route::get('/location/{id}/services', 'HomeController@viewLocationServiceTemplate');
Route::get('/site/{id}/services', 'HomeController@viewSiteSerivceTemplate');
Route::post('/site/{id}/services', 'HomeController@viewSiteSerivceTemplate');
Route::post('/servicetemplate', 'ServiceController@storeServiceTemplate');
Route::post('/servicetemplate/edit', 'ServiceController@updateServiceTemplate');
Route::get('/servicetemplate', 'HomeController@getServiceTemplate');
Route::get('/getSiteLocations/{id}', 'HomeController@getsiteLocations');
Route::post('/service/import', 'ServiceController@import');
Route::post('/service/importReference', 'ServiceController@importReference');
Route::post('/service/reference-upload', 'ServiceController@referenceUpload');
Route::get('/service/export', 'HomeController@exportService');
Route::get('/service/export', 'ServiceController@export');
Route::post('/service/upload', 'ServiceController@upload');
//Route::get('/upload-service-template', 'HomeController@getUploadServiceTemplate');


Route::get('/shift-type', 'ShiftTypeController@index');
Route::put('/shift-type', 'ShiftTypeController@index');
//Route::get('/shift-type/new', 'HomeController@createShiftType');
Route::get('/shift-type/new', 'ShiftTypeController@create');
Route::post('/shift-type', 'ShiftTypeController@store');
Route::get('/shift-type/edit/{id}', 'ShiftTypeController@edit');
Route::put('/shift-type/{id}', 'ShiftTypeController@update');
Route::get('/shift-type/delete/{id}', 'ShiftTypeController@destroy');
Route::get('/shift-type/export', 'ShiftTypeController@export');
Route::post('/shift-type/import', 'ShiftTypeController@import');
Route::post('/shift-type/import/reference-data', 'ShiftTypeController@importReferenceData');
Route::post('/shift-type/upload', 'ShiftTypeController@upload');
Route::post('/shift-type/upload/reference-data', 'ShiftTypeController@uploadReferenceData');
Route::get('/getRotaGroupShiftTypes/{did}/{gid}', 'HomeController@getRotaGroupShiftTypes');

Route::get('/rota-template', 'HomeController@indexRotaTemplate');
Route::get('/rota-template/new', 'HomeController@createRotaTemplate');
Route::put('/rota-template', 'HomeController@indexRotaTemplate');
Route::post('/rota-template', 'HomeController@storeRotaTemplate');
Route::get('/rota-template/edit/{id}', 'HomeController@editRotaTemplate');
Route::get('/rota-template/view/{id}', 'HomeController@viewRotaTemplate');
Route::put('/rota-template/{id}', 'HomeController@updateRotaTemplate');
Route::get('/rota-template/delete/{id}', 'HomeController@destroyRotaTemplate');
Route::get('/getRotaGroupRotaTemplates/{id}', 'HomeController@getRotaGroupRotaTemplates');
Route::get('/export-rota-template', 'HomeController@exportRotaTemplate')->name('export-rota-template');
Route::post('/import-rota-template', 'HomeController@importRotaTemplate')->name('import-rota-template');

Route::get('/getshiftdata/{did}/{sid}', 'HomeController@getShiftsTypeData');
Route::get('/getrotatemplate/{id}', 'HomeController@getRotaTemplateFromDirectorate');
Route::get('/getrotatemplateweeks/{id}', 'HomeController@getRotaTemplateWeekCycle');
Route::get('/links', 'HomeController@getWorkingLinks');

/*
 * category routes
 */
Route::get('/category', 'HomeController@showAllCategory')->name('showall-category');
Route::get('/category/new', 'HomeController@createCategory');
Route::get('/category/edit/{id}', 'HomeController@editCategory');
Route::post('/category', 'HomeController@storeCategory');
Route::put('/category/{id}', 'HomeController@updateCategory');
Route::get('/category/delete/{id}', 'HomeController@destroyCategory');
Route::get('/getcategory/{id}', 'HomeController@getCategories');
Route::get('category-export','HomeController@exportCategory')->name('category-export');
Route::post('category-import','HomeController@importCategory')->name('category-import');
Route::post('category-import-upload','HomeController@importCategoryUpload')->name('category-import-upload');

Route::get('/sub-category', 'SubCategoryController@index');
Route::put('/sub-category', 'SubCategoryController@index');
Route::get('/sub-category/{id}/sub-category', 'SubCategoryController@indexChild');
Route::get('/sub-category/new', 'SubCategoryController@create');
Route::get('/sub-category/edit/{id}', 'SubCategoryController@edit');
Route::post('/sub-category', 'SubCategoryController@store');
Route::put('/sub-category/{id}', 'SubCategoryController@update');
Route::get('/sub-category/delete/{id}', 'SubCategoryController@destroy');
Route::get('/getsubcategories/{d_id}/{c_id}', 'HomeController@getSubCategories');
Route::get('/getSpecialitySubcategory/{d_id}/{c_id}/{s_id}', 'HomeController@getSpecialitySubCategories');
Route::get('/getChildSubcategory/{id}/{s_id}', 'SubCategoryController@getChildSubCategories');

Route::get('/shift/{id}/shift-detail', 'HomeController@showAllShiftDetail');
Route::get('/shift/{id}/shift-detail/new', 'HomeController@createShiftDetail');
Route::get('/shift/{id}/shift-detail/edit', 'HomeController@editShiftDetail');
Route::post('/shift-detail', 'HomeController@storeShiftDetail');
Route::put('/shift-detail/', 'HomeController@updateShiftDetail');
Route::get('/shift-detail/delete/{id}', 'HomeController@destroyShiftDetail');

Route::get('/rota-group', 'RotaGroupController@index');
Route::put('/rota-group', 'RotaGroupController@index');
Route::get('/rota-group/new', 'RotaGroupController@create');
Route::get('/rota-group/edit/{id}', 'RotaGroupController@edit');
Route::post('/rota-group', 'RotaGroupController@store');
Route::put('/rota-group/{id}', 'RotaGroupController@update');
Route::get('/rota-group/delete/{id}', 'RotaGroupController@destroy');
Route::get('/rota-group/export', 'HomeController@exportRotaGroup');
Route::post('/rota-group/import', 'HomeController@importRotaGroup');
Route::post('/rota-group/upload', 'HomeController@uploadRotaGroup');

Route::get('/staff-overview', 'HomeController@showAllAdditionalGradeTest');
Route::put('/staff-overview', 'HomeController@showAllAdditionalGradeTest');
Route::get('/additional-grade', 'HomeController@indexAdditionalGrade');
Route::put('/additional-grade', 'HomeController@indexAdditionalGrade');
Route::get('/additional-grade/new', 'HomeController@createAdditionalGrade');
Route::put('/additional-grade/new', 'HomeController@createAdditionalGrade');
Route::get('/additional-grade/edit/{id}', 'HomeController@editAdditionalGrade');
Route::post('/additional-grade', 'HomeController@storeAdditionalGrade');
Route::put('/additional-grade/{id}', 'HomeController@updateAdditionalGrade');
Route::get('/additional-grade/delete/{id}', 'HomeController@destroyAdditionalGrade');
Route::get('/additional-grade/export', 'HomeController@exportAdditionalGrade');
Route::post('/additional-grade/import', 'HomeController@importAdditionalGrade');
Route::post('/additional-grade/upload', 'HomeController@uploadAdditionalGrade');
Route::get('/getAdditionalGradePositions/{id}/{d_id}', 'HomeController@getAdditionalGradePositions');

Route::get('/groups', 'GroupController@index');
Route::get('/group/new', 'GroupController@create');
Route::get('/group/edit/{id}', 'GroupController@edit');
Route::post('/group', 'GroupController@store');
Route::put('/group/{id}', 'GroupController@update');
Route::get('/group/delete/{id}', 'GroupController@destroy');
Route::get('/group/export', 'GroupController@export');
Route::post('/group/import', 'GroupController@import');
Route::post('/group/upload', 'GroupController@upload');
Route::get('/getgroups/{id}', 'HomeController@getGroups');

Route::get('/subgroups', 'SubgroupController@index');
Route::put('/subgroups', 'SubgroupController@index');
Route::get('/subgroup/new', 'SubgroupController@create');
Route::get('/subgroup/edit/{id}', 'SubgroupController@edit');
Route::post('/subgroup', 'SubgroupController@store');
Route::put('/subgroup/{id}', 'SubgroupController@update');
Route::get('/subgroup/delete/{id}', 'SubgroupController@destroy');
Route::get('/subgroup/export', 'SubgroupController@export');
Route::post('/subgroup/import', 'SubgroupController@import');
Route::post('/subgroup/upload', 'SubgroupController@upload');
Route::get('/getsubgroups/{g_id}/{d_id}', 'HomeController@getSubgroups');

Route::get('/directorate-services', 'DirectorateServiceController@index');
//Route::put('/directorate-service', 'HomeController@showAllRotaGroup');
Route::get('/directorate-service/new', 'DirectorateServiceController@create');
Route::get('/directorate-service/edit/{id}', 'DirectorateServiceController@edit');
Route::post('/directorate-service', 'DirectorateServiceController@store');
Route::put('/directorate-service/{id}', 'DirectorateServiceController@update');
Route::get('/directorate-service/delete/{id}', 'DirectorateServiceController@destroy');
Route::get('/directorate-service/template/{id}', 'DirectorateServiceController@viewSerivceTemplate');
Route::get('/directorate-service/template/add/{id}', 'DirectorateServiceController@createSerivceTemplate');
Route::post('/directorate-service/template', 'DirectorateServiceController@storeSerivceTemplate');
Route::get('/directorate-service/template/edit/{id}', 'DirectorateServiceController@editSerivceTemplate');
Route::post('/directorate-service/template/edit', 'DirectorateServiceController@updateSerivceTemplate');
Route::get('/directorate-service/template/delete/{id}', 'DirectorateServiceController@destroyServiceTemplate');
Route::get('/directorateservicetemplate', 'DirectorateServiceController@getSerivceTemplate');
Route::get('/directorate-service/export', 'DirectorateServiceController@export');
Route::post('/directorate-service/import', 'DirectorateServiceController@import');
Route::post('/directorate-service/upload', 'DirectorateServiceController@upload');
Route::post('/directorate-service/import/reference-data', 'DirectorateServiceController@importReferenceData');
Route::post('/directorate-service/upload/reference-data', 'DirectorateServiceController@uploadReferenceData');
/*
 * Qualification
 */
Route::get('/qualifications', 'QualificationController@index')->name('qualificaiotn-index');
Route::get('/qualification/new', 'QualificationController@create')->name('create-qualification');
Route::get('/qualification/edit/{id}', 'QualificationController@edit');
Route::post('/qualification', 'QualificationController@store');
Route::put('/qualification/{id}', 'QualificationController@update');
Route::get('/qualification/delete/{id}', 'QualificationController@destroy');
Route::get('export-qualification','QualificationController@exportQualification')->name('export-qualification');
Route::post('import-qualification','QualificationController@importQualification')->name('import-qualification');
Route::post('qualification-upload','QualificationController@uploadQualification')->name('qualification-upload');

Route::get('/additional-parameter', 'AdditionalParameterController@index');
Route::put('/additional-parameter', 'AdditionalParameterController@index');
Route::get('/additional-parameter/new', 'AdditionalParameterController@create');
Route::get('/additional-parameter/edit/{id}', 'AdditionalParameterController@edit');
Route::post('/additional-parameter', 'AdditionalParameterController@store');
Route::put('/additional-parameter/{id}', 'AdditionalParameterController@update');
Route::get('/additional-parameter/delete/{id}', 'AdditionalParameterController@destroy');
Route::get('/additional-parameter-export', 'AdditionalParameterController@exportAdditionalParameter')->name('additional-parameter-export');
Route::post('/additional-parameter-import', 'AdditionalParameterController@importAdditionalParameter')->name('additional-parameter-import');
Route::post('/additional-parameter-upload', 'AdditionalParameterController@uploadAdditionalParameter')->name('additional-parameter-upload');
//Route::get('/directorate-service/export', 'QualificationController@export');
//Route::post('/directorate-service/import', 'QualificationController@import');
//Route::post('/directorate-service/upload', 'QualificationController@upload');

Route::get('/category-priority', 'CategoryPriorityController@index');
Route::put('/category-priority', 'CategoryPriorityController@index');
Route::get('/category-priority/test', 'CategoryPriorityController@test');
Route::get('/category-priority/new', 'CategoryPriorityController@create');
Route::get('/category-priority/edit/{id}', 'CategoryPriorityController@edit');
Route::post('/category-priority', 'CategoryPriorityController@store');
Route::put('/category-priority/{id}', 'CategoryPriorityController@update');
Route::get('/category-priority/delete/{id}', 'CategoryPriorityController@destroy');
Route::post('/category-priority/update/requirement-order', 'CategoryPriorityController@updateRequirementOrder');

Route::post('/speciality-priority-set', 'SpecialityPrioritySetController@store');
Route::get('/speciality-priority-set/delete/{id}', 'SpecialityPrioritySetController@destroy');

Route::get('/priority-set/new', 'PrioritySetController@create');
Route::post('/priority-set', 'PrioritySetController@store');
Route::get('/priority-set/edit/{id}', 'PrioritySetController@edit');
Route::put('/priority-set/{id}', 'PrioritySetController@update');
Route::get('/priority-set/delete/{id}', 'PrioritySetController@destroy');

Route::get('/service-post-view', 'HomeController@servicePostView');
Route::post('/service-post-view', 'HomeController@servicePostView');
Route::get('/service-overview', 'ServiceOverviewController@index');
Route::post('/service-overview', 'ServiceOverviewController@index');
Route::post('/service-assign', 'ServiceOverviewController@assignService');
Route::put('/service-assign/update', 'ServiceOverviewController@updateAssignService');
Route::get('/service-overview/test', 'ServiceOverviewController@timeline');


Route::get('/colors', 'ColorController@index');
Route::get('/color/new', 'ColorController@create');
Route::get('/color/edit/{id}', 'ColorController@edit');
Route::post('/color', 'ColorController@store');
Route::put('/color/{id}', 'ColorController@update');
Route::get('/color/delete/{id}', 'ColorController@destroy');

Route::get('/transition-times', 'TransitionTimeController@index');
Route::get('/transition-time/new', 'TransitionTimeController@create');
Route::get('/transition-time/edit/{id}', 'TransitionTimeController@edit');
Route::post('/transition-time', 'TransitionTimeController@store');
Route::put('/transition-time/{id}', 'TransitionTimeController@update');
Route::get('/transition-time/delete/{id}', 'TransitionTimeController@destroy');
//Route::get('/service/{id}/requirement-view', 'ServiceOverviewController@testing');
//Route::post('/service/{id}/requirement-view', 'ServiceOverviewController@testing');

Route::get('/directorate-service/{id}/requirements', 'DirectorateServiceController@requirement');
Route::post('/directorate-service/requirement', 'DirectorateServiceController@storeRequirement');
Route::get('/directorate-service/requirement/delete/{id}', 'DirectorateServiceController@destroyRequirement');

Route::get('/service/{id}/requirements', 'ServiceController@requirement');
Route::post('/service/requirement', 'ServiceController@storeRequirement');
Route::get('/service/requirement/delete/{id}', 'ServiceController@destroyRequirement');
//Route::get('/servicepostdata', 'HomeController@servicePostData');
Route::get('service/get-requirement-data','ServiceController@getRequirementData');
Route::get('service/save-requirement-condition','ServiceController@saveRequirementCondition');
Route::get('service/delete-requirement-condition','ServiceController@deleteRequirementCondition');
Route::get('service/operator-type','ServiceController@operatorTypes')->name('operator-type');
Route::get('service/get-operatorby-parameter','ServiceController@getOperatorbyParameter')->name('get-operatorby-parameter');

Route::get('/getDirectorateRotaGroups/{id}', 'HomeController@getDirectorateRotaGroups');
Route::get('/getShiftTypeService/{d_id}/{s_id}/{c_id}/{sb_id}', 'HomeController@getShiftTypeService');
Route::get('/getServiceDropdown/{d_id}/{s_id}/{c_id}/{sb_id}', 'ServiceController@getServiceDropdown');
Route::get('/getDirectorateServiceDropdown/{d_id}/{c_id}/{sb_id}', 'DirectorateServiceController@getServiceDropdown');
Route::post('/set-date', 'HomeController@setDateInSession');
Route::post('/set-calender-date', 'HomeController@setCalenderDateInSession');
Route::get('/getValues/{value}/{d_id}', 'DirectorateServiceController@getValues');
Route::get('/getOperators/{type}', 'serviceController@operatorsWithType');
Route::get('/getAdditionalValues/{d_id}/{s_id}/{p_id}', 'PostController@getAdditionalParameter');
Route::get('/getCategoryPriority/{id}', 'CategoryPriorityController@getCategoryPriority');
Route::get('/getColorsCode', 'ColorController@getColorsCode');
Route::get('/searchSubcategory', 'SubCategoryController@searchSubcategory');
Route::get('/getCategoryTree/{id}', 'HomeController@getCategoryTree');
Route::get('/serviceTimeline', 'ServiceOverviewController@getServicesTimeline');
Route::get('/staffTimeline', 'ServiceOverviewController@getStaffTimeline');
Route::get('/getRequiremetByDateTime', 'ServiceOverviewController@getRequiremetByDateTime');

Route::get('/testing', 'ServiceOverviewController@testing');
Route::post('/testing', 'ServiceOverviewController@testing');
Route::get('/test', 'ServiceOverviewController@requirementData');
Route::get('/over-view', 'ServiceOverviewController@requirementData');



/*User Access Roles Routes Start Here*/
  Route::get('/user-roles', 'UserRolesController@index')->name('user-roles');
  Route::get('/user-roles/new', 'UserRolesController@create');
  Route::post('/user-roles', 'UserRolesController@store');
  Route::get('/user-roles/edit/{id}', 'UserRolesController@edit');

  Route::get('role/permissions/{id}','UserRolesController@rolePermissions')->name('permissions');
  Route::post('add-role-permissions','UserRolesController@storeRolePermissions')->name('add-role-permissions');


/*User Access Roles Routes End Here*/


/*Trust Signup Request Routes End Here*/

    Route::get('/trust-requests', 'SignupRequestController@getTrustRequests')->name('trust-requests');
    Route::post('/update-request', [
        'uses' => 'SignupRequestController@updateRequest'
    ]);

/*Trust Signup Request Routes End Here*/

    /*Email Templates Routes End Here*/

    Route::get('/email-templates', 'EmailTemplatesController@index')->name('email-templates');
    Route::get('/get-trust-template', 'EmailTemplatesController@getTrustTemplate');
    Route::get('/get-email-templates', 'EmailTemplatesController@getEmailTypeTemplate');
    Route::post('/update-template', 'EmailTemplatesController@updateTemplate')->name('create-new-template');
    Route::get('/create-new-template', 'EmailTemplatesController@createNewTemplate')->name('create-new-template');
    Route::post('/create-template', 'EmailTemplatesController@storeNewTemplate');

    /*Email Templates Routes End Here*/


    Route::post('/uploadEmailTemplateImage', 'HomeController@uploadEmailTemplateImage');


/*
 * paramete routes
 */
Route::resources(['service/requirement/parameter' => 'ServiceRequirementParameterController']);
/*
 * Operator routes
 */
Route::get('operator-index','OperatorController@index')->name('operator-index');
Route::get('operator-add','OperatorController@create')->name('operator-add');
Route::post('operator-store','OperatorController@store')->name('operator-store');
Route::get('operator-getParameter/{id}','OperatorController@getParameter')->name('operator-getParameter');

Route::get('condition','ServiceController@condition')->name('condition');
Route::post('condition','ServiceController@conditionPost')->name('condition');



Route::get('/command','CommandController@index');
Route::get('/optimal-assignment','CommandController@optimalAssignment');

Route::get('/account-security',function () {

    return view('account-security');
})->name('account-security');

/*
 * Email Verificaton middleware routes start
 */

Route::get('/email-verification-disable',function () {
    return view('email-code-verification.disable-code-verification');
})->name('email-verification-disable');
Route::post('/email-verification-disable','EmailLoginSecurityController@disableEmailCodeVerification')->name('email-verification-disable');
Route::get('/email-verification-enable','EmailLoginSecurityController@enableEmailCodeVerification')->name('email-verification-enable');

    /*
     * Email Verificaton middleware routes End
     */


/*
   * SMS Verificaton middleware routes start
  */

Route::get('/sms-verification-disable',function () {
        return view('sms-code-verification.disable-code-verification');
    })->name('sms-verification-disable');
Route::post('/sms-verification-disable','SmsLoginSecurityController@disableSmsCodeVerification')->name('sms-verification-disable');
Route::get('/sms-verification-enable','SmsLoginSecurityController@enableSmsCodeVerification')->name('sms-verification-enable');
    /*
       * SMS Verificaton middleware routes End

    */

    Route::get('/user-emails','HomeController@getUserEmails')->name('user-emails');

});


Route::get('/google-2f-authenticate','Google2FAuthenticator@show2faForm')->name('google-2f-authenticate');
Route::post('/generate2faSecret','Google2FAuthenticator@generate2faSecret')->name('generate2faSecret');
Route::post('/enable2fa','Google2FAuthenticator@enable2fa')->name('enable2fa');
Route::post('/disable2fa','Google2FAuthenticator@disable2fa')->name('disable2fa');
Route::post('/2faVerify', function () {
    App\Helpers\LoginAuthenticationHelper::updateLoginVerificationInLoginDetailTbl('googleApp');
    return redirect('/');
})->name('2faVerify')->middleware('2fa');


Route::post('/verify-email-code','EmailLoginSecurityController@verifyCode')->name('verify-email-code');
Route::get('/email-code-verification','EmailLoginSecurityController@emailCodeVerifcation')->name('email-code-verification');
Route::get('/send-email-verification-code','EmailLoginSecurityController@sendCode')->name('send-email-verification-code');

Route::post('/verify-sms-code','SmsLoginSecurityController@verifyCode')->name('verify-sms-code');
Route::get('/sms-code-verification','SmsLoginSecurityController@smsCodeVerifcation')->name('sms-code-verification');
Route::get('/send-sms-verification-code','SmsLoginSecurityController@sendCode')->name('send-sms-verification-code');
