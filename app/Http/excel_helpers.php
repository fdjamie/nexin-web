<?php

namespace App\Http;

use Maatwebsite\Excel\Facades\Excel;

class ExcelHelper {

    public static function exportExcel($file_name, $data, $ref_data = null, $master_data_ref = null) {
        $success = Excel::create($file_name . '(' . date('Y-M-d') . ')', function($excel) use($data, $ref_data, $master_data_ref) {
                    $excel->sheet('Data', function($sheet) use($data) {
                        $sheet->fromArray($data, null, 'A1', false, false);
                    });
                    if (!is_null($ref_data)) {
                        $excel->sheet('Reference Data', function($sheet) use($ref_data) {
                            $sheet->fromArray($ref_data, null, 'A1', false, false);
                        });
                    }
                    if(!is_null($master_data_ref )) {
                        $excel->sheet('Master Data', function($sheet) use($master_data_ref) {
                            $sheet->fromArray($master_data_ref, null, 'A1', false, false);
                        });
                    }
                })->download('xlsx');
        if ($success)
            return true;
        else
            return false;
    }

    public static function importExcel($file) {
        $records = Excel::selectSheetsByIndex(0)->load($file, function($reader) {
                    //$reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        return $records;
    }
    
    public static function importExcelNew($file,$index) {
        $records = Excel::selectSheetsByIndex($index)->load($file, function($reader) {
                    //$reader->ignoreEmpty();
                    $records = $reader->all();
                    return $records->toArray();
                })->toArray();
        return $records;
    }

}
