<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginDetail extends Model
{
    protected $table = 'tb_login_details';

    protected $guarded=[];

    protected $casts = [
        'verified_by' => 'array'
    ];




}
