<?php

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


function __get($api_path, $params = [], $ajax = null)
{
    $response = array();
    $client = new Client();
    try {

        if (!empty($params)) {
            $encoded = http_build_query($params);
            $api_path = $api_path . '?' . $encoded;
        }
        $GuzzleResponse = $client->get($api_path, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . auth()->user()->api_token,
                ],
            ]
        );
        $gResponse = $GuzzleResponse->getBody();
        $apiResponseData = json_decode($gResponse->getContents());
        $response['status'] = true;
        $response['apiResponseData'] = $apiResponseData;
        /*return $response;*/

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        /* Log::error($e->getMessage());*/
        Log::error($e);
        $response['status'] = false;
        $response['apiResponseException'] = $e->getResponse()->getBody();
        /* if(empty($ajax))
         {
             $errors=['Error Occure.Releaod Page Or Contact With Admin!'];
             return redirect()->back()->withErrors($errors);
         }*/

    }
    return $response;
}


function __post($api_path, $params = [])
{

    $client = new Client();
    try {
        $GuzzleResponse = $client->post($api_path, [
            'form_params' => $params,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . auth()->user()->api_token,
            ],
        ]);
        $gResponse = $GuzzleResponse->getBody();
        $apiResponseData = json_decode($gResponse->getContents());
        $response['status'] = true;
        $response['apiResponseData'] = $apiResponseData;

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        /* Log::error($e->getMessage());*/
        Log::error($e);
        $response['status'] = false;
        $response['apiResponseException'] = $e->getResponse()->getBody();

    }

    return $response;
}


function __delete($api_path, $params = array())
{

    $client = new Client();
    try {
        $GuzzleResponse = $client->delete($api_path, [
            'form_params' => $params,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . auth()->user()->api_token,
            ],
        ]);
        $gResponse = $GuzzleResponse->getBody();
        $apiResponseData = json_decode($gResponse->getContents());
        $response['status'] = true;
        $response['apiResponseData'] = $apiResponseData;

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        Log::error($e->getMessage());
        $response['status'] = false;
        /* $response['apiResponseException']= $e->getResponse();*/
        $response['apiResponseException'] = $e->getMessage();

    }
    return $response;
}


function __put($api_path, $params = [])
{
    $client = new Client();
    try {
        $GuzzleResponse = $client->put($api_path, [
            'form_params' => $params,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . auth()->user()->api_token,
            ],
        ]);
        $gResponse = $GuzzleResponse->getBody();
        $apiResponseData = json_decode($gResponse->getContents());
        $response['status'] = true;
        $response['apiResponseData'] = $apiResponseData;

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        Log::error($e->getMessage());
        $response['status'] = false;
        /* $response['apiResponseException']= $e->getResponse();*/
        $response['apiResponseException'] = $e->getMessage();

    }
    return $response;
}


function __getUnauthorize($api_path, $params = [], $ajax = null)
{
    $response = array();
    $client = new Client();
    try {

        if (!empty($params)) {
            $encoded = http_build_query($params);
            $api_path = $api_path . '?' . $encoded;
        }
        $GuzzleResponse = $client->get($api_path, [
                'headers' => [
                    'Accept' => 'application/json',

                ],
            ]
        );
        $gResponse = $GuzzleResponse->getBody();
        $apiResponseData = json_decode($gResponse->getContents());
        $response['status'] = true;
        $response['apiResponseData'] = $apiResponseData;
        /*return $response;*/

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        /* Log::error($e->getMessage());*/
        Log::error($e);
        $response['status'] = false;
        $response['apiResponseException'] = $e->getResponse()->getBody();
        /* if(empty($ajax))
         {
             $errors=['Error Occure.Releaod Page Or Contact With Admin!'];
             return redirect()->back()->withErrors($errors);
         }*/

    }
    return $response;
}

function __postUnauthorize($api_path, $params = [])
{

    $client = new Client();
    try {
        $GuzzleResponse = $client->post($api_path, [
            'form_params' => $params,
            'headers' => [
                'Accept' => 'application/json',

            ],
        ]);
        $gResponse = $GuzzleResponse->getBody();
        $apiResponseData = json_decode($gResponse->getContents());
        $response['status'] = true;
        $response['apiResponseData'] = $apiResponseData;

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        /* Log::error($e->getMessage());*/
        Log::error($e);
        $response['status'] = false;
        $response['apiResponseException'] = $e->getResponse()->getBody();

    }

    return $response;
}



function __authorize($module,$permission,$redirect=null){

    if(Auth::user()->is_admin){
        if(Auth::user()->is_admin ==2)
        {
            if($redirect)
            {
                /*dd('Access Denied!Permission Not Granted');*/
                abort(403,'Permission Not Granted');
            }
            else
            {
                return false;
            }
        }



        return true;
    }
    else
    {
        $userRolesAndPermissions=session('userRolesPermissions');
        $modulePermissions=array();
        if(!empty($userRolesAndPermissions->module_permission))
        $modulePermissions=$userRolesAndPermissions->module_permission;
        $permissionToCheck=$module."_".$permission;
        if(in_array($permissionToCheck,$modulePermissions)){
            return true;
        }
        else{

            if($redirect)
            {
                /*dd('Access Denied!Permission Not Granted');*/
                abort(403,'Permission Not Granted');
            }
            else
            {
                return false;
            }

        }
    }

}

?>