<?php
namespace App\Helpers;
use Jenssegers\Agent\Agent;
use App\LoginDetail;
use Auth;
use Request;



class LoginAuthenticationHelper
{


    /*
     * Added By William
     * Save Login Detail Ip,Browser,Device in loginDetail table

     */
public static function saveLoginDetail()
    {

        $agent = new Agent();
        $browser = $agent->browser();
        if ($agent->isDesktop()) {
            $device_type = "Desktop";
        } else if ($agent->isMobile()) {
            $device_type = "Mobile";
        } else if ($agent->isTablet()) {
            $device_type = "Tables";
        }
        $device = $agent->device();
        $loginDetails = LoginDetail::firstOrCreate(
            [
                'user_id' => Auth::user()->id,
                'ip' => Request::ip(),
                'device' => $device,
                'device_type' => $device_type,
                'browser' => $browser,
            ]
        );
        session(['loginDetailId' => $loginDetails->id]);
        return;
    }


    /*
     * Added By William
     * Check Login if login for specific IP,BROWSER,DEVICE Verified before or not

     */
    public static  function checkIfLoginVerified($verificationType)
    {
        if (!empty(session('loginDetailId'))) {
            $user = auth()->user();
            $loginDetailId = session('loginDetailId');
            $loginVerified = $user->loginDetail->find($loginDetailId)->verified_by;
            if (!empty($loginVerified) && !empty($loginVerified['login_verified_by']) && in_array($verificationType, $loginVerified['login_verified_by'])) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }


    /*
     * Update login verification in login detail table for specific type i-e sms ,email or googleApp .
     * i-e if a login from specific IP,BROWSER Once verified for sms then we have to add sms as to login_verified_by Json arrray and then add that to verified_by Field

     */

public static function updateLoginVerificationInLoginDetailTbl($verificationType)
    {

        if (!empty(session('loginDetailId'))) {
            $user = auth()->user();
            $loginDetailId = session('loginDetailId');
            $loginDetail = $user->loginDetail->find($loginDetailId);
            $loginVerifiedArray = $loginDetail->verified_by;
            if (empty($loginVerifiedArray)) {
                $loginVerifiedArray = ['login_verified_by'];
            }

            $loginVerifiedArray['login_verified_by'][] = $verificationType;
            $loginDetail->verified_by = $loginVerifiedArray;
            $loginDetail->save();

        } else {
            return false;
        }


    }





}























?>