<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'tb_user';
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    protected $appends = [ 'trust_id' ];


    
   /* public function trust(){
        return $this->belongsTo('App\Trust');
    }*/

    public function trusts(){
        return $this->belongsToMany('App\Trust','tb_user_trust');
    }


    public function google2Fauth()
    {
        return $this->hasOne('App\LoginSecurity')->where('type','authApp');
    }

    public function emailSecurity()
    {
        return $this->hasOne('App\LoginSecurity')->where('type','email');
    }

    public function smsSecurity()
    {
        return $this->hasOne('App\LoginSecurity')->where('type','sms');
    }

    public function loginDetail()
    {
        return $this->hasMany('App\LoginDetail');
    }
}
